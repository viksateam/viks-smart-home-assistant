/*
 * HomePage.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents the home page that is visible on the far left of Vik's Smart Home Assistant and is
 * always present in the Rotary Layout.
 */

#include "HomePage.h"
#include "ShaColors.h"
#include "ShaFont.h"
#include "ShaGraphics.h"
#include "ShaStation.h"
#include "WindowHelpers.h"

using namespace vik::graphics;

#define HOME_PAGE_TEXT              ("Vik's Smart Home Assistant")

#define BUTTON_SIZE                 (200)
#define BUTTON_PADDING              (10)

/*
 * Button Points
 */
const std::vector<Point> vik::graphics::itunesPoints
{
    // Outer border
    Point(1.17f, 1.92f), Point(1.17f, 0.54f), Point(2.37f, 0.41f), Point(2.36f, 1.95f),

    // Right loop
    Point(2.35f, 1.99f), Point(2.33f, 2.03f), Point(2.30f, 2.07f), Point(2.26f, 2.11f),
    Point(2.22f, 2.14f), Point(2.16f, 2.17f), Point(2.10f, 2.19f), Point(2.05f, 2.20f),
    Point(1.94f, 2.20f), Point(1.89f, 2.19f), Point(1.84f, 2.17f), Point(1.81f, 2.15f),
    Point(1.77f, 2.11f), Point(1.75f, 2.08f), Point(1.74f, 2.05f), Point(1.73f, 2.01f),
    Point(1.73f, 1.98f), Point(1.74f, 1.94f), Point(1.76f, 1.89f), Point(1.82f, 1.82f),
    Point(1.87f, 1.78f), Point(1.95f, 1.74f), Point(2.02f, 1.72f), Point(2.17f, 1.72f),
    Point(2.21f, 1.73f),

    // Inner border
    Point(2.27f, 1.75f), Point(2.26f, 0.91f), Point(1.27f, 1.00f), Point(1.27f, 2.13f),

    // Left loop
    Point(1.26f, 2.17f), Point(1.24f, 2.21f), Point(1.21f, 2.25f), Point(1.17f, 2.29f),
    Point(1.13f, 2.32f), Point(1.07f, 2.35f), Point(1.01f, 2.37f), Point(0.96f, 2.38f),
    Point(0.85f, 2.38f), Point(0.80f, 2.37f), Point(0.75f, 2.35f), Point(0.72f, 2.33f),
    Point(0.68f, 2.29f), Point(0.66f, 2.26f), Point(0.65f, 2.23f), Point(0.64f, 2.19f),
    Point(0.64f, 2.16f), Point(0.65f, 2.12f), Point(0.67f, 2.07f), Point(0.73f, 2.00f),
    Point(0.78f, 1.96f), Point(0.86f, 1.92f), Point(0.93f, 1.90f), Point(1.08f, 1.90f),
    Point(1.12f, 1.91f),
};



/*
 * Constructor(s)/Destructor
 */
HomePage::HomePage(void) :
    HomePage(Rect(0, 0, 0, 0))
{
}

HomePage::HomePage(_In_ GLfloat width, _In_ GLfloat height) :
    HomePage(Rect(0, 0, width, height))
{
}

HomePage::HomePage(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
    HomePage(Rect(x, y, width, height))
{
}

HomePage::HomePage(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    HomePage(Rect(pt, width, height))
{
}

HomePage::HomePage(_In_ const Rect &rc) :
    Layout(rc)
{
    // Initialize the iTunes button
    ImageButton *iTunes = new ImageButton(0, 0, 3, 3);
    iTunes->AddPoints(std::begin(itunesPoints), std::end(itunesPoints));
    iTunes->SetForegroundColor(ShaColors::ForegroundColor);
    iTunes->SetBackgroundColor(ShaColors::BackgroundColor);
    iTunes->SetLabelColor(ShaColors::ButtonHoverColor);
    iTunes->SetText("iTunes");
    iTunes->SetRect(Rect(0, 0, BUTTON_SIZE, BUTTON_SIZE));
    iTunes->SetThickness(2);
    iTunes->OnButtonUp(this, (ButtonBase::ButtonEventCallback)&HomePage::_OnItunesButtonPressed);
    this->_buttons.push_back(iTunes);
}

HomePage::~HomePage(void)
{
    for (auto button : this->_buttons)
    {
        delete button;
    }
}



/*
 * UIElement
 */
GLvoid HomePage::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    WindowDrawText(offX + TITLE_X_OFFSET, offY + TITLE_Y_OFFSET, HOME_PAGE_TEXT, Fonts::TitleFont,
        ShaColors::ForegroundColor);

    // Draw all buttons
    for (auto button : this->_buttons)
    {
        button->Draw(offX, offY);
    }
}

bool HomePage::OnPointerMoved(_In_ Point pt)
{
    if (Layout::OnPointerMoved(pt))
    {
        return true;
    }

    for (auto button : this->_buttons)
    {
        if (button->GetRect().Contains(pt) && button->OnPointerEntered(button->GetLocation() - pt))
        {
            this->_pointerTarget = button;
            return true;
        }
    }

    return false;
}



/*
 * LayoutAwareElement
 */
void HomePage::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    LayoutAwareElement::_OnSizeUpdated(width, height);
    this->_RefreshLayout();
}



void HomePage::_OnItunesButtonPressed(_In_ ButtonBase *sender, _In_ const Point &pt)
{
    (void)sender;
    (void)pt;
    StartITunes();
}

void HomePage::_RefreshLayout(void)
{
    GLfloat x = SIDE_PADDING + 100;
    GLfloat y = TOP_PADDING + 75;
    GLfloat width = this->GetWidth() - 2 * SIDE_PADDING;

    GLfloat offX = 0, offY = 0;
    for (auto button : this->_buttons)
    {
        button->SetLocation(Point(x + offX, y + offY));
        offX += BUTTON_SIZE + BUTTON_PADDING;

        if (offX + BUTTON_SIZE > width)
        {
            offX = 0;
            offY += BUTTON_SIZE + BUTTON_PADDING;
        }
    }
}
