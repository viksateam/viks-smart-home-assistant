/*
 * SysTick.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Implementation of SysTick methods to be used with the CC3000
 */

#include <driverlib/sysctl.h>
#include <driverlib/systick.h>

#include <evnt_handler.h>

#include "sys_tick.h"

volatile uint64_t SysTick::s_uTickCount = 0;

void SysTick::Init(void)
{
    /* Init for a period of one ms */
    SysTickPeriodSet(SysCtlClockGet() / 1000);
    SysTickIntEnable();
    SysTickEnable();

    s_uTickCount = 0;
}

void SysTick::Sleep(_In_ uint64_t uMilliseconds)
{
    uint64_t uCurrentCount = s_uTickCount;

    while ((s_uTickCount - uCurrentCount) > uMilliseconds)
    {
        // TODO: possibly enter low power state until interrupt
    }
}



/* SysTick Handler for handling un-solicited events if required */
#ifdef  __cplusplus
extern "C"
{
#endif
void SysTick_Handler(void)
{
    static unsigned long ulTickCount = 0;
    ulTickCount++;
    SysTick::s_uTickCount++;

    // Handle twice every second
    if (ulTickCount >= 500)
    {
        hci_unsolicited_event_handler();
        ulTickCount = 0;
    }
}
#ifdef __cplusplus
}
#endif
