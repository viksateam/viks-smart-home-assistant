/*
 * InetAddress.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Class that is useful for dealing with internet addresses.
 */

#include "InetAddress.h"

using namespace vik::communication;

InetAddress::InetAddress(void) :
    _usPort(0),
    _ulIpAddr(0)
{
}

InetAddress::InetAddress(_In_ const sockaddr_in &addr) :
    _ulIpAddr(ntohl(addr.sin_addr.s_addr)),
    _usPort(ntohs(addr.sin_port))
{
}

InetAddress::InetAddress(_In_ ULONG ulIpAddr, _In_ USHORT usPort) :
    _usPort(usPort),
    _ulIpAddr(ulIpAddr)
{
}

InetAddress::InetAddress(_In_ const char *pszIpAddr, _In_ USHORT usPort) :
    _usPort(usPort)
{
    // NOTE: We expect ip addresses to be represented in host-byte-order, but inet_addr returns a
    // value in network-byte-order, so we need to change it back
    this->_ulIpAddr = ntohl(inet_addr(pszIpAddr));
}



InetAddress &InetAddress::operator=(_In_ const sockaddr_in &addr)
{
    // sockaddr_in structures should always be in network-byte-order, so change it back
    this->_ulIpAddr = ntohl(addr.sin_addr.s_addr);
    this->_usPort = ntohs(addr.sin_port);

    return *this;
}

InetAddress::operator sockaddr_in(void) const
{
    sockaddr_in addr = { 0 };
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(this->_ulIpAddr);
    addr.sin_port = htons(this->_usPort);

    return addr;
}

USHORT InetAddress::GetPort(void) const
{
    return this->_usPort;
}

ULONG InetAddress::GetIpAddress(void) const
{
    return this->_ulIpAddr;
}

const char *InetAddress::GetIpAddressAsString(void) const
{
    in_addr addr = ((sockaddr_in)*this).sin_addr;
    return inet_ntoa(addr);
}
