/*
 * Layout.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents the base class for all layouts. A Layout consists of a collection of UIElement
 * objects. Each Layout derivative defines how elements within that Layout will get displayed on
 * screen. Each Layout derivative must define the AddChild function. Each derivative can, of
 * course, define its own behavior for the AddChild function, however the expected action is to
 * "add to the end" of the collection. The Layout is allowed to ignore the addition of the element
 * to the Layout, in which case the UIElement being added must be deallocated (and so, it is not
 * recommended that UIElements be added using the AddChild function).
 */

#include "Layout.h"

using namespace vik::graphics;
using namespace vik::graphics::animations;

/*
 * Constructors/Destructor
 */
Layout::Layout(void) :
    Layout(Rect(0, 0, 0, 0))
{
}

Layout::Layout(_In_ GLfloat width, _In_ GLfloat height) :
    Layout(Rect(0, 0, width, height))
{
}

Layout::Layout(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
    Layout(Rect(x, y, width, height))
{
}

Layout::Layout(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    Layout(Rect(pt, width, height))
{
}

Layout::Layout(_In_ const Rect &rc) :
    LayoutAwareElement(rc),
    _pointerTarget(nullptr),
    _pointerState(PS_NORMAL),
    _insertionAnimationEngine(nullptr),
    _repositionAnimationEngine(nullptr),
    _growthAnimationEngine(nullptr),
    _removalAnimationEngine(nullptr)
{
}

Layout::~Layout(void)
{
    delete this->_insertionAnimationEngine;
    delete this->_repositionAnimationEngine;
    delete this->_growthAnimationEngine;
    delete this->_removalAnimationEngine;
}



/*
 * UIElement
 */
bool Layout::OnPointerEntered(_In_ Point pt)
{
    // Since Layouts keep track of the elements that have pointer focus, OnPointerMoved takes care
    // of calling OnPointerEntered for each element.
    return this->OnPointerMoved(pt);
}

bool Layout::OnPointerExited(void)
{
    if (this->_pointerTarget)
    {
        bool result = this->_pointerTarget->OnPointerExited();

        // If the pointer is pressed, we need to remain the reference to the target (as they have
        // not yet received the OnPointerUp event).
        if (this->_pointerState == PS_NORMAL)
        {
            this->_pointerTarget = nullptr;
        }
        else
        {
            this->_pointerState = PS_EXITED;
        }

        return result;
    }

    return false;
}

bool Layout::OnPointerMoved(_In_ Point pt)
{
    // First, check to see if the pointer is still in the bounds of the child it was previously in
    if (this->_pointerTarget)
    {
        if (this->_pointerTarget->GetRect().Contains(pt))
        {
            // Need to send another OnPointerEntered if the pointer was dragged back in
            if (this->_pointerState == PS_EXITED)
            {
                this->_pointerTarget->OnPointerEntered(pt);
                this->_pointerState = PS_PRESSED;
            }

            if (this->_pointerTarget->OnPointerMoved(pt - this->_pointerTarget->GetLocation()))
            {
                return true;
            }
        }

        if (this->_pointerState == PS_NORMAL)
        {
            // Normal exit; we no longer wish to reference the target
            this->_pointerTarget->OnPointerExited();
            this->_pointerTarget = nullptr;
            return false;
        }
        else if (this->_pointerState == PS_PRESSED)
        {
            // We only wish to send on OnPointerExited event
            this->_pointerTarget->OnPointerExited();
            this->_pointerState = PS_EXITED;
        }

        // Pointer down and OnPointerExited has been sent; target still needs the update
        assert(this->_pointerState == PS_EXITED);
        this->_pointerTarget->OnPointerMoved(pt - this->_pointerTarget->GetLocation());
        return true;
    }

    // Derived classes should call this function first, and if it returns false, they should then
    // check their elements
    return false;
}

bool Layout::OnPointerDown(_In_ Point pt)
{
    assert(this->_pointerState == PS_NORMAL);

    if (this->OnPointerMoved(pt))
    {
        assert(this->_pointerTarget);

        this->_pointerState = PS_PRESSED;
        return this->_pointerTarget->OnPointerDown(pt - this->_pointerTarget->GetLocation());
    }

    assert(!this->_pointerTarget);
    return false;
}

bool Layout::OnPointerUp(_In_ Point pt)
{
    if (this->_pointerState == PS_NORMAL)
    {
        return false;
    }

    assert(this->_pointerTarget);
    this->_pointerState = PS_NORMAL;
    bool result = this->_pointerTarget->OnPointerUp(pt - this->_pointerTarget->GetLocation());

    if (!this->_pointerTarget->GetRect().Contains(pt))
    {
        this->_pointerTarget->OnPointerExited();
        this->_pointerTarget = nullptr;
    }

    return result;
}



/*
 * Animation Engines
 */
void Layout::SetInsertionAnimationEngine(_In_ IInsertionAnimationEngine *eng)
{
    delete this->_insertionAnimationEngine;
    this->_insertionAnimationEngine = eng;
}

void Layout::SetRepositionAnimationEngine(_In_ IRepositionAnimationEngine *eng)
{
    delete this->_repositionAnimationEngine;
    this->_repositionAnimationEngine = eng;
}

void Layout::SetGrowthAnimationEngine(_In_ IGrowthAnimationEngine *eng)
{
    delete this->_growthAnimationEngine;
    this->_growthAnimationEngine = eng;
}

void Layout::SetRemovalAnimationEngine(_In_ IRemovalAnimationEngine *eng)
{
    delete this->_removalAnimationEngine;
    this->_removalAnimationEngine = eng;
}
