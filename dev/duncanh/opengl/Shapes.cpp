/*
 * Shapes.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Functions used to draw shapes in OpenGL. In general, it is not recommended that you call these
 * functions directly unless you are creating a custom UIElement.
 */

#include "Shapes.h"

using namespace sha::graphics;

/*
 * Rectangles
 */
static GLvoid _DrawRect(GLenum mode, const Rect &rc)
{
    glBegin(mode);

    glVertex2f(rc.pt.x, rc.pt.y);
    glVertex2f(rc.pt.x + rc.width, rc.pt.y);
    glVertex2f(rc.pt.x + rc.width, rc.pt.y - rc.height);
    glVertex2f(rc.pt.x, rc.pt.y - rc.height);

    glEnd();
}

GLvoid sha::graphics::DrawRect(GLfloat x, GLfloat y, GLfloat width, GLfloat height)
{
    Rect rc(x, y, width, height);
    DrawRect(rc);
}

GLvoid sha::graphics::DrawRect(
    GLfloat x,
    GLfloat y,
    GLfloat width,
    GLfloat height,
    const Color &color)
{
    glColor4fv(color);
    DrawRect(x, y, width, height);
}

GLvoid sha::graphics::DrawRect(const Rect &rc)
{
    _DrawRect(GL_LINE_LOOP, rc);
}

GLvoid sha::graphics::DrawRect(const Rect &rc, const Color &color)
{
    glColor4fv(color);
    DrawRect(rc);
}



GLvoid sha::graphics::FillRect(GLfloat x, GLfloat y, GLfloat width, GLfloat height)
{
    Rect rc(x, y, width, height);
    FillRect(rc);
}

GLvoid sha::graphics::FillRect(
    GLfloat x,
    GLfloat y,
    GLfloat width,
    GLfloat height,
    const Color &color)
{
    glColor4fv(color);
    FillRect(x, y, width, height);
}

GLvoid sha::graphics::FillRect(const Rect &rc)
{
    _DrawRect(GL_QUADS, rc);
}

GLvoid sha::graphics::FillRect(const Rect &rc, const Color &color)
{
    glColor4fv(color);
    FillRect(rc);
}



/*
 * Ovals (and Circles)
 */
static GLvoid _DrawOval(GLenum mode, const Oval &oval)
{
    glVertexPointer(2, GL_FLOAT, 0, &oval.vertices[0]);
    glDrawArrays(mode, 0, oval.vertices.size());
}

GLvoid sha::graphics::DrawOval(GLfloat x, GLfloat y, GLfloat width, GLfloat height, uint32_t res)
{
    Oval oval(x, y, width, height, res);
    DrawOval(oval);
}

GLvoid sha::graphics::DrawOval(
    GLfloat x,
    GLfloat y,
    GLfloat width,
    GLfloat height,
    uint32_t res,
    const Color &color)
{
    glColor4fv(color);
    DrawOval(x, y, width, height, res);
}

GLvoid sha::graphics::DrawOval(const Oval &oval)
{
    _DrawOval(GL_LINE_LOOP, oval);
}

GLvoid sha::graphics::DrawOval(const Oval &oval, const Color &color)
{
    glColor4fv(color);
    DrawOval(oval);
}



GLvoid sha::graphics::FillOval(GLfloat x, GLfloat y, GLfloat width, GLfloat height, uint32_t res)
{
    Oval oval(x, y, width, height, res);
    FillOval(oval);
}

GLvoid sha::graphics::FillOval(
    GLfloat x,
    GLfloat y,
    GLfloat width,
    GLfloat height,
    uint32_t res,
    const Color &color)
{
    glColor4fv(color);
    FillOval(x, y, width, height, res);
}

GLvoid sha::graphics::FillOval(const Oval &oval)
{
    _DrawOval(GL_POLYGON, oval);
}

GLvoid sha::graphics::FillOval(const Oval &oval, const Color &color)
{
    glColor4fv(color);
    FillOval(oval);
}

