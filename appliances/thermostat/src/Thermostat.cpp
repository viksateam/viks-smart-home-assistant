/*
 * Thermostat.cpp
 *
 * Author(s):
 *      Josh Randall
 *      Peter Vu
 *
 * The thermostat appliance driver functions
 */

#include "Console.h"
#include "Debug.h"
#include "GPIO.h"
#include "Server.h"
#include "ST7735.h"
#include "sys_tick.h"
#include "tiva.h"
#include "Thermostat.h"

#include <string.h>
#include <driverlib/sysctl.h>

// Thermostat Settings
int Thermostat::s_nTempActual;
int Thermostat::s_nTempSetting;
int Thermostat::s_nSystemSetting; // 1 is heat, 0 is cool, 2 is off (set externally)
bool Thermostat::s_fFanSetting; // true is on, false is auto (set externally)

// Thermostat Strings
char Thermostat::s_rgszTempSetting[8];
char Thermostat::s_rgszTempActual[8];
char Thermostat::s_rgszSystemStatus[8];
char Thermostat::s_rgszFanStatus[8];

// Thermostat Outputs
int Thermostat::s_nHeating; // heating system on(1)/off(0)
int Thermostat::s_nCooling; // cooling system on(1)/off(0)
int Thermostat::s_nFanning; // Fan is on(1)/off(0)

// NOTE: Move to somewhere in 'common' directory
char *itoa(int valueAsInt, char valueAsString[]) // integer to ascii
{
    char const digit[] = "0123456789";
    char* ptr = valueAsString;
    int shift;
    
    // Insert minus sign if negative
    if (valueAsInt < 0)
    {
        *ptr++ = '-';
        valueAsInt *= -1;
    }
    shift = valueAsInt;
    
    //Move to the end
    do
    {
        ++ptr;
        shift /= 10;
    } while (shift);
    *ptr = '\0';
    
    //Move back while inserting digits
    do
    {
        *--ptr = digit[valueAsInt % 10];
        valueAsInt /= 10;
    } while (valueAsInt);
    
    return valueAsString;
}

void Thermostat::Init()
{
    // Initialize thermostat settings
    s_nTempActual = 75;
    s_nTempSetting = 75;
    s_nSystemSetting = 0;    // Set to cool
    s_fFanSetting = false; // Set to auto
    
    // Initialize thermostat
    strcpy(s_rgszTempSetting, "75");
    strcpy(s_rgszTempActual, "75");
    strcpy(s_rgszSystemStatus, "cool");
    strcpy(s_rgszFanStatus, "auto");
    
    // Initialize thermostat outputs
    s_nHeating = 0;
    s_nCooling = 0;
    s_nFanning = 0;
}

void Thermostat::ButtonTask()
{
    // Debounce the switch
    // SysCtlDelay(1000000); // 25ms sleep
    SysCtlDelay(50000); // 1.25ms sleep
    
    if (GPIO_E::IsSet(GPIO_BIT_1) && !GPIO_E::Get(GPIO_BIT_1)) // UP button
    {
        SetTempSetting(s_nTempSetting + 1);
        
        // Acknowledge interrupt on bit1
        GPIO_E::AcknowledgeInt(GPIO_BIT_1);
    }
    else if (GPIO_E::IsSet(GPIO_BIT_2) && !GPIO_E::Get(GPIO_BIT_2)) // DOWN button
    {
        SetTempSetting(s_nTempSetting - 1);
        
        // Acknowledge interrupt on bit2
        GPIO_E::AcknowledgeInt(GPIO_BIT_2);
    }
    else if (GPIO_E::IsSet(GPIO_BIT_3) && !GPIO_E::Get(GPIO_BIT_3)) // HEAT/COOL/OFF toggle button
    {        
        // Toggle s_nHeatSetting
        if (s_nSystemSetting == HEAT) // heat - set to off
        {
            SetSystemSetting(OFF);
        }
        else if (s_nSystemSetting == COOL) // cool - set to heat
        {
            SetSystemSetting(HEAT);
        }
        else // off - set to cool
        {
            SetSystemSetting(COOL);
        }
        Update();
        
        // Acknowledge interrupt on bit3
        GPIO_E::AcknowledgeInt(GPIO_BIT_3);
    }
    else if (GPIO_E::IsSet(GPIO_BIT_4) && !GPIO_E::Get(GPIO_BIT_4)) // FAN ON/FAN AUTO button
    {
        // Toggle s_fFanSetting
        if (s_fFanSetting == true) // Set to auto
        {
            SetFanSetting(false);
        }
        else // Set to on
        {
            SetFanSetting(true);
        }
        Update();
        
        // Acknowledge interrupt on bit4
        GPIO_E::AcknowledgeInt(GPIO_BIT_4);
    }
}

void Thermostat::SetTempSetting(_In_ int TS)
{
    if (TS != s_nTempSetting)
    {
        SetTempSettingString(TS);
        ST7735_Digit(1, 1, 2, TS, 85, 0, 100);
        s_nTempSetting = TS;
        
        Update();
    }
}

int Thermostat::GetTempSetting()
{
    return s_nTempSetting;
}

void Thermostat::SetTempActual(_In_ int TA)
{
    //ST7735_Message(1, 1, "ADC:", TA);
    //int nNewTemp = ((-TA * 5) / 100) + 233;
    int nNewTemp = ((-TA * 5) + 23340) / 100; // try to replace with shifts to make faster
    
    Console::Write("Old temp: ");
    Console::Write(s_nTempActual);
    Console::Write(", New temp: ");
    Console::WriteLine(nNewTemp);
    
    if (nNewTemp != s_nTempActual)
    {
        int oldTemp = s_nTempActual;
        int newTemp = nNewTemp;
        
        SetTempActualString(nNewTemp);
        s_nTempActual = nNewTemp;
        
        ST7735_Digit(0, 0, 6, s_nTempActual, 0, 8, 100); // ***** try to only update 10's digit when necessary *****
        /*
        int oldDigit = oldTemp / 100;
        int newDigit = newTemp / 100;
        // Update LCD
        if (oldDigit != newDigit)  
        {
            if (newDigit != 0)
            {
                ST7735_Digit(0, 0, 6, newDigit, 0, 8, 1);
            }
        }
        
        oldDigit = (oldTemp % 100) / 10;
        newDigit = (newTemp % 100) / 10;
        if (oldDigit != newDigit)  
        {
            if (newDigit != 0)
            {
                ST7735_Digit(0, 0, 6, newDigit, 34, 8, 1);
            }
            else // print a '0' instead of a space
            {
                ST7735_DrawChar(34, 8, '0', ST7735_BLACK, ST7735_WHITE, 6);
            }
        }
        
        oldDigit = (oldTemp % 10);
        newDigit = (newTemp % 10);
        if (oldDigit != newDigit)  
        {
            if (newDigit != 0)
            {
                ST7735_Digit(0, 0, 6, newDigit, 68, 8, 1);
            }
            else // print a '0' instead of a space
            {
                ST7735_DrawChar(68, 8, '0', ST7735_BLACK, ST7735_WHITE, 6);
            }
        }*/
        
        //ST7735_Digit(0, 0, 6, s_nTempActual, 0, 8, 100); // ***** try to only update 10's digit when necessary *****
        Update();
    }
}

int Thermostat::GetTempActual()
{
    return s_nTempActual;
}

char *Thermostat::GetTempSettingString()
{
    return s_rgszTempSetting;
}

char *Thermostat::GetTempActualString()
{
    return s_rgszTempActual;
}

void Thermostat::SetTempSettingString(_In_ int newTS)
{
    if (newTS != s_nTempSetting)
    {
        strcpy(s_rgszTempSetting, "");
        itoa(newTS, s_rgszTempSetting);
    }
}

void Thermostat::SetTempActualString(_In_ int newTA)
{
    if (newTA != s_nTempActual)
    {
        strcpy(s_rgszTempActual, "");
        itoa(newTA, s_rgszTempActual);
    }
}

void Thermostat::SetSystemSetting(_In_ int H)
{
    s_nSystemSetting = H;
    
    if (H == HEAT)
    {
        strcpy(s_rgszSystemStatus, "");
        strcat(s_rgszSystemStatus, "heat");
        ST7735_Message(1, 2, " heat       ", 0);
    }
    else if (H == COOL)
    {
        strcpy(s_rgszSystemStatus, "");
        strcat(s_rgszSystemStatus, "cool");
        ST7735_Message(1, 2, "        cool", 0);
    }
    else if (H == OFF)// off
    {
        strcpy(s_rgszSystemStatus, "");
        strcat(s_rgszSystemStatus, "off");
        ST7735_Message(1, 2, "     off    ", 0);
    }
}

int Thermostat::GetSystemSetting()
{
    return s_nSystemSetting;
}

char *Thermostat::GetSystemSettingString()
{
    return s_rgszSystemStatus;
}

void Thermostat::SetFanSetting(_In_ bool FS)
{
    s_fFanSetting = FS;
    
    if (FS)
    {
        strcpy(s_rgszFanStatus, "");
        strcat(s_rgszFanStatus, "on");
        ST7735_Message(1, 3, "   on       ", 0);
    }
    else
    {
        strcpy(s_rgszFanStatus, "");
        strcat(s_rgszFanStatus, "auto");
        ST7735_Message(1, 3, "        auto", 0);
    }
}

bool Thermostat::GetFanSetting()
{
    return s_fFanSetting;
}

char *Thermostat::GetFanSettingString()
{
    return s_rgszFanStatus;
}

void Thermostat::HeatOn()
{
    s_nHeating = 1;
    GPIO_C::Set(GPIO_BIT_4);
}

void Thermostat::HeatOff()
{
    s_nHeating = 0;
    GPIO_C::Clear(GPIO_BIT_4);
}

void Thermostat::CoolOn()
{
    s_nCooling = 1;
    GPIO_C::Set(GPIO_BIT_6);
}

void Thermostat::CoolOff()
{
    s_nCooling = 0;
    GPIO_C::Clear(GPIO_BIT_6);
}

void Thermostat::FanOn()
{
    s_nFanning = 1;
    GPIO_C::Set(GPIO_BIT_5);
    GPIO_D::Set(GPIO_BIT_3);
}

void Thermostat::FanOff()
{
    s_nFanning = 0;
    GPIO_C::Clear(GPIO_BIT_5);
    GPIO_D::Clear(GPIO_BIT_3);
}

void Thermostat::Update()
{
    // Update heat/cool/off setting
    if (s_nSystemSetting == HEAT) // heat
    {
        if (s_nTempActual < (s_nTempSetting - 1))
        {
            Thermostat::HeatOn();
            Thermostat::FanOn();
        }
        else
        {
            Thermostat::HeatOff();
            Thermostat::FanOff();
        }
        Thermostat::CoolOff();
    }
    else if (s_nSystemSetting == COOL) // cool
    {
        if (s_nTempActual > (s_nTempSetting + 1))
        {
            Thermostat::CoolOn();
            Thermostat::FanOn();
        }
        else
        {
            Thermostat::CoolOff();
            Thermostat::FanOff();
        }
        Thermostat::HeatOff();
    }
    else // off
    {
        Thermostat::HeatOff();
        Thermostat::CoolOff();
        Thermostat::FanOff();
    }
    
    // Update fan setting
    if (s_fFanSetting == true)
    {
        Thermostat::FanOn();
    }
}
