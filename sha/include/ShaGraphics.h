/*
 * ShaGraphics.h
 *
 * Author(s):
 *      Duncan Horn
 *      Jerry Lin
 *
 * The various functions used by the Smart Home Assistant for performing important operations
 * regarding graphical output. In general, these functions should not be called directly except by
 * the windowing code.
 */
#pragma once

#include "sha.h"

#include <GL/gl.h>

#include "ShaLeap.h"
#include "ShaServer.h"
#include "ShaStation.h"

namespace vik
{
    struct RequestInfo
    {
        typedef void(*RequestCompleteFunc)(_In_ const RequestInfo &sender, _In_ const char *msg);

        std::string         targetId;
        RequestCompleteFunc func;
    };

    namespace graphics
    {
        extern GLsizei g_WindowWidth;
        extern GLsizei g_WindowHeight;
        extern bool    g_PointerIsVisible;
        extern bool    g_PointerIsPressed;

        GLvoid InitializeGraphics(_In_ GLsizei width, _In_ GLsizei height);
        GLvoid OnClose(void);
        GLvoid DrawSmartHomeAssistant(void);
        GLvoid PostNotification(_In_ std::string message);

        GLvoid OnWindowResize(_In_ GLsizei width, _In_ GLsizei height);
        GLvoid OnKeyPressed(_In_ int keyCode);
        GLvoid OnPointerEntered(_In_ GLfloat x, _In_ GLfloat y);
        GLvoid OnPointerExited(void);
        GLvoid OnPointerMoved(_In_ GLfloat x, _In_ GLfloat y);
        GLvoid OnPointerTapped(_In_ GLfloat x, _In_ GLfloat y);
        GLvoid OnPointerPressed(_In_ GLfloat x, _In_ GLfloat y);
        GLvoid OnPointerReleased(_In_ GLfloat x, _In_ GLfloat y);
        GLvoid OnPinchGesture(_In_ GLfloat x, _In_ GLfloat y);
        GLvoid OnPinchReleasedGesture(_In_ GLfloat x, _In_ GLfloat y);
        GLvoid OnSwipeGesture(_In_ leap::SwipeDirection swipeDirection);

        GLvoid CheckStationTimeouts(void);
        GLvoid OnStationConnect(_In_ vik::ShaStation *station);
        GLvoid OnStationDisconnect(_Inout_ char *id);
        GLvoid OnStationResponse(_In_ communication::request_cookie cookie, _Inout_ char *msg);
        bool   SubmitStationRequest(
            _In_ communication::request_cookie cookie,
            _In_ const RequestInfo &info);

        void        StartITunes(void);
        void        OnITunesResponse(_In_ bool succeeded);
        std::string ITunesTrackRequest(_In_ TrackRequest request);
        std::string ITunesSongRequest(_In_ ITunesStatus::SongStatus request);
        bool        GetITunesStatus(_Inout_ ITunesStatus &status);
        bool        IsITunesRunning(void);
    }
}
