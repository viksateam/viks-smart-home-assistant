
#include <iostream>

#include "UDPSocket.h"

using namespace vik::communication;

DWORD WINAPI MessageSendThreadProc(_In_ void *pAux);
DWORD WINAPI MessageReceiveThreadProc(_In_ void *pAux);

static void printError(char *msg)
{
    std::cout << "ERROR " << msg << ": (" << WSAGetLastError() << ")" << std::endl;
}

UDPSocket sockIn;
UDPSocket sockOut;
USHORT port;
int main(void)
{
    WSADATA wsaData;

    if (WSAStartup(MAKEWORD(2, 2), &wsaData))
    {
        printError("startup");
        return 1;
    }

    std::cout << "Enter the port: ";
    std::cin >> port;

    sockIn.Open(port);
    sockOut.Open(port + 1);
    if (!sockIn.IsConnected())
    {
        printError("open");
        WSACleanup();
        return 1;
    }

    HANDLE sendProc = CreateThread(nullptr, 0, MessageSendThreadProc, nullptr, 0, nullptr);
    HANDLE readProc = CreateThread(nullptr, 0, MessageReceiveThreadProc, nullptr, 0, nullptr);

    if (!sendProc || !readProc)
    {
        std::cout << "ERROR: could not start threads" << std::endl;
        return 1;
    }

    WaitForSingleObject(sendProc, INFINITE);
    sockIn.Close();
    sockOut.Close();
    WaitForSingleObject(readProc, INFINITE);

    CloseHandle(sendProc);
    CloseHandle(readProc);
    WSACleanup();
    return 0;
}

DWORD WINAPI MessageSendThreadProc(_In_ void *pAux)
{
    UNREFERENCED_PARAMETER(pAux);

    std::string command;
    std::getline(std::cin, command);
    std::getline(std::cin, command);
    while (command != "exit")
    {
        command += "\n";
        if (sockOut.Broadcast(port, command.c_str(), command.size()) < 0)
        {
            sockOut.Close();
            break;
        }

        std::getline(std::cin, command);
    }

    return 0;
}

char szBuffer[1024];
DWORD WINAPI MessageReceiveThreadProc(_In_ void *pAux)
{
    UNREFERENCED_PARAMETER(pAux);

    while (sockIn.IsConnected())
    {
        InetAddress sender;
        int nLen = sockIn.Receive(szBuffer, sizeof(szBuffer), &sender);
        if (nLen > 0)
        {
            std::string msg(szBuffer, szBuffer + nLen);
            std::cout << sender.GetIpAddressAsString() << ": " << msg;
        }
    }

    return 0;
}
