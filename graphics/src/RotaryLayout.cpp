/*
 * RotaryLayout.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a rotary layout that hosts UIElements in a linear fashion. At most three elements are
 * shown on screen at one time: the current element on top in the center, the previous off to the
 * left, and the next off to the right. The layout looks (roughly) as follows:
 *
 *                      -------------------------
 *                      |                       |
 *                 -----+--                   --+-----
 *                 |    | |                   | |    |
 *                 |    | |                   | |    |
 *                 -----+--                   --+-----
 *                      |                       |
 *                      -------------------------
 *
 */

#include "DeletionAnimation.h"
#include "Drawing.h"
#include "RotaryLayout.h"

using namespace vik::graphics;
using namespace vik::graphics::animations;

#define PAGE_INDICATOR_HEIGHT               (3)
#define PAGE_INDICATOR_VELOCITY             (.0025f)

#define DEFAULT_CHILD_VELOCITY              (.005f)

/*
 * Constructor(s)/Destructor
 */
RotaryLayout::RotaryLayout(void) :
    RotaryLayout(Rect(0, 0, 0, 0))
{
}

RotaryLayout::RotaryLayout(_In_ GLfloat width, _In_ GLfloat height) :
    RotaryLayout(Rect(0, 0, width, height))
{
}

RotaryLayout::RotaryLayout(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    RotaryLayout(Rect(x, y, width, height))
{
}

RotaryLayout::RotaryLayout(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    RotaryLayout(Rect(pt, width, height))
{
}

RotaryLayout::RotaryLayout(_In_ const Rect &rc) :
    Layout(rc),
    _currentIndex(-1)
{
    this->_OnSizeUpdated(rc.width, rc.height);
}

RotaryLayout::~RotaryLayout(void)
{
    for (auto &child : this->_children)
    {
        delete child;
    }
}



/*
 * UIElement
 */
GLvoid RotaryLayout::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    // First, draw the two layouts that are peeking in
    if (this->_ElementVisible(this->_currentIndex - 1))
    {
        this->_children[this->_currentIndex - 1]->Draw(offX, offY);
    }
    if (this->_ElementVisible(this->_currentIndex + 1))
    {
        this->_children[this->_currentIndex + 1]->Draw(offX, offY);
    }

    // Now, draw the visible layout
    if (this->_ElementVisible(this->_currentIndex))
    {
        this->_children[this->_currentIndex]->Draw(offX, offY);
    }

    // Finally, draw the bar identifying the current page
    if (this->_children.size() >= 2)
    {
        this->_pageIndicator.Draw(offX, offY);
    }
}

void RotaryLayout::SetForegroundColor(_In_ const Color &color)
{
    UIElement::SetForegroundColor(color);

    // Our foreground color is the page indicator's background color
    this->_pageIndicator.SetBackgroundColor(color);
}

bool RotaryLayout::OnPointerMoved(_In_ Point pt)
{
    if (Layout::OnPointerMoved(pt))
    {
        return true;
    }

    if (this->_children.size() > 0)
    {
        auto elem = this->_children[this->_currentIndex];
        if (elem->GetRect().Contains(pt) && elem->OnPointerEntered(pt - elem->GetLocation()))
        {
            this->_pointerTarget = elem;
            return true;
        }
    }

    return false;
}



/*
 * Layout
 */
void RotaryLayout::AddChild(_In_ UIElement *elem)
{
    this->AddChild(elem, -1);
}



/*
 * RotaryLayout
 */
void RotaryLayout::AddChild(_In_ UIElement *elem, _In_ int index)
{
    assert(elem);
    if ((index >= (int)this->_children.size()) || (index == -1))
    {
        index = this->_children.size();
        this->_children.push_back(elem);
    }
    else
    {
        this->_children.insert(std::begin(this->_children) + index, elem);
    }

    if (this->_currentIndex == -1)
    {
        // Layout currently empty; Set as visible element
        this->_currentIndex = 0;
    }
    else if (this->_currentIndex >= index)
    {
        // Layout not empty; preserve the current visible element
        this->_currentIndex++;
    }

    if (this->_ElementVisible(index))
    {
        // Update the layout of the inserted element. Note that the inserted element can only
        // "kick" an element that was previously either on the left or the right (never the center
        // element). Therefore, there is no need to update the layout of any other element as those
        // "kicked" out will simply no longer get drawn.
        this->_AnimateChildForInsertion(index);
    }

    this->_UpdatePageIndicator();
}

bool RotaryLayout::RemoveChild(_In_ const UIElement *layout)
{
    auto itr = std::find(std::begin(this->_children), std::end(this->_children), layout);

    if (itr != std::end(this->_children))
    {
        if (this->_pointerTarget == layout)
        {
            this->_pointerTarget = nullptr;
            this->_pointerState = PS_NORMAL;
        }

        // Since the calling function holds a reference to the object, it is their
        // responsibility to destroy that object
        if (this->_removalAnimationEngine)
        {
            (*itr)->SubmitOverrideAnimation(this->_removalAnimationEngine->CreateAnimation(*itr));
        }

        int index = std::distance(std::begin(this->_children), itr);

        this->_children.erase(itr);
        this->_OnChildRemoved(index);
        this->_UpdatePageIndicator();
        return true;
    }

    return false;
}

bool RotaryLayout::RemoveChild(_In_ int index)
{
    if ((size_t)index >= this->_children.size())
    {
        return false;
    }

    auto itr = std::begin(this->_children) + index;
    if (this->_removalAnimationEngine)
    {
        (*itr)->SubmitOverrideAnimation(this->_removalAnimationEngine->CreateAnimation(*itr));
    }

    if (*itr == this->_pointerTarget)
    {
        this->_pointerTarget = nullptr;
        this->_pointerState = PS_NORMAL;
    }

    // We assume that all references to the object have been severed (though the caller is
    // technically allowed to use it unil AnimationManager::Update gets called (which they control)
    (*itr)->SubmitPendingAnimation(new DeletionAnimation(*itr));

    this->_children.erase(itr);
    this->_OnChildRemoved(index);
    this->_UpdatePageIndicator();
    return true;
}

bool RotaryLayout::AdvanceLeft(void)
{
    if (this->_currentIndex <= 0)
    {
        return false;
    }

    this->_currentIndex--;
    this->_AnimateChildForInsertion(this->_currentIndex - 1);
    this->_AnimateChildToPosition(this->_currentIndex);
    this->_AnimateChildToPosition(this->_currentIndex + 1);
    this->_UpdatePageIndicator();

    return true;
}

bool RotaryLayout::AdvanceRight(void)
{
    if (this->_currentIndex + 1 >= (int)this->_children.size())
    {
        return false;
    }

    this->_currentIndex++;
    this->_AnimateChildToPosition(this->_currentIndex - 1);
    this->_AnimateChildToPosition(this->_currentIndex);
    this->_AnimateChildForInsertion(this->_currentIndex + 1);
    this->_UpdatePageIndicator();

    return true;
}



/*
 * Private Functions
 */
void RotaryLayout::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    Layout::_OnSizeUpdated(width, height);

    // The center rectangle is always the same size as the layout
    this->_centerRect = this->GetRect();

    // The left and right rectangles are two thirds the size of the layout and placed such that
    // only one third is visible
    this->_leftRect = this->GetRect();
    this->_leftRect.width *= 2.0f / 3.0f;
    this->_leftRect.height *= 2.0f / 3.0f;
    this->_leftRect.pt.y += this->_leftRect.height / 4;
    this->_rightRect = this->_leftRect;

    this->_leftRect.pt.x -= this->_leftRect.width * 2.0f / 3.0f;
    this->_rightRect.pt.x += this->_rightRect.width * 7.0f / 6.0f;

    // Now cause the layout to update
    this->_UpdateLayoutForIndex(this->_currentIndex - 1);
    this->_UpdateLayoutForIndex(this->_currentIndex);
    this->_UpdateLayoutForIndex(this->_currentIndex + 1);

    this->_UpdatePageIndicator();
}

bool RotaryLayout::_ElementVisible(_In_ int index) const
{
    if ((size_t)index >= this->_children.size())
    {
        return false;
    }

    return (index >= this->_currentIndex - 1) && (index <= this->_currentIndex + 1);
}

const Rect &RotaryLayout::_GetRectForIndex(_In_ int index) const
{
    assert(this->_ElementVisible(index));

    if (index == this->_currentIndex - 1)
    {
        return this->_leftRect;
    }
    else if (index == this->_currentIndex)
    {
        return this->_centerRect;
    }
    else if (index == this->_currentIndex + 1)
    {
        return this->_rightRect;
    }

    // Unreached
    assert(false);
    return *(Rect *)nullptr;
}

void RotaryLayout::_UpdateLayoutForIndex(_In_ int index)
{
    if (!this->_ElementVisible(index))
    {
        return;
    }

    this->_children[index]->SetRect(this->_GetRectForIndex(index));
}

void RotaryLayout::_AnimateChildToPosition(_In_ int index)
{
    if (!this->_ElementVisible(index))
    {
        return;
    }

    IAnimation *ani = nullptr;
    if (this->_growthAnimationEngine)
    {
        ani = this->_growthAnimationEngine->CreateAnimation(this->_children[index],
            this->_GetRectForIndex(index));
    }
    else
    {
        // By default, use a LinearGrowthAnimation
        ani = new LinearGrowthAnimation(this->_children[index], this->_GetRectForIndex(index),
            DEFAULT_CHILD_VELOCITY);
    }

    assert(ani);
    this->_children[index]->SubmitOverrideAnimation(ani);
}

void RotaryLayout::_AnimateChildForInsertion(_In_ int index)
{
    if (!this->_ElementVisible(index))
    {
        return;
    }

    // First, need to move element to the proper location
    this->_UpdateLayoutForIndex(index);

    if (this->_insertionAnimationEngine)
    {
        auto child = this->_children[index];
        child->SubmitOverrideAnimation(this->_insertionAnimationEngine->CreateAnimation(child));
    }
}

void RotaryLayout::_UpdatePageIndicator(void)
{
    if (this->_children.size() >= 2)
    {
        GLfloat x = this->_currentIndex * this->GetWidth() / this->_children.size();
        GLfloat y = this->GetHeight() - PAGE_INDICATOR_HEIGHT;
        GLfloat width = this->GetWidth() / this->_children.size();
        GLfloat height = PAGE_INDICATOR_HEIGHT;

        this->_pageIndicator.SetY(y);
        auto ani = new LinearGrowthAnimation(&this->_pageIndicator, x, y, width, height,
            PAGE_INDICATOR_VELOCITY);
        this->_pageIndicator.SubmitOverrideAnimation(ani);
    }
    else
    {
        // Don't animate (as it's not drawn)
        this->_pageIndicator.CancelCurrentAnimation();

        this->_pageIndicator.SetX(0);
        this->_pageIndicator.SetY(this->GetY() - PAGE_INDICATOR_HEIGHT);
        this->_pageIndicator.SetWidth(this->GetWidth());
        this->_pageIndicator.SetHeight(PAGE_INDICATOR_HEIGHT);
    }
}

void RotaryLayout::_OnChildRemoved(_In_ int index)
{
    assert(this->_currentIndex >= 0);

    if (this->_children.size() == 0)
    {
        // Special case; nothing to do
        this->_currentIndex = -1;
        return;
    }

    if (index < this->_currentIndex - 1)
    {
        // Off screen to the left
        this->_currentIndex--;
    }
    else if (index == this->_currentIndex - 1)
    {
        // On screen to the left
        this->_currentIndex--;
        if (this->_currentIndex > 0)
        {
            this->_AnimateChildForInsertion(this->_currentIndex - 1);
        }
    }
    else if (index == this->_currentIndex)
    {
        // On screen in the center
        if (index > 0)
        {
            this->_currentIndex--;
            if (this->_currentIndex > 0)
            {
                this->_AnimateChildForInsertion(this->_currentIndex - 1);
            }
        }
        else if (this->_currentIndex + 1 < (int)this->_children.size())
        {
            this->_AnimateChildForInsertion(this->_currentIndex + 1);
        }

        this->_AnimateChildToPosition(this->_currentIndex);
    }
    else if (index == this->_currentIndex + 1)
    {
        // On screen to the right
        if (this->_currentIndex + 1 < (int)this->_children.size())
        {
            this->_AnimateChildForInsertion(this->_currentIndex + 1);
        }
    }
    // Else off screen to the right (NOP)
}
