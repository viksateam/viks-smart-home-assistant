/*
 * Slider.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a slider that can take a variable width, height, orientation, and range that is
 * manipulatable by the user.
 */

#include "LinearTranslationAnimation.h"
#include "Slider.h"

using namespace vik::graphics;
using namespace vik::graphics::animations;

#define SNAP_VELOCITY       (.0001f)

/*
 * SliderAnimation
 */
Slider::SliderAnimation::SliderAnimation(_In_ Slider *target, _In_ GLfloat end, _In_ GLfloat vel) :
    target(target),
    end(end),
    vel(vel)
{
}

void Slider::SliderAnimation::OnAnimationBegin(void)
{
    AnimationBase::OnAnimationBegin();

    GLfloat distance = this->end - this->target->_value;

    // Duration (us) is distance (px) / velocity (px / us)
    this->duration = std::abs(distance) / this->vel;

    // Protect against the possibility of a division by zero
    if (this->duration == 0)
    {
        this->duration = 1e-6f;
    }

    this->delta = distance / this->duration;
}

bool Slider::SliderAnimation::OnAnimationUpdate(_In_ uint64_t timeMicro)
{
    // Need to convert update time to GLfloat (for more accuracy in position updates)
    GLfloat time = static_cast<GLfloat>(timeMicro);

    // Ensure we don't move too far
    if (time > this->duration)
    {
        time = this->duration;
    }

    this->target->_value += this->delta * time;
    this->target->_RefreshLayout();

    this->duration -= time;
    return this->duration == 0;
}

void Slider::SliderAnimation::OnAnimationCompleted(void)
{
    AnimationBase::OnAnimationCompleted();
    assert(std::abs(target->_value - this->end) < 1e-2);
}



/*
 * Constructor(s)/Destructor
 */
Slider::Slider(void) :
    Slider(Rect(0, 0, 0, 0), HORIZONTAL, 0, 1, 0.5)
{
}

Slider::Slider(
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ Orientation orientation,
    _In_ GLfloat min,
    _In_ GLfloat max,
    _In_ GLfloat initVal) :
    Slider(Rect(0, 0, width, height), orientation, min, max, initVal)
{
}

Slider::Slider(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ Orientation orientation,
    _In_ GLfloat min,
    _In_ GLfloat max,
    _In_ GLfloat initVal) :
    Slider(Rect(x, y, width, height), orientation, min, max, initVal)
{
}

Slider::Slider(
    _In_ const Point &pt,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ Orientation orientation,
    _In_ GLfloat min,
    _In_ GLfloat max,
    _In_ GLfloat initVal) :
    Slider(Rect(pt, width, height), orientation, min, max, initVal)
{
}

Slider::Slider(
    _In_ const Rect &rc,
    _In_ Orientation orientation,
    _In_ GLfloat min,
    _In_ GLfloat max,
    _In_ GLfloat initVal) :
    LayoutAwareElement(rc),
    _orientation(orientation),
    _hoverColor(Colors::Transparent),
    _loValue(min),
    _hiValue(max),
    _value(initVal),
    _stopValue(1),
    _thumbSize(5),
    _state(SS_NORMAL),
    _manipulationStartedCallback(nullptr),
    _manipulationCompletedCallback(nullptr),
    _manipulationStartedTarget(nullptr),
    _manipulationCompletedTarget(nullptr),
    _valueCookie(INVALID_ANIMATION_COOKIE)
{
    this->_RefreshLayout();
}



/*
 * UIElement
 */
GLvoid Slider::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    this->_loRect.Draw(offX, offY);
    this->_hiRect.Draw(offX, offY);
    this->_thumb.Draw(offX, offY);
}

bool Slider::OnPointerEntered(_In_ Point pt)
{
    // We only respond to pointer events that are within the bounds of the thumb
    return this->OnPointerMoved(pt);
}

bool Slider::OnPointerExited(void)
{
    if (this->_state == SS_HOVER)
    {
        this->_state = SS_NORMAL;
        this->_thumb.SetBackgroundColor(Colors::Transparent);
    }

    return true;
}

bool Slider::OnPointerMoved(_In_ Point pt)
{
    Rect targetRect(this->_thumb.GetX() - 20, this->_thumb.GetY() - 20,
        this->_thumb.GetWidth() + 40, this->_thumb.GetHeight() + 40);

    if (this->_state == SS_MANIPULATING)
    {
        GLfloat val;
        GLfloat size;

        switch (this->_orientation)
        {
        case HORIZONTAL:
            if (pt.x < 0)
            {
                pt.x = 0;
            }
            else if (pt.x > this->GetWidth() - this->_thumbSize)
            {
                pt.x = this->GetWidth() - this->_thumbSize;
            }

            val = this->GetWidth() - this->_thumbSize - pt.x;
            size = this->GetWidth() - this->_thumbSize;
            break;

        case VERTICAL:
            if (pt.y < 0)
            {
                pt.y = 0;
            }
            else if (pt.y > this->GetHeight() - this->_thumbSize)
            {
                pt.y = this->GetHeight() - this->_thumbSize;
            }

            val = this->GetHeight() - this->_thumbSize - pt.y;
            size = this->GetHeight() - this->_thumbSize;
            break;

        default:
            val = size = 0;
            break;
        }

        this->_value = this->_loValue + (this->_hiValue - this->_loValue) * (val / size);
        this->_RefreshLayout();
    }
    else if (targetRect.Contains(pt))
    {
        if (this->_state == SS_NORMAL)
        {
            this->_state = SS_HOVER;
            this->_thumb.SetBackgroundColor(this->_hoverColor);
        }
    }
    else
    {
        if (this->_state == SS_HOVER)
        {
            this->_state = SS_NORMAL;
            this->_thumb.SetBackgroundColor(Colors::Transparent);
        }

        return false;
    }

    return true;
}

bool Slider::OnPointerDown(_In_ Point pt)
{
    AnimationManager::GetInstance()->CancelAnimation(this->_valueCookie);
    Rect targetRect(this->_thumb.GetX() - 20, this->_thumb.GetY() - 20,
        this->_thumb.GetWidth() + 40, this->_thumb.GetHeight() + 40);

    if (targetRect.Contains(pt))
    {
        assert(this->_state == SS_HOVER);
        this->_state = SS_MANIPULATING;
        this->_thumb.SetBackgroundColor(this->_thumb.GetBorderColor());

        if (this->_manipulationStartedCallback)
        {
            (this->_manipulationStartedTarget->*this->_manipulationStartedCallback)(this,
                this->_value);
        }

        return true;
    }

    return false;
}

bool Slider::OnPointerUp(_In_ Point pt)
{
    if (this->_state != SS_MANIPULATING)
    {
        return false;
    }

    this->OnPointerMoved(pt);
    Rect targetRect(this->_thumb.GetX() - 20, this->_thumb.GetY() - 20,
        this->_thumb.GetWidth() + 40, this->_thumb.GetHeight() + 40);

    if (targetRect.Contains(pt))
    {
        this->_state = SS_HOVER;
        this->_thumb.SetBackgroundColor(this->_hoverColor);
    }
    else
    {
        this->_state = SS_NORMAL;
        this->_thumb.SetBackgroundColor(Colors::Transparent);
    }

    if (this->_manipulationCompletedCallback)
    {
        (this->_manipulationCompletedTarget->*this->_manipulationCompletedCallback)(this,
            this->_value);
    }

    return true;
}



/* UI Properties */
void Slider::SetLoColor(_In_ const Color &color)
{
    this->_loRect.SetBackgroundColor(color);
}

void Slider::SetHiColor(_In_ const Color &color)
{
    this->_hiRect.SetBackgroundColor(color);
}

void Slider::SetHoverColor(_In_ const Color &color)
{
    this->_hoverColor = color;
}

void Slider::SetBorderColor(_In_ const Color &color)
{
    this->_loRect.SetBorderColor(color);
    this->_hiRect.SetBorderColor(color);
    this->_thumb.SetBorderColor(color);
}

void Slider::SetBorderThickness(_In_ GLfloat thickness)
{
    this->_loRect.SetBorderThickness(thickness);
    this->_hiRect.SetBorderThickness(thickness);
}

void Slider::SetThumbBorderThickness(_In_ GLfloat thickness)
{
    this->_thumb.SetBorderThickness(thickness);
}

void Slider::SetThumbSize(_In_ GLfloat thumbSize)
{
    this->_thumbSize = thumbSize;
    this->_RefreshLayout();
}



/*
 * Manipulation
 */
void Slider::OnManipulationStarted(
    _In_ ManipulationEventCallback callback,
    _In_ UIElement *target)
{
    this->_manipulationStartedCallback = callback;
    this->_manipulationStartedTarget = target;
}

void Slider::OnManipulationCompleted(
    _In_ ManipulationEventCallback callback,
    _In_ UIElement *target)
{
    this->_manipulationCompletedCallback = callback;
    this->_manipulationCompletedTarget = target;
}

bool Slider::IsManipulating(void) const
{
    return this->_state == SS_MANIPULATING;
}

void Slider::SetValue(_In_ GLfloat value)
{
    if (this->_state == SS_MANIPULATING)
    {
        return;
    }

    this->_SnapToValue(value);
}

GLfloat Slider::GetValue(void) const
{
    return this->_value;
}



/*
 * LayoutAwareElement
 */
void Slider::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    LayoutAwareElement::_OnSizeUpdated(width, height);
    this->_RefreshLayout();
}



void Slider::_RefreshLayout(void)
{
    // Protect against zero size
    if (this->GetWidth() <= 0 || this->GetHeight() <= 0)
    {
        this->_loRect.SetRect(Rect(0, 0, 1, 1));
        this->_hiRect.SetRect(Rect(0, 0, 1, 1));
        this->_thumb.SetRect(Rect(0, 0, 1, 1));
        return;
    }

    GLfloat loX, loY, loWidth, loHeight;
    GLfloat hiX, hiY, hiWidth, hiHeight;
    GLfloat thumbX, thumbY, thumbWidth, thumbHeight;

    switch (this->_orientation)
    {
    case HORIZONTAL:
        loX = loY = hiY = thumbY = 0;
        loHeight = hiHeight = thumbHeight = this->GetHeight();
        thumbWidth = this->_thumbSize;
        loWidth = (this->GetWidth() - this->_thumbSize) * this->_SizeRatio();
        hiWidth = this->GetWidth() - this->_thumbSize - loWidth;
        thumbX = loWidth;
        hiX = loWidth + this->_thumbSize;
        break;

    case VERTICAL:
        loX = hiX = thumbX = hiY = 0;
        loWidth = hiWidth = thumbWidth = this->GetWidth();
        thumbHeight = this->_thumbSize;
        loHeight = (this->GetHeight() - this->_thumbSize) * this->_SizeRatio();
        hiHeight = this->GetHeight() - this->_thumbSize - loHeight;
        thumbY = hiHeight;
        loY = hiHeight + this->_thumbSize;
        break;

    default:
        loX = loY = hiX = hiY = thumbX = thumbY = 0;
        loWidth = loHeight = hiWidth = hiHeight = thumbWidth = thumbHeight = 0;
        break;
    }

    this->_loRect.SetRect(Rect(loX, loY, loWidth, loHeight));
    this->_hiRect.SetRect(Rect(hiX, hiY, hiWidth, hiHeight));
    this->_thumb.SetRect(Rect(thumbX, thumbY, thumbWidth, thumbHeight));
}

GLfloat Slider::_SizeRatio(void) const
{
    return (this->_value - this->_loValue) / (this->_hiValue - this->_loValue);
}

void Slider::_SnapToValue(_In_ GLfloat value)
{
    if (value == this->_value)
    {
        return;
    }

    auto ani = new SliderAnimation(this, value, SNAP_VELOCITY);
    AnimationManager::GetInstance()->CancelAnimation(this->_valueCookie);
    this->_valueCookie = AnimationManager::GetInstance()->SubmitAnimation(ani);
}
