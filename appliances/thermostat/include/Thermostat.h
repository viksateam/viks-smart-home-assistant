/*
 * Thermostat.h
 *
 * Author(s):
 *      Josh Randall
 *      Peter Vu
 *
 * Implements the thermostat appliance driver functions
 */
#pragma once

#include "sha.h"

#include <stdint.h>

// Heat Settings
#define COOL    0
#define HEAT    1
#define OFF     2

class Thermostat
{
private:
    /* Can't instantiate an instance of a Console object */
    Thermostat(void) {}

public:
    static void Init();
    static void Update();
    static void ButtonTask();

    static int GetTempSetting();
    static int GetTempActual();
    static int GetSystemSetting();
    static bool GetFanSetting();
    static char *GetTempSettingString();
    static char *GetTempActualString();
    static char *GetSystemSettingString();
    static char *GetFanSettingString();

    /* Input */
    static void SetTempSetting(_In_ int TS);
    static void SetTempActual(_In_ int TA);
    static void SetSystemSetting(_In_ int H);
    static void SetFanSetting(_In_ bool FS);
    static void SetTempSettingString(_In_ int newTS);
    static void SetTempActualString(_In_ int newTA);

    /* Output */
    static void HeatOn();
    static void HeatOff();
    static void CoolOn();
    static void CoolOff();
    static void FanOn();
    static void FanOff();

private:
    static int s_nTempSetting; 
    static int s_nTempActual;

    static int s_nSystemSetting; // 1 is heat, 0 is cool, 2 is off (set externally)
    static bool s_fFanSetting; // 1 is on, 0 is auto

    static int s_nHeating; // Heating system on(1)/off(0)
    static int s_nCooling; // Cooling system on(1)/off(0)
    static int s_nFanning; // Fan is on(1)/off(0)

    static char s_rgszTempSetting[8];
    static char s_rgszTempActual[8];
    static char s_rgszSystemStatus[8];
    static char s_rgszFanStatus[8];
};
