/*
 * Button.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a button
 */

#include "Button.h"
#include "WindowHelpers.h"

using namespace vik::graphics;

/*
 * Constructor(s)/Destructor
 */
Button::Button(void) :
    Button(Rect(0, 0, 0, 0))
{
}

Button::Button(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
    Button(Rect(x, y, width, height))
{
}

Button::Button(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    Button(Rect(pt, width, height))
{
}

Button::Button(_In_ const Rect &rc) :
    ButtonBase(rc),
    _content(0, 0, rc.width, rc.height),
    _state(BS_NORMAL),
    _font(Fonts::ApplianceFont),
    _borderColor(Colors::Transparent),
    _hoverBackgroundColor(Colors::Transparent),
    _hoverForegroundColor(Colors::Transparent),
    _hoverBorderColor(Colors::Transparent),
    _pressedBackgroundColor(Colors::Transparent),
    _pressedForegroundColor(Colors::Transparent),
    _pressedBorderColor(Colors::Transparent)
{
    this->_content.SetBorderThickness(3);
}



/* UIElement */
GLvoid Button::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    this->_content.Draw(offX, offY);

    if (!this->_text.empty())
    {
        // Draw the text
        Color textColor;
        switch (this->_state)
        {
        case BS_NORMAL:
        case BS_EXITED:
            textColor = this->_foregroundColor;
            break;

        case BS_HOVER:
            textColor = this->_hoverForegroundColor;
            break;

        case BS_PRESSED:
            textColor = this->_pressedForegroundColor;
            break;

        default:
            // Unreached
            textColor = Colors::Transparent;
            break;
        }

        GLfloat x = offX + (this->GetWidth() - this->_textLength) / 2;
        GLfloat y = offY + (this->GetHeight() - this->_font.height) / 2;
        WindowDrawText(x, y, this->_text.c_str(), this->_font, textColor);
    }
}

void Button::SetBackgroundColor(_In_ const Color &color)
{
    ButtonBase::SetBackgroundColor(color);
    this->_OnStateChanged(this->_state);
}

void Button::SetForegroundColor(_In_ const Color &color)
{
    ButtonBase::SetForegroundColor(color);
    this->_OnStateChanged(this->_state);
}

bool Button::OnPointerEntered(_In_ Point pt)
{
    ButtonState bs = this->_state;
    if (this->_state == BS_EXITED)
    {
        this->_state = BS_PRESSED;
    }
    else
    {
        this->_state = BS_HOVER;
    }

    this->_OnStateChanged(bs);

    return ButtonBase::OnPointerEntered(pt);
}

bool Button::OnPointerExited(void)
{
    ButtonState bs = this->_state;
    if (this->_state == BS_PRESSED)
    {
        this->_state = BS_EXITED;
    }
    else
    {
        this->_state = BS_NORMAL;
    }

    this->_OnStateChanged(bs);

    return ButtonBase::OnPointerExited();
}

bool Button::OnPointerDown(_In_ Point pt)
{
    ButtonState bs = this->_state;
    this->_state = BS_PRESSED;
    this->_OnStateChanged(bs);

    return ButtonBase::OnPointerDown(pt);
}

bool Button::OnPointerUp(_In_ Point pt)
{
    ButtonState bs = this->_state;
    if (this->_state == BS_PRESSED)
    {
        this->_state = BS_HOVER;
    }
    else
    {
        this->_state = BS_NORMAL;
    }
    this->_OnStateChanged(bs);

    // We don't want to send the event to any callback functions if the point isn't actually in
    // our bounds
    if (bs == BS_EXITED)
    {
        return true;
    }

    return ButtonBase::OnPointerUp(pt);
}



/*
 * Text
 */
void Button::SetFont(_In_ const ShaFont &font)
{
    this->_font = font;
    this->_CalculateTextLength();
}

void Button::SetText(_In_ const std::string &str)
{
    this->_text = str;
    this->_CalculateTextLength();
}

const ShaFont &Button::GetFont(void) const
{
    return this->_font;
}

const std::string &Button::GetText(void) const
{
    return this->_text;
}



/*
 * Button Colors
 */
void Button::SetBorderColor(_In_ const Color &color)
{
    this->_borderColor = color;
    this->_OnStateChanged(this->_state);
}

void Button::SetHoverBackgroundColor(_In_ const Color &color)
{
    this->_hoverBackgroundColor = color;
    this->_OnStateChanged(this->_state);
}

void Button::SetHoverForegroundColor(_In_ const Color &color)
{
    this->_hoverForegroundColor = color;
    this->_OnStateChanged(this->_state);
}

void Button::SetHoverBorderColor(_In_ const Color &color)
{
    this->_hoverBorderColor = color;
    this->_OnStateChanged(this->_state);
}

void Button::SetPressedBackgroundColor(_In_ const Color &color)
{
    this->_pressedBackgroundColor = color;
    this->_OnStateChanged(this->_state);
}

void Button::SetPressedForegroundColor(_In_ const Color &color)
{
    this->_pressedForegroundColor = color;
    this->_OnStateChanged(this->_state);
}

void Button::SetPressedBorderColor(_In_ const Color &color)
{
    this->_pressedBorderColor = color;
    this->_OnStateChanged(this->_state);
}

void Button::SetPreferredColors(
    _In_ const Color &textColor,
    _In_ const Color &fillColor,
    _In_ const Color &hoverColor,
    _In_ const Color &borderColor)
{
    // Normal state
    this->_foregroundColor = textColor;
    this->_backgroundColor = fillColor;
    this->_borderColor = borderColor;

    // Hover state
    this->_hoverForegroundColor = textColor;
    this->_hoverBackgroundColor = hoverColor;
    this->_hoverBorderColor = borderColor;

    // Pressed state
    this->_pressedForegroundColor = fillColor;
    this->_pressedBackgroundColor = borderColor;
    this->_pressedBorderColor = borderColor;

    this->_OnStateChanged(this->_state);
}



/*
 * LayoutAwareElement
 */
void Button::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    ButtonBase::_OnSizeUpdated(width, height);

    this->_content.SetWidth(width);
    this->_content.SetHeight(height);
}

void Button::_OnStateChanged(_In_ ButtonState prevState)
{
    // TODO
    (void)prevState;

    switch (this->_state)
    {
    case BS_NORMAL:
    case BS_EXITED:
        this->_content.SetBorderColor(this->_borderColor);
        this->_content.SetForegroundColor(this->_foregroundColor);
        this->_content.SetBackgroundColor(this->_backgroundColor);
        break;

    case BS_HOVER:
        this->_content.SetBorderColor(this->_hoverBorderColor);
        this->_content.SetForegroundColor(this->_hoverForegroundColor);
        this->_content.SetBackgroundColor(this->_hoverBackgroundColor);
        break;

    case BS_PRESSED:
        this->_content.SetBorderColor(this->_pressedBorderColor);
        this->_content.SetForegroundColor(this->_pressedForegroundColor);
        this->_content.SetBackgroundColor(this->_pressedBackgroundColor);
        break;
    }
}

void Button::_CalculateTextLength(void)
{
    int widths[256];
    GetCharacterWidths(this->_font, widths);

    this->_textLength = 0;
    for (char ch : this->_text)
    {
        this->_textLength += widths[ch];
    }
}
