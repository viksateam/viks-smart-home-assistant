/*
 * FloatingPointAnimation.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * This class is mostly a hack. In some cases we wish to animate some arbitrary property of an
 * element (e.g. the red value in its color). Animation classes could be written for specific
 * situations, but then we end up with a bunch of highly specialized classes (and a bunch of
 * classes touching other classes' privates.
 */
#pragma once

#include "sha.h"

#include <gl/GL.h>

#include "AnimationBase.h"

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            class FloatingPointAnimation :
                public AnimationBase
            {
            public:
                FloatingPointAnimation(_In_ GLfloat *target, _In_ GLfloat end, _In_ GLfloat vel);

                /* IAnimation */
                virtual void OnAnimationBegin(void);
                virtual bool OnAnimationUpdate(_In_ uint64_t timeMicro);
                virtual void OnAnimationCompleted(void);

            private:

                GLfloat *_target;
                GLfloat _end;
                GLfloat _vel;

                GLfloat _delta;
                GLfloat _duration;
            };
        }
    }
}
