/*
 * FocusPoint.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Used as a point of emphasis on the screen, generally as a cursor or something similar. A
 * FocusPoint differs from other UIElements in that it does not obey its positioning precisely, so
 * you should avoid using this inside of layouts. The center of the FocusPoint is located at its
 * (x, y) value and expands radius pixels in all directions. Use res to set the resolution of the
 * circle that represents the FocusPoint, and stops to set the number of color changes that occur
 * within the FocusPoint.
 *
 * Visually, a FocusPoint begins with its ForegroundColor in the center and progressively changes
 * to its BackgroundColor.
 */
#pragma once

#include "UIElement.h"

namespace vik
{
    namespace graphics
    {
        class FocusPoint :
            public UIElement
        {
        public:
            FocusPoint(_In_ int res = 100, _In_ int stops = 10);
            FocusPoint(
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat radius,
                _In_ int res = 100,
                _In_ int stops = 10);
            FocusPoint(
                _In_ const Point &pt,
                _In_ GLfloat radius,
                _In_ int res = 100,
                _In_ int stops = 10);

            /* IAnimatable */
            virtual void    SetWidth(_In_ GLfloat width);
            virtual void    SetHeight(_In_ GLfloat height);
            virtual void    SetRect(_In_ const Rect &rc);
            virtual GLfloat IncreaseWidth(_In_ GLfloat deltaWidth);
            virtual GLfloat IncreaseHeight(_In_ GLfloat deltaHeight);

            /* UIElement */
            virtual GLvoid  Draw(_In_ GLfloat offX, _In_ GLfloat offY);

        private:

            void _GeneratePoints(void);

            int _res;
            int _stops;
            std::vector<Point> _points;
        };
    }
}
