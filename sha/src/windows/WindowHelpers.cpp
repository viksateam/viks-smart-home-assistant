/*
 * WindowHelpers.cpp
 *
 * Author(s):
 *      Duncan Horn
 *      Jerry Lin
 *
 * The WindowInfo implementation for the Windows operating system.
 */

#include "ShaFont.h"
#include "ShaGraphics.h"
#include "ShaLeap.h"
#include "ShaServer.h"
#include "ShaStation.h"
#include "WindowHelpers.h"

#define MS_IN_SEC           (1000)
#define STATION_TIMEOUT     (MS_IN_SEC * 4)     // 4 second timeout

using namespace vik;
using namespace vik::communication;
using namespace vik::graphics;
using namespace vik::leap;

HWND  g_hWnd;
HDC   g_hDc;
HGLRC g_hRc;

LRESULT CALLBACK WndProc(_In_ HWND hWnd, _In_ UINT uMsg, _In_ WPARAM wParam, _In_ LPARAM lParam);
VOID CALLBACK TimerProc(_In_ HWND hWnd, _In_ UINT uMsg, _In_ UINT_PTR idEvent, _In_ DWORD dwTime);

bool vik::InitWindow(_In_ const WindowInfo &info, _In_ const ShaArgs &args)
{
    UNREFERENCED_PARAMETER(args);

    WNDCLASSEX wcex    = { 0 };
    wcex.cbSize        = sizeof(WNDCLASSEX);
    wcex.style         = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc   = WndProc;
    wcex.hInstance     = info.hInstance;
    wcex.hIcon         = LoadIcon(0, MAKEINTRESOURCE(1));
    wcex.hCursor       = LoadCursor(0, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName  = nullptr;
    wcex.lpszClassName = info.className;
    wcex.hIconSm       = nullptr;

    if (!RegisterClassEx(&wcex))
    {
        MessageBox(nullptr, L"ERROR: RegisterClass failed!", nullptr, 0);
        return false;
    }

    g_hWnd = CreateWindow(info.className, info.windowName, WS_OVERLAPPEDWINDOW, info.x, info.y,
        info.width, info.height, nullptr, nullptr, info.hInstance, nullptr);

    if (!g_hWnd)
    {
        MessageBox(nullptr, L"ERROR: CreateWindow failed!", nullptr, 0);
        return false;
    }

    // We need to ensure that the server is started after g_hWnd gets assigned to as the response
    // to our broadcast calls ShaPostMessage
    if (!ShaServer::Connect() || !ShaServer::Start())
    {
        MessageBox(g_hWnd, TEXT("ERROR: Could not start server"), nullptr, MB_OK);
        return false;
    }

    if (SetTimer(g_hWnd, 0, STATION_TIMEOUT, TimerProc) == 0)
    {
        MessageBox(g_hWnd, TEXT("ERROR: Could not start timer"), nullptr, MB_OK);
        return false;
    }

    ShowWindow(g_hWnd, info.cmdShow);
    UpdateWindow(g_hWnd);

    return true;
}

int vik::MessagePump(void)
{
    MSG msg = { 0 };

    while (msg.message != WM_QUIT)
    {
        // Process window messages if they exist; otherwise update UI
        if (PeekMessage(&msg, nullptr, 0, 0, PM_NOREMOVE))
        {
            if (GetMessage(&msg, nullptr, 0, 0))
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
            /* Otherwise, result was WM_QUIT */
        }
        else
        {
            DrawSmartHomeAssistant();
            SwapBuffers(g_hDc);
        }
    }

    return (int)msg.wParam;
}

static BOOL SetupPixelFormat(_In_ HDC hdc)
{
    PIXELFORMATDESCRIPTOR pfd = { 0 };
    int pixelformat;

    pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iLayerType = PFD_MAIN_PLANE;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 32;

    pixelformat = ChoosePixelFormat(hdc, &pfd);

    if (!pixelformat)
    {
        MessageBox(NULL, TEXT("ERROR: ChoosePixelFormat failed"), nullptr, MB_OK);
        return FALSE;
    }

    if (!SetPixelFormat(hdc, pixelformat, &pfd))
    {
        MessageBox(NULL, TEXT("ERROR: SetPixelFormat failed"), nullptr, MB_OK);
        return FALSE;
    }

    return TRUE;
}

static void InitializeFont(_In_ ShaFont &font)
{
    font.hFont = CreateFont((int)std::round(font.height / FONT_HEIGHT_RATIO), 0, 0, 0,
        font.bold ? FW_BOLD : FW_NORMAL, font.italic, font.underline, FALSE, ANSI_CHARSET,
        OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, font.fontName);

    SelectObject(g_hDc, font.hFont);
    wglUseFontBitmaps(g_hDc, 0, 256, font.listBase);
}

static void InitializeFonts(void)
{
    InitializeFont(const_cast<ShaFont &>(Fonts::TitleFont));
    InitializeFont(const_cast<ShaFont &>(Fonts::ApplianceFont));
    InitializeFont(const_cast<ShaFont &>(Fonts::NotificationFont));
    InitializeFont(const_cast<ShaFont &>(Fonts::LargeFont));
    InitializeFont(const_cast<ShaFont &>(Fonts::ArtistFont));
}

static void DestroyFonts(void)
{
    DeleteObject(Fonts::TitleFont.hFont);
    DeleteObject(Fonts::ApplianceFont.hFont);
    DeleteObject(Fonts::NotificationFont.hFont);
    DeleteObject(Fonts::LargeFont.hFont);
    DeleteObject(Fonts::ArtistFont.hFont);
}

LRESULT CALLBACK WndProc(_In_ HWND hWnd, _In_ UINT uMsg, _In_ WPARAM wParam, _In_ LPARAM lParam)
{
    LRESULT lr = 0;
    RECT rect;

    switch (uMsg)
    {
    case WM_CREATE:
        g_hDc = GetDC(hWnd);
        if (!SetupPixelFormat(g_hDc))
        {
            PostQuitMessage(0);
        }

        g_hRc = wglCreateContext(g_hDc);
        wglMakeCurrent(g_hDc, g_hRc);
        GetClientRect(hWnd, &rect);
        InitializeGraphics(rect.right, rect.bottom);

        InitializeFonts();
        break;

    case WM_SIZE:
        GetClientRect(g_hWnd, &rect);
        OnWindowResize(rect.right, rect.bottom);
        DrawSmartHomeAssistant();
        SwapBuffers(g_hDc);
        break;

    case WM_CLOSE:
        if (g_hRc)
        {
            wglDeleteContext(g_hRc);
        }
        if (g_hDc)
        {
            ReleaseDC(hWnd, g_hDc);
        }
        g_hRc = 0;
        g_hDc = 0;
        OnClose();
        ShaServer::Close();
        DestroyFonts();
        DestroyWindow(g_hWnd);
        break;

    case WM_DESTROY:
        if (g_hRc)
        {
            wglDeleteContext(g_hRc);
        }
        if (g_hDc)
        {
            ReleaseDC(hWnd, g_hDc);
        }

        PostQuitMessage(0);
        break;

    case WM_KEYDOWN:
        OnKeyPressed(wParam);
        break;

    case AM_POINTER_MOVED:
    {
        GLfloat x = LOWORD(lParam);
        GLfloat y = HIWORD(lParam);

        if (g_PointerIsVisible)
        {
            OnPointerMoved(x, y);
        }
        else
        {
            OnPointerEntered(x, y);
        }
    }
        break;

    case AM_POINTER_EXITED:
        if (g_PointerIsVisible)
        {
            OnPointerExited();
        }
        break;

    case AM_POINTER_PRESSED:
    {
        GLfloat x = LOWORD(lParam);
        GLfloat y = HIWORD(lParam);

        if (!g_PointerIsPressed)
        {
            OnPointerPressed(x, y);
        }
    }
        break;

    case AM_POINTER_RELEASED:
    {
        GLfloat x = LOWORD(lParam);
        GLfloat y = HIWORD(lParam);

        if (g_PointerIsPressed)
        {
            OnPointerReleased(x, y);
        }
    }
        break;

    case AM_POINTER_TAPPED:
    {
        GLfloat x = LOWORD(lParam);
        GLfloat y = HIWORD(lParam);

        OnPointerTapped(x, y);
    }
        break;

    case AM_PINCH_GESTURE:
    {
        GLfloat x = LOWORD(lParam);
        GLfloat y = HIWORD(lParam);

        OnPinchGesture(x, y);
    }
        break;

    case AM_PINCH_RELEASE_GESTURE:
    {
        GLfloat x = LOWORD(lParam);
        GLfloat y = HIWORD(lParam);

        OnPinchReleasedGesture(x, y);
    }
        break;

    case AM_SWIPE_GESTURE:
    {
        SwipeDirection swipeDirection = (SwipeDirection)wParam;

        OnSwipeGesture(swipeDirection);
    }
        break;

    case WM_MOUSELEAVE:
        if (g_PointerIsVisible)
        {
            OnPointerExited();
        }
        break;

    case WM_MOUSEMOVE:
    {
        GLfloat x = LOWORD(lParam);
        GLfloat y = HIWORD(lParam);

        if (g_PointerIsVisible)
        {
            OnPointerMoved(x, y);
        }
        else
        {
            OnPointerEntered(x, y);

            TRACKMOUSEEVENT tme = { 0 };
            tme.cbSize = sizeof(tme);
            tme.dwFlags = TME_LEAVE;
            tme.hwndTrack = g_hWnd;
            TrackMouseEvent(&tme);
        }
    }
        break;

    case WM_LBUTTONDOWN:
        OnPointerPressed(LOWORD(lParam), HIWORD(lParam));
        break;

    case WM_LBUTTONUP:
        OnPointerReleased(LOWORD(lParam), HIWORD(lParam));
        break;

    case AM_STATION_CONNECTED:
    {
        ShaStation *pStation = (ShaStation *)lParam;
        OnStationConnect(pStation);
    }
        break;

    case AM_STATION_RESPONSE:
    {
        request_cookie cookie = (request_cookie)wParam;
        char *message = (char *)lParam;

        OnStationResponse(cookie, message);
    }
        break;

    case AM_STATION_DISCONNECTED:
    {
        char *pszId = (char *)lParam;
        OnStationDisconnect(pszId);
    }
        break;

    case AM_ITUNES_STARTED:
        OnITunesResponse(!!wParam);
        break;

    case WM_ERASEBKGND:
        // Lie and say we erased the background to eliminate flashing
        lr = TRUE;
        break;

    default:
        lr = DefWindowProc(hWnd, uMsg, wParam, lParam);
        break;
    }

    return lr;
}

VOID CALLBACK TimerProc(_In_ HWND hWnd, _In_ UINT uMsg, _In_ UINT_PTR idEvent, _In_ DWORD dwTime)
{
    UNREFERENCED_PARAMETER(hWnd);
    UNREFERENCED_PARAMETER(uMsg);
    UNREFERENCED_PARAMETER(idEvent);
    UNREFERENCED_PARAMETER(dwTime);

    CheckStationTimeouts();
}



bool vik::GetCharacterWidths(_In_ const ShaFont &font, _Out_writes_(256) int *pBuffer)
{
    SelectObject(g_hDc, font.hFont);
    return !!GetCharWidth32(g_hDc, 0, 255, pBuffer);
}

GLfloat vik::GetStringWidth(_In_ const graphics::ShaFont &font, _In_ const char *str)
{
    int rgnWidths[256];
    GLfloat fWidth = 0;

    if (GetCharacterWidths(font, rgnWidths))
    {
        while (*str)
        {
            fWidth += rgnWidths[*(str++)];
        }
    }

    return fWidth;
}

GLvoid vik::WindowDrawText(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_z_ const char *text,
    _In_ const ShaFont &font,
    _In_ const Color &color)
{
    // OpenGL drops the request if the point is invalid
    if (x < 0)
    {
        int widths[256];
        GetCharacterWidths(font, widths);

        while (x < 0 && *text)
        {
            x += widths[*text];
            text++;
        }
    }

    glColor4fv(color);

    glRasterPos2f(x, y + font.height);
    glListBase(font.listBase);
    glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);
    glFlush();
}

bool vik::ShaPostMessage(_In_ unsigned int msg, _In_ uintptr_t wParam, _In_ intptr_t lParam)
{
    if (!g_hWnd)
    {
        // Cannot post messages before g_hWnd is assigned to by the call to CreateWindow
        return false;
    }
    return !!PostMessage(g_hWnd, msg, wParam, lParam);
}
