/*
 * ShaStation.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents the relevant information needed for communicating with either an appliance or a
 * control station. Please note that properties pertaining to the control/appliance station are not
 * stored in this structure. Instead, please use separate structures for keeping track of relevant
 * information.
 */

#include "CoffeeMachineElement.h"
#include "DeletionAnimation.h"
#include "ITunesElement.h"
#include "LightElement.h"
#include "ShaCommunicationManager.h"
#include "ShaConnectionManager.h"
#include "ShaGraphics.h"
#include "ShaStation.h"
#include "ThermostatElement.h"

using namespace vik;
using namespace vik::communication;
using namespace vik::graphics;
using namespace vik::graphics::animations;

#define ALIVE_COUNT         (2)

/*
 * ShaStation
 */
ShaStation::ShaStation(
    _In_ ShaStationType type,
    _In_ const InetAddress &addr,
    _In_ const std::string &id,
    _In_ const std::string &name) :
    _type(type),
    _addr(addr),
    _id(id),
    _name(name),
    _alive(ALIVE_COUNT),
    _element(nullptr)
{
}

ShaStation::~ShaStation(void)
{
    if (this->_element)
    {
        this->_element->CancelCurrentAnimation();
        delete this->_element;
    }
}



ShaStation *ShaStation::Convert(
    _In_ const ShaStation *prev,
    _In_ const std::string &type,
    _In_ const std::vector<std::string> &args)
{
    if (type == COFFEE_MAKER_TYPE)
    {
        return new CoffeeMachine(*prev, args);
    }
    else if (type == LIGHT_TYPE)
    {
        return new Light(*prev, args);
    }
    else if (type == THERMOSTAT_TYPE)
    {
        return new Thermostat(*prev, args);
    }
    else if (type == ITUNES_TYPE)
    {
        return new ITunes(*prev, args);
    }

    // Otherwise, not specialized
    return nullptr;
}

const ShaStationType &ShaStation::Type(void) const
{
    return this->_type;
}

const InetAddress &ShaStation::InetAddr(void) const
{
    return this->_addr;
}

const std::string &ShaStation::Id(void) const
{
    return this->_id;
}

const std::string &ShaStation::Name(void) const
{
    return this->_name;
}

bool ShaStation::PostGetStatusRequest(void)
{
    // There is nothing to do for a generic ShaStation element
    return false;
}

void ShaStation::PostStationRequest(_In_ const std::string &msg) const
{
    auto cookie = ShaCommunicationManager::PostServerRequest(this, RT_POST, msg);
    SubmitStationRequest(cookie, RequestInfo{ this->_id,
        [](_In_ const RequestInfo &sender, _In_ const char *msg)
    {
        (void)msg;

        auto target = ShaConnectionManager::GetStation(sender.targetId);
        if (target)
        {
            // Always post a status request, even if the operation failed as the reason why the
            // operation failed is likely because our status is incorrect
            target->PostGetStatusRequest();
        }
    }});
}

UIElement *ShaStation::GetElementRepresentation(void)
{
    if (!this->_element)
    {
        this->_element = this->_CreateElementRepresentation();
    }

    return this->_element;
}

bool ShaStation::IsAlive(void) const
{
    return this->_alive > 0;
}

void ShaStation::SetAlive(_In_ bool isAlive)
{
    if (isAlive)
    {
        this->_alive = ALIVE_COUNT;
    }
    else
    {
        this->_alive--;
    }
}



/*
 * Protected Functions
 */
UIElement *ShaStation::_CreateElementRepresentation(void)
{
    return nullptr;
}



/*
 * CoffeeMachine
 */
#pragma region CoffeeMachine
CoffeeMachine::CoffeeMachine(
    _In_ const ShaStation &station,
    _In_ const std::vector<std::string> &args) :
    ShaStation(station)
{
    for (auto &arg : args)
    {
        auto itr = std::find(std::begin(arg), std::end(arg), '=');
        if (itr != std::end(arg))
        {
            std::string key(std::begin(arg), itr);
            std::string value(itr + 1, std::end(arg));

            if (key == "cups")
            {
                while (!value.empty())
                {
                    auto it = std::find(std::begin(value), std::end(value), ',');
                    std::string opt(std::begin(value), it);

                    if (it != std::end(value))
                    {
                        it++;
                    }
                    value.erase(std::begin(value), it);

                    this->_options.push_back(opt);
                }
            }
        }
    }

    this->_status = CoffeeMachineStatus{ CoffeeMachineStatus::COFFEE_OFF, 0 };
    this->PostGetStatusRequest();
}

const CoffeeMachineStatus &CoffeeMachine::GetStatus(void) const
{
    return this->_status;
}

void CoffeeMachine::SetStatus(_In_ const CoffeeMachineStatus &status)
{
    this->_status = status;

    auto elem = dynamic_cast<CoffeeMachineElement *>(this->_element);
    if (elem)
    {
        elem->OnStatusChanged(status);
    }
}

const std::vector<std::string> &CoffeeMachine::GetOptions(void) const
{
    return this->_options;
}

bool CoffeeMachine::PostGetStatusRequest(void)
{
    auto cookie = ShaCommunicationManager::PostServerRequest(this, RT_GET, "status");
    SubmitStationRequest(cookie,
        RequestInfo{ this->_id, [](_In_ const RequestInfo &sender, _In_ const char *msg)
    {
        CoffeeMachine *target =
            dynamic_cast<CoffeeMachine *>(ShaConnectionManager::GetStation(sender.targetId));
        if (target && msg)
        {
            std::istringstream stream(msg);
            CoffeeMachineStatus cms{ CoffeeMachineStatus::COFFEE_OFF, 0 };
            std::string status;

            stream >> status;

            if (status == "on")
            {
                cms.state = CoffeeMachineStatus::COFFEE_ON;

                if (!stream.eof())
                {
                    std::string amt;
                    stream >> amt;

                    auto pos = std::find(std::begin(amt), std::end(amt), '=');
                    if (pos != std::end(amt))
                    {
                        // atoi returns zero on failure which is preferable as we use zero to
                        // indicate no set number of cups
                        cms.cups = atoi(std::string(pos + 1, std::end(amt)).c_str());
                    }
                }
            }
            else if (status == "done")
            {
                cms.state = CoffeeMachineStatus::COFFEE_DONE;
            }

            target->SetStatus(cms);
        }
    }});

    return true;
}

void CoffeeMachine::PostStatusRequest(_In_ const CoffeeMachineStatus &status) const
{
    assert(status.state != CoffeeMachineStatus::COFFEE_DONE);

    std::stringstream stream;
    stream << "coffee ";
    if (status.state == CoffeeMachineStatus::COFFEE_ON)
    {
        stream << "on";
        if (status.cups != 0)
        {
            stream << " cups=" << status.cups;
        }
    }
    else
    {
        stream << "off";
    }

    this->PostStationRequest(stream.str());
}

UIElement *CoffeeMachine::_CreateElementRepresentation(void)
{
    return new CoffeeMachineElement(this);
}
#pragma endregion



/*
 * ITunes
 */
#pragma region ITunes
ITunes::ITunes(_In_ const ShaStation &station, _In_ const std::vector<std::string> &args) :
    ShaStation(station)
{
    (void)args;
    this->_status = ITunesStatus{ ITunesStatus::SS_PAUSE, "N/A", "N/A", "N/A" };
    this->PostGetStatusRequest();
}

const ITunesStatus &ITunes::GetStatus(void) const
{
    return this->_status;
}

void ITunes::SetStatus(_In_ const ITunesStatus &status)
{
    this->_status = status;

    ITunesElement *elem = dynamic_cast<ITunesElement *>(this->_element);
    if (elem)
    {
        elem->OnStatusChanged(this->_status);
    }
}

bool ITunes::PostGetStatusRequest(void)
{
    if (this->_id == ShaCommunicationManager::StationId())
    {
        ITunesStatus status;
        if (GetITunesStatus(status))
        {
            this->SetStatus(status);
            return true;
        }

        return false;
    }
    else
    {
        auto cookie = ShaCommunicationManager::PostServerRequest(this, RT_GET, "status");
        SubmitStationRequest(cookie,
            RequestInfo{ this->_id, [](_In_ const RequestInfo &sender, _In_ const char *msg)
        {
            ITunes *target =
                dynamic_cast<ITunes *>(ShaConnectionManager::GetStation(sender.targetId));
            if (target && msg && strcmp(msg, "invalid"))
            {
                ITunesStatus status;
                std::istringstream stream(msg);

                std::string songStatus;
                stream >> songStatus >> status.artistName >> status.albumName >> status.trackName;

                std::replace(std::begin(status.artistName), std::end(status.artistName), '_', ' ');
                std::replace(std::begin(status.albumName), std::end(status.albumName), '_', ' ');
                std::replace(std::begin(status.trackName), std::end(status.trackName), '_', ' ');

                if (songStatus == "play")
                {
                    status.songState = ITunesStatus::SS_PLAY;
                }
                else if (songStatus == "pause")
                {
                    status.songState = ITunesStatus::SS_PAUSE;
                }
                else
                {
                    return;
                }

                target->SetStatus(status);
            }
        }});
    }

    return true;
}

void ITunes::PostStatusRequest(_In_ const ITunesStatus::SongStatus status)
{
    if (this->_id == ShaCommunicationManager::StationId())
    {
        ITunesSongRequest(status);
        this->PostGetStatusRequest();
    }
    else
    {
        std::string cmd = "song ";

        switch (status)
        {
        case ITunesStatus::SS_PLAY:
            cmd += "play";
            break;

        case ITunesStatus::SS_PAUSE:
            cmd += "pause";
            break;
        }

        this->PostStationRequest(cmd);
    }
}

void ITunes::PostStatusRequest(_In_ TrackRequest status)
{
    if (this->_id == ShaCommunicationManager::StationId())
    {
        ITunesTrackRequest(status);
        this->PostGetStatusRequest();
    }
    else
    {
        std::string cmd = "track ";

        switch (status)
        {
        case TR_NEXT:
            cmd += "next";
            break;

        case TR_PREVIOUS:
            cmd += "previous";
            break;

        default:
            // Unreached
            abort();
            break;
        }

        this->PostStationRequest(cmd);
    }
}

UIElement *ITunes::_CreateElementRepresentation(void)
{
    return new ITunesElement(this);
}
#pragma endregion



/*
 * Light
 */
#pragma region Light
Light::Light(
    _In_ const ShaStation &station,
    _In_ const std::vector<std::string> &args) :
    ShaStation(station),
    _status(LIGHT_OFF)
{
    (void)args;

    this->PostGetStatusRequest();
}

LightStatus Light::GetStatus(void) const
{
    return this->_status;
}

void Light::SetStatus(_In_ LightStatus status)
{
    this->_status = status;

    auto elem = dynamic_cast<LightElement *>(this->_element);
    if (elem)
    {
        elem->OnStatusChanged(status);
    }
}

bool Light::PostGetStatusRequest(void)
{
    auto cookie = ShaCommunicationManager::PostServerRequest(this, RT_GET, "status");
    SubmitStationRequest(cookie,
        RequestInfo{ this->_id, [](_In_ const RequestInfo &sender, _In_ const char *msg)
    {
        Light *target = dynamic_cast<Light *>(ShaConnectionManager::GetStation(sender.targetId));
        if (target && msg)
        {
            if (!strcmp(msg, "on"))
            {
                target->SetStatus(LIGHT_ON);
            }
            else if (!strcmp(msg, "off"))
            {
                target->SetStatus(LIGHT_OFF);
            }
        }
    }});

    return true;
}

void Light::PostStatusRequest(_In_ LightStatus status) const
{
    std::string cmd = (status == LIGHT_ON) ? "light on" : "light off";
    this->PostStationRequest(cmd);
}

UIElement *Light::_CreateElementRepresentation(void)
{
    return new LightElement(this);
}
#pragma endregion



/*
 * Thermostat
 */
#pragma region Thermostat
Thermostat::Thermostat(
    _In_ const ShaStation &station,
    _In_ const std::vector<std::string> &args) :
    ShaStation(station),
    _minTemp(0),
    _maxTemp(100)
{
    for (auto &arg : args)
    {
        auto itr = std::find(std::begin(arg), std::end(arg), '=');
        if (itr != std::end(arg))
        {
            std::string key(std::begin(arg), itr);
            std::string value(itr + 1, std::end(arg));

            if (key == "min")
            {
                this->_minTemp = atoi(value.c_str());
            }
            else if (key == "max")
            {
                this->_maxTemp = atoi(value.c_str());
            }
        }
    }

    if (this->_minTemp >= this->_maxTemp)
    {
        this->_minTemp = 0;
        this->_maxTemp = 100;
    }

    this->_status = ThermostatStatus{ ThermostatStatus::SS_COOL, ThermostatStatus::FS_AUTO, 0, 0 };
    this->PostGetStatusRequest();
}

const ThermostatStatus &Thermostat::GetStatus(void) const
{
    return this->_status;
}

void Thermostat::SetStatus(_In_ const ThermostatStatus &status)
{
    this->_status = status;

    auto elem = dynamic_cast<ThermostatElement *>(this->_element);
    if (elem)
    {
        elem->OnStatusChanged(status);
    }
}

int Thermostat::GetMinTemp(void) const
{
    return this->_minTemp;
}

int Thermostat::GetMaxTemp(void) const
{
    return this->_maxTemp;
}

bool Thermostat::PostGetStatusRequest(void)
{
    auto cookie = ShaCommunicationManager::PostServerRequest(this, RT_GET, "status");
    SubmitStationRequest(cookie,
        RequestInfo{ this->_id, [](_In_ const RequestInfo &sender, _In_ const char *msg)
    {
        Thermostat *target =
            dynamic_cast<Thermostat *>(ShaConnectionManager::GetStation(sender.targetId));
        if (target && msg)
        {
            ThermostatStatus status = target->_status;
            std::istringstream stream(msg);

            std::string system, fan;
            stream >> system >> fan >> status.setTemp >> status.currTemp;

            if (system == "heat")
            {
                status.system = ThermostatStatus::SS_HEAT;
            }
            else if (system == "cool")
            {
                status.system = ThermostatStatus::SS_COOL;
            }
            else if (system == "off")
            {
                status.system = ThermostatStatus::SS_OFF;
            }

            if (fan == "on")
            {
                status.fan = ThermostatStatus::FS_ON;
            }
            else if (fan == "auto")
            {
                status.fan = ThermostatStatus::FS_AUTO;
            }

            target->SetStatus(status);
        }
    }});

    return true;
}

void Thermostat::PostStatusRequest(_In_ const ThermostatStatus &status) const
{
    if (status.system != this->_status.system)
    {
        this->_PostSystemStatusRequest(status.system);
    }
    if (status.fan != this->_status.fan)
    {
        this->_PostFanStatusRequest(status.fan);
    }
    if (status.setTemp != this->_status.setTemp)
    {
        this->_PostTempRequest(status.setTemp);
    }
}

UIElement *Thermostat::_CreateElementRepresentation(void)
{
    return new ThermostatElement(this);
}

void Thermostat::_PostSystemStatusRequest(_In_ ThermostatStatus::SystemStatus status) const
{
    std::string cmd = "system ";

    switch (status)
    {
    case ThermostatStatus::SS_OFF:
        cmd += "off";
        break;

    case ThermostatStatus::SS_COOL:
        cmd += "cool";
        break;

    case ThermostatStatus::SS_HEAT:
        cmd += "heat";
        break;
    }

    this->PostStationRequest(cmd);
}

void Thermostat::_PostFanStatusRequest(_In_ ThermostatStatus::FanStatus status) const
{
    std::string cmd = "fan ";

    switch (status)
    {
    case ThermostatStatus::FS_AUTO:
        cmd += "auto";
        break;

    case ThermostatStatus::FS_ON:
        cmd += "on";
        break;
    }

    this->PostStationRequest(cmd);
}

void Thermostat::_PostTempRequest(_In_ int temp) const
{
    std::stringstream stream;
    stream << "set " << temp;

    this->PostStationRequest(stream.str());
}
#pragma endregion
