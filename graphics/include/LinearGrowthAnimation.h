/*
 * LinearGrowthAnimation.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents an animation that grows to fit either a specified size or a specified rectangle at a
 * constant velocity. If growing to a specified rectangle, the IAnimatable target will translate as
 * it grows. If growing to a specified size, the target will translate according to the input
 * GrowthTranslationDirection. If GTD_ABSOLUTE is specified (not recommended), the target will
 * animate back to its location when the animation was constructed.
 */
#pragma once

#include "Animatable.h"
#include "AnimationBase.h"

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            enum GrowthTranslationDirection
            {
                GTD_ABSOLUTE,       // Translate to the desired ending point
                GTD_TOP_LEFT,       // Keep top left corner fixed
                GTD_TOP_RIGHT,      // Keep top right corner fixed
                GTD_BOTTOM_LEFT,    // Keep bottom left corner fixed
                GTD_BOTTOM_RIGHT,   // Keep bottom right corner fixed
                GTD_CENTER          // Keep center fixed (all corners translate evenly
            };

            class LinearGrowthAnimation :
                public AnimationBase
            {
            public:
                LinearGrowthAnimation(
                    _In_ IAnimatable *target,
                    _In_ GLfloat endWidth,
                    _In_ GLfloat endHeight,
                    _In_ GLfloat vel,
                    _In_ GrowthTranslationDirection gtd);
                LinearGrowthAnimation(
                    _In_ IAnimatable *target,
                    _In_ GLfloat endX,
                    _In_ GLfloat endY,
                    _In_ GLfloat endWidth,
                    _In_ GLfloat endHeight,
                    _In_ GLfloat vel);
                LinearGrowthAnimation(
                    _In_ IAnimatable *target,
                    _In_ const Point &endLocation,
                    _In_ GLfloat endWidth,
                    _In_ GLfloat endHeight,
                    _In_ GLfloat vel);
                LinearGrowthAnimation(
                    _In_ IAnimatable *target,
                    _In_ const Rect &endRect,
                    _In_ GLfloat vel);

                /* IAnimation */
                virtual void OnAnimationBegin(void);
                virtual bool OnAnimationUpdate(_In_ uint64_t timeMicro);
                virtual void OnAnimationCompleted(void);

            private:

                Rect    _endRect;
                GLfloat _velocity;
                GrowthTranslationDirection _gtd;

                // Set during OnAnimationBegin
                GLfloat _deltaX;
                GLfloat _deltaY;
                GLfloat _deltaWidth;
                GLfloat _deltaHeight;
                GLfloat _duration;
            };
        }
    }
}
