/*
 * DelayAnimation.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Used to represent a delay between two separate animations. The duration of the delay is
 * specified in units of microseconds.
 */
#pragma once

#include "AnimationBase.h"

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            class DelayAnimation final :
                public AnimationBase
            {
            public:
                DelayAnimation(_In_ IAnimatable *target, _In_ int64_t durationMicro);

                /* IAnimation */
                virtual bool OnAnimationUpdate(_In_ uint64_t timeMicro);

            private:

                int64_t _duration;
            };
        }
    }
}
