// Timer0A.c
// Runs on LM4F120
// Use Timer0A in periodic mode to request interrupts at a particular
// period.
// Daniel Valvano
// May 13, 2013

/* This example accompanies the book
   "Embedded Systems: Introduction to ARM Cortex M Microcontrollers"
   ISBN: 978-1469998749, Jonathan Valvano, copyright (c) 2013
   Volume 1, Program 9.8

  "Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers",
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2013
   Volume 2, Program 7.5, example 7.6

 Copyright 2013 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */
 
#include "driverlib/timer.h"
#include "Timer1A.h"

#define TIMER1_BASE             0x40031000UL
#define NVIC_EN0_INT21          0x00200000  // Interrupt 21 enable
#define NVIC_EN0_R              (*((volatile unsigned long *)0xE000E100))  // IRQ 0 to 31 Set Enable Register
#define NVIC_PRI5_R             (*((volatile unsigned long *)0xE000E414))  // IRQ 20 to 23 Priority Register
//*****************************************************************************
//
// Timer registers (TIMER1)
//
//*****************************************************************************
#define TIMER1_CFG_R            (*((volatile unsigned long *)0x40031000))
#define TIMER1_TAMR_R           (*((volatile unsigned long *)0x40031004))
#define TIMER1_TBMR_R           (*((volatile unsigned long *)0x40031008))
#define TIMER1_CTL_R            (*((volatile unsigned long *)0x4003100C))
#define TIMER1_SYNC_R           (*((volatile unsigned long *)0x40031010))
#define TIMER1_IMR_R            (*((volatile unsigned long *)0x40031018))
#define TIMER1_RIS_R            (*((volatile unsigned long *)0x4003101C))
#define TIMER1_MIS_R            (*((volatile unsigned long *)0x40031020))
#define TIMER1_ICR_R            (*((volatile unsigned long *)0x40031024))
#define TIMER1_TAILR_R          (*((volatile unsigned long *)0x40031028))
#define TIMER1_TBILR_R          (*((volatile unsigned long *)0x4003102C))
#define TIMER1_TAMATCHR_R       (*((volatile unsigned long *)0x40031030))
#define TIMER1_TBMATCHR_R       (*((volatile unsigned long *)0x40031034))
#define TIMER1_TAPR_R           (*((volatile unsigned long *)0x40031038))
#define TIMER1_TBPR_R           (*((volatile unsigned long *)0x4003103C))
#define TIMER1_TAPMR_R          (*((volatile unsigned long *)0x40031040))
#define TIMER1_TBPMR_R          (*((volatile unsigned long *)0x40031044))
#define TIMER1_TAR_R            (*((volatile unsigned long *)0x40031048))
#define TIMER1_TBR_R            (*((volatile unsigned long *)0x4003104C))
#define TIMER1_TAV_R            (*((volatile unsigned long *)0x40031050))
#define TIMER1_TBV_R            (*((volatile unsigned long *)0x40031054))
#define TIMER1_RTCPD_R          (*((volatile unsigned long *)0x40031058))
#define TIMER1_TAPS_R           (*((volatile unsigned long *)0x4003105C))
#define TIMER1_TBPS_R           (*((volatile unsigned long *)0x40031060))
#define TIMER1_TAPV_R           (*((volatile unsigned long *)0x40031064))
#define TIMER1_TBPV_R           (*((volatile unsigned long *)0x40031068))
#define TIMER1_PP_R             (*((volatile unsigned long *)0x40031FC0))
#define TIMER_CFG_16_BIT        0x00000004  // 16-bit timer configuration,
                                            // function is controlled by bits
                                            // 1:0 of GPTMTAMR and GPTMTBMR
#define TIMER_TAMR_TACDIR       0x00000010  // GPTM Timer A Count Direction
#define TIMER_TAMR_TAMR_PERIOD  0x00000002  // Periodic Timer mode
#define TIMER_CTL_TAEN          0x00000001  // GPTM TimerA Enable
#define TIMER_IMR_TATOIM        0x00000001  // GPTM TimerA Time-Out Interrupt
                                            // Mask
#define TIMER_ICR_TATOCINT      0x00000001  // GPTM TimerA Time-Out Raw
                                            // Interrupt
#define TIMER_TAILR_TAILRL_M    0x0000FFFF  // GPTM TimerA Interval Load
                                            // Register Low

#define SYSCTL_RCGCTIMER_R      (*((volatile unsigned long *)0x400FE604))
#define SYSCTL_RCGCTIMER_R1     0x00000002  // Timer 1 Run Mode Clock Gating
                                            // Control

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical(void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode
void (*PeriodicTask1A)(void);   // user function

unsigned long periodMs1A;
unsigned long currentMs1A;

// ***************** Timer0A_Init ****************
// Activate Timer0A interrupts to run user task periodically
// Inputs:  task is a pointer to a user function
//          period in usec
// Outputs: none
void Timer1A_Init(void(*task)(void), unsigned long period)
{
    SYSCTL_RCGCTIMER_R = SYSCTL_RCGCTIMER_R1; // activate timer1
    periodMs1A = 0;
    currentMs1A = 0;
    PeriodicTask1A = task;             // user function (this line also allows time to finish activating)
    TIMER1_CTL_R &= ~0x00000001;     // 1) disable timer0A during setup
    TIMER1_CFG_R = 0x00000004;       // 2) configure for 16-bit timer mode
    TIMER1_TAMR_R = 0x00000002;      // 3) configure for periodic mode, default down-count settings
    TIMER1_TAILR_R = period-1;       // 4) reload value
    TIMER1_TAPR_R = 49;              // 5) 1us timer0A
    TIMER1_ICR_R = 0x00000001;       // 6) clear timer0A timeout flag
    TIMER1_IMR_R |= 0x00000001;      // 7) arm timeout interrupt
    NVIC_PRI5_R = (NVIC_PRI5_R&0x00FFFFFF)|0x40000000; // 8) priority 2
    NVIC_EN0_R = NVIC_EN0_INT21;     // 9) enable interrupt 19 in NVIC
    TimerIntRegister(TIMER1_BASE, TIMER_A, &TIMER1A_Handler);
    //TIMER0_CTL_R |= 0x00000001;      // 10) enable timer0A
}

void Timer1A_Enable(void)
{
    TIMER1_CTL_R |= 0x00000001;
}

void Timer1A_Disable(void)
{
    TIMER1_CTL_R &= ~0x00000001;
}

void Timer1A_SetTask(void(*task) (void))
{
    PeriodicTask1A = task;
}

void Timer1A_SetPeriodInMs(unsigned long ms)
{
    periodMs1A = ms;
}

void Timer1A_Reset(void)
{
    currentMs1A = 0;
}

void TIMER1A_Handler(void)
{
    TIMER1_ICR_R = TIMER_ICR_TATOCINT;// acknowledge timer0A timeout
    
    if (currentMs1A >= periodMs1A)
    {
        (*PeriodicTask1A)();                // execute user task
        currentMs1A = 0;
    }
    
    currentMs1A++;
}
