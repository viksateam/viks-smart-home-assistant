/*
 * LinearGrowthAnimation.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents an animation that grows to fit either a specified size or a specified rectangle at a
 * constant velocity. If growing to a specified rectangle, the IAnimatable target will translate as
 * it grows. If growing to a specified size, the target will translate according to the input
 * GrowthTranslationDirection. If GTD_ABSOLUTE is specified (not recommended), the target will
 * animate back to its location when the animation was constructed.
 */

#include <algorithm>

#include "ConcurrentAnimation.h"
#include "LinearGrowthAnimation.h"

using namespace vik::graphics;
using namespace vik::graphics::animations;

/*
 * Constructors/Destructor
 */
LinearGrowthAnimation::LinearGrowthAnimation(
    _In_ IAnimatable *target,
    _In_ GLfloat endWidth,
    _In_ GLfloat endHeight,
    _In_ GLfloat vel,
    _In_ GrowthTranslationDirection gtd) :
    AnimationBase(target),
    _endRect(target->GetLocation(), endWidth, endHeight),
    _velocity(vel),
    _gtd(gtd)
{
    assert(target);
    assert(vel > 0);
}

LinearGrowthAnimation::LinearGrowthAnimation(
    _In_ IAnimatable *target,
    _In_ GLfloat endX,
    _In_ GLfloat endY,
    _In_ GLfloat endWidth,
    _In_ GLfloat endHeight,
    _In_ GLfloat vel) :
    AnimationBase(target),
    _endRect(endX, endY, endWidth, endHeight),
    _velocity(vel),
    _gtd(GrowthTranslationDirection::GTD_ABSOLUTE)
{
    assert(target);
    assert(vel > 0);
}

LinearGrowthAnimation::LinearGrowthAnimation(
    _In_ IAnimatable *target,
    _In_ const Point &endLocation,
    _In_ GLfloat endWidth,
    _In_ GLfloat endHeight,
    _In_ GLfloat vel) :
    AnimationBase(target),
    _endRect(endLocation, endWidth, endHeight),
    _velocity(vel),
    _gtd(GrowthTranslationDirection::GTD_ABSOLUTE)
{
    assert(target);
    assert(vel > 0);
}

LinearGrowthAnimation::LinearGrowthAnimation(
    _In_ IAnimatable *target,
    _In_ const Rect &endRect,
    _In_ GLfloat vel) :
    AnimationBase(target),
    _endRect(endRect),
    _velocity(vel),
    _gtd(GrowthTranslationDirection::GTD_ABSOLUTE)
{
    assert(target);
    assert(vel > 0);
}



/*
 * IAnimation
 */
void LinearGrowthAnimation::OnAnimationBegin(void)
{
    AnimationBase::OnAnimationBegin();

    // Regardless of the GrowthTranslationDirection, we always animate to the target width/height
    GLfloat deltaWidth = this->_endRect.width - this->_target->GetWidth();
    GLfloat deltaHeight = this->_endRect.height - this->_target->GetHeight();

    // Figure out how far we have to translate
    GLfloat deltaX = 0;
    GLfloat deltaY = 0;
    switch (this->_gtd)
    {
    case GTD_ABSOLUTE:
        // Translate to the ending point
        deltaX = this->_endRect.pt.x - this->_target->GetX();
        deltaY = this->_endRect.pt.y - this->_target->GetY();
        break;

    case GTD_TOP_LEFT:
        break;

    case GTD_TOP_RIGHT:
        // Only need to translate in the x direction
        deltaX = -deltaWidth;
        break;

    case GTD_BOTTOM_LEFT:
        // Only need to translate in the y direction
        deltaY = -deltaHeight;
        break;

    case GTD_BOTTOM_RIGHT:
        // Need to translate in both x and y direction
        deltaX = -deltaWidth;
        deltaY = -deltaHeight;
        break;

    case GTD_CENTER:
        // Translate in both x and y direction, but half as much
        deltaX = -deltaWidth / 2;
        deltaY = -deltaHeight / 2;
        break;

    default:
        assert(false);
        break;
    }

    // Determine how far we have to grow/translate so we can determine durations
    GLfloat growthDistance = std::sqrt(deltaWidth * deltaWidth + deltaHeight * deltaHeight);
    GLfloat translateDistance = std::sqrt(deltaX * deltaX + deltaY * deltaY);

    GLfloat growthDuration = growthDistance / this->_velocity;
    GLfloat translationDuration = translateDistance / this->_velocity;

    this->_duration = std::max(translationDuration, growthDuration);

    if (this->_duration == 0)
    {
        // Protect against dividing by zero
        this->_duration = 1e-6f;
    }

    // Finally, calculate the deltas (which are really velocities)
    this->_deltaWidth = deltaWidth / this->_duration;
    this->_deltaHeight = deltaHeight / this->_duration;
    this->_deltaX = deltaX / this->_duration;
    this->_deltaY = deltaY / this->_duration;
}

bool LinearGrowthAnimation::OnAnimationUpdate(_In_ uint64_t timeMicro)
{
    // Need to convert update time to GLfloat (for more accuracy in position updates)
    GLfloat time = static_cast<GLfloat>(timeMicro);

    // Ensure we don't move too far
    if (time > this->_duration)
    {
        time = this->_duration;
    }

    this->_target->IncreaseWidth(this->_deltaWidth * time);
    this->_target->IncreaseHeight(this->_deltaHeight * time);
    this->_target->Translate(this->_deltaX * time, this->_deltaY * time);

    this->_duration -= time;

    // _tranlationDuration is guaranteed to be at least as long as the growth duration, so use that
    // to determine whether the animation is done or not
    return this->_duration == 0;
}

void LinearGrowthAnimation::OnAnimationCompleted(void)
{
    AnimationBase::OnAnimationCompleted();

    // No action needs to be taken. Assert that the IAnimatable object is in the correct location
    // with the correct dimensions for debugging purposes
    if (this->_gtd == GrowthTranslationDirection::GTD_ABSOLUTE)
    {
        assert(std::abs(this->_target->GetX() - this->_endRect.pt.x) < 1e-2);
        assert(std::abs(this->_target->GetY() - this->_endRect.pt.y) < 1e-2);
    }

    assert(std::abs(this->_target->GetWidth() - this->_endRect.width) < 1e-2);
    assert(std::abs(this->_target->GetHeight() - this->_endRect.height) < 1e-2);
}
