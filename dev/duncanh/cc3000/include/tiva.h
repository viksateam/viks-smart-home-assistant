/*
 * tiva.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Useful functions specific to the Tiva C Series TM4C123G LaunchPad Evaluation Kit microcontroller
 */
#pragma once

class Tiva
{
private:
    /* Can't instantiate an instance of a Tiva object */
    Tiva(void) {}

public:

    static void InitClock(void);

};
