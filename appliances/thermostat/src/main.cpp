/*
 * main.cpp
 *
 * Author(s):
 *      Josh Randall
 *      Peter Vu
 *
 * The main implementation for the thermostat appliance
 */

#include "ADCT0ATrigger.h"
#include "Console.h"
#include "Debug.h"
#include "GPIO.h"
#include "ip.h"
#include "Server.h"
#include "ST7735.h"
#include "sys_tick.h"
#include "tiva.h"
#include "Thermostat.h"

#include <string.h>

#define NVIC_PRI1_R             (*((volatile unsigned long *)0xE000E404))  // IRQ 4 to 7 Priority Register

#ifdef __cplusplus
extern "C"
{
#endif

void DisableInterrupts(void); // Disable interrupts
void EnableInterrupts(void);  // Enable interrupts
long StartCritical (void);    // previous I bit, disable interrupts
void EndCritical(long sr);    // restore I bit to previous value
void WaitForInterrupt(void);  // low power mode

#ifdef __cplusplus
}
#endif

char buffer[128];

static char *IncomingMessageHandler2(_Inout_ char *pszMessage)
{
    // Parse message
    if (strcmp(pszMessage, "request get info") == 0)
    {
        return "thermostat min=60 max=90\n";
    }
    else if (strcmp(pszMessage, "request get status") == 0)
    {
        // System status
        strcpy(buffer, "");
        strcat(buffer, Thermostat::GetSystemSettingString());
        strcat(buffer, " ");
        
        // Fan status
        strcat(buffer, Thermostat::GetFanSettingString());
        strcat(buffer, " ");
        
        // TempSetting
        strcat(buffer, Thermostat::GetTempSettingString());
        strcat(buffer, " ");
        
        // TempActual
        strcat(buffer, Thermostat::GetTempActualString());
        strcat(buffer, "\n");
        
        return buffer;
    }
    else
    {
        return "invalid\n";
    }
}

static char *IncomingMessageHandler(_Inout_ char *pszMessage)
{
    char* ret;
    char* token;
    
    Console::WriteLine(pszMessage);
    
    // Parse message for command
    token = strtok(pszMessage, " ");
    
    // Parse message
    if (strcmp(token, "request") == 0)
    {
        token = strtok(NULL, " ");
        
        if (strcmp(token, "get") == 0)
        {
            ret = "ack\n";
            token = strtok(NULL, " ");
            
            if (strcmp(token, "info") == 0)
            {
                ret = "thermostat min=60 max=90\n";
            }
            else if (strcmp(token, "status") == 0)
            {
                // System status
                strcpy(buffer, "");
                strcat(buffer, Thermostat::GetSystemSettingString());
                strcat(buffer, " ");
                
                // Fan status
                strcat(buffer, Thermostat::GetFanSettingString());
                strcat(buffer, " ");
                
                // TempSetting
                strcat(buffer, Thermostat::GetTempSettingString());
                strcat(buffer, " ");
                
                // TempActual
                strcat(buffer, Thermostat::GetTempActualString());
                strcat(buffer, "\n");
                
                ret = buffer;
            }
            else if (strcmp(token, "system") == 0)
            {
                // System status
                strcpy(buffer, "");
                strcat(buffer, Thermostat::GetSystemSettingString());
                strcat(buffer, "\n");
                ret = buffer;
            }
            else if (strcmp(token, "fan") == 0)
            {
                // Fan status
                strcpy(buffer, "");
                strcat(buffer, Thermostat::GetFanSettingString());
                strcat(buffer, "\n");
                ret = buffer;
            }
            else if (strcmp(token, "temp") == 0)
            {
                token = strtok(NULL, " ");
                
                if (strcmp(token, "actual") == 0)
                {
                    // Current actual temperature to the control station
                    strcpy(buffer, "");
                    strcat(buffer, Thermostat::GetTempActualString());
                    strcat(buffer, "\n");
                    
                    ret = buffer;
                }
                else if (strcmp(token, "set") == 0)
                {
                    // Current actual temperature to the control station
                    strcpy(buffer, "");
                    strcat(buffer, Thermostat::GetTempSettingString());
                    strcat(buffer, "\n");
                    
                    ret = buffer;
                }
                else
                {
                    ret = "invalid\n";
                }
            }
            else
            {
                ret = "invalid\n";
            }
        }
        else if (strcmp(token, "post") == 0)
        {
            ret = "succeeded\n";
            token = strtok(NULL, " ");
            
            if (strcmp(token, "system") == 0)
            {
                token = strtok(NULL, " ");
                
                if (strcmp(token, "heat") == 0)
                {
                    if (Thermostat::GetSystemSetting() != HEAT){
                        Thermostat::SetSystemSetting(HEAT);
                        Thermostat::Update();
                    }
                    else
                    {
                        return "failed\n";
                    }
                }
                else if (strcmp(token, "cool") == 0)
                {
                    if (Thermostat::GetSystemSetting() != COOL){
                        Thermostat::SetSystemSetting(COOL);
                        Thermostat::Update();
                    }
                    else
                    {
                        return "failed\n";
                    }
                }
                else if (strcmp(token, "off") == 0)
                {
                    if (Thermostat::GetSystemSetting() != OFF){
                        Thermostat::SetSystemSetting(OFF);
                        Thermostat::Update();
                    }
                    else
                    {
                        return "failed\n";
                    }
                }
                else
                {
                    ret = "invalid\n";
                }
            }
            else if (strcmp(token, "fan") == 0)
            {
                token = strtok(NULL, " ");
                
                if (strcmp(token, "on") == 0)
                {
                    if (!Thermostat::GetFanSetting())
                    {
                        Thermostat::SetFanSetting(true);
                        Thermostat::Update();
                    }
                    else
                    {
                        ret = "failed\n";
                    }
                }
                else if (strcmp(token, "auto") == 0)
                {
                    if (Thermostat::GetFanSetting())
                    {
                        Thermostat::SetFanSetting(false);
                        Thermostat::Update();
                    }
                    else
                    {
                        ret = "failed\n";
                    }
                }
                else
                {
                    ret = "invalid\n";
                }
            }
            else if (strcmp(token, "set") == 0)
            {
                int temp = 0;
                
                token = strtok(NULL, " ");
                
                // Parse message for tempature
                temp = string_to_uint(token);
                
                // Check args
                if (temp >= 60 && temp <= 90 && temp != Thermostat::GetTempSetting())
                {
                    Thermostat::SetTempSetting(temp);
                }
                else
                {
                    ret = "failed\n";
                }
            }
            else
            {
                ret = "invalid\n";
            }
        }
        else
        {
            ret = "invalid\n";
        }
    }
    else
    {
        ret = "invalid\n";
    }
    
    return ret;
}

int main(void)
{
    // Initialize the board
    Tiva::InitClock();
    Debug::Init();
    Sys_Tick::Init();
    Console::Init(); // GPIO_A::Bits0-1
    NVIC_PRI1_R = (NVIC_PRI1_R&0xFFFF00FF)|0x00002000;  
    Thermostat::Init();
    
    // Initialize GPIO
    DisableInterrupts();
    
    // GPIO_C::Bits7-4 (CIRCUIT, COOLING, FANNING, HEATING respectively)
    GPIO_C::Init(GPIO_BITS_HI, DIR_OUTPUT, DEN_ENABLE);
    
    // GPIO_D::Bit3 (FANNING output pin for FAN device)
    GPIO_D::Init(GPIO_BIT_3, DIR_OUTPUT, DEN_ENABLE);
    
    // GPIO_E::Bits4-1 (FAN ON/FAN AUTO, HEAT/COOL, DOWN, UP respectively)
    GPIO_E::InitInterrupt(GPIO_BIT_1 | GPIO_BIT_2 | GPIO_BIT_3 | GPIO_BIT_4, IS_EDGE, IBE_SINGLE, IEV_LOW, &Thermostat::ButtonTask);
    NVIC_PRI1_R = (NVIC_PRI1_R&0xFFFFFF00)|0x00000020;
    
    //GPIO_F::InitInterrupt(GPIO_BIT_0 | GPIO_BIT_4, IS_EDGE, IBE_SINGLE, IEV_LOW, &Thermostat::ButtonTask);
    GPIO_A::Init(GPIO_BIT_4, DIR_OUTPUT, DEN_ENABLE); // Debug
    
    // Initialize ADC
    ADC0_InitTimer0ATriggerSeq3(5, 249, 50000);// GPIO_D::Bit2,               4 Hz sampling
    
    // Initialize Adafruit LCD screen
    ST7735_InitR(INITR_REDTAB); // NEEDS TO BE C++-ified // Uses GPIO_A::Bits2,3,5,6,7
    
    // Draw initial lines and characters onto LCD screen
    ST7735_DrawFastHLine(0, ((ST7735_TFTHEIGHT * 3) / 4) - 4, ST7735_TFTWIDTH, ST7735_BLACK);
    ST7735_DrawFastHLine(0, ((ST7735_TFTHEIGHT * 7) / 8) - 4, ST7735_TFTWIDTH, ST7735_BLACK);
    ST7735_DrawChar(104, 4, 'o', ST7735_BLACK, ST7735_WHITE, 2); // tempActual degres
    ST7735_DrawChar(122, 98, 'o', ST7735_BLACK, ST7735_WHITE, 1); // tempSetting degrees
    
    // Show settings on LCD screen
    Thermostat::SetFanSetting(false);
    Thermostat::SetSystemSetting(false);
    ST7735_Message(1, 0, "Temperature", 0);
    ST7735_Message(1, 1, "Setting", Thermostat::GetTempSetting());
    ST7735_Digit(0, 0, 6, 7, 34, 8, 1);
    ST7735_Digit(0, 0, 6, 5, 68, 8, 1);
    
    Thermostat::Update();
    
    EnableInterrupts();
    
    // Server takes care of SPI, etc. initialization
    Server::Init(IncomingMessageHandler, "VikBEE Brand Thermostat", "vikbee.thermostat.1.");
    
    // Run the server forever
    Server::Run();
}
