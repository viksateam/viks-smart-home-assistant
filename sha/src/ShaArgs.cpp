/*
* ShaArgs.cpp
*
* Author(s):
*      Duncan Horn
*
* Convenience class for parsing command-line options and setting variables to be used later.
* Currently, there are no command-line options.
*/

#include "ShaArgs.h"

using namespace vik;

ShaArgs::ShaArgs(_In_ int argc, _In_ char *argv[])
{
    // TODO
    (void)argc;
    (void)argv;
}

bool ShaArgs::IsFullScreen(void) const
{
    return this->_fullScreen;
}

bool ShaArgs::ShowDebugInfo(void) const
{
    return this->_debugInfo;
}
