/*
 * spi.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Implementation of the SSI interface that is to be used with the CC3000 in combination with the
 * Tiva C Series TM4C123G LaunchPad Evaluation Kit.
 */
#pragma once

#include "sha.h"

#include <hci.h>

typedef void (*SpiHandleRx)(void *p);
typedef void (*SpiHandleTx)(void);

extern unsigned char wlan_tx_buffer[];

class WlanSPI
{
private:
    /* Can't instantiate an instance of a WlanSPI object */
    WlanSPI(void) {}

public:

    static void Init(void);
    static void CleanGPIOISR(void);

    static void Open(SpiHandleRx pfnRxHandler);
    static void Close(void);
    static void Write(
        _Inout_updates_(usLength) unsigned char *pucUserBuffer,
        _In_ unsigned short usLength);
    static void Resume(void);
    static void OnGPIOInterrupt(void);
    static void OnSSIInterrupt(void);

    static long ReadWlanInterrupt(void);
    static void WlanInterruptEnable(void);
    static void WlanInterruptDisable(void);
    static void WriteWlan(_In_ unsigned char ucVal);

private:

    static void _SSIConfigure(
        _In_ unsigned long ulSSIFreq,
        _In_ unsigned long bForceGpioConfiguration,
        _In_ unsigned long ulSysClck);
    static void _FirstWrite(
        _In_reads_(usLength) unsigned char *pucBuff,
        _In_ unsigned short usLength);
    static void _WriteDataSynchronous(
        _In_reads_(usSize) const unsigned char *pucData,
        _In_ unsigned short usSize);
    static void _WriteAsync(
        _In_reads_(usSize) const unsigned char *pucData,
        _In_ unsigned short usSize);
    static void _FlushRxFifo(void);
    static void _ReadHeader(void);
    static void _ReadData(
        _In_reads_(usSize) const unsigned char *pucData,
        _In_ unsigned short usSize);
    static long _ContinueReadData(void);
    static void _ContinueReadOperation(void);
    static void _TriggerRxProcessing(void);
    static void _DisableInterrupts(void);
    static void _DisableSSIDMAChannels(void);
    static bool _IsDmaStop(_In_ long lChannel);
    static unsigned long _CheckDMAStatus(long lChannel);

    static volatile unsigned long s_ulState;
    static unsigned char *s_pucTxPacket;
    static unsigned short s_usTxPacketLength;
    static unsigned char *s_pucRxPacket;
    static unsigned short s_usRxPacketLength;

    static SpiHandleRx s_pfnRxHandler;
};
