/*
 * ShaColors.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Theme colors for the Smart Home Assistant
 */
#pragma once

#include "Colors.h"

namespace vik
{
    namespace graphics
    {
        class ShaColors
        {
        private:
            ShaColors(void) {}

        public:

            static const Color BackgroundColor;
            static const Color ForegroundColor;
            static const Color ThemeColor;

            static const Color PointerFocusPointColor;

            static const Color ButtonHoverColor;

            static const Color CoffeeBrown;
        };
    }
}
