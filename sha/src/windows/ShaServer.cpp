/*
 * ShaServer.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * The implementation of the server for the Smart Home Assistant
 */

#include <cassert>
#include <sstream>
#include <vector>

#include "ShaCommunicationManager.h"
#include "ShaServer.h"
#include "WindowHelpers.h"

using namespace vik;
using namespace vik::communication;

#define SERVER_RECEIVE_BUFFER_SIZE          (1024)

static DWORD WINAPI ServerThreadProc(_In_ LPVOID lpParam);
static DWORD WINAPI ClientThreadProc(_In_ LPVOID lpParam);
static DWORD WINAPI ConnectionThreadProc(_In_ LPVOID lpParam);
static DWORD WINAPI BroadcastThreadProc(_In_ LPVOID lpParam);

ServerSocket ShaServer::s_socket;
UDPSocket ShaServer::s_broadcastSocket;

HANDLE ShaServer::s_hServerThread;
HANDLE ShaServer::s_hBroadcastThread;

/* NOTE: You must call WSAStartup BEFORE calling any server functions */
bool ShaServer::Connect(void)
{
    // Can only connect once
    if (s_socket.IsRunning() && s_broadcastSocket.IsConnected())
    {
        return false;
    }

    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData))
    {
        return false;
    }

    return s_socket.Start(SHA_PORT) && s_broadcastSocket.Open(SHA_BROADCAST_PORT);
}

bool ShaServer::Start(void)
{
    if (!s_socket.IsRunning() || !s_broadcastSocket.IsConnected())
    {
        return false;
    }

    // Start the server thread so we can accept any broadcast responses. Note that we sleep for a
    // small amount of time in order to give the server thread time to make its accept call
    s_hServerThread = CreateThread(nullptr, 0, ServerThreadProc, &s_socket, 0, nullptr);

    // Send the connection message
    std::string connectionMessage = "connect " + ShaCommunicationManager::GetInfoString() +
        " -- reply\n";
    s_broadcastSocket.Broadcast(SHA_BROADCAST_PORT, connectionMessage.c_str(),
        connectionMessage.size());
    s_hBroadcastThread = CreateThread(nullptr, 0, BroadcastThreadProc, &s_broadcastSocket, 0, nullptr);

    return s_hServerThread != nullptr && s_hBroadcastThread != nullptr;
}

bool ShaServer::Close(void)
{
    // Stop the server thread. Wait for the thread to terminate, but don't force it to close as
    // doing so is highly discouraged
    s_socket.Close();
    WaitForSingleObject(s_hServerThread, 10);

    // Broadcast the disconnect message
    std::string disconnectMsg = "disconnect " + ShaCommunicationManager::StationId() + "\n";
    s_broadcastSocket.Broadcast(SHA_BROADCAST_PORT, disconnectMsg.c_str(), disconnectMsg.size());
    s_broadcastSocket.Close();
    WaitForSingleObject(s_hBroadcastThread, 10);

    CloseHandle(s_hServerThread);
    CloseHandle(s_hBroadcastThread);
    return !WSACleanup();
}



static DWORD WINAPI ServerThreadProc(_In_ LPVOID lpParam)
{
    ServerSocket *pSocket = (ServerSocket *)lpParam;

    while (pSocket->IsRunning())
    {
        Socket client = pSocket->Accept();
        if (client.IsConnected())
        {
            // Need newly allocated memory for the thread procedure
            Socket *pNewClient = new Socket(client);
            HANDLE hClient = CreateThread(nullptr, 0, ClientThreadProc, pNewClient, 0, nullptr);
            if (!hClient)
            {
                // Failed to create client thread
                pNewClient->Close();
                delete pNewClient;
            }
            else
            {
                // We don't hold references to previously created threads
                CloseHandle(hClient);
            }
        }
    }

    return 0;
}

static std::vector<std::string> GetMessagesFromConnection(
    _In_ Socket *pSock,
    _Inout_ std::stringstream &stream)
{
    std::vector<std::string> result;

    char rgszBuffer[SERVER_RECEIVE_BUFFER_SIZE];
    int nLen = pSock->Receive(rgszBuffer, sizeof(rgszBuffer)-1);

    if (nLen > 0)
    {
        rgszBuffer[nLen] = '\0';
        int nLines = std::count(rgszBuffer, rgszBuffer + nLen, SENTINEL_CHARACTER);
        stream << rgszBuffer;

        for (int i = 0; i < nLines; i++)
        {
            std::string line;
            std::getline(stream, line);
            result.push_back(line);
        }
    }
    else
    {
        // Error occurred, or the socket was closed on the other end
        pSock->Close();
    }

    return result;
}

static DWORD WINAPI ClientThreadProc(_In_ LPVOID lpParam)
{
    DWORD dwResult = 1;
    Socket *pSocket = (Socket *)lpParam;
    InetAddress addr = pSocket->GetInetAddr();

    static std::stringstream stream;
    while (pSocket->IsConnected())
    {
        std::vector<std::string> messages = GetMessagesFromConnection(pSocket, stream);

        for (auto &msg : messages)
        {
            dwResult = 0;
            auto response = ShaCommunicationManager::OnConnectionMessageReceived(msg, addr);
            if (!response.empty())
            {
                assert(response[response.size() - 1] == SENTINEL_CHARACTER);
                pSocket->Send(response.c_str(), response.size());
            }
        }
    }

    delete pSocket;
    return dwResult;
}



struct ConnectionData
{
    InetAddress    addr;
    std::string    message;
    bool           fWaitForResponse;
    request_cookie cookie;
};

static DWORD WINAPI ConnectionThreadProc(_In_ LPVOID lpParam)
{
    DWORD dwResult = 1;
    ConnectionData *pData = (ConnectionData *)lpParam;
    char *pszResponse = nullptr;

    // Message should not be empty (as we are the one contacting them). Additionally, all messages
    // should end with the sentinel character
    assert(!pData->message.empty());
    assert(pData->message[pData->message.size() - 1] == SENTINEL_CHARACTER);

    Socket socket(pData->addr);
    if (socket.IsConnected())
    {
        if (socket.Send(pData->message.c_str(), pData->message.size()) ==
            (int)pData->message.size())
        {
            dwResult = 0;
            if (pData->fWaitForResponse)
            {
                std::stringstream stream;
                std::vector<std::string> response = GetMessagesFromConnection(&socket, stream);

                if (!response.empty())
                {
                    pszResponse = new char[response[0].size() + 1];
                    strcpy_s(pszResponse, response[0].size() + 1, response[0].c_str());
                }
                else
                {
                    dwResult = 1;
                }
            }
        }
    }

    if (pData->fWaitForResponse)
    {
        ShaPostMessage(AM_STATION_RESPONSE, pData->cookie, (intptr_t)pszResponse);
    }

    socket.Close();
    delete pData;
    return dwResult;
}

bool ShaServer::SendSingleMessage(_In_ const InetAddress &addr, _In_ const std::string &message)
{
    ConnectionData *pData = new ConnectionData();
    pData->addr = addr;
    pData->message = message;
    pData->fWaitForResponse = false;
    pData->cookie = INVALID_REQUEST_COOKIE;

    HANDLE hConnection = CreateThread(nullptr, 0, ConnectionThreadProc, pData, 0, nullptr);
    if (!hConnection)
    {
        // Failed to create thread
        delete pData;
        return false;
    }

    return !!CloseHandle(hConnection);
}

bool ShaServer::StartConnection(
    _In_ const InetAddress &addr,
    _In_ const std::string &message,
    _In_ request_cookie cookie)
{
    ConnectionData *pData = new ConnectionData();
    pData->addr = addr;
    pData->message = message;
    pData->fWaitForResponse = true;
    pData->cookie = cookie;

    HANDLE hConnection = CreateThread(nullptr, 0, ConnectionThreadProc, pData, 0, nullptr);
    if (!hConnection)
    {
        // Failed to create thread
        delete pData;
        return false;
    }

    return !!CloseHandle(hConnection);
}



static std::stringstream broadcastStream;
static DWORD WINAPI BroadcastThreadProc(_In_ LPVOID lpParam)
{
    UDPSocket *pSocket = (UDPSocket *)lpParam;

    // Continuously listen for broadcast messages
    char rgszBuffer[SERVER_RECEIVE_BUFFER_SIZE + 1];
    while (pSocket->IsConnected())
    {
        InetAddress addr;
        int nLen = pSocket->Receive(rgszBuffer, sizeof(rgszBuffer)-1, &addr);

        if (nLen >= 0)
        {
            rgszBuffer[nLen] = '\0';
            int nLines = std::count(rgszBuffer, rgszBuffer + nLen, SENTINEL_CHARACTER);
            broadcastStream << rgszBuffer;

            for (int i = 0; i < nLines; i++)
            {
                std::string line;
                std::getline(broadcastStream, line);

                ShaCommunicationManager::OnBroadcastMessageReceived(line, addr);
            }
        }
        else
        {
            // Error occurred; close the socket
            pSocket->Close();
        }
    }

    return 0;
}
