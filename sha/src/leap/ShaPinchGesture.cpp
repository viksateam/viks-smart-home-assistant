/*
 * ShaPinchGesture.cpp
 *
 * Author(s):
 *      Jerry Lin
 *
 * Detects right-handed, thumb-to-pointer finger pinch and release motions. The other three fingers
 * must be tucked away and not shown.
 */

#include "ShaPinchGesture.h"

using namespace vik;
using namespace vik::leap;

/*
 * Constructors / Destructors
 */
ShaPinchGesture::ShaPinchGesture(void)
{
    this->_Reset();
}



/*
 * Public functions
 */
Vector ShaPinchGesture::GetPinchPosition(void) const
{
    return this->_lastPalmPosition + this->_palmToPinchPosition;
}

GestureType ShaPinchGesture::GetType(void) const
{
    return GT_PINCH;
}

bool ShaPinchGesture::IsPinched(void) const
{
    return (this->_state == GS_UPDATE && !this->_isReleased);
}

bool ShaPinchGesture::IsReleased(void) const
{
    return (this->_state == GS_UPDATE && this->_isReleased) || this->_state == GS_STOP;
}

void ShaPinchGesture::Update(_In_ const Frame &frame)
{
    const HandList &hands = frame.hands();
    if (!hands.isEmpty())
    {
        const Hand &hand = hands.frontmost();
        const FingerList &fingers = hand.fingers();

        //Don't conflict with swipes, when palm is facing left or right
        if (abs(hand.palmNormal().x) > abs(hand.palmNormal().z) &&
            abs(hand.palmNormal().x) > abs(hand.palmNormal().y))
        {
            this->_Reset();
        }
        else
        {
            switch (this->_state)
            {
            case GS_STOP:
            {
                // If there are only two fingers, they are moving toward each other in
                // successive frames, and they're not too separate in the z or y directions,
                // we start the PinchGesture.
                if (fingers.count() == 2)
                {
                    auto firstPos = fingers.leftmost().tipPosition();
                    auto secondPos = fingers[this->_GetSecondFingerIndex(fingers)].tipPosition();

                    float distance = (secondPos - firstPos).magnitude();
                    Vector pinchPos = this->_GetEstimatedPinchPosition(firstPos, secondPos);
                    Vector estimatedPinchPos = this->_GetLastPinchPosition(hand);

                    if (this->_lastDistance > distance && 
                        abs(secondPos.z - firstPos.z) - abs(secondPos.x - firstPos.x) < 5 &&
                        abs(secondPos.y - firstPos.y) - abs(secondPos.x - firstPos.x) < 5)
                    {
                        this->_state = GS_START;
                        this->_id = ShaGesture::CreateID();
                    }

                    this->_UpdateHandInformation(hand, pinchPos);
                    this->_lastDistance = distance;
                }
            }
                break;

            case GS_START:
            {
                if (fingers.count() == 2)
                {
                    auto firstPos = fingers.leftmost().tipPosition();
                    auto secondPos = fingers[this->_GetSecondFingerIndex(fingers)].tipPosition();

                    float distance = (secondPos - firstPos).magnitude();
                    Vector pinchPos = this->_GetEstimatedPinchPosition(firstPos, secondPos);
                    Vector lastPinchPos = this->_GetLastPinchPosition(hand);

                    if (this->_lastDistance > distance &&
                        abs(secondPos.z - firstPos.z) - abs(secondPos.x - firstPos.x) < 2 &&
                        abs(secondPos.y - firstPos.y) - abs(secondPos.x - firstPos.x) < 2)
                    {
                        this->_UpdateHandInformation(hand, pinchPos);
                        this->_lastDistance = distance;
                    }
                    else
                    {
                        this->_Reset();
                    }
                }
                else
                {
                    //Pinch: one or no fingers in hand
                    this->_UpdateHandInformation(hand, this->_GetLastPinchPosition(hand));
                    this->_lastDistance = 0;
                    this->_state = GS_UPDATE;
                }
            }
                break;

            case GS_UPDATE:
            {
                if (fingers.count() <= 1)
                {
                    this->_UpdateHandInformation(hand, this->_GetLastPinchPosition(hand));
                }
                else if (fingers.count() == 2)
                {
                    auto firstPos = fingers.leftmost().tipPosition();
                    auto secondPos = fingers[this->_GetSecondFingerIndex(fingers)].tipPosition();
                    float distance = (secondPos - firstPos).magnitude();

                    if (this->_lastDistance == 0)
                    {
                        this->_lastDistance = distance;
                    }
                    else if (distance > this->_lastDistance)
                    {
                        this->_lastDistance = distance;
                        this->_isReleased = true;
                    }
                    else
                    {
                        this->_Reset();
                    }
                }
                else
                {
                    this->_Reset();
                }
            }
                break;

            default:
                this->_Reset();
                break;
            } // switch (GestureState ...)
        } // if palmNormal not facing in the x direction
    } // if !handList.empty()
    else
    {
        this->_Reset();
    }
}



/*
 * Private helper functions
 */
Vector ShaPinchGesture::_GetLastPinchPosition(_In_ const Hand &hand) const
{
    return hand.palmPosition() + this->_palmToPinchPosition;
}

Vector ShaPinchGesture::_GetEstimatedPinchPosition(_In_ const Vector &pos1, _In_ const Vector &pos2) const
{
    return pos2;
    (void)pos1;
}

int ShaPinchGesture::_GetSecondFingerIndex(_In_ const FingerList &fingers) const
{
    int index = 0;
    float minX = fingers.rightmost().tipPosition().x;

    for (int i = 0; i < fingers.count(); i++)
    {
        const Finger &finger = fingers[i];
        if (finger != fingers.leftmost())
        {
            float x = finger.tipPosition().x;
            if (x <= minX)
            {
                minX = x;
                index = i;
            }
        }
    }

    return index;
}

void ShaPinchGesture::_UpdateHandInformation(
    _In_ const Hand &hand,
    _In_ const Vector &pinchPosition)
{
    this->_lastPalmPosition = hand.palmPosition();
    this->_palmToPinchPosition = pinchPosition - hand.palmPosition();
}

void ShaPinchGesture::_Reset(void)
{
    this->_lastDistance = -1;
    this->_lastPalmPosition = Vector::zero();
    this->_palmToPinchPosition = Vector::zero();
    this->_state = GS_STOP;
    this->_isReleased = false;
}
