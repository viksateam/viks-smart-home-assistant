/*
 * Layout.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents the base class for all layouts. A Layout consists of a collection of UIElement
 * objects. Each Layout derivative defines how elements within that Layout will get displayed on
 * screen. Each Layout derivative must define the AddChild function. Each derivative can, of
 * course, define its own behavior for the AddChild function, however the expected action is to
 * "add to the end" of the collection. The Layout is allowed to ignore the addition of the element
 * to the Layout, in which case the UIElement being added must be deallocated (and so, it is not
 * recommended that UIElements be added using the AddChild function).
 */
#pragma once

#include "AnimationEngine.h"
#include "LayoutAwareElement.h"

namespace vik
{
    namespace graphics
    {
        enum PointerState
        {
            PS_NORMAL,
            PS_PRESSED,
            PS_EXITED
        };

        class Layout :
            public LayoutAwareElement
        {
        public:
            Layout(void);
            Layout(_In_ GLfloat width, _In_ GLfloat height);
            Layout(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
            Layout(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            Layout(_In_ const Rect &rc);
            ~Layout(void);

            /* UIElement */
            virtual bool OnPointerEntered(_In_ Point pt);
            virtual bool OnPointerExited(void);
            virtual bool OnPointerMoved(_In_ Point pt);
            virtual bool OnPointerDown(_In_ Point pt);
            virtual bool OnPointerUp(_In_ Point pt);

            /* Animation Engines */
            virtual void SetInsertionAnimationEngine(
                _In_ animations::IInsertionAnimationEngine *eng);
            virtual void SetRepositionAnimationEngine(
                _In_ animations::IRepositionAnimationEngine *eng);
            virtual void SetGrowthAnimationEngine(_In_ animations::IGrowthAnimationEngine *eng);
            virtual void SetRemovalAnimationEngine(_In_ animations::IRemovalAnimationEngine *eng);

        protected:

            UIElement    *_pointerTarget;
            PointerState _pointerState;

            animations::IInsertionAnimationEngine  *_insertionAnimationEngine;
            animations::IRepositionAnimationEngine *_repositionAnimationEngine;
            animations::IGrowthAnimationEngine     *_growthAnimationEngine;
            animations::IRemovalAnimationEngine    *_removalAnimationEngine;
        };
    }
}
