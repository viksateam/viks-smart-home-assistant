/*
 * Colors.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines commonly used colors for the Smart Home Assistant. In general, you should use one of
 * these "theme" colors instead of defining your own in a separate file. Consult with all other
 * developers doing UI work before adding a color to this file as there might be a more suitable
 * existing color.
 */
#pragma once

#include "sha.h"

#include <GL/gl.h>

namespace vik
{
    namespace graphics
    {
        struct Color
        {
            union
            {
                GLfloat value[4];
                struct
                {
                    GLfloat r;
                    GLfloat g;
                    GLfloat b;
                    GLfloat a;
                };
            };

            operator const GLfloat *(void) const
            {
                return this->value;
            }
        };

        class Colors
        {
        private:
            Colors(void) {}

        public:

            /* Standard colors */
            static const Color Red;
            static const Color Green;
            static const Color Blue;
            static const Color Magenta;
            static const Color Cyan;
            static const Color Yellow;
            static const Color Orange;
            static const Color Brown;
            static const Color Black;
            static const Color White;
            static const Color Transparent;

            static inline Color FromRGBA(
                _In_ GLfloat r,
                _In_ GLfloat g,
                _In_ GLfloat b,
                _In_ GLfloat a)
            {
                Color color = { { { r, g, b, a } } };
                return color;
            }
        };

        /* Useful wrapper functions when a corresponding gl*4fv function does not exist */
        inline GLvoid ClearColor(_In_ const Color &color)
        {
            glClearColor(color.r, color.g, color.b, color.a);
        }
    }
}

