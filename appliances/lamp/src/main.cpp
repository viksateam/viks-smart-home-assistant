/*
 * main.cpp
 *
 * Author(s):
 *      Duncan Horn
 *      Peter Vu
 *
 * The main implementation for the coffee machine appliance
 */

#include "Console.h"
#include "Debug.h"
#include "GPIO.h"
#include "Server.h"
#include "sys_tick.h"
#include "tiva.h"

#include <string.h>

#define NVIC_PRI1_R             (*((volatile unsigned long *)0xE000E404))  // IRQ 4 to 7 Priority Register

bool fStatus = false; // true = ON, false = OFF

/*
// Hardcoded Message Handler
static char *IncomingMessageHandler(_Inout_ char *pszMessage)
{
    if (strcmp(pszMessage, "request get status") == 0)
    {
        if (fStatus)
        {
            return "on\n";
        }
        else
        {
            return "off\n";
        }
    }
    else if (strcmp(pszMessage, "request get info") == 0)
    {
        return "light\n";
    }
    else if (strcmp(pszMessage, "request post light on") == 0)
    {
        if (!fStatus)
        {
            GPIO_A::Set(GPIO_BIT_7);
            fStatus = true;
            return "succeeded\n";
        }
        else
        {
            return "failed\n";
        }
    }
    else if (strcmp(pszMessage, "request post light off") == 0)
    {
        if (fStatus)
        {
            GPIO_A::Clear(GPIO_BIT_7);
            fStatus = false;
            return "succeeded\n";
        }
        else
        {
            return "failed\n";
        }
    }
    else
    {
        return "invalid\n";
    }
}
*/

static char *IncomingMessageHandler(_Inout_ char *pszMessage)
{
    char* ret;
    char* token;
    
    Console::WriteLine(pszMessage);
    
    // Parse message for command
    token = strtok(pszMessage, " ");
    
    // Parse message
    if (strcmp(token, "request") == 0)
    {
        token = strtok(NULL, " ");
        
        if (strcmp(token, "get") == 0)
        {
            ret = "ack\n";
            token = strtok(NULL, " ");
            
            if (strcmp(token, "info") == 0)
            {
                ret = "light\n";
            }
            else if (strcmp(token, "status") ==0)
            {
                if (fStatus)
                {
                    ret = "on\n";
                }
                else
                {
                    ret = "off\n";
                }
            }
            else
            {
                ret = "invalid\n";
            }
        }
        else if (strcmp(token, "post") == 0)
        {
            ret = "succeeded\n";
            token = strtok(NULL, " ");
            
            // Parse command
            if (strcmp(token, "light") == 0)
            {
                token = strtok(NULL, " ");
                
                if (strcmp(token, "on") == 0)
                {
                    if (!fStatus) // off
                    {
                        GPIO_A::Set(GPIO_BIT_7);
                        fStatus = true;
                    }
                    else
                    {
                        ret = "failed\n";
                    }
                    
                }
                else if (strcmp(token, "off") == 0)
                {
                    if (fStatus) // on
                    {
                        GPIO_A::Clear(GPIO_BIT_7);
                        fStatus = false;
                    }
                    else
                    {
                        ret = "failed\n";
                    }
                }
                else
                {
                    ret = "invalid\n";
                }
            }
            else
            {
                ret = "invalid\n";
            }
        }
        else
        {
            ret = "invalid\n";
        }
    }
    else
    {
        ret = "invalid\n";
    }
    
    return ret;
}

int main(void)
{
    // Initialize the board
    Tiva::InitClock();
    Debug::Init();
    Sys_Tick::Init();
    Console::Init();
    NVIC_PRI1_R = (NVIC_PRI1_R&0xFFFF00FF)|0x00002000; // Set Console interrupt priority to 2
    
    GPIO_A::Init(GPIO_BIT_7, DIR_OUTPUT, DEN_ENABLE);
    
    // Server takes care of SPI, etc. initialization
    Server::Init(IncomingMessageHandler, "VikBEE Brand Lamp", "vikbee.light.1");

    // Run the server forever
    Server::Run();
}
