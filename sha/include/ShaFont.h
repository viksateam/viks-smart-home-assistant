/*
 * ShaFont.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * 
 */
#pragma once

#include "sha.h"

#include <GL/gl.h>

#define FONT_HEIGHT_RATIO       (9.0f / 16.0f)

namespace vik
{
    namespace graphics
    {
        struct ShaFont
        {
            GLsizei height;
            bool    bold;
            bool    italic;
            bool    underline;
#ifdef  WIN32
            LPCTSTR fontName;
            GLuint  listBase;

            HFONT   hFont;
#else

#endif  /* WIN32 */
        };

        class Fonts
        {
        private:
            Fonts(void) {}

        public:

            static ShaFont TitleFont;
            static ShaFont ApplianceFont;
            static ShaFont NotificationFont;
            static ShaFont LargeFont;
            static ShaFont ArtistFont;
        };
    }
}
