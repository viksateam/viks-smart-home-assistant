/*
 * Common.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Common functions and defines. These are generally functions or definitions that are operating
 * system or compiler specific, so it is recommended that every header file include this one. For
 * example, mock SAL annotations are defined here for building outside of Visual Studio.
 */
#pragma once

#ifdef  WIN32
#define NOMINMAX

#include <crtdbg.h>
#include <WinSock2.h>
#include <Windows.h>
#else
#include "MockSal.h"
#endif  /* WIN32 */
