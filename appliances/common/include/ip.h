/*
 * ip.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Useful functions for dealing with ip addresses
 */
#pragma once

#include "sha.h"

#include <stdint.h>

/* Convert 4 bytes to an ip address in host-byte-order */
#define MAKE_IP(b0, b1, b2, b3)     ((b0 << 24) | (b1 << 16) | (b2 << 8) | (b3))



/* Converts an ipv4 address in standard dot-decimal notation into its integer representation in
   host-byte-order */
uint32_t string_to_ip(_In_z_ const char *pszAddr);

/* Converts an ip address stored in host-byte-order to a standard dot-decimal ipv4 string. Returns
   a pointer to the terminated null character */
char *ip_to_string(_In_ uint32_t uIpAddr, _Out_writes_z_(return - pszAddr) char *pszAddr);

uint32_t string_to_uint(_In_z_ const char *psz);
