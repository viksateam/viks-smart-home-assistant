/*
 * ButtonBase.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * A useful base class for button-like UIElements.
 */
#pragma once

#include "LayoutAwareElement.h"

namespace vik
{
    namespace graphics
    {
        class ButtonBase :
            public LayoutAwareElement
        {
        public:
            ButtonBase(void);
            ButtonBase(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
            ButtonBase(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            ButtonBase(_In_ const Rect &rc);

            /* UIElement */
            virtual bool OnPointerEntered(_In_ Point pt);
            virtual bool OnPointerExited(void);
            virtual bool OnPointerDown(_In_ Point pt);
            virtual bool OnPointerUp(_In_ Point pt);

            typedef bool (UIElement::*ButtonEventCallback)(ButtonBase *sender, const Point &pt);
            virtual void OnButtonEntered(_In_ UIElement *target, _In_ ButtonEventCallback callback);
            virtual void OnButtonExited(_In_ UIElement *target, _In_ ButtonEventCallback callback);
            virtual void OnButtonDown(_In_ UIElement *target, _In_ ButtonEventCallback callback);
            virtual void OnButtonUp(_In_ UIElement *target, _In_ ButtonEventCallback callback);

        protected:

            ButtonEventCallback _buttonEnteredCallback;
            ButtonEventCallback _buttonExitedCallback;
            ButtonEventCallback _buttonDownCallback;
            ButtonEventCallback _buttonUpCallback;

            UIElement *_buttonEnteredTarget;
            UIElement *_buttonExitedTarget;
            UIElement *_buttonDownTarget;
            UIElement *_buttonUpTarget;
        };
    }
}
