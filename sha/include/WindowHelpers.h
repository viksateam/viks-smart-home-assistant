/*
 * WindowHelpers.h
 *
 * Author(s):
 *      Duncan Horn
 *      Jerry Lin
 *
 * The information that is required by the operating system in order to create a window. Currently,
 * only Windows and Linux (Ubuntu) are supported. Add as needed. The worker function, InitWindow,
 * is defined in either windows/WindowInfo.cpp or linux/WindowInfo.cpp. You should compile and link
 * with the appropriate file depending on the operating system you are running.
 */
#pragma once

#include "sha.h"

#include <cstdint>
#include <GL/gl.h>

#include "Colors.h"
#include "ShaArgs.h"
#include "ShaFont.h"

#include "iTunesCOMInterface.h"
#include <wrl\client.h>

#ifndef WIN32
#define MAKELPARAM(x, y)            ((((x) & 0x0000ffff) << 16) | ((y) & 0x0000ffff))
#endif  /* WIN32 */

#define MAKE_APP_MESSAGE(x)         (WM_APP | x)

/* Leap Events */
#define AM_POINTER_MOVED            MAKE_APP_MESSAGE(0x0001)
#define AM_POINTER_EXITED           MAKE_APP_MESSAGE(0x0002)
#define AM_POINTER_PRESSED          MAKE_APP_MESSAGE(0x0003)
#define AM_POINTER_RELEASED         MAKE_APP_MESSAGE(0x0004)
#define AM_POINTER_TAPPED           MAKE_APP_MESSAGE(0x0005)
#define AM_PINCH_GESTURE            MAKE_APP_MESSAGE(0x0010)
#define AM_PINCH_RELEASE_GESTURE    MAKE_APP_MESSAGE(0x0011)
#define AM_SWIPE_GESTURE            MAKE_APP_MESSAGE(0x0012)

/* Communication Events */
#define AM_STATION_CONNECTED        MAKE_APP_MESSAGE(0x0100)
#define AM_STATION_RESPONSE         MAKE_APP_MESSAGE(0x0101)
#define AM_STATION_DISCONNECTED     MAKE_APP_MESSAGE(0x0110)

/* iTunes Events */
#define AM_ITUNES_STARTED           MAKE_APP_MESSAGE(0x0200)

namespace vik
{
    struct WindowInfo
    {
        /* Window location information */
        int x;
        int y;
        int width;
        int height;

#ifdef  WIN32
        LPCTSTR     className;
        LPCTSTR     windowName;
        HINSTANCE   hInstance;
        int         cmdShow;
#else
        const char *windowName;
#endif  /* WIN32 */
    };

    bool   InitWindow(_In_ const WindowInfo &info, _In_ const ShaArgs &args);
    int    MessagePump(void);

    bool    GetCharacterWidths(_In_ const graphics::ShaFont &font, _Out_writes_(256) int *pBuffer);
    GLfloat GetStringWidth(_In_ const graphics::ShaFont &font, _In_ const char *str);
    GLvoid  WindowDrawText(
          _In_ GLfloat x,
          _In_ GLfloat y,
          _In_z_ const char *text,
          _In_ const graphics::ShaFont &font,
          _In_ const graphics::Color &color);
    bool    ShaPostMessage(_In_ unsigned int msg, _In_ uintptr_t wParam, _In_ intptr_t lParam);
}
