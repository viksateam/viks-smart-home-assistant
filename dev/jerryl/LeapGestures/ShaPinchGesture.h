/*
* ShaPinchGesture.h
*
* Author(s):
*      Jerry Lin
*
* Detects right-handed, thumb-to-pointer finger pinch and release motions. The other three fingers
* must be tucked away and not shown.
*/

#include "ShaGesture.h"

#define DEFAULT_PINCH_THRESHOLD     35
#define DEFAULT_RELEASE_THRESHOLD   50

namespace sha
{
    namespace leap
    {
        class ShaPinchGesture final :
            public ShaGesture
        {
        public:
            ShaPinchGesture(void);

            Vector              GetPinchLocation(void) const;
            virtual GestureType GetType(void) const;
            bool                IsPinched(void) const;
            bool                IsReleased(void) const;
            virtual void        Update(_In_ const Frame &frame);

        private:

            Vector _GetEstimatedPinchPosition(_In_ const Vector &pos1, _In_ const Vector &pos2) const;
            Vector _GetLastPinchPosition(_In_ const Hand &hand) const;
            int    _GetSecondFingerIndex(_In_ const FingerList &fingers) const;
            bool   _IsPinchGestureStarting(_In_ const Hand &hand) const;
            void   _UpdateHandInformation(
                _In_ const Hand &hand,
                _In_ const Vector &pinchPosition);
            void   _Reset(void);

            bool   _isReleased;
            float  _lastDistance;
            float  _pinchThreshold;
            float  _releaseThreshold;
            Vector _lastPalmPosition;
            Vector _palmToPinchPosition;
        };
    }
}
