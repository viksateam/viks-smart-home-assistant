/*
 * RotaryLayout.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a rotary layout that hosts UIElements in a linear fashion. At most three elements are
 * shown on screen at one time: the current element on top in the center, the previous off to the
 * left, and the next off to the right. The layout looks (roughly) as follows:
 *
 *                      -------------------------
 *                      |                       |
 *                 -----+--                   --+-----
 *                 |    | |                   | |    |
 *                 |    | |                   | |    |
 *                 -----+--                   --+-----
 *                      |                       |
 *                      -------------------------
 *
 */
#pragma once

#include "Layout.h"
#include "Rectangle.h"

namespace vik
{
    namespace graphics
    {
        class RotaryLayout final :
            public Layout
        {
        public:
            RotaryLayout(void);
            RotaryLayout(_In_ GLfloat width, _In_ GLfloat height);
            RotaryLayout(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
            RotaryLayout(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            RotaryLayout(_In_ const Rect &rc);
            virtual ~RotaryLayout(void);

            /* Disable copying */
            RotaryLayout(_In_ const RotaryLayout &other) = delete;
            RotaryLayout &operator=(_In_ const RotaryLayout &other) = delete;

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual void   SetForegroundColor(_In_ const Color &color);
            virtual bool   OnPointerMoved(_In_ Point pt);

            /* Layout */
            virtual void AddChild(_In_ UIElement *elem);

            /* RotaryLayout */
            void AddChild(_In_ UIElement *elem, _In_ int index);
            bool RemoveChild(_In_ const UIElement *layout);
            bool RemoveChild(_In_ int index);
            bool AdvanceLeft(void);
            bool AdvanceRight(void);

        private:

            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            bool       _ElementVisible(_In_ int index) const;
            const Rect &_GetRectForIndex(_In_ int index) const;
            void       _UpdateLayoutForIndex(_In_ int index);
            void       _AnimateChildToPosition(_In_ int index);
            void       _AnimateChildForInsertion(_In_ int index);
            void       _UpdatePageIndicator(void);
            void       _OnChildRemoved(_In_ int index);

            std::vector<UIElement *> _children;
            int                      _currentIndex;
            shapes::Rectangle        _pageIndicator;

            Rect _leftRect;
            Rect _rightRect;
            Rect _centerRect;
        };
    }
}
