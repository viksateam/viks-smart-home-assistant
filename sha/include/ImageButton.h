/*
 * ImageButton.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a button with an image and a label
 */
#pragma once

#include "ButtonBase.h"
#include "Polygon.h"
#include "Rectangle.h"

namespace vik
{
    namespace graphics
    {
        class ImageButton final :
            public ButtonBase
        {
            enum ButtonState
            {
                BS_NORMAL,
                BS_HOVER,
                BS_PRESSED,
                BS_EXITED
            };

        public:
            ImageButton(void);
            ImageButton(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
            ImageButton(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            ImageButton(_In_ const Rect &rc);

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual bool   OnPointerEntered(_In_ Point pt);
            virtual bool   OnPointerExited(void);
            virtual bool   OnPointerDown(_In_ Point pt);
            virtual bool   OnPointerUp(_In_ Point pt);
            virtual void   SetForegroundColor(_In_ const Color &color);

            /* Button Properties */
            void SetText(_In_ const std::string &text);
            void SetLabelColor(_In_ const Color &color);
            void SetThickness(_In_ const GLfloat thickness);

            /* Image */
            void AddPoint(_In_ const Point &pt);
            template <typename _It>
            void AddPoints(_In_ _It begin, _In_ _It end)
            {
                this->_image.AddPoints(begin, end);
            }

        private:

            /* LayoutAwareElement */
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            void _UpdateState(_In_ ButtonState state);
            void _RefreshLayout(void);

            shapes::Rectangle _bkgnd;
            shapes::Polygon   _image;
            std::string       _text;
            GLfloat           _textWidth;
            Color             _labelColor;
            Color             _textColor;

            ButtonState _state;
        };
    }
}
