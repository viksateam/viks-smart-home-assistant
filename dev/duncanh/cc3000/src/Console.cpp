/*
 * Console.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Implements a simple Console interface for the Tiva C Series TM4C123G LaunchPad Evaluation Kit
 */

#include <ctype.h>

#include <driverlib/gpio.h>
#include <driverlib/interrupt.h>
#include <driverlib/rom_map.h>
#include <driverlib/sysctl.h>
#include <driverlib/uart.h>
#include <inc/hw_ints.h>
#include <inc/hw_memmap.h>

#include "Console.h"

#define ESC         (0x1B)

volatile char Console::s_rgszBuffer[CONSOLE_BUFFER_SIZE];
volatile int  Console::s_nStartOfBuffer = 0;
volatile int  Console::s_nEndOfBuffer = 0;
volatile int  Console::s_nSeekIndex = 0;

bool Console::s_fEnabled = true;
bool Console::s_fTrace = false;

void Console::Init(void)
{
    // Enable corresponding peripherals modules
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    // Set GPIO A0 and A1 as UART pins
    GPIOPadConfigSet(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1, GPIO_STRENGTH_2MA,
        GPIO_PIN_TYPE_STD_WPU);
    GPIOPinConfigure(0x00000001); //GPIO_PA0_U0RX
    GPIOPinConfigure(0x00000401); //GPIO_PA1_U0TX
    MAP_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Configure the UART for 9,600, 8-N-1 operation
    MAP_UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
    MAP_UARTIntDisable(UART0_BASE, 0xFFFFFFFF);

    // Set both the TX and RX trigger thresholds to 4. This will be used by the uDMA controller to
    // signal when more data should be transferred. The uDMA TX and RX channels will be configured
    // so that it can transfer 2 bytes in a burst when the UART is ready to transfer more data.
    UARTFIFOLevelSet(UART0_BASE, UART_FIFO_TX1_8, UART_FIFO_RX1_8);

    // Enable the UART interrupt but first clear all the pending interrupts
    MAP_UARTIntClear(UART0_BASE, 0xFFFFFFFF);
    MAP_UARTIntEnable(UART0_BASE, UART_INT_RT | UART_INT_RX);
    MAP_IntEnable(INT_UART0);

    SysCtlDelay(1000000);

    // Buffer must always be null-terminated
    s_rgszBuffer[0] = '\0';
}

void Console::Enable(void)
{
    s_fEnabled = true;
}

void Console::Disable(void)
{
    s_fEnabled = false;
}

void Console::StartTrace(void)
{
    s_fTrace = true;
}

void Console::EndTrace(void)
{
    s_fTrace = false;
}

/* Determines if there is a next word, terminated by a non-letter */
bool Console::HasNext(void)
{
    // Cache start and end value to help avoid race conditions
    int nOldStart = s_nStartOfBuffer;
    int nOldEnd = s_nEndOfBuffer;
    int i;

    // Need to first move past all non-letters at the beginning
    for (i = nOldStart; i != nOldEnd; i = _NextIndex(i))
    {
        if (isalpha(s_rgszBuffer[i]))
        {
            break;
        }
    }

    // Now find a non-letter
    for (; i != nOldEnd; i = _NextIndex(i))
    {
        if (!isalpha(s_rgszBuffer[i]))
        {
            return true;
        }
    }

    return false;
}

/* Determines if there is a next line, terminated by a new-line character */
bool Console::HasNextLine(void)
{
    // Cache start and end value to help avoid race conditions
    int nOldStart = s_nStartOfBuffer;
    int nOldEnd = s_nEndOfBuffer;

    for (int i = nOldStart; i != nOldEnd; i = _NextIndex(i))
    {
        if (s_rgszBuffer[i] == '\n')
        {
            return true;
        }
    }

    return false;
}

void Console::OnInput(_In_ int32_t nKey)
{
    // Ignore key if disabled
    if (!s_fEnabled)
    {
        return;
    }

    if ((nKey < ' ') || (nKey == 0x7F))
    {
        // Special character
        switch (nKey)
        {
        case '\r':
            // Return (i.e. enter)
            _OnReturnPressed();
            break;

        case '\b':
        case 0x7F:
            // Backspace
            _OnBackspacePressed();
            break;

        case ESC:
            // Escape sequence
            _OnEscapeSequence();
            break;
        }
    }
    else
    {
        // Non-special character
        _OnNormalKeyPressed(nKey);
    }
}



/*
 * Input Functions; all of them block if the data is not yet available
 */
void Console::Read(_Out_ char *pszBuff)
{
    // May need to wait for input at the very beginning
    while (!HasNext())
    {
        // TODO: possibly wait for interrupt
    }

    while (!isalpha(s_rgszBuffer[s_nStartOfBuffer]))
    {
        s_nStartOfBuffer = _NextIndex(s_nStartOfBuffer);
    }

    for (; isalpha(s_rgszBuffer[s_nStartOfBuffer]);
        s_nStartOfBuffer = _NextIndex(s_nStartOfBuffer))
    {
        *pszBuff = s_rgszBuffer[s_nStartOfBuffer];
        pszBuff++;
    }

    *pszBuff = '\0';
}

void Console::ReadLine(_Out_ char *pszBuff)
{
    // May need to wait for input at the very beginning
    while (!HasNextLine())
    {
        // TODO: possibly wait for interrupt
    }

    for (; s_rgszBuffer[s_nStartOfBuffer] != '\n'; s_nStartOfBuffer = _NextIndex(s_nStartOfBuffer))
    {
        *pszBuff = s_rgszBuffer[s_nStartOfBuffer];
        pszBuff++;
    }

    // Move one past the last new-line character and null-terminate the buffer
    *pszBuff = '\0';
    s_nStartOfBuffer = _NextIndex(s_nStartOfBuffer);
}



/*
 * Output Functions
 */
void Console::WriteByte(_In_ char ch)
{
    MAP_UARTCharPut(UART0_BASE, ch);
}

void Console::Write(_In_z_ const char *psz)
{
    while (*psz)
    {
        WriteByte(*psz);
        psz++;
    }
}

void Console::Write(_In_ uint32_t uVal)
{
    uint32_t uLen = 0;
    char rgcBytes[10];

    // Special case for when the value is zero
    if (uVal == 0)
    {
        WriteByte('0');
        return;
    }

    for (; uVal; uLen++)
    {
        rgcBytes[uLen] = uVal % 10;
        uVal /= 10;
    }

    for (; uLen; uLen--)
    {
        WriteByte(rgcBytes[uLen - 1] + '0');
    }
}

void Console::WriteBuffer(_In_reads_(uLen) char *psz, _In_ uint32_t uLen)
{
    for (uint32_t i = 0; i < uLen; i++)
    {
        WriteByte(psz[i]);
    }
}

void Console::WriteLine(_In_z_ const char *psz)
{
    Write(psz);
    WriteByte('\r');
    WriteByte('\n');
}

void Console::WriteLine(_In_ uint32_t uVal)
{
    Write(uVal);
    WriteByte('\r');
    WriteByte('\n');
}

void Console::Trace(_In_z_ const char *psz)
{
    if (s_fTrace)
    {
        Write(psz);
    }
}

void Console::TraceLine(_In_z_ const char *psz)
{
    if (s_fTrace)
    {
        WriteLine(psz);
    }
}



/*
 * Private Functions
 */
void Console::_OnNormalKeyPressed(_In_ int32_t nKey)
{
    // Make sure the buffer has room. If not, just ignore the key press
    if ((s_nStartOfBuffer == _NextIndex(s_nEndOfBuffer)) ||
        (s_nStartOfBuffer == _NextIndex(s_nEndOfBuffer + 1)))
    {
        return;
    }

    // Otherwise, save the character in the buffer and echo it to the console. The first step that
    // we need to take here is every character after s_nSeekIndex needs to shift over one location
    // and get printed to the console again.
    if (s_nSeekIndex != s_nEndOfBuffer)
    {
        // We need to copy from right to left, so go ahead and copy the characters before we seek
        for (int i = s_nEndOfBuffer; i != s_nSeekIndex; i = _PrevIndex(i))
        {
            s_rgszBuffer[i] = s_rgszBuffer[_PrevIndex(i)];
        }

        // Echo the previously typed characters (which will seek at the same time)
        _Seek(SEEK_DIR_RIGHT);
        for (int i = s_nSeekIndex; i != s_nEndOfBuffer; )
        {
            WriteByte(s_rgszBuffer[i = _NextIndex(i)]);
        }

        // Need to seek back to the start
        _Seek(SEEK_DIR_LEFT);
        for (int i = s_nSeekIndex; i != s_nEndOfBuffer; i = _NextIndex(i))
        {
            _Seek(SEEK_DIR_LEFT);
        }
    }

    // We are now ready to add the key to the input buffer (and echo the key to the screen)
    s_rgszBuffer[s_nSeekIndex] = nKey;
    WriteByte(nKey);

    s_nEndOfBuffer = _NextIndex(s_nEndOfBuffer);
    s_nSeekIndex = _NextIndex(s_nSeekIndex);

    // Always null terminate
    s_rgszBuffer[s_nEndOfBuffer] = '\0';
}

void Console::_OnReturnPressed(void)
{
    // We always save one character at the end of the buffer for the final return. However, this
    // does not guarantee an availible spot (as the previous key may have been return)
    if (s_nStartOfBuffer == _NextIndex(s_nEndOfBuffer))
    {
        return;
    }

    // Need to seek to the very end and add a new line character. Even though we only add '\n' to
    // the end of the string, we need to output both '\r' and '\n' to get the full "new line"
    // effect
    WriteLine("");

    // Don't null terminate as we may overflow the buffer (there's one guaranteed spot)
    s_rgszBuffer[s_nEndOfBuffer] = '\n';

    s_nEndOfBuffer = _NextIndex(s_nEndOfBuffer);
    s_nSeekIndex = s_nEndOfBuffer;
}

void Console::_OnBackspacePressed(void)
{
    // Disregard if seek index is at the start of the buffer, or if the previous character was a
    // new line
    if (s_nSeekIndex == s_nStartOfBuffer || s_rgszBuffer[_PrevIndex(s_nSeekIndex)] == '\n')
    {
        return;
    }

    _Seek(SEEK_DIR_LEFT);
    for (int i = s_nSeekIndex; i != s_nEndOfBuffer; i = _NextIndex(i))
    {
        s_rgszBuffer[_PrevIndex(i)] = s_rgszBuffer[i];
        WriteByte(s_rgszBuffer[i]);
    }

    // Need to clear the last character, so just write a space in its place
    WriteByte(' ');

    // Seek back to the correct location
    _Seek(SEEK_DIR_LEFT);
    for (int i = s_nSeekIndex; i != s_nEndOfBuffer; i = _NextIndex(i))
    {
        _Seek(SEEK_DIR_LEFT);
    }

    s_nSeekIndex--;
    s_nEndOfBuffer--;
}

void Console::_OnEscapeSequence(void)
{
    // First, check to see if this is indeed an escape sequence, or if it is just the escape key
    // getting pressed
    int32_t nEscapeType = UARTCharGetNonBlocking(UART0_BASE);

    // All escape sequences have '[' as the next character
    if (nEscapeType == '[')
    {
        int32_t nType = UARTCharGet(UART0_BASE);

        switch (nType)
        {
        case SEEK_DIR_UP:
        case SEEK_DIR_DOWN:
            // Do nothing on up/down
            break;

        case SEEK_DIR_RIGHT:
            // Only seek if seek index is less than end index
            if (s_nSeekIndex != s_nEndOfBuffer)
            {
                s_nSeekIndex++;
                _Seek(SEEK_DIR_RIGHT);
            }
            break;

        case SEEK_DIR_LEFT:
            // Only seek if (1) seek index is greater than the start of the buffer, and (2) the
            // character right before the seek index is not a new line character
            if ((s_nSeekIndex != s_nStartOfBuffer) &&
                (s_rgszBuffer[_PrevIndex(s_nSeekIndex)] != '\n'))
            {
                s_nSeekIndex--;
                _Seek(SEEK_DIR_LEFT);
            }
            break;
        }
    }
}

inline void Console::_Seek(_In_ SEEK_DIR dir)
{
    char szEscapeSequence[] = { ESC, '[', dir, '\0' };
    Write(szEscapeSequence);
}

inline int Console::_NextIndex(_In_ int nIndex)
{
    return (nIndex + 1) % CONSOLE_BUFFER_SIZE;
}

inline int Console::_PrevIndex(_In_ int nIndex)
{
    return (nIndex + CONSOLE_BUFFER_SIZE - 1) % CONSOLE_BUFFER_SIZE;
}

/* Blocks while data is not yet available */
inline void Console::_Block(void)
{
    while (s_nStartOfBuffer == s_nEndOfBuffer)
    {
        // TODO: possibly wait until interrupt
    }
}



/* UART0 Handler for console I/O */
#ifdef  __cplusplus
extern "C"
{
#endif
void UART0_Handler(void)
{
    if (UARTIntStatus(UART0_BASE, true))
    {
        int32_t nChar = UARTCharGet(UART0_BASE);
        Console::OnInput(nChar);
    }
}
#ifdef  __cplusplus
}
#endif
