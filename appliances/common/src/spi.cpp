/*
 * wlan_spi.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 *
 */

#include <driverlib/gpio.h>
#include <driverlib/interrupt.h>
#include <driverlib/pin_map.h>
#include <driverlib/rom_map.h>
#include <driverlib/ssi.h>
#include <driverlib/sysctl.h>
#include <driverlib/udma.h>
#include <inc/hw_ints.h>
#include <inc/hw_memmap.h>
#include <inc/hw_ssi.h>
#include <inc/hw_types.h>

#include <cc3000_common.h>

#include "Bitwise.h"
#include "spi.h"

#define SPI_BASE                            SSI2_BASE
#define SPI_CLK_PIN                         GPIO_PIN_4
#define SPI_RX_PIN                          GPIO_PIN_6
#define SPI_TX_PIN                          GPIO_PIN_7

#define SYSCTL_PERIPH_SPI                   SYSCTL_PERIPH_SSI2
#define SYSCTL_PERIPH_SPI_BASE              SYSCTL_PERIPH_GPIOB

#define SPI_PORT                            GPIO_PORTB_BASE
#define SPI_CLK_MUX_SEL                     GPIO_PB4_SSI2CLK
#define SPI_RX_MUX_SEL                      GPIO_PB6_SSI2RX
#define SPI_TX_MUX_SEL                      GPIO_PB7_SSI2TX

#define SPI_UDMA_RX_CHANNEL                 UDMA_CH12_SSI2RX
#define SPI_UDMA_TX_CHANNEL                 UDMA_CH13_SSI2TX

#define FWT_DELAY                           4000
#define DMA_WINDOW_SIZE                     1024

#define SPI_WINDOW_SIZE                     DMA_WINDOW_SIZE
#define DMA_CHANNEL_CONTROL_STRUCTURE_SIZE  (512)
#define READ                                (3)
#define WRITE                               (1)

//IRQ settings PB2
#define SYSCTL_PERIPH_IRQ_PORT              SYSCTL_PERIPH_GPIOB
#define SPI_GPIO_IRQ_BASE                   GPIO_PORTB_BASE
#define INT_GPIO_SPI                        INT_GPIOB
#define INT_SPI                             INT_SSI2

//CS settings  PE0
#define SPI_CS_PORT                         GPIO_PORTE_BASE
#define SPI_CS_PIN                          GPIO_PIN_0
#define SYSCTL_PERIPH_SPI_PORT              SYSCTL_PERIPH_GPIOE

// SPI IRQ and WLAN EN (PB5) PIN defines
#define SPI_IRQ_PIN                         GPIO_PIN_2
#define SPI_EN_PIN                          GPIO_PIN_5

#define SPI_HEADER_SIZE                     (5)

#define SPI_STATE_POWERUP                   (0)
#define SPI_STATE_INITIALIZED               (1)
#define SPI_STATE_IDLE                      (2)
#define SPI_STATE_WRITE_IRQ                 (3)
#define SPI_STATE_WRITE_FIRST_PORTION       (4)
#define SPI_STATE_WRITE_EOT                 (5)
#define SPI_STATE_READ_IRQ                  (6)
#define SPI_STATE_READ_FIRST_PORTION        (7)
#define SPI_STATE_READ_EOT                  (8)

#define CC3000_BUFFER_MAGIC_NUMBER          (0xDE)

#define ASSERT_CS()                         (MAP_GPIOPinWrite(SPI_CS_PORT, SPI_CS_PIN, 0))
#define DEASSERT_CS()                       (MAP_GPIOPinWrite(SPI_CS_PORT, SPI_CS_PIN, 0xFF))

#define HEADERS_SIZE_EVNT                   (SPI_HEADER_SIZE + 5)



unsigned char rgucSpiReadHeader[] = { READ, 0, 0, 0, 0 };

volatile unsigned long WlanSPI::s_ulState = SPI_STATE_POWERUP;
unsigned char *WlanSPI::s_pucTxPacket = NULL;
unsigned short WlanSPI::s_usTxPacketLength = 0;
unsigned char *WlanSPI::s_pucRxPacket = NULL;
unsigned short WlanSPI::s_usRxPacketLength = 0;

SpiHandleRx WlanSPI::s_pfnRxHandler = NULL;

/*
 * __no_init is used to prevent the buffer initialization in order to prevent hardware WDT
 * expiration before entering to 'main()'. For every IDE, different syntax exists :
 *      1.  __CCS__ for CCS v5
 *      2.  __IAR_SYSTEMS_ICC__ for IAR Embedded Workbench
 *      3.  __arm__ for Keil
 */
#ifdef __CCS__

unsigned char wlan_rx_buffer[CC3000_RX_BUFFER_SIZE];
unsigned char wlan_tx_buffer[CC3000_TX_BUFFER_SIZE];
unsigned char chBuffer[CC3000_RX_BUFFER_SIZE];
#pragma DATA_ALIGN(ucDMAChannelControlStructure, 1024);
static unsigned char ucDMAChannelControlStructure[DMA_CHANNEL_CONTROL_STRUCTURE_SIZE];

#elif __IAR_SYSTEMS_ICC__

__no_init unsigned char wlan_rx_buffer[CC3000_RX_BUFFER_SIZE];
__no_init unsigned char wlan_tx_buffer[CC3000_TX_BUFFER_SIZE];
__no_init unsigned char chBuffer[CC3000_RX_BUFFER_SIZE];
#pragma data_alignment=1024
__no_init static unsigned char ucDMAChannelControlStructure[DMA_CHANNEL_CONTROL_STRUCTURE_SIZE];

#elif __arm__

__attribute__((zero_init))
unsigned char wlan_rx_buffer[CC3000_RX_BUFFER_SIZE];
__attribute__((zero_init))
unsigned char wlan_tx_buffer[CC3000_TX_BUFFER_SIZE];
__attribute__((zero_init))
unsigned char chBuffer[CC3000_RX_BUFFER_SIZE];
__attribute__((zero_init))
static unsigned char
ucDMAChannelControlStructure[DMA_CHANNEL_CONTROL_STRUCTURE_SIZE] __attribute__((aligned(1024)));

#elif WIN32

unsigned char wlan_rx_buffer[CC3000_RX_BUFFER_SIZE];
unsigned char wlan_tx_buffer[CC3000_TX_BUFFER_SIZE];
unsigned char chBuffer[CC3000_RX_BUFFER_SIZE];
static unsigned char ucDMAChannelControlStructure[DMA_CHANNEL_CONTROL_STRUCTURE_SIZE];

#else

#error Compiler not supported

#endif



void WlanSPI::Init(void)
{
    // Configure the system peripheral bus that IRQ & EN pin are map to
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_IRQ_PORT);

    // Disable all the interrupts before configuring the lines
    MAP_GPIOIntDisable(SPI_GPIO_IRQ_BASE, 0xFF);

    // Cofigure WLAN_IRQ pin as input
    MAP_GPIOPinTypeGPIOInput(SPI_GPIO_IRQ_BASE, SPI_IRQ_PIN);
    GPIOPadConfigSet(SPI_GPIO_IRQ_BASE, SPI_IRQ_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

    // Setup the GPIO interrupt for this pin
    MAP_GPIOIntTypeSet(SPI_GPIO_IRQ_BASE, SPI_IRQ_PIN, GPIO_FALLING_EDGE);

    // Configure WLAN chip
    MAP_GPIOPinTypeGPIOOutput(SPI_GPIO_IRQ_BASE, SPI_EN_PIN);
    MAP_GPIODirModeSet(SPI_GPIO_IRQ_BASE, SPI_EN_PIN, GPIO_DIR_MODE_OUT);
    MAP_GPIOPadConfigSet(SPI_GPIO_IRQ_BASE, SPI_EN_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPD);

    MAP_GPIOPinWrite(SPI_GPIO_IRQ_BASE, SPI_EN_PIN, 0x00);
    SysCtlDelay(600000);
    SysCtlDelay(600000);
    SysCtlDelay(600000);

    // Disable WLAN CS with pull up Resistor
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_SPI_PORT);
    MAP_GPIOPinTypeGPIOOutput(SPI_CS_PORT, SPI_CS_PIN);
    GPIOPadConfigSet(SPI_CS_PORT, SPI_CS_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
    MAP_GPIOPinWrite(SPI_CS_PORT, SPI_CS_PIN, 0xFF);

    // Enable interrupt for WLAN_IRQ pin
    MAP_GPIOIntEnable(SPI_GPIO_IRQ_BASE, SPI_IRQ_PIN);

    // Clear interrupt status
    WlanSPI::CleanGPIOISR();

    MAP_IntEnable(INT_GPIO_SPI);

    // Configure SPI with CC3000
    _SSIConfigure(1000000, 1, 50000000);
}

void WlanSPI::CleanGPIOISR(void)
{
    unsigned long ulStatus;

    // Get the reason for the interrupt
    ulStatus = MAP_GPIOIntStatus(SPI_GPIO_IRQ_BASE, true);

    // Clear the asserted interrupts
    MAP_GPIOIntClear(SPI_GPIO_IRQ_BASE, ulStatus);
}



/*
 * SPI Functions
 */
void WlanSPI::Open(SpiHandleRx pfnRxHandler)
{
    s_ulState = SPI_STATE_POWERUP;

    s_pfnRxHandler = pfnRxHandler;
    s_usTxPacketLength = 0;
    s_pucTxPacket = NULL;
    s_pucRxPacket = wlan_rx_buffer;
    s_usRxPacketLength = 0;
    wlan_rx_buffer[CC3000_RX_BUFFER_SIZE - 1] = CC3000_BUFFER_MAGIC_NUMBER;
    wlan_tx_buffer[CC3000_TX_BUFFER_SIZE - 1] = CC3000_BUFFER_MAGIC_NUMBER;

    // Enable interrupt on the GPIOB ping of WLAN IRQ
    tSLInformation.WlanInterruptEnable();

    // Enable interrupts in NVIC
    MAP_IntEnable(INT_GPIO_SPI);
    MAP_IntEnable(INT_SPI);
}

void WlanSPI::Close(void)
{
    if (s_pucRxPacket)
    {
        s_pucRxPacket = NULL;
    }

    //  Disable Interrupt in GPIOB module...
    tSLInformation.WlanInterruptDisable();

    // Disable interrupt for SPI IRQ and SSI module
    MAP_IntDisable(INT_GPIO_SPI);
    MAP_IntDisable(INT_SPI);
}

void WlanSPI::Write(
    _Inout_updates_(usLength) unsigned char *pucUserBuffer,
    _In_ unsigned short usLength)
{
    unsigned char ucPad = 0;

    // Figure out the total length of the packet in order to figure out if there is padding or not
    if (!(usLength & 0x0001))
    {
        ucPad++;
    }

    pucUserBuffer[0] = WRITE;
    pucUserBuffer[1] = HI_BYTE(usLength + ucPad);
    pucUserBuffer[2] = LO_BYTE(usLength + ucPad);
    pucUserBuffer[3] = 0;
    pucUserBuffer[4] = 0;

    usLength += (SPI_HEADER_SIZE + ucPad);

    // The magic number that resides at the end of the TX/RX buffer (1 byte after the allocated
    // size) for the purpose of detection of the overrun. If the magic number is overwritten -
    // buffer overrun occurred - and we will stuck here forever!
    if (wlan_tx_buffer[CC3000_TX_BUFFER_SIZE - 1] != CC3000_BUFFER_MAGIC_NUMBER)
    {
        while (true);
    }

    if (s_ulState == SPI_STATE_POWERUP)
    {
        while (s_ulState != SPI_STATE_INITIALIZED);
    }

    if (s_ulState == SPI_STATE_INITIALIZED)
    {
        // This is time for first TX/RX transactions over SPI: the IRQ is down - so need to send read buffer size command
        _FirstWrite(pucUserBuffer, usLength);

        // Due to the fact that we are currently implementing a blocking situation
        // here we will wait till end of transaction
    }
    else
    {
        // We need to prevent here race that can occur in case 2 back to back packets are sent to the
        // device, so the state will move to IDLE and once again to not IDLE due to IRQ

        while (s_ulState != SPI_STATE_IDLE);

        s_ulState = SPI_STATE_WRITE_IRQ;
        s_pucTxPacket = pucUserBuffer;
        s_usTxPacketLength = usLength;

        // Assert the CS line and wait till SSI IRQ line is active and then initialize write operation
        ASSERT_CS();
    }

    // Due to the fact that we are currently implementing a blocking situation
    // here we will wait till end of transaction
    while (s_ulState != SPI_STATE_IDLE);
}

void WlanSPI::Resume(void)
{
    MAP_IntEnable(INT_SPI);
    MAP_IntEnable(INT_GPIO_SPI);
}

void WlanSPI::OnGPIOInterrupt(void)
{
    CleanGPIOISR();

    if (!MAP_GPIOPinRead(SPI_GPIO_IRQ_BASE, SPI_IRQ_PIN))
    {
        if (s_ulState == SPI_STATE_POWERUP)
        {
            // This means IRQ line was low call a callback of HCI Layer to inform on event
            s_ulState = SPI_STATE_INITIALIZED;
        }
        else if (s_ulState == SPI_STATE_IDLE)
        {
            s_ulState = SPI_STATE_READ_IRQ;

            // IRQ line goes down - we are start reception
            ASSERT_CS();

            // Wait for TX/RX Compete which will come as DMA interrupt
            _ReadHeader();
        }
        else if (s_ulState == SPI_STATE_WRITE_IRQ)
        {
            _WriteAsync(s_pucTxPacket, s_usTxPacketLength);
        }
    }
}

void WlanSPI::OnSSIInterrupt(void)
{
    bool fTxFinished, fRxFinished;
    unsigned short data_to_recv;
    unsigned char *evnt_buff;

    fTxFinished = _IsDmaStop(SPI_UDMA_TX_CHANNEL);
    fRxFinished = _IsDmaStop(SPI_UDMA_RX_CHANNEL);
    evnt_buff = s_pucRxPacket;
    data_to_recv = 0;

    if (s_ulState == SPI_STATE_READ_IRQ)
    {
        // If one of DMA's still did not finished its operation - we need to stay and wait till it
        // will finish
        if (fTxFinished && fRxFinished)
        {
            // If SSI Int is pending - this can be second DMA - clean it...
            IntPendClear(INT_SPI);
            _ContinueReadOperation();
        }
    }
    else if (s_ulState == SPI_STATE_READ_FIRST_PORTION)
    {
        if (fRxFinished)
        {
            // If SSI Int is pending - this can be second DMA - clean it...
            IntPendClear(INT_SPI);
            STREAM_TO_UINT16((char *)(evnt_buff + SPI_HEADER_SIZE), HCI_DATA_LENGTH_OFFSET, data_to_recv);

            // We need to read the rest of data..
            data_to_recv -= SPI_WINDOW_SIZE;

            if (!((HEADERS_SIZE_EVNT + data_to_recv) & 1))
            {
                data_to_recv++;
            }

            _ReadData(s_pucRxPacket + 10 + SPI_WINDOW_SIZE, data_to_recv);

            s_ulState = SPI_STATE_READ_EOT;
        }
    }
    else if (s_ulState == SPI_STATE_READ_EOT)
    {
        // All the data was read - finalize handling by switching to the task and calling from task
        // Event Handler
        if (fRxFinished)
        {
            // If SSI Int is pending - this can be second DMA - clean it...
            IntPendClear(INT_SPI);

            _TriggerRxProcessing();
        }
    }
    else if (s_ulState == SPI_STATE_WRITE_EOT)
    {
        if (fTxFinished)
        {
            while (SSIBusy(SPI_BASE));

            DEASSERT_CS();

            // Since SSI is full duplex and RX data is irrelevant - flush it
            _FlushRxFifo();
            s_ulState = SPI_STATE_IDLE;
        }
    }
    else if (s_ulState == SPI_STATE_WRITE_FIRST_PORTION)
    {
        if (fTxFinished)
        {
            // Since SSI is full duplex and RX data is irrelevant - flush it
            _FlushRxFifo();

            s_ulState = SPI_STATE_WRITE_EOT;

            _WriteAsync(s_pucTxPacket + SPI_WINDOW_SIZE, s_usTxPacketLength - SPI_WINDOW_SIZE);
        }
    }
}



/*
 * Wlan Callbacks
 */
long WlanSPI::ReadWlanInterrupt(void)
{
    return MAP_GPIOPinRead(SPI_GPIO_IRQ_BASE, SPI_IRQ_PIN);
}

void WlanSPI::WlanInterruptEnable(void)
{
    MAP_GPIOIntEnable(SPI_GPIO_IRQ_BASE, SPI_IRQ_PIN);
}

void WlanSPI::WlanInterruptDisable(void)
{
    MAP_GPIOIntDisable(SPI_GPIO_IRQ_BASE, SPI_IRQ_PIN);
}

void WlanSPI::WriteWlan(_In_ unsigned char ucVal)
{
    if (ucVal)
    {
        MAP_GPIOPinWrite(SPI_GPIO_IRQ_BASE, SPI_EN_PIN, 0xFF);
    }
    else
    {
        MAP_GPIOPinWrite(SPI_GPIO_IRQ_BASE, SPI_EN_PIN, 0x00);
    }
}



/*
 * Private Functions
 */
void WlanSPI::_SSIConfigure(
    _In_ unsigned long ulSSIFreq,
    _In_ unsigned long bForceGpioConfiguration,
    _In_ unsigned long ulSysClck)
{
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_SPI_PORT);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_SPI);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_SPI_BASE);
    GPIOPinConfigure(SPI_CLK_MUX_SEL);
    GPIOPinConfigure(SPI_RX_MUX_SEL);
    GPIOPinConfigure(SPI_TX_MUX_SEL);

    // Configure the appropriate pins to be SSI instead of GPIO
    MAP_GPIOPinTypeSSI(SPI_PORT, SPI_TX_PIN | SPI_RX_PIN | SPI_CLK_PIN);

    MAP_GPIOPadConfigSet(SPI_PORT,
        SPI_CLK_PIN, GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD_WPD);

    // Configure and enable the SSI port for master mode
    MAP_SysCtlPeripheralReset(SYSCTL_PERIPH_SPI);

    // Ensure that the SSE bit in the SSICR1 register is clear before making any configuration
    // changes
    MAP_SSIDisable(SPI_BASE);

    // 16MHz SSI with bit 8 bits data (DSS); Polarity '0' Phase '1'
    MAP_SSIConfigSetExpClk(SPI_BASE, ulSysClck, SSI_FRF_MOTO_MODE_1, SSI_MODE_MASTER, ulSSIFreq, 8);

    // Enable EOT mode for the SSIRIS EOT bit
    HWREG(SPI_BASE + SSI_O_CR1) |= SSI_CR1_EOT;

    // Enable the SSI by setting the SSE
    MAP_SSIEnable(SPI_BASE);

    // Enable DMA mode for both RX and TX
    MAP_SSIDMAEnable(SPI_BASE, SSI_DMA_TX);

    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UDMA);

    // Enable the uDMA controller.
    MAP_uDMAEnable();

    //Configure the DMA channels for the selected SPI Module
    MAP_uDMAChannelAssign(SPI_UDMA_RX_CHANNEL);
    MAP_uDMAChannelAssign(SPI_UDMA_TX_CHANNEL);

    // Point at the control table to use for channel control structures
    MAP_uDMAControlBaseSet(ucDMAChannelControlStructure);

    // Put the attributes in a known state for the uDMA SSIRX channel. These should already be
    // disabled by default.
    MAP_uDMAChannelAttributeDisable(SPI_UDMA_RX_CHANNEL,
        UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST | UDMA_ATTR_HIGH_PRIORITY | UDMA_ATTR_REQMASK);

    // Configure the control parameters for the primary control structure for the SSI RX channel.
    // The transfer data size is 8 bits, the source address does not increment since it will be
    // reading from a register. The destination address increment is byte 8-bit bytes. The
    // arbitration size is set to 1 to match the RX FIFO trigger threshold. The uDMA controller
    // will use a 4 byte burst transfer if possible.
    MAP_uDMAChannelControlSet(SPI_UDMA_RX_CHANNEL | UDMA_PRI_SELECT,
        UDMA_SIZE_8 | UDMA_SRC_INC_NONE | UDMA_DST_INC_8 | UDMA_ARB_1);

    // Put the attributes in a known state for the uDMA SSITX channel. These should already be
    // disabled by default.
    MAP_uDMAChannelAttributeDisable(SPI_UDMA_TX_CHANNEL,
        UDMA_ATTR_ALTSELECT | UDMA_ATTR_USEBURST | UDMA_ATTR_HIGH_PRIORITY | UDMA_ATTR_REQMASK);

    // Configure the control parameters for the SSI TX. The uDMA SSI TX channel is used to
    // transfer a block of data from a buffer to the SSI. The data size is 8 bits. The source
    // address increment is 8-bit bytes since the data is coming from a buffer. The destination
    // increment is none since the data is to be written to the SSI data register. The arbitration
    // size is set to 8, which matches the SSI TX FIFO trigger threshold.
    MAP_uDMAChannelControlSet(SPI_UDMA_TX_CHANNEL | UDMA_PRI_SELECT,
        UDMA_SIZE_8 | UDMA_SRC_INC_8 | UDMA_DST_INC_NONE | UDMA_ARB_4);

    // Now both the uDMA SSI TX and RX channels are primed to start a transfer. As soon as the
    // channels are enabled, the peripheral will issue a transfer request and the data transfers
    // will begin. The uDMA TX/RX channel must be disabled.
    MAP_uDMAChannelDisable(SPI_UDMA_RX_CHANNEL);
    MAP_uDMAChannelDisable(SPI_UDMA_TX_CHANNEL);

    // Enable the SSI interrupt
    MAP_IntEnable(INT_SPI);
}

void WlanSPI::_FirstWrite(
    _In_reads_(usLength) unsigned char *pucBuff,
    _In_ unsigned short usLength)
{
    // workaround for first transaction
    ASSERT_CS();

    SysCtlDelay(FWT_DELAY / 4);

    // SPI writes first 4 bytes of data
    _WriteDataSynchronous(pucBuff, 4);
    while (SSIBusy(SPI_BASE));
    SysCtlDelay(FWT_DELAY / 4);

    _WriteDataSynchronous(pucBuff + 4, usLength - 4);

    // From this point on - operate in a regular way
    s_ulState = SPI_STATE_IDLE;
    while (SSIBusy(SPI_BASE));
    DEASSERT_CS();
}

void WlanSPI::_WriteDataSynchronous(
    _In_reads_(usSize) const unsigned char *pucData,
    _In_ unsigned short usSize)
{
    // We are in the SYNC write to SPI - we do not want to receive ANY SSI related interrupts
    MAP_IntDisable(INT_SPI);
    _WriteAsync(pucData, usSize);

    // The last one to finish operation is RX channel since TX is finished before the last bit is
    // on the bus XXX was RX
    while (SSIBusy(SPI_BASE));

    // Clear The SSI & DMA Interrupt and enable back SSI
    IntPendClear(INT_SPI);
    IntPendClear(INT_UDMA);

    // Enable back SSI ISR
    MAP_IntEnable(INT_SPI);
}

void WlanSPI::_WriteAsync(
    _In_reads_(usSize) const unsigned char *pucData,
    _In_ unsigned short usSize)
{
    // LMS uDMA is capable to transfer data in one shot only of size up to 1024 so in case of
    // bigger transfer size will need to perform 2 transfers (scatter-gather uDMA  mode also can be
    // used but it will not be very useful since CPU anyway is blocked on SPI write operation and
    // can not do anything till the end.
    if (usSize <= SPI_WINDOW_SIZE)
    {
        s_ulState = SPI_STATE_WRITE_EOT;

        // The uDMA TX/RX channel must be disabled.
        _DisableSSIDMAChannels();

        // Start another DMA transfer to SSI TX.
        MAP_uDMAChannelTransferSet((SPI_UDMA_TX_CHANNEL | UDMA_PRI_SELECT), UDMA_MODE_BASIC,
            (void *)pucData, (void *)(SPI_BASE + SSI_O_DR), usSize);

        // Flush out buffer
        _FlushRxFifo();

        MAP_SSIDMAEnable(SPI_BASE, SSI_DMA_TX);

        // Send the Data
        MAP_uDMAChannelEnable(SPI_UDMA_TX_CHANNEL);
    }
    else
    {
        // Send the data over SPI and wait for complete interrupt to transfer the rest
        s_ulState = SPI_STATE_WRITE_FIRST_PORTION;

        // The uDMA TX/RX channel must be disabled.
        _DisableSSIDMAChannels();

        // Start another DMA transfer to SSI TX.
        MAP_uDMAChannelTransferSet(SPI_UDMA_TX_CHANNEL | UDMA_PRI_SELECT, UDMA_MODE_BASIC,
            (void *)pucData, (void *)(SPI_BASE + SSI_O_DR), SPI_WINDOW_SIZE);

        // The uDMA TX channel must be re-enabled.
        MAP_uDMAChannelEnable(SPI_UDMA_TX_CHANNEL);
        MAP_uDMAChannelEnable(SPI_UDMA_RX_CHANNEL);
    }
}

void WlanSPI::_FlushRxFifo(void)
{
    uint32_t uIdx;
    while (MAP_SSIDataGetNonBlocking(SPI_BASE, &uIdx))
    {
        //Clear the RX overrun flag
        SSIIntClear(SPI_BASE, SSI_RIS_RORRIS);
    }
}

void WlanSPI::_ReadHeader(void)
{
    s_ulState = SPI_STATE_READ_IRQ;
    _ReadData(s_pucRxPacket, 10);
}

void WlanSPI::_ReadData(
    _In_reads_(usSize) const unsigned char *pucData,
    _In_ unsigned short usSize)
{
    // The uDMA TX/RX channel must be disabled.
    _DisableSSIDMAChannels();

    // Start another DMA transfer to SSI TX.
    MAP_uDMAChannelTransferSet((SPI_UDMA_TX_CHANNEL | UDMA_PRI_SELECT), UDMA_MODE_BASIC,
        (void *)&rgucSpiReadHeader, (void *)(SPI_BASE + SSI_O_DR), usSize);

    // Start another DMA transfer to SSI RX.
    MAP_uDMAChannelTransferSet(SPI_UDMA_RX_CHANNEL | UDMA_PRI_SELECT, UDMA_MODE_BASIC,
        (void *)(SPI_BASE + SSI_O_DR), (void *)pucData, usSize);

    _FlushRxFifo();
    MAP_SSIDMAEnable(SPI_BASE, SSI_DMA_TX | SSI_DMA_RX);

    // The uDMA RX channel must be re-enabled. The uDMA TX channel is not required - then it will
    // not be enabled at all (for second part of RX it is required, since the host issues write
    // command)
    MAP_uDMAChannelEnable(SPI_UDMA_TX_CHANNEL);
    MAP_uDMAChannelEnable(SPI_UDMA_RX_CHANNEL);
}

long WlanSPI::_ContinueReadData(void)
{
    long lData = 0;
    unsigned char *evnt_buff, type;

    //determine what type of packet we have
    evnt_buff = s_pucRxPacket;
    STREAM_TO_UINT8((char *)(evnt_buff + SPI_HEADER_SIZE), HCI_PACKET_TYPE_OFFSET, type);

    switch (type)
    {
    case HCI_TYPE_DATA:
        STREAM_TO_UINT16((char *)(evnt_buff + SPI_HEADER_SIZE), HCI_DATA_LENGTH_OFFSET, lData);

        if (lData >= SPI_WINDOW_SIZE)
        {
            lData = SPI_STATE_READ_FIRST_PORTION;
            _ReadData(evnt_buff + 10, SPI_WINDOW_SIZE);
            s_ulState = SPI_STATE_READ_FIRST_PORTION;
        }
        else
        {
            // We need to read the rest of data..
            if (!((HEADERS_SIZE_EVNT + lData) & 1))
            {
                lData++;
            }

            if (lData)
            {
                _ReadData(evnt_buff + 10, lData);
            }

            s_ulState = SPI_STATE_READ_EOT;
        }
        break;

    case HCI_TYPE_EVNT:
        // Calculate the rest length of the data
        STREAM_TO_UINT8((char *)(evnt_buff + SPI_HEADER_SIZE), HCI_EVENT_LENGTH_OFFSET, lData);
        lData -= 1;

        // Add padding byte if needed
        if ((HEADERS_SIZE_EVNT + lData) & 1)
        {
            lData++;
        }

        if (lData)
        {
            _ReadData(evnt_buff + 10, lData);
        }

        s_ulState = SPI_STATE_READ_EOT;
        break;
    }

    return lData;
}

void WlanSPI::_ContinueReadOperation(void)
{
    // The header was read - continue with  the payload read
    if (!_ContinueReadData())
    {
        // All the data was read - finalize handling by switching to the task and calling from task
        // Event Handler
        _TriggerRxProcessing();
    }
}

void WlanSPI::_TriggerRxProcessing(void)
{
    // Trigger Rx processing
    _DisableInterrupts();
    while (SSIBusy(SPI_BASE));
    DEASSERT_CS();

    // The magic number that resides at the end of the TX/RX buffer (1 byte after the allocated
    // size) for the purpose of detection of the overrun. If the magic number is overwritten -
    // buffer overrun occurred - and we will stuck here forever!
    if (s_pucRxPacket[CC3000_RX_BUFFER_SIZE - 1] != CC3000_BUFFER_MAGIC_NUMBER)
    {
        while (1);
    }

    MAP_SSIDMADisable(SPI_BASE, SSI_DMA_RX);
    s_ulState = SPI_STATE_IDLE;
    s_pfnRxHandler(s_pucRxPacket + SPI_HEADER_SIZE);
}

void WlanSPI::_DisableInterrupts(void)
{
    MAP_IntDisable(INT_SPI);
    MAP_IntDisable(INT_GPIO_SPI);
}

void WlanSPI::_DisableSSIDMAChannels(void)
{
    MAP_uDMAChannelDisable(SPI_UDMA_TX_CHANNEL);
    MAP_uDMAChannelDisable(SPI_UDMA_RX_CHANNEL);
}

bool WlanSPI::_IsDmaStop(_In_ long lChannel)
{
    long lMode = _CheckDMAStatus(lChannel);
    long lEnable = MAP_uDMAChannelIsEnabled(lChannel);

    if ((lMode == UDMA_MODE_STOP) && (!lEnable))
    {
        return true;
    }

    return false;
}

unsigned long WlanSPI::_CheckDMAStatus(long lChannel)
{
    // Check the DMA control table to see if the basic transfer is complete. The basic transfer
    // uses receive buffer g_ucInBuffer, and the primary control structure.
    lChannel = (lChannel == SPI_UDMA_TX_CHANNEL) ? (SPI_UDMA_TX_CHANNEL | UDMA_PRI_SELECT) :
        (SPI_UDMA_RX_CHANNEL | UDMA_PRI_SELECT);

    return MAP_uDMAChannelModeGet(lChannel);
}



#ifdef  __cplusplus
extern "C" {
#endif  /* __cplusplus */
void GPIOB_Handler(void)
{
    WlanSPI::OnGPIOInterrupt();
}

void SSI2_Handler(void)
{
    WlanSPI::OnSSIInterrupt();
}
#ifdef  __cplusplus
}
#endif  /* __cplusplus */
