/*
 * SysTick.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Implementation of SysTick methods to be used with the CC3000
 */
#pragma once

#include "sha.h"

#include <stdint.h>

extern "C" void SysTick_Handler(void);

class Sys_Tick
{
private:
    /* Can't instantiate an instance of a SysTick object */
    Sys_Tick(void) {}

public:

    static void Init(void);
    static void Sleep(_In_ uint64_t uMilliseconds);
    static uint64_t GetTickCount(void);

private:

    /* Increments every ms */
    static volatile uint64_t s_uTickCount;

    friend void SysTick_Handler(void);
};
