/*
 * main.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * 
 */

#include "GPIO.h"

int main(void)
{
    GPIO_F::Init(0x0E, DIR_OUTPUT);
    
    // PF3-1 now enabled. Turn them on!
    for (volatile int count = 0; count < 100; count++)
    {
        GPIO_F::Set(0x02);
        for (volatile int i = 0; i < 100000; i++);
        GPIO_F::Set(0x04);
        for (volatile int i = 0; i < 100000; i++);
        GPIO_F::Set(0x08);
        for (volatile int i = 0; i < 100000; i++);
        
        GPIO_F::Clear(0x02);
        for (volatile int i = 0; i < 100000; i++);
        GPIO_F::Clear(0x04);
        for (volatile int i = 0; i < 100000; i++);
        GPIO_F::Clear(0x08);
        for (volatile int i = 0; i < 100000; i++);
    }
}
