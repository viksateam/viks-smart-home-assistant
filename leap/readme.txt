This directory is for placing the LEAP sdk files. Since we do not want to bog
down the repository, please download the SDK on your own and place it in this
directory.

The folder should contain only the "LeapDeveloperKit" and its zip file that
the folder is extracted from. Currently using:
	LeapDeveloperKit_release_win_1.0.9+8391
