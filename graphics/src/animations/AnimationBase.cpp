/*
 * AnimationBase.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Base class for animations which provides default implementations for some of the IAnimation
 * functions.
 */

#include "AnimationBase.h"
#include "UIElement.h"

using namespace vik::graphics;
using namespace vik::graphics::animations;

/*
 * Constructor(s)/Destructor
 */
AnimationBase::AnimationBase(void) :
    _beginCallback(nullptr),
    _completedCallback(nullptr),
    _cancelledCallback(nullptr),
    _target(nullptr),
    _parent(nullptr),
    _isCancelled(false)
{
}

AnimationBase::AnimationBase(_In_ IAnimatable *target) :
    _beginCallback(nullptr),
    _completedCallback(nullptr),
    _cancelledCallback(nullptr),
    _target(target),
    _parent(nullptr),
    _isCancelled(false)
{
}



/*
 * IAnimation
 */
#pragma region IAnimation
void AnimationBase::OnAnimationBegin(void)
{
    if (this->_beginCallback)
    {
        this->_beginCallback(this, this->_target);
    }
}

void AnimationBase::OnAnimationCompleted(void)
{
    if (this->_completedCallback)
    {
        this->_completedCallback(this, this->_target);
    }
}

void AnimationBase::OnAnimationCancelled(void)
{
    assert(!this->_isCancelled);
    this->_isCancelled = true;

    if (this->_cancelledCallback)
    {
        this->_cancelledCallback(this, this->_target);
    }
}

bool AnimationBase::IsCancelled(void)
{
    return this->_isCancelled;
}

void AnimationBase::SetParent(_In_ UIElement *parent)
{
    this->_parent = parent;
}

UIElement *AnimationBase::GetParent(void)
{
    return this->_parent;
}

void AnimationBase::SetAnimationBeginCallback(_In_ AnimationEventCallback func)
{
    this->_beginCallback = func;
}

void AnimationBase::SetAnimationCompletedCallback(_In_ AnimationEventCallback func)
{
    this->_completedCallback = func;
}

void AnimationBase::SetAnimationCancelledCallback(_In_ AnimationEventCallback func)
{
    this->_cancelledCallback = func;
}

void AnimationBase::AddPendingAnimation(_In_ IAnimation *ani)
{
    this->_pending.push_back(ani);
}

const AnimationBase::AnimationList &AnimationBase::GetPendingAnimations(void)
{
    return this->_pending;
}
#pragma endregion
