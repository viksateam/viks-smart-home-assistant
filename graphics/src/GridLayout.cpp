/*
* GridLayout.cpp
*
* Author(s):
*      Duncan Horn
       Jerry Lin
*
* Represents a Layout that positions children in a grid-like fashion. Elements added to the
* GridLayout are automatically resized to match the dimensions of the cell they occupy. By
* default, each cell is given a width and height of 1. The width and height are described as
* percentages as opposed to absolute sizes.
*/

#include "GridLayout.h"
#include "DeletionAnimation.h"

using namespace vik::graphics;
using namespace vik::graphics::animations;


/*
* Constructors/Destructor
*/
GridLayout::GridLayout(void) :
Layout()
{
}

GridLayout::GridLayout(_In_ GLfloat width, _In_ GLfloat height) :
Layout(width, height)
{
}

GridLayout::GridLayout(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    Layout(x, y, width, height)
{
}

GridLayout::GridLayout(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
Layout(pt, width, height)
{
}

GridLayout::GridLayout(_In_ const Rect &rc) :
Layout(rc)
{
}



/*
* UIElement
*/
void GridLayout::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    GLfloat left_bound = this->GetX() + this->_boundarySize + offX;
    GLfloat right_bound = this->GetWidth() - this->_boundarySize + offX;
    GLfloat top_bound = this->GetY() + this->_boundarySize + offY;
    GLfloat bottom_bound = this->GetHeight() - this->_boundarySize + offY;

    GLfloat totalColumnMargins = (GLfloat)(this->GetNumCols() - 1) * this->_marginSize;
    GLfloat totalRowMargins = (GLfloat)(this->GetNumRows() - 1) * this->_marginSize;

    GLfloat usableWidth = right_bound - left_bound;
    GLfloat usableHeight = bottom_bound - top_bound;
    GLfloat cellWidth = (usableWidth - totalColumnMargins) / this->GetNumCols();
    GLfloat cellHeight = (usableHeight - totalRowMargins) / this->GetNumRows();

    (void)cellWidth;
    (void)cellHeight;

    for (size_t i = 0; i < this->Size(); i++)
    {
        UIElement* elem = this->_children[i];

        size_t col = this->_GetColFromIndex(i);
        size_t row = this->_GetRowFromIndex(i);

        GLfloat xPos = left_bound + (col * (cellWidth + this->_marginSize));
        GLfloat yPos = top_bound + (row * (cellHeight + this->_marginSize));

        elem->SetWidth(cellWidth);
        elem->SetHeight(cellHeight);

        elem->Draw(xPos, yPos);
    }
}



/*
* Layout
*/
size_t GridLayout::GetNumCols(void) const
{
    return this->_cols;
}

size_t GridLayout::GetNumRows(void) const
{
    return this->_rows;
}

size_t GridLayout::GetBoundarySize(void) const
{
    return this->_boundarySize;
}

size_t GridLayout::GetMarginSize(void) const
{
    return this->_marginSize;
}

void GridLayout::SetNumCols(_In_ size_t cols)
{
    this->_cols = cols;
}

void GridLayout::SetNumRows(_In_ size_t rows)
{
    this->_rows = rows;
}

void GridLayout::SetBoundarySize(_In_ size_t boundarySize)
{
    this->_boundarySize = boundarySize;
}

void GridLayout::SetMarginSize(_In_ size_t marginSize)
{
    this->_marginSize = marginSize;
}


/*
* Children
*/
void GridLayout::AddChild(_In_ UIElement *elem)
{
    this->_children.push_back(elem);

    // Submit the insertion animation if there is one
    if (this->_insertionAnimationEngine)
    {
        elem->SubmitAnimation(this->_insertionAnimationEngine->CreateAnimation(elem));
    }
}

bool GridLayout::RemoveChild(_In_ UIElement *elem)
{
    for (auto itr = std::begin(this->_children); itr != std::end(this->_children); ++itr)
    {
        if (*itr == elem)
        {
            // Since the calling function holds a reference to the UIElement child, it is their
            // responsibility to deallocate the object
            this->_children.erase(itr);

            // Submit a removal animation if there is an engine
            if (this->_removalAnimationEngine)
            {
                elem->SubmitOverrideAnimation(this->_removalAnimationEngine->CreateAnimation(elem));
            }

            return true;
        }
    }

    return false;
}

bool GridLayout::RemoveChild(_In_ size_t index)
{
    if (index >= this->_children.size())
    {
        return false;
    }

    UIElement *elem = this->_children[index];
    this->_children.erase(std::begin(this->_children) + index);

    // Submit a removal animation if there is an engine
    if (this->_removalAnimationEngine)
    {
        elem->SubmitOverrideAnimation(this->_removalAnimationEngine->CreateAnimation(elem));
    }

    // Since the calling function is removing by index, it is our responsibility to deallocate the
    // element on removal
    elem->SubmitPendingAnimation(new DeletionAnimation(elem));

    return true;
}

UIElement *GridLayout::GetChild(_In_ size_t index) const
{
    if (index >= this->_children.size())
    {
        return nullptr;
    }

    return this->_children[index];
}

size_t GridLayout::Size(void) const
{
    return this->_children.size();
}



/*
 * Private helper functions 
 */
size_t GridLayout::_GetColFromIndex(_In_ size_t index) const
{
    return index % this->_cols;
}

size_t GridLayout::_GetRowFromIndex(_In_ size_t index) const
{
    return index / this->_cols;
}
