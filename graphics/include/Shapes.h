/*
 * Shapes.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines various classes that are useful when doing graphics operations (e.g. Point, etc.)
 */
#pragma once

#include "sha.h"

#include <cassert>
#include <cmath>
#include <cstdint>
#include <GL/gl.h>
#include <vector>

namespace vik
{
    namespace graphics
    {
        /* Represents 2-dimensional points */
        struct Point
        {
            Point(_In_ GLfloat x, _In_ GLfloat y) :
                x(x), y(y)
            {
            }

            /* Addition operators */
            Point operator+(_In_ const Point &pt) const
            {
                return Point(this->x + pt.x, this->y + pt.y);
            }
            Point &operator+=(_In_ const Point &pt)
            {
                this->x += pt.x;
                this->y += pt.y;
                return *this;
            }

            /* Negation/subtraction operators */
            Point operator-(void) const
            {
                return Point(-this->x, -this->y);
            }
            Point operator-(_In_ const Point &pt) const
            {
                return *this + -pt;
            }
            Point &operator-=(_In_ const Point &pt)
            {
                return *this += -pt;
            }

            operator const GLfloat *(void) const
            {
                return this->vertex;
            }

            union
            {
                GLfloat vertex[2];
                struct
                {
                    GLfloat x;
                    GLfloat y;
                };
            };
        };

        /* Represents 2-dimensional rectangles */
        struct Rect
        {
            Rect(void) :
                pt(0, 0),
                width(0),
                height(0)
            {
            }

            Rect(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
                pt(x, y),
                width(width),
                height(height)
            {
                assert(width >= -1e-2);
                assert(height >= -1e-2);
            }

            Rect(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
                pt(pt),
                width(width),
                height(height)
            {
                assert(width >= -1e-2);
                assert(height >= -1e-2);
            }

            Rect &operator=(_In_ const Rect &rc)
            {
                assert(rc.width >= -1e-2);
                assert(rc.height >= -1e-2);
                this->pt = rc.pt;
                this->width = rc.width;
                this->height = rc.height;

                return *this;
            }

            bool Contains(_In_ const Point &pt)
            {
                return (pt.x >= this->pt.x) && (pt.y >= this->pt.y) &&
                    (pt.x <= this->pt.x + this->width) && (pt.y <= this->pt.y + this->height);
            }

            /* Add and subtract points for translation */
            Rect operator+(_In_ const Point &pt) const
            {
                return Rect(this->pt + pt, this->width, this->height);
            }
            Rect &operator+=(_In_ const Point &pt)
            {
                this->pt += pt;
                return *this;
            }
            Rect operator-(_In_ const Point &pt) const
            {
                return Rect(this->pt - pt, this->width, this->height);
            }
            Rect &operator-=(_In_ const Point &pt)
            {
                this->pt -= pt;
                return *this;
            }

            /* pt represents the top-left corner */
            Point   pt;
            GLfloat width;
            GLfloat height;
        };

        /* Represents 2-dimensional ovals. Ovals are, in a basic sense, equivalent to rectangles
           and thus uses Rect as its base. However, OpenGL does not allow us to simply "draw an
           oval." Therefore, the Oval structure caches the points needed to draw the oval to avoid
           unnecessary processing each time the oval is drawn on screen */
        struct Oval :
            public Rect
        {
            Oval(
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height,
                _In_ uint32_t res) :
                Rect(x, y, width, height)
            {
                this->_GeneratePoints(res);
            }

            Oval(
                _In_ const Point &pt,
                _In_ GLfloat width,
                _In_ GLfloat height,
                _In_ uint32_t res) :
                Rect(pt, width, height)
            {
                this->_GeneratePoints(res);
            }

            std::vector<Point> vertices;

        private:

            /* Generates points for the oval. These points start at the far right (positive x-axis)
               and go counter-clockwise. */
            void _GeneratePoints(_In_ uint32_t res)
            {
                assert(this->vertices.size() == 0);
                assert(res >= 3);

                /* It is more convenient for us to describe all points relative to the center */
                GLfloat center_x = this->pt.x + (this->width / 2.0f);
                GLfloat center_y = this->pt.y + (this->height / 2.0f);

                /* res is the number of points to draw. Therefore, each angle increment is
                   (2 * PI) / res */
                GLfloat angle_delta = (float)(2.0f * M_PI) / res;
                GLfloat angle = 0;

                for (uint32_t i = 0; i < res; i++)
                {
                    GLfloat x = center_x + (this->width * cos(angle) / 2);
                    GLfloat y = center_y + (this->height * sin(angle) / 2);
                    this->vertices.push_back(Point(x, y));

                    angle += angle_delta;
                }
            }
        };
    }
}

