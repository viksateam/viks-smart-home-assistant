/*
 * Shape.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * A simple extension to the UIElement class that adds a border thickness and color
 */
#pragma once

#include "UIElement.h"

namespace vik
{
    namespace graphics
    {
        namespace shapes
        {
            class Shape :
                public UIElement
            {
            public:
                Shape(void);
                Shape(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
                Shape(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
                Shape(_In_ const Rect &rc);

                virtual GLfloat     GetBorderThickness(void) const;
                virtual void        SetBorderThickness(_In_ GLfloat thickness);
                virtual const Color &GetBorderColor(void) const;
                virtual void        SetBorderColor(_In_ const Color &color);

            protected:

                GLfloat _borderThickness;
                Color   _borderColor;
            };
        }
    }
}
