/*
* ShaSwipeGesture.cpp
*
* Author(s):
*      Jerry Lin
*
* Detects and reports a swipe gesture based on palm velocity (in mm/s).
*/

#include "ShaSwipeGesture.h"

using namespace sha;
using namespace sha::leap;

/*
 * Constructors / Destructors
 */
ShaSwipeGesture::ShaSwipeGesture(void) : ShaGesture()
{
    this->_distanceTravelled = 0;
    this->_lastPalmPosition = Vector::zero();
    this->_lastPalmVelocity = Vector::zero();
    this->_minDistance = DEFAULT_SWIPE_MIN_DISTANCE;
    this->_minVelocity = DEFAULT_SWIPE_MIN_VELOCITY;
    this->_swipeDirection = SD_NONE;
}



/*
 * Public Functions
 */
SwipeDirection ShaSwipeGesture::GetDirection(void) const
{
    return this->_swipeDirection;
}

GestureType ShaSwipeGesture::GetType(void) const
{
    return GT_SWIPE;
}

bool ShaSwipeGesture::IsComplete(void) const
{
    return (this->_state == GS_UPDATE && this->_distanceTravelled > this->_minDistance);
}

bool ShaSwipeGesture::SetMinDistance(_In_ float minDistance)
{
    bool succeeded = false;
    if (minDistance >= 50)
    {
        this->_minDistance = minDistance;
    }
    return succeeded;
}

bool ShaSwipeGesture::SetMinVelocity(_In_ float minVelocity)
{
    bool succeeded = false;
    if (minVelocity >= 500)
    {
        this->_minVelocity = minVelocity;
    }
    return succeeded;
}

void ShaSwipeGesture::Update(_In_ const Frame &frame)
{
    const HandList &hands = frame.hands();
    if (!hands.isEmpty())
    {
        const Hand &hand = hands.frontmost();
        const Vector &palmVelocity = hand.palmVelocity();
        const Vector &palmPosition = hand.palmPosition();
        SwipeDirection swipeDirection = this->_GetSwipeDirection(palmVelocity);
        Vector unitVector = this->_GetDirectionalUnitVector(this->_swipeDirection);

        if (swipeDirection == SD_NONE)
        {
            this->_Reset();
        }
        else if (this->_swipeDirection == SD_NONE)
        {
            if (swipeDirection == SD_LEFT || swipeDirection == SD_RIGHT)
            {
                if (abs(hand.palmNormal().y) > abs(hand.palmNormal().x))
                {
                    return;
                }
            }

            this->_id = ShaGesture::CreateID();
            this->_distanceTravelled = 0;
            this->_lastPalmPosition = palmPosition;
            this->_lastPalmVelocity = palmVelocity;
            this->_swipeDirection = swipeDirection;
            this->_state = GS_START;
        }
        else if (this->_swipeDirection != swipeDirection)
        {
            this->_Reset();
        }
        else
        {
            float currentSpeed = abs(palmVelocity.dot(unitVector));
            float lastSpeed = abs(this->_lastPalmVelocity.dot(unitVector));
            bool isAccelerating = currentSpeed > lastSpeed;

            float distanceInSwipeDirection = 
                abs(palmPosition.dot(unitVector) - this->_lastPalmPosition.dot(unitVector));
            this->_distanceTravelled += distanceInSwipeDirection;

            if (isAccelerating)
            {
                switch (this->_state)
                {
                case GS_START:
                    if (currentSpeed > this->_minVelocity)
                    {
                        this->_state = GS_UPDATE;
                    }
                    break;
                }
            }
            else
            {
                switch (this->_state)
                {
                case GS_START:
                    //this->_Reset();
                    break;
                case GS_UPDATE:
                {
                    if (currentSpeed < this->_minVelocity / 4)
                    {
                        this->_Reset();
                    }
                }
                    break;
                }
            }
        }
    }
    else
    {
        this->_Reset();
    }
}



/*
 * Private helper functions
 */
Vector ShaSwipeGesture::_GetDirectionalUnitVector(_In_ SwipeDirection swipeDirection) const
{
    Vector unitVector = Vector::zero();
    if (this->_swipeDirection == SD_UP || this->_swipeDirection == SD_DOWN)
    {
        unitVector = Vector::yAxis();
    }
    else if (this->_swipeDirection == SD_LEFT || this->_swipeDirection == SD_RIGHT)
    {
        unitVector = Vector::xAxis();
    }
    return unitVector;
}

SwipeDirection ShaSwipeGesture::_GetSwipeDirection(_In_ const Vector &direction) const
{
    SwipeDirection swipeDirection = SD_NONE;

    if (abs(direction.x) > abs(direction.y) && abs(direction.x) > abs(direction.z))
    {
        //Swipe is in the horizontal direction.
        if (direction.x < 0)
        {
            swipeDirection = SD_LEFT;
        }
        else
        {
            swipeDirection = SD_RIGHT;
        }
    }
    else if (abs(direction.y) > abs(direction.x) && abs(direction.y) > abs(direction.z))
    {
        //Swipe is in the vertical direction.
        if (direction.y < 0)
        {
            swipeDirection = SD_DOWN;
        }
        else
        {
            swipeDirection = SD_UP;
        }
    }
    
    return swipeDirection;
}

void ShaSwipeGesture::_Reset(void)
{
    this->_distanceTravelled = 0;
    this->_lastPalmPosition = Vector::zero();
    this->_lastPalmVelocity = Vector::zero();
    this->_swipeDirection = SD_NONE;
    this->_state = GS_STOP;
}
