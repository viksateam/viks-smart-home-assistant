/*
 * CoffeeMachineElement.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * The UIElement that graphically represents a CoffeeMachine ShaStation object. When the
 * CoffeeMachine class is updated (i.e. a new supported feature is added), this class needs to get
 * updated in order for the addition to make an effect graphically.
 */
#pragma once

#include "Button.h"
#include "Layout.h"
#include "Polygon.h"
#include "ShaStation.h"
#include "SliderComboBox.h"

namespace vik
{
    namespace graphics
    {
        class CoffeeMachineElement final :
            public Layout
        {
        public:
            CoffeeMachineElement(_In_ CoffeeMachine *target);
            CoffeeMachineElement(
                _In_ CoffeeMachine *target,
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height);
            CoffeeMachineElement(
                _In_ CoffeeMachine *target,
                _In_ const Point &pt,
                _In_ GLfloat width,
                _In_ GLfloat height);
            CoffeeMachineElement(_In_ CoffeeMachine *target, _In_ const Rect &rc);

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual bool   OnPointerMoved(_In_ Point pt);

            void OnStatusChanged(_In_ const CoffeeMachineStatus &status);

        private:

            /* LayoutAwareElement */
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            void _StartDripAnimation(void);
            void _StartPotAnimation(void);
            bool _OnButtonPressed(_In_ ButtonBase *sender, _In_ const Point &pt);

            CoffeeMachine *_target;
            Button        _button;

            shapes::Polygon   _top;
            shapes::Polygon   _body;
            shapes::Polygon   _base;
            shapes::Polygon   _dripper;
            shapes::Polygon   _carafeTop;
            shapes::Polygon   _carafe;
            shapes::Rectangle _drip;
            shapes::Polygon   _potBottom;
            shapes::Rectangle _pot;
            bool              _potIsVisible;

            CoffeeMachineStatus _status;
            std::string         _statusString;
            std::string         _amtString;
            Color               _statusColor;
            GLfloat             _mainWidth;
            GLfloat             _statusWidth;
            GLfloat             _statusY;

            SliderComboBox _comboBox;

            static GLfloat s_statusWidth;
        };
    }
}
