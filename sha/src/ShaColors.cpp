/*
 * ShaColors.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Theme colors for the Smart Home Assistant
 */

#include "ShaColors.h"

using namespace vik::graphics;

#define FROM_RGBA(r, g, b, a)       { { { r, g, b, a } } }

/* "Theme" Colors */
const Color ShaColors::BackgroundColor        = FROM_RGBA(0.1098039f, 0.1098039f, 0.1098039f, 1.0);
const Color ShaColors::ForegroundColor        = FROM_RGBA(1.0, 1.0, 1.0, 1.0);
const Color ShaColors::ThemeColor             = FROM_RGBA(0.8f, 0.3333333f, 0.0, 1.0);
const Color ShaColors::PointerFocusPointColor = FROM_RGBA(0.0, 0.0, 1.0, 1.0);
const Color ShaColors::ButtonHoverColor       = FROM_RGBA(0.2f, 0.2f, 0.2f, 1.0f);
const Color ShaColors::CoffeeBrown            = FROM_RGBA(0.43529f, 0.30588f, 0.21569f, 1.0f);
