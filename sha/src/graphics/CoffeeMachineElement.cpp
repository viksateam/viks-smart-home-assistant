/*
 * CoffeeMachineElement.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * The UIElement that graphically represents a CoffeeMachine ShaStation object. When the
 * CoffeeMachine class is updated (i.e. a new supported feature is added), this class needs to get
 * updated in order for the addition to make an effect graphically.
 */

#include <sstream>

#include "CoffeeMachineElement.h"
#include "LinearGrowthAnimation.h"
#include "ShaColors.h"
#include "ShaFont.h"
#include "WindowHelpers.h"

using namespace vik;
using namespace vik::graphics;
using namespace vik::graphics::animations;

#define STATUS_STRING           "Status: "

#define BUTTON_WIDTH            (500)
#define BUTTON_HEIGHT           (150)

#define DRIP_X                  (1.3f)
#define DRIP_Y                  (1.5f)
#define DRIP_WIDTH              (0.4f)
#define DRIP_HEIGHT             (2.0f)
#define DRIP_VEL                (.0005f)

#define POT_X                   (0.3f)
#define POT_Y                   (3.3f)
#define POT_WIDTH               (2.4f)
#define POT_HEIGHT              (0.7f)
#define POT_VEL                 (.0001f)

GLfloat CoffeeMachineElement::s_statusWidth = 0;

/*
 * Image Points
 */
static const std::vector<Point> topPoints
{
    Point(0, 0), Point(3, 0), Point(3, 1), Point(0, 1)
};

static const std::vector<Point> bodyPoints
{
    Point(0, 1), Point(3, 1), Point(3, 3.5f), Point(0, 3.5f)
};

static const std::vector<Point> basePoints
{
    Point(0, 3.5), Point(3, 3.5), Point(3, 4), Point(0, 4)
};

static const std::vector<Point> dripperPoints
{
    Point(0, 1), Point(3, 1), Point(2.3f, 1.5f), Point(0.7f, 1.5f)
};

static const std::vector<Point> carafeTopPoints
{
    Point(0.9f, 1.8f), Point(2.1f, 1.8f), Point(2.1f, 2), Point(0.9f, 2)
};

static const std::vector<Point> carafePoints
{
    Point(0.7f, 2), Point(2.3f, 2),

    // Top right curve
    Point(2.37803f, 2.00576f), Point(2.45307f, 2.02283f), Point(2.52222f, 2.05055f),
    Point(2.58284f, 2.08786f),
    Point(2.63258f, 2.13332f), Point(2.66955f, 2.18519f), Point(2.69231f, 2.24147f),

    Point(2.7f, 2.3f), Point(2.7f, 3.3f),

    // Bottom right curve
    Point(2.69615f, 3.33901f), Point(2.68477f, 3.37653f), Point(2.66629f, 3.41111f),
    Point(2.64142f, 3.44142f),
    Point(2.61111f, 3.46629f), Point(2.57653f, 3.48477f), Point(2.53901f, 3.49615f),

    Point(2.5f, 3.5f), Point(0.5f, 3.5f),

    // Bottom left curve
    Point(0.46098f, 3.49615f), Point(0.42346f, 3.48477f), Point(0.38888f, 3.46629f),
    Point(0.35857f, 3.44142f),
    Point(0.33370f, 3.41111f), Point(0.31522f, 3.37653f), Point(0.30384f, 3.33901f),

    Point(0.3f, 3.3f), Point(0.3f, 2.3f),

    // Top left curve
    Point(0.30768f, 2.24147f), Point(0.33044f, 2.18519f), Point(0.36741f, 2.13332f),
    Point(0.41715f, 2.08786f),
    Point(0.47777f, 2.05055f), Point(0.54692f, 2.02283f), Point(0.62196f, 2.00576f),
};

static const std::vector<Point> potBottomPoints
{
    Point(2.7f, 3.3f),

    // Bottom right curve
    Point(2.69615f, 3.33901f), Point(2.68477f, 3.37653f), Point(2.66629f, 3.41111f),
    Point(2.64142f, 3.44142f),
    Point(2.61111f, 3.46629f), Point(2.57653f, 3.48477f), Point(2.53901f, 3.49615f),

    Point(2.5f, 3.5f), Point(0.5f, 3.5f),

    // Bottom left curve
    Point(0.46098f, 3.49615f), Point(0.42346f, 3.48477f), Point(0.38888f, 3.46629f),
    Point(0.35857f, 3.44142f),
    Point(0.33370f, 3.41111f), Point(0.31522f, 3.37653f), Point(0.30384f, 3.33901f),

    Point(0.3f, 3.3f)
};



/*
 * Constructor(s)/Destructor
 */
CoffeeMachineElement::CoffeeMachineElement(_In_ CoffeeMachine *target) :
    CoffeeMachineElement(target, Rect(0, 0, 0, 0))
{
}

CoffeeMachineElement::CoffeeMachineElement(
    _In_ CoffeeMachine *target,
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    CoffeeMachineElement(target, Rect(x, y, width, height))
{
}

CoffeeMachineElement::CoffeeMachineElement(
    _In_ CoffeeMachine *target,
    _In_ const Point &pt,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    CoffeeMachineElement(target, Rect(pt, width, height))
{
}

CoffeeMachineElement::CoffeeMachineElement(_In_ CoffeeMachine *target, _In_ const Rect &rc) :
    Layout(rc),
    _target(target),
    _top(0, 0, 3, 4),
    _body(0, 0, 3, 4),
    _base(0, 0, 3, 4),
    _dripper(0, 0, 3, 4),
    _carafeTop(0, 0, 3, 4),
    _carafe(0, 0, 3, 4),
    _potBottom(0, 0, 3, 4),
    _potIsVisible(false)
{
    // Only calculate the width once
    if (s_statusWidth == 0)
    {
        s_statusWidth = GetStringWidth(Fonts::ApplianceFont, STATUS_STRING);
    }

    // Initialize the button
    this->_button.SetPreferredColors(ShaColors::ForegroundColor, ShaColors::BackgroundColor,
        ShaColors::ButtonHoverColor, ShaColors::ForegroundColor);
    this->_button.OnButtonUp(this,
        (ButtonBase::ButtonEventCallback)&CoffeeMachineElement::_OnButtonPressed);

    // Initialize the SliderComboBox
    this->_comboBox.SetBorderColor(ShaColors::ForegroundColor);
    this->_comboBox.SetForegroundColor(ShaColors::ForegroundColor);
    this->_comboBox.SetBorderThickness(2);
    this->_comboBox.SetPadding(Padding(20));
    this->_comboBox.SetFont(Fonts::ApplianceFont);
    auto &options = this->_target->GetOptions();
    if (options.size() > 0)
    {
        this->_comboBox.AddOption("N/A");
        for (auto &opt : options)
        {
            this->_comboBox.AddOption(opt);
        }
    }

    // Initialize the images
    this->_top.AddPoints(std::begin(topPoints), std::end(topPoints));
    this->_top.SetBorderColor(ShaColors::ForegroundColor);
    this->_top.SetBorderThickness(3);

    this->_body.AddPoints(std::begin(bodyPoints), std::end(bodyPoints));
    this->_body.SetBorderColor(ShaColors::ForegroundColor);
    this->_body.SetBorderThickness(3);

    this->_base.AddPoints(std::begin(basePoints), std::end(basePoints));
    this->_base.SetBorderColor(ShaColors::ForegroundColor);
    this->_base.SetBorderThickness(3);

    this->_dripper.AddPoints(std::begin(dripperPoints), std::end(dripperPoints));
    this->_dripper.SetBorderColor(ShaColors::ForegroundColor);
    this->_dripper.SetBorderThickness(3);

    this->_carafeTop.AddPoints(std::begin(carafeTopPoints), std::end(carafeTopPoints));
    this->_carafeTop.SetBorderColor(ShaColors::ForegroundColor);
    this->_carafeTop.SetBorderThickness(3);

    this->_carafe.AddPoints(std::begin(carafePoints), std::end(carafePoints));
    this->_carafe.SetBorderColor(ShaColors::ForegroundColor);
    this->_carafe.SetBorderThickness(3);

    this->_potBottom.AddPoints(std::begin(potBottomPoints), std::end(potBottomPoints));
    this->_potBottom.SetBackgroundColor(ShaColors::CoffeeBrown);

    this->_drip.SetBackgroundColor(ShaColors::CoffeeBrown);
    this->_pot.SetBackgroundColor(ShaColors::CoffeeBrown);

    this->OnStatusChanged(this->_target->GetStatus());
    this->_OnSizeUpdated(this->GetWidth(), this->GetHeight());
}



/*
 * UIElement
 */
GLvoid CoffeeMachineElement::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    WindowDrawText(offX + TITLE_X_OFFSET, offY + TITLE_Y_OFFSET, this->_target->Name().c_str(),
        Fonts::TitleFont, ShaColors::ForegroundColor);

    // Draw the image
    this->_drip.Draw(offX, offY);
    if (this->_potIsVisible)
    {
        this->_potBottom.Draw(offX, offY);
        this->_pot.Draw(offX, offY);
    }
    this->_top.Draw(offX, offY);
    this->_body.Draw(offX, offY);
    this->_base.Draw(offX, offY);
    this->_dripper.Draw(offX, offY);
    this->_carafeTop.Draw(offX, offY);
    this->_carafe.Draw(offX, offY);

    // Draw the status
    GLfloat x = this->GetWidth() - SIDE_PADDING - (this->_mainWidth + this->_statusWidth) / 2;
    GLfloat y = this->_statusY;
    WindowDrawText(x + offX, y + offY, STATUS_STRING, Fonts::ApplianceFont,
        ShaColors::ForegroundColor);

    x += s_statusWidth;
    WindowDrawText(x + offX, y + offY, this->_statusString.c_str(),
        Fonts::ApplianceFont, this->_statusColor);

    if (!this->_amtString.empty())
    {
        x += this->_statusWidth - s_statusWidth + 10;
        GLfloat offset = (GLfloat)(Fonts::ApplianceFont.height - Fonts::NotificationFont.height);
        WindowDrawText(x + offX, y + offY + offset, this->_amtString.c_str(),
            Fonts::NotificationFont, ShaColors::ButtonHoverColor);
    }

    // Only draw the cups slider if there are any options
    if (this->_comboBox.Size() > 0)
    {
        x = this->GetWidth() - SIDE_PADDING - (this->_mainWidth * 3 / 4);
        y += Fonts::ApplianceFont.height * 2.5f;
        WindowDrawText(x + offX, y + offY, "Cups:", Fonts::ApplianceFont,
            ShaColors::ForegroundColor);
        this->_comboBox.Draw(offX, offY);
    }

    this->_button.Draw(offX, offY);
}

bool CoffeeMachineElement::OnPointerMoved(_In_ Point pt)
{
    if (Layout::OnPointerMoved(pt))
    {
        return true;
    }

    if (this->_button.GetRect().Contains(pt) &&
        this->_button.OnPointerEntered(pt - this->_button.GetLocation()))
    {
        this->_pointerTarget = &this->_button;
        return true;
    }
    else if (this->_comboBox.GetRect().Contains(pt) &&
        this->_comboBox.OnPointerEntered(pt - this->_comboBox.GetLocation()))
    {
            this->_pointerTarget = &this->_comboBox;
            return true;
    }

    return false;
}



/*
 * Status
 */
void CoffeeMachineElement::OnStatusChanged(_In_ const CoffeeMachineStatus &status)
{
    if (this->_status.state == CoffeeMachineStatus::COFFEE_OFF &&
        status.state == CoffeeMachineStatus::COFFEE_ON)
    {
        this->_StartDripAnimation();
    }
    else if (status.state != CoffeeMachineStatus::COFFEE_ON)
    {
        this->_drip.CancelCurrentAnimation();
        this->_pot.CancelCurrentAnimation();
        this->_drip.SetHeight(0);

        if (status.state == CoffeeMachineStatus::COFFEE_OFF)
        {
            this->_pot.SetHeight(0);
            this->_potIsVisible = false;
        }
    }

    switch (status.state)
    {
    case CoffeeMachineStatus::COFFEE_OFF:
        this->_statusString = "OFF";
        this->_button.SetText("Brew");
        this->_statusColor = Colors::Red;
        this->_amtString.clear();
        break;

    case CoffeeMachineStatus::COFFEE_ON:
    case CoffeeMachineStatus::COFFEE_DONE:
        if (status.state == CoffeeMachineStatus::COFFEE_ON)
        {
            this->_statusString = "ON";
        }
        else
        {
            this->_statusString = "DONE";
        }

        this->_button.SetText("Turn Off");
        this->_statusColor = Colors::Green;
        if (status.cups != 0)
        {
            std::stringstream stream;
            stream << status.cups;
            stream << " cup";
            if (status.cups > 1)
            {
                stream << "s";
            }

            this->_amtString = stream.str();
        }
        break;

    default:
        // Unreached
        abort();
        break;
    }

    this->_status = status;
    this->_statusWidth = s_statusWidth +
        GetStringWidth(Fonts::ApplianceFont, this->_statusString.c_str());
    this->_OnSizeUpdated(this->GetWidth(), this->GetHeight());
}



/*
 * LayoutAwareElement
 */
void CoffeeMachineElement::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    LayoutAwareElement::_OnSizeUpdated(width, height);

    this->_mainWidth = (width - 2 * SIDE_PADDING) / 2;
    GLfloat mainHeight = height - (TOP_PADDING + BOTTOM_PADDING);

    GLfloat imageWidth = this->_mainWidth;
    this->_statusY = TOP_PADDING;

    // Handle the "picture" size/location
    if (mainHeight < 0 || imageWidth < 0)
    {
        // Prevent setting rectangle widths to zero
        this->_top.SetRect(Rect(0, 0, 3, 4));
        this->_body.SetRect(Rect(0, 0, 3, 4));
        this->_base.SetRect(Rect(0, 0, 3, 4));
        this->_dripper.SetRect(Rect(0, 0, 3, 4));
        this->_carafeTop.SetRect(Rect(0, 0, 3, 4));
        this->_carafe.SetRect(Rect(0, 0, 3, 4));
        this->_potBottom.SetRect(Rect(0, 0, 3, 4));
        this->_button.SetRect(Rect(0, 0, 0, 0));
        this->_comboBox.SetRect(Rect(0, 0, 0, 0));
        this->_drip.SetRect(Rect(0, 0, 0, 0));
        this->_pot.SetRect(Rect(0, 0, 0, 0));
    }
    else
    {
        GLfloat old_dim = this->_top.GetWidth() / 3;
        GLfloat dim = std::min(mainHeight / 4, imageWidth / 3);
        GLfloat x = SIDE_PADDING + (imageWidth - dim * 3) / 2;
        GLfloat y = TOP_PADDING + (mainHeight - dim * 4) / 2;

        this->_top.SetRect(Rect(x, y, dim * 3, dim * 4));
        this->_body.SetRect(Rect(x, y, dim * 3, dim * 4));
        this->_base.SetRect(Rect(x, y, dim * 3, dim * 4));
        this->_dripper.SetRect(Rect(x, y, dim * 3, dim * 4));
        this->_carafeTop.SetRect(Rect(x, y, dim * 3, dim * 4));
        this->_carafe.SetRect(Rect(x, y, dim * 3, dim * 4));
        this->_potBottom.SetRect(Rect(x, y, dim * 3, dim * 4));

        // Handle the drip changes
        this->_drip.SetX(x + dim * DRIP_X);
        this->_drip.SetY(y + dim * DRIP_Y);
        this->_drip.SetWidth(dim * DRIP_WIDTH);
        this->_drip.SetHeight(this->_drip.GetHeight() * dim / old_dim);
        this->_drip.CancelCurrentAnimation();
        if (this->_status.state == CoffeeMachineStatus::COFFEE_ON)
        {
            this->_StartDripAnimation();
        }

        // Handle the pot changes
        height = this->_pot.GetHeight() * dim / old_dim;
        this->_pot.SetX(x + dim * POT_X);
        this->_pot.SetY(y + dim * POT_Y + (POT_HEIGHT - height));
        this->_pot.SetWidth(dim * POT_WIDTH);
        this->_pot.SetHeight(height);
        if (this->_pot.CancelCurrentAnimation())
        {
            this->_StartPotAnimation();
        }


        // Handle the button size/location
        GLfloat buttonWidth = std::min(imageWidth, (GLfloat)BUTTON_WIDTH);
        x = width - SIDE_PADDING - (imageWidth + buttonWidth) / 2;
        y = TOP_PADDING + mainHeight - BUTTON_HEIGHT * 1.5f;
        this->_button.SetRect(Rect(x, y, buttonWidth, BUTTON_HEIGHT));

        // Handle the combo box size/location
        x = this->GetWidth() - SIDE_PADDING - this->_mainWidth;
        y = this->_statusY + Fonts::ApplianceFont.height * 4;
        this->_comboBox.SetRect(Rect(x, y, this->_mainWidth, 100));
    }
}

void CoffeeMachineElement::_StartDripAnimation(void)
{
    Rect rc = this->_drip.GetRect();
    rc.height = this->_top.GetHeight() * DRIP_HEIGHT / 4;

    auto ani = new LinearGrowthAnimation(&this->_drip, rc, DRIP_VEL);
    ani->SetParent(this);
    ani->SetAnimationCompletedCallback([](_In_ IAnimation *sender, _In_ IAnimatable *target)
    {
        (void)target;
        CoffeeMachineElement *elem = dynamic_cast<CoffeeMachineElement *>(sender->GetParent());
        assert(elem);

        if (elem->_status.state != CoffeeMachineStatus::COFFEE_OFF)
        {
            elem->_potIsVisible = true;
            elem->_StartPotAnimation();
        }
    });

    this->_drip.SubmitOverrideAnimation(ani);
}

void CoffeeMachineElement::_StartPotAnimation(void)
{
    Rect rc = this->_pot.GetRect();
    GLfloat dim = this->_top.GetHeight() / 4;
    GLfloat delta = (POT_HEIGHT * dim) - this->_pot.GetHeight();
    rc.height = POT_HEIGHT * dim;
    rc.pt.y -= delta;

    auto ani = new LinearGrowthAnimation(&this->_pot, rc, POT_VEL);
    this->_pot.SubmitOverrideAnimation(ani);
}

bool CoffeeMachineElement::_OnButtonPressed(_In_ ButtonBase *sender, _In_ const Point &pt)
{
    (void)sender;
    (void)pt;

    CoffeeMachineStatus status{ CoffeeMachineStatus::COFFEE_OFF, 0 };

    if (this->_status.state == CoffeeMachineStatus::COFFEE_OFF)
    {
        // Both the on and done states lead to a COFFEE_OFF message
        status.state = CoffeeMachineStatus::COFFEE_ON;

        auto &options = this->_target->GetOptions();
        if (options.size() > 0)
        {
            int index = this->_comboBox.GetSelectedIndex();
            if (index > 0)
            {
                assert((size_t)index <= options.size());
                status.cups = atoi(options[index - 1].c_str());
            }
        }
    }

    this->_target->PostStatusRequest(status);
    return true;
}
