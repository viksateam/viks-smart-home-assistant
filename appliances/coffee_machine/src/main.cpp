/*.5
 * main.cpp
 *
 * Author(s):
 *      Peter Vu
 *
 * The main implementation for the coffee machine appliance
 */

#include "Console.h"
#include "Debug.h"
#include "GPIO.h"
#include "ip.h"
#include "Server.h"
#include "sys_tick.h"
#include "Timer0A.h"
#include "tiva.h"

#include <string.h>
#include <driverlib/sysctl.h>

// Coffee Status settings
#define OFF         0
#define ON          1
#define WARM        2

// Periodic Timer settings (250ms timer)
#define ONE_CUP     320 // in 250ms (1:30 minutes MAX)
#define WARM_ON     2   // in 250ms (0.5 second)
#define WARM_OFF    40  // in 250ms (10 seconds)

#define MAX_CUPS    4

void coffeeToggle();

// Declare global variables
char buffer[64];
char rgszStatus[64] = "off\n";
char rgszNumCups[64];
uint32_t nCoffeeStatus = 0; // 0 = off, 1 = on, 2 = making coffee
uint32_t nWarm = 0; // 0 = warmOff, 1 = warmOn 

static char *IncomingMessageHandler(_Inout_ char *pszMessage)
{
    char* ret;
    char* token;
    
    Console::WriteLine(pszMessage);
    
    // Parse message for command
    token = strtok(pszMessage, " ");
    
    // Parse message
    if (strcmp(token, "request") == 0)
    {
        token = strtok(NULL, " ");
        if (strcmp(token, "get") == 0)
        {
            ret = "ack\n";
            token = strtok(NULL, " ");
            
            if (strcmp(token, "info") == 0)
            {
                ret = "coffee_machine cups=1,2,3,4\n";
            }
            else if (strcmp(token, "status") == 0)
            {
                ret = rgszStatus;
            }
            else if (strcmp(token, "times") == 0) // Times not currently implemented
            {
                ret = "times\n";
            }
            else
            {
                ret = "invalid\n";
            }
        }
        else if (strcmp(token, "post") == 0)
        {
            ret = "succeeded\n";
            
            token = strtok(NULL, " ");
            if (strcmp(token, "coffee") == 0)
            {
                token = strtok(NULL, " ");
                if (strcmp(token, "on") == 0)
                {
                    if (nCoffeeStatus != ON)
                    {
                        // Check for arguments
                        token = strtok(NULL, " "); // Arg1: <cups=amount> amount are comma separated positive integers with no spaces
                        if (token != NULL) // Make cup(s) of coffee
                        {
                            if (strcmp(strtok(token, "="), "cups") == 0)
                            {
                                token = strtok(NULL, "=");
                                strcpy(rgszNumCups, token);
                                
                                int nCups = string_to_uint(token);
                                
                                if (nCups <= MAX_CUPS)
                                {
                                    Timer0A_Disable();
                                    Timer0A_SetPeriodInMs(ONE_CUP * nCups); // TURN ON periodic timer to make nCups cups
                                    
                                    strcpy(rgszStatus, "on cups=");
                                    strcat(rgszStatus, rgszNumCups);
                                    strcat(rgszStatus, "\n");
                                    
                                    GPIO_A::Set(GPIO_BIT_5 | GPIO_BIT_6);
                                    nCoffeeStatus = ON;
                                    Timer0A_Enable();
                                }
                            }
                        }
                        else // on until it is turned off
                        {
                            Timer0A_Disable();
                            GPIO_A::Set(GPIO_BIT_5 | GPIO_BIT_6);
                            nCoffeeStatus = ON;
                            
                            strcpy(rgszStatus, "on\n");
                        }
                    }
                    else // already is on
                    {
                        ret = "failed\n";
                    }
                }
                else if (strcmp(token, "off") == 0)
                {
                    if (nCoffeeStatus != OFF)
                    {
                        Timer0A_Disable();
                        Timer0A_Reset();
                        
                        GPIO_A::Clear(GPIO_BIT_5 | GPIO_BIT_6);
                        strcpy(rgszStatus, "off\n");
                        nCoffeeStatus = OFF;
                    }
                    else
                    {
                        return "failed\n";
                    }
                }
                else
                {
                    ret = "invalid\n";
                }
            }
            else if (strcmp(token, "cancel") == 0) // ***** NOT IMPLEMENTED *****
            {
                // <time=hh:mm> 24hr military clock
                ret = "invalid\n";
            }
            else
            {
                ret = "invalid\n";  
            }
        }
        else
        {
            ret = "invalid\n";  
        }
    }
    else
    {
        ret = "invalid\n";
    }
    
    return ret;
}

void coffeeTask(void)
{
    switch (nCoffeeStatus)
    {
        case 0: // OFF
            Timer0A_Disable();
            Timer0A_Reset();
        
            GPIO_A::Clear(GPIO_BIT_5 | GPIO_BIT_6);
            strcpy(rgszStatus, "off\n");
            break;
        case 1: // ON: Finished brewing cups; go to WARM
            Timer0A_Disable();
            Timer0A_Reset();
        
            GPIO_A::Clear(GPIO_BIT_5 | GPIO_BIT_6);
            nCoffeeStatus = 2;
            strcpy(rgszStatus, "done cups=");
            strcat(rgszStatus, rgszNumCups);
            strcat(rgszStatus, "\n");
        
            Timer0A_SetPeriodInMs(WARM_OFF);
            Timer0A_Enable();
            break;
        case 2: // WARM (no brew, toggle)
            Timer0A_Disable();
            Timer0A_Reset();
            
            nWarm ^= 1;
            if (nWarm) // warmOn
            {
                Timer0A_SetPeriodInMs(WARM_ON);
                GPIO_A::Set(GPIO_BIT_5 | GPIO_BIT_6);
            }
            else // warmOff
            {
                Timer0A_SetPeriodInMs(WARM_OFF);
                GPIO_A::Clear(GPIO_BIT_5 | GPIO_BIT_6);
            }
            
            Timer0A_Enable();
            break;
        default:
            break;
    }
}

// GPIO handler for manual input switch should call this function
void coffeeToggle()
{
    // Debounce Switch
    //SysCtlDelay(1000000); // 25ms sleep
    SysCtlDelay(50000); // 1.25ms sleep
    
    if (GPIO_E::IsSet(GPIO_BIT_4) && !GPIO_E::Get(GPIO_BIT_4))
    {
        // Disable timer
        Timer0A_Disable();
        Timer0A_Reset();
        
        // Toggle between ON and OFF
        if (nCoffeeStatus == WARM || nCoffeeStatus == ON)
        {
            GPIO_A::Clear(GPIO_BIT_5 | GPIO_BIT_6);
            
            strcpy(rgszStatus, "off\n");
            nCoffeeStatus = OFF;
        }
        else if (nCoffeeStatus == OFF)
        {
            GPIO_A::Set(GPIO_BIT_5 | GPIO_BIT_6);
            
            strcpy(rgszStatus, "on\n");
            nCoffeeStatus = ON;
        }
    }
    
    GPIO_E::AcknowledgeInt(GPIO_BIT_4);
}

int main(void)
{
    // Initialize the board
    Tiva::InitClock();
    Debug::Init();
    Sys_Tick::Init();
    Console::Init();
    Timer0A_Init(coffeeTask, 1000000000); // 250ms timer
    
    GPIO_A::Init(GPIO_BIT_5 | GPIO_BIT_6, DIR_OUTPUT, DEN_ENABLE); // PA5 = Coffee Machine, PA6 = LED
    
    // Initialize Coffee machinee and LED
    GPIO_A::Clear(GPIO_BIT_5 | GPIO_BIT_6);
    
    // Initialize power switch
    GPIO_E::InitInterrupt(GPIO_BIT_4, IS_EDGE, IBE_SINGLE, IEV_LOW, &coffeeToggle);
    
    // Server takes care of SPI, etc. initialization
    Server::Init(IncomingMessageHandler, "VikBEE Brand Coffee Machine", "vikbee.coffee_machine.1.");

    // Run the server forever
    Server::Run();
}
