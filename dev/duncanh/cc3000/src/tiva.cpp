/*
 * tiva.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Useful functions specific to the Tiva C Series TM4C123G LaunchPad Evaluation Kit microcontroller
 */

#include <driverlib/rom_map.h>
#include <driverlib/sysctl.h>

#include "Tiva.h"

void Tiva::InitClock(void)
{
    // 16 MHz Crystal on Board. SSI Freq - configure M4 Clock to be ~50 MHz
    MAP_SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);
}
