/*
 * ip.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Useful functions for dealing with ip addresses
 */

#include "ip.h"

#include "Bitwise.h"

static char *byte_to_string(_In_ uint8_t uByte, _Out_writes_z_(return - psz) char *psz);
uint32_t string_to_uint(_In_z_ const char *psz);

uint32_t string_to_ip(_In_z_ const char *pszAddr)
{
    uint32_t uIpAddr = 0;

    for (int i = 0; *pszAddr && (i < 4); i++)
    {
        uIpAddr = (uIpAddr << 8) | string_to_uint(pszAddr);

        // Move to the next entry
        while ((*pszAddr >= '0') && (*pszAddr <= '9'))
        {
            pszAddr++;
        }
        if (*pszAddr)
        {
            pszAddr++;
        }
    }

    return uIpAddr;
}

char *ip_to_string(_In_ uint32_t uIpAddr, _Out_writes_z_(return - pszAddr) char *pszAddr)
{
    pszAddr = byte_to_string(BYTE_3(uIpAddr), pszAddr);
    *pszAddr = '.';
    pszAddr = byte_to_string(BYTE_2(uIpAddr), pszAddr + 1);
    *pszAddr = '.';
    pszAddr = byte_to_string(BYTE_1(uIpAddr), pszAddr + 1);
    *pszAddr = '.';
    pszAddr = byte_to_string(BYTE_0(uIpAddr), pszAddr + 1);
    *pszAddr = '\0';

    return pszAddr;
}



static char *byte_to_string(_In_ uint8_t uByte, _Out_writes_z_(return - psz) char *psz)
{
    int iLen;
    char rgcDigits[3];

    if (uByte == 0)
    {
        *psz = '0';
        return psz + 1;
    }

    // Cache the digits
    for (iLen = 0; uByte; iLen++)
    {
        rgcDigits[iLen] = uByte % 10;
        uByte /= 10;
    }

    // Digits are located backwards
    for (; iLen > 0; iLen--)
    {
        *psz = rgcDigits[iLen - 1] + '0';
        psz++;
    }

    *psz = '\0';
    return psz;
}

uint32_t string_to_uint(_In_z_ const char *psz)
{
    uint32_t uVal = 0;

    while ((*psz >= '0') && (*psz <= '9'))
    {
        uVal = (uVal * 10) + (*psz - '0');
        psz++;
    }

    return uVal;
}
