/*
 * Sys_Tick.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Implementation of Sys_Tick methods to be used with the CC3000
 */

#include <driverlib/sysctl.h>
#include <driverlib/systick.h>

#include <evnt_handler.h>

#include "sys_tick.h"

volatile uint64_t Sys_Tick::s_uTickCount = 0;

void Sys_Tick::Init(void)
{
    /* Init for a period of one ms */
    SysTickPeriodSet(SysCtlClockGet() / 1000);
    SysTickIntEnable();
    SysTickEnable();

    s_uTickCount = 0;
}

void Sys_Tick::Sleep(_In_ uint64_t uMilliseconds)
{
    uint64_t uCurrentCount = s_uTickCount;

    while ((s_uTickCount - uCurrentCount) < uMilliseconds)
    {
        // TODO: possibly enter low power state until interrupt
    }
}

uint64_t Sys_Tick::GetTickCount(void)
{
    return s_uTickCount;
}



/* Sys_TickHandler for handling un-solicited events if required */
#ifdef  __cplusplus
extern "C"
{
#endif
void SysTick_Handler(void)
{
    static unsigned long ulTickCount = 0;
    ulTickCount++;
    Sys_Tick::s_uTickCount++;

    // Handle twice every second
    if (ulTickCount >= 500)
    {
        hci_unsolicited_event_handler();
        ulTickCount = 0;
    }
}
#ifdef __cplusplus
}
#endif
