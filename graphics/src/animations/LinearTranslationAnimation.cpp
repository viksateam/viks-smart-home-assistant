/*
 * LinearTranslationAnimation.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines an animation that translates an IAnimatable object from one point to another target
 * point at a constant velocity. The velocity is in units of pixels-per-microsecond.
 */

#include "ConcurrentAnimation.h"
#include "LinearTranslationAnimation.h"

using namespace vik::graphics::animations;

/*
 * Constructors/Destructors
 */
LinearTranslationAnimation::LinearTranslationAnimation(
    _In_ IAnimatable *target,
    _In_ GLfloat endX,
    _In_ GLfloat endY,
    _In_ GLfloat vel) :
    AnimationBase(target),
    _endX(endX),
    _endY(endY),
    _velocity(vel)
{
    // The target should not be null
    assert(target);
    assert(vel > 0);
}



/* IAnimation */
void LinearTranslationAnimation::OnAnimationBegin(void)
{
    AnimationBase::OnAnimationBegin();

    // Need to calculate update information based off the IAnimatable's current location
    GLfloat deltaX = this->_endX - this->_target->GetX();
    GLfloat deltaY = this->_endY - this->_target->GetY();
    GLfloat distance = std::sqrt(deltaX * deltaX + deltaY * deltaY);

    // Duration (us) is distance (px) / velocity (px / us)
    this->_duration = distance / this->_velocity;

    // Protect against the possibility of a division by zero
    if (this->_duration == 0)
    {
        this->_duration = 1e-6f;
    }

    // _deltaX and _deltaY are really velocities (change in position (px) per update unit (us))
    this->_deltaX = deltaX / this->_duration;
    this->_deltaY = deltaY / this->_duration;
}

bool LinearTranslationAnimation::OnAnimationUpdate(_In_ uint64_t timeMicro)
{
    // Need to convert update time to GLfloat (for more accuracy in position updates)
    GLfloat time = static_cast<GLfloat>(timeMicro);

    // Ensure we don't move too far
    if (time > this->_duration)
    {
        time = this->_duration;
    }

    this->_target->Translate(this->_deltaX * time, this->_deltaY * time);

    this->_duration -= time;
    return this->_duration == 0;
}

void LinearTranslationAnimation::OnAnimationCompleted(void)
{
    AnimationBase::OnAnimationCompleted();

    // No action needs to be taken. Assert that the IAnimatable object is in the correct location
    // for debugging purposes
    assert(std::abs(this->_target->GetX() - this->_endX) < 1e-2);
    assert(std::abs(this->_target->GetY() - this->_endY) < 1e-2);
}
