/*
 * DeletionAnimation.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * The existence of this class is mostly a hack. The intent of this animation is to delete the
 * target IAnimatable object when the animation is run. Thus, when this object is submitted, it is
 * assumed that all references to the target object have been removed and it is okay to destroy the
 * object.
 */

#include "DeletionAnimation.h"

using namespace vik::graphics::animations;

/*
 * Constructors/Destructor
 */
DeletionAnimation::DeletionAnimation(_In_ IAnimatable *target) :
    AnimationBase(target)
{
}



/*
 * IAnimation
 */
bool DeletionAnimation::OnAnimationUpdate(_In_ uint64_t timeMicro)
{
    (void)timeMicro;

    // This is not a "true" animation. All we need to do is delete the object which only needs to
    // be done once (and thus, one update).
    return true;
}

void DeletionAnimation::OnAnimationCompleted(void)
{
    AnimationBase::OnAnimationCompleted();

    delete this->_target;
    this->_target = nullptr;
}

void DeletionAnimation::OnAnimationCancelled(void)
{
    // A DeletionAnimation should never be cancelled since that implies a reference to the target
    // object. However, it is possible for multiple deletion aminations to get scheduled for the
    // same object on program termination, so just go ahead and cancel (delay) the deletion
    AnimationBase::OnAnimationCancelled();
    this->_target = nullptr;
}
