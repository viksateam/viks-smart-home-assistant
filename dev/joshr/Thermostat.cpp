/*
 * main.cpp
 *
 * Author(s):
 *      Josh Randall
 *
 * The main implementation for the thermostat appliance
 */

#include "Console.h"
#include "Debug.h"
#include "GPIO.h"
#include "Server.h"
#include "sys_tick.h"
#include "tiva.h"

#include <string.h>

int Fan_setting = 0; // 1 is on, 0 is auto
int Fan_needed = 0; // Fan will be used 
int heat = 0; // 1 is heat, 0 is cool
int Temp_setting = 70; 
int Temp_actual = 70;

static char *IncomingMessageHandler(_Inout_ char *pszMessage)
{
    Console::WriteLine(pszMessage);
    
    // Parse message
    if (strcmp(pszMessage, "heat") == 0)
    {
		heat = 1;
        //GPIO_A::Set(GPIO_BIT_7);
    }
    else if (strcmp(pszMessage, "cool") == 0)
    {
		heat = 0;
        //GPIO_A::Clear(GPIO_BIT_7);
    }
	else if (strcmp(pszMessage, "fan_on") == 0)
    {
		Fan_setting = 1;
        //GPIO_A::Clear(GPIO_BIT_7);
    }
	else if (strcmp(pszMessage, "fan_auto") == 0)
    {
		Fan_setting = 0;
        //GPIO_A::Clear(GPIO_BIT_7);
    }
	else if (strcmp(pszMessage, "get_temp") == 0)
    {
		//Send the current actual temperature to the control station
        //GPIO_A::Clear(GPIO_BIT_7);
    }
	else if (strcmp(pszMessage, "set_temp") == 0)
    {
		//Find a way to handle a number to set the temperature
        //GPIO_A::Clear(GPIO_BIT_7);
    }
    else
    {
        return "FAIL\n";
    }
    
    return "OK\n";
}

int main(void)
{
    // Initialize the board
    Tiva::InitClock();
    Debug::Init();
    Sys_Tick::Init();
    Console::Init();
    GPIO_A::Init(GPIO_BIT_7, DIR_OUTPUT);

    // Server takes care of SPI, etc. initialization
    Server::Init(IncomingMessageHandler);

    // Run the server forever
    Server::Run();
	
	while(1){
		Temp_actual = getTemp();
		if(heat == 2){
			if(Temp_actual < Temp_setting){
				Heat_on();
				Fan_needed = 1;
			} else{
				Heat_off();
				Fan_needed = 0;
			}
		} else if(heat == 0){
			if(Temp_actual > Temp_setting){
				Cool_on();
				Fan_needed = 1;
			} else{
				Cool_off();
				Fan_needed = 0;
			}
		} else{
			Heat_off();
			Cool_off();
			Fan_needed = 0;
		}
		if (Fan_setting == 1){
			Fan_on();
		} else{
			if(Fan_needed == 1){
				Fan_on();
			} else{
				Fan_off();
			}
		}
	}
}

public int getTemp(){
	// Turn on sensing circuit
	// Wait momentarily
	// Sample ADC and convert
	// Turn off sensing circuit
	// return actual temperature
	
	return 70;
}

public void setTemp(int Temp_setting){
	
}

public void Heat_on(){

}

public void Heat_off(){

}

public void Cool_on(){

}

public void Cool_off(){

}

public void Fan_on(){

}

public void Fan_off(){

}