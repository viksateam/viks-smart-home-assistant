/*
 * ShaGesture.cpp
 * 
 * Author(s):
 *      Jerry Lin
 *
 * Provides underlying structure for Sha-specific gestures.
 */

#include "ShaGesture.h"

using namespace sha;
using namespace sha::leap;

static int ShaGestureID = 0;
int ShaGesture::CreateID(void)
{
    return ShaGestureID++;
}



/*
 * Constructors / Destructors
 */
ShaGesture::ShaGesture(void)
{
    this->_id = ShaGesture::CreateID();
    this->_frameDuration = 0;
}



int ShaGesture::GetId(void) const
{
    return this->_id;
}

GestureState ShaGesture::GetState(void) const
{
    return this->_state;
}

GestureType ShaGesture::GetType(void) const
{
    return GT_INVALID;
}

void ShaGesture::Update(const Frame& frame)
{
    this->_frameDuration++;
}
