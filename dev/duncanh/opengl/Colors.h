/*
 * Colors.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines commonly used colors for the Smart Home Assistant. In general, you should use one of
 * these "theme" colors instead of defining your own in a separate file. Consult with all other
 * developers doing UI work before adding a color to this file as there might be a more suitable
 * existing color.
 */
#pragma once

#ifdef  WIN32
#include <Windows.h>
#endif  /* WIN32 */

#include <GL/gl.h>

namespace sha
{
    namespace graphics
    {
        struct Color
        {
            union
            {
                GLfloat value[4];
                struct
                {
                    GLfloat r;
                    GLfloat g;
                    GLfloat b;
                    GLfloat a;
                };
            };

            operator const GLfloat *(void) const
            {
                return this->value;
            }
        };

        class Colors
        {
        public:

            /* Standard colors */
            static const Color Red;
            static const Color Green;
            static const Color Blue;
            static const Color Magenta;
            static const Color Cyan;
            static const Color Yellow;
            static const Color Black;
            static const Color White;
            static const Color Transparent;

            /* "Theme" colors */
            static const Color BackgroundColor;
        };

        /* Useful wrapper functions when a corresponding gl*4fv function does not exist */
        inline GLvoid ClearColor(const Color &color)
        {
            glClearColor(color.r, color.g, color.b, color.a);
        }
    }
}

