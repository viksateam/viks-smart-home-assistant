/*
 * UDPSocket.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Useful class for dealing with UDP sockets
 */

#define NOMINMAX

#include <algorithm>
#include <cassert>

#include "UDPSocket.h"

using namespace vik::communication;

/*
 * Constructors/Destructor
 */
UDPSocket::UDPSocket(void) :
    _socket(INVALID_SOCKET),
    _nErrorCode(0),
    _state(UDPSOCKET_CLOSED)
{
}

UDPSocket::UDPSocket(_In_ USHORT usPort) :
    UDPSocket()
{
    this->Open(usPort);
}



bool UDPSocket::IsConnected(void) const
{
    return this->_state == UDPSOCKET_BOUND;
}

int UDPSocket::GetError(void) const
{
    return this->_nErrorCode;
}



bool UDPSocket::Open(_In_ USHORT usPort)
{
    // Open the socket if it is not yet open
    if (this->_state == UDPSOCKET_CLOSED && !this->_OpenSocket())
    {
        return false;
    }

    if (this->_state != UDPSOCKET_OPEN)
    {
        this->_nErrorCode = WSAEISCONN;
        return false;
    }

    // Bind the socket
    sockaddr_in addr = { 0 };
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(usPort);
    if (bind(this->_socket, (sockaddr *)&addr, sizeof(addr)) == SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
        return false;
    }

    this->_state = UDPSOCKET_BOUND;
    return true;
}

bool UDPSocket::Close(void)
{
    if (this->_state == UDPSOCKET_CLOSED)
    {
        this->_nErrorCode = WSAENOTCONN;
        return false;
    }

    this->_state = UDPSOCKET_CLOSED;
    if (closesocket(this->_socket) == SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
        return false;
    }

    return true;
}

_Success_(return >= 0)
int UDPSocket::Send(
    _In_ const InetAddress &inetAddr,
    _In_reads_(nLen) const void *pBuffer,
    _In_ int nLen)
{
    if (this->_state != UDPSOCKET_BOUND)
    {
        this->_nErrorCode = WSAENOTCONN;
        return SOCKET_ERROR;
    }

    sockaddr_in addr = inetAddr;
    int nResult =
        sendto(this->_socket, (const char *)pBuffer, nLen, 0, (sockaddr *)&addr, sizeof(addr));

    if (nResult == SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
    }

    return nResult;
}

_Success_(return >= 0)
int UDPSocket::Broadcast(_In_ USHORT usPort, _In_reads_(nLen) const void *pBuffer, _In_ int nLen)
{
    char name[80];
    if (gethostname(name, sizeof(name)) == SOCKET_ERROR)
    {
        this->Close();
        this->_nErrorCode = WSAGetLastError();
        return SOCKET_ERROR;
    }

    hostent *host = gethostbyname(name);
    if (!host)
    {
        this->Close();
        this->_nErrorCode = WSAGetLastError();
        return SOCKET_ERROR;
    }

    int nSent = SOCKET_ERROR;
    for (int i = 0; host->h_addr_list[i]; i++)
    {
        // NOTE: assume network mask of 255.255.255.0
        ULONG ulIpAddr = ntohl(((in_addr *)host->h_addr_list[i])->s_addr);
        ulIpAddr &= 0xFFFFFF00;
        ulIpAddr |= ~(0xFFFFFF00);

        int nBytes = this->Send(InetAddress(ulIpAddr, usPort), pBuffer, nLen);
        nSent = std::max(nSent, nBytes);
    }

    return nSent;
}

_Success_(return >= 0)
int UDPSocket::Receive(
    _Out_writes_bytes_to_(nLen, return) void *pBuffer,
    _In_ int nLen,
    _Out_opt_ InetAddress *pInetAddr)
{
    if (this->_state != UDPSOCKET_BOUND)
    {
        this->_nErrorCode = WSAENOTCONN;
        return SOCKET_ERROR;
    }

    sockaddr_in addr = { 0 };
    int nAddrLen = sizeof(addr);
    int nResult = recvfrom(this->_socket, (char *)pBuffer, nLen, 0, (sockaddr *)&addr, &nAddrLen);

    if (nResult == SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
    }

    if (pInetAddr)
    {
        *pInetAddr = addr;
    }

    return nResult;
}



/*
 * Private Functions
 */
bool UDPSocket::_OpenSocket(void)
{
    assert(this->_state == UDPSOCKET_CLOSED);
    this->_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (this->_socket == INVALID_SOCKET)
    {
        this->_nErrorCode = WSAGetLastError();
        return false;
    }

    int nOpt = TRUE;
    if (setsockopt(this->_socket, SOL_SOCKET, SO_BROADCAST, (char *)&nOpt, sizeof(nOpt)) ==
        SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
        return false;
    }

    this->_state = UDPSOCKET_OPEN;
    return true;
}
