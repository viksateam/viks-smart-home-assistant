/*
 * Server.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * An implementation of a server for the Tiva C Series TM4C123G LaunchPad Evaluation Kit with the
 * CC3000 Booster Pack. This server does little more than open up a listening socket on some
 * specified port, accept incoming connections, relay incoming messages, and send responses.
 */
#pragma once

#include "sha.h"

#include "socket.h"
#include "wlan.h"

#define INVALID_SOCKET                  (~0)

// From the VikBEE Standard
#define SHA_PORT                        (1992)
#define SHA_BROADCAST_PORT              (12392)
#define SENTINEL_CHARACTER              ('\n')

#define IP_ADDRESS_MAX_LEN              (16)        // 4 bytes, 3 periods, and 1 null
#define MAX_CONNECTIONS                 (3)
#define CONNECTION_BUFFER_LEN           (256)
#define BROADCAST_BUFFER_LEN            (256)

#define MAX_ID_LEN                      (64)
#define MAX_NAME_LEN                    (64)

#define DHCP_LEASE_TIMEOUT_INF          (0)         // 0 or 0xFFFFFFFF = infinity
#define ARP_REFRESH_TIMEOUT_INF         (0)         // 0 or 0xFFFFFFFF = infinity
#define KEEPALIVE_EVENT_TIMEOUT_INF     (0)         // 0 or 0xFFFFFFFF = infinity
#define SOCKET_INACTIVITY_TIMEOUT_INF   (0)         // 0 or 0xFFFFFFFF = infinity

typedef char *(*MessageHandler)(_Inout_ char *pszMessage);

class Server
{
private:
    /* Can't instantiate an instance of a Server object */
    Server(void) {}

    struct Connection
    {
        long lSocketHandle;
        char rgszAddr[IP_ADDRESS_MAX_LEN];

        char rgcBuffer[CONNECTION_BUFFER_LEN];
        int nBuffLen;
        char *pcBufferIn;

        void reset(void)
        {
            lSocketHandle = INVALID_SOCKET;
            nBuffLen = 0;
            pcBufferIn = rgcBuffer;
        }
    };

public:

    static void Init(_In_ MessageHandler pfnHandler, _In_opt_ const char *szName = NULL, _In_opt_ const char *szIdHeader = NULL);

    static bool Connect(
        _In_z_ char *pszSsid,
        _In_opt_ int nEncryptionType = WLAN_SEC_UNSEC,
        _In_opt_ const char *pszPassword = NULL);
    static bool InitServerSocket(void);
    static bool CloseServerSocket(void);
    static bool InitBroadcastSocket(void);
    static bool CloseBroadcastSocket(void);

    static bool BroadcastMessage(_In_ char *pszMessage);

    static bool Run(void);

    static const char *GetIpAddress(void);

private:

    static bool _CloseConnection(_Inout_ Connection &conn);
    static bool _ResetConnections(void);
    static void _CloseTerminatedConnections(void);
    static bool _OnNewConnection(_In_ long lSocketHandle, _In_ sockaddr_in &addr);

    static bool _ProcessIncomingMessages(void);
    static bool _ProcessIncomingBroadcasts(void);
    static bool _OnConnectionReceivedData(_Inout_ Connection &conn);
    static char *_GetNextCommand(_Inout_ Connection &conn);

    static void _WlanCallback(_In_ long lEventType, char *pszData, unsigned char ucLength);
    static char *_SendNullPatch(_Out_ unsigned long *pulLength);

    static volatile bool s_fCheckSocket;
    static volatile bool s_fConfig;
    static volatile bool s_fConnected;
    static volatile bool s_fIPValid;
    static volatile bool s_fRunning;
    static volatile bool s_fShutdownOkay;

    static long s_lSocketHandle;
    static long s_lBroadcastSocketHandle;

    static MessageHandler s_pfnMessageHandler;
    static const char *s_pszName;
    static const char *s_pszId;

    static char s_rgcBroadcastRecvbuf[BROADCAST_BUFFER_LEN];
    static char s_rgszIPAddress[IP_ADDRESS_MAX_LEN];
    static Connection s_rgConnections[MAX_CONNECTIONS];
    static int s_nNumConnections;
    
    static long s_lBroadcastIPAddress;
};
