/*
 * FocusPoint.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Used as a point of emphasis on the screen, generally as a cursor or something similar. A
 * FocusPoint differs from other UIElemnts in that it does not obey its positioning precisely, so
 * you should avoid using this inside of layouts. The center of the FocusPoint is located at its
 * (x, y) value and expands radius pixels in all directions. Use res to set the resolution of the
 * circle that represents the FocusPoint, and stops to set the number of color changes that occur
 * within the FocusPoint.
 *
 * Visually, a FocusPoint begins with its ForegroundColor in the center and progressively changes
 * to its BackgroundColor.
 */

#include "FocusPoint.h"

using namespace vik::graphics;

/*
 * Constructors
 */
FocusPoint::FocusPoint(_In_ int res, _In_ int stops) :
    _res(res),
    _stops(stops)
{
    this->_GeneratePoints();
}

FocusPoint::FocusPoint(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat radius,
    _In_ int res,
    _In_ int stops) :
    UIElement(x, y, radius, radius),
    _res(res),
    _stops(stops)
{
    this->_GeneratePoints();
}

FocusPoint::FocusPoint(_In_ const Point &pt, _In_ GLfloat radius, _In_ int res, _In_ int stops) :
    UIElement(pt, radius, radius),
    _res(res),
    _stops(stops)
{
    this->_GeneratePoints();
}



/*
 * IAnimatable
 */
#pragma region IAnimatable
void FocusPoint::SetWidth(_In_ GLfloat width)
{
    UIElement::SetWidth(width);
    UIElement::SetHeight(width);
}

void FocusPoint::SetHeight(_In_ GLfloat height)
{
    UIElement::SetWidth(height);
    UIElement::SetHeight(height);
}

void FocusPoint::SetRect(_In_ const Rect &rc)
{
    // There is no reason anyone should ever try and manually set the bounding rectangle for the
    // FocusPoint (since it is always a circle). Therefore, assert fail on debug builds and do
    // nothing otherwise
    (void)rc;
    assert(false);
}

GLfloat FocusPoint::IncreaseWidth(_In_ GLfloat deltaWidth)
{
    GLfloat width = UIElement::IncreaseWidth(deltaWidth);
    UIElement::SetHeight(width);

    return width;
}

GLfloat FocusPoint::IncreaseHeight(_In_ GLfloat deltaHeight)
{
    GLfloat height = UIElement::IncreaseHeight(deltaHeight);
    UIElement::SetWidth(height);

    return height;
}
#pragma endregion



/*
 * UIElement
 */
#include "Drawing.h"
GLvoid FocusPoint::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();
    const Color &outerColor = this->GetBackgroundColor();
    const Color &innerColor = this->GetForegroundColor();

    GLfloat radius = this->GetWidth();

    // Determine the proper delta for making a "smooth" transition from the outer to inner color
    GLfloat deltaR = (innerColor.r - outerColor.r) / this->_stops;
    GLfloat deltaG = (innerColor.g - outerColor.g) / this->_stops;
    GLfloat deltaB = (innerColor.b - outerColor.b) / this->_stops;
    GLfloat deltaA = (innerColor.a - outerColor.a) / this->_stops;

    for (int i = 0; i <= this->_stops; i++)
    {
        Color color = Colors::FromRGBA(outerColor.r + deltaR * i,
                                       outerColor.g + deltaG * i,
                                       outerColor.b + deltaB * i,
                                       outerColor.a + deltaA * i);
        glColor4fv(color);
        glBegin(GL_TRIANGLE_STRIP);

        for (size_t j = 0; j < this->_points.size(); j += this->_stops + 2)
        {
            // The points of interest are at _points[j + i] and _points[j + i + 1]
            Point pt1 = this->_points[j + i];
            Point pt2 = this->_points[j + i + 1];

            pt1.x = pt1.x * radius + offX;
            pt1.y = pt1.y * radius + offY;
            pt2.x = pt2.x * radius + offX;
            pt2.y = pt2.y * radius + offY;

            glVertex2fv(pt1);
            glVertex2fv(pt2);
        }

        glEnd();
    }
}



/*
 * Private Functions
 */
void FocusPoint::_GeneratePoints(void)
{
    assert(this->_res >= 3);
    assert(this->_stops >= 1);
    this->_points.clear();

    // res is the number of points to draw. Therefore, each angle increment is (2 * PI) / res
    GLfloat angle_delta = (float)(2.0f * M_PI) / this->_res;
    GLfloat angle = 0;

    // Need one extra iteration because GL_TRIANGLE_STRIP does not loop
    for (int i = 0; i <= this->_res; i++)
    {
        GLfloat x = cos(angle);
        GLfloat y = sin(angle);

        // Add one point for each stop (with an extra for the outer edge)
        for (int j = 0; j <= this->_stops + 1; j++)
        {
            GLfloat ratio = (GLfloat)(this->_stops + 1 - j) / (this->_stops + 1);
            this->_points.emplace_back(x * ratio, y * ratio);
        }

        angle += angle_delta;
    }
}
