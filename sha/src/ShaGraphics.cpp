/*
 * ShaGraphics.cpp
 *
 * Author(s):
 *      Duncan Horn
 *      Jerry Lin
 *
 * The various functions used by the Smart Home Assistant for performing important operations
 * regarding graphical output. In general, these functions should not be called directly except by
 * the windowing code.
 */

#include <map>
#include <thread>

#include "Colors.h"
#include "DelayAnimation.h"
#include "DeletionAnimation.h"
#include "FocusPoint.h"
#include "HomePage.h"
#include "ITunesElement.h"
#include "LinearTranslationAnimation.h"
#include "Notification.h"
#include "RotaryLayout.h"
#include "ShaColors.h"
#include "ShaCommunicationManager.h"
#include "ShaConnectionManager.h"
#include "ShaFont.h"
#include "ShaGraphics.h"
#include "ShaLeap.h"
#include "WindowHelpers.h"

#include "iTunesCOMInterface.h"

using namespace Microsoft::WRL;

using namespace vik;
using namespace vik::communication;
using namespace vik::graphics;
using namespace vik::graphics::animations;
using namespace vik::leap;

#define POINTER_MOVE_VELOCITY       (.005f)
#define NOTIFICATION_MOVE_VELOCITY  (.0005f)

#define NOTIFICATION_DISPLAY_TIME   (5 * US_IN_SEC)
#define POINTER_TAP_DELAY           (100 * US_IN_MS)



GLsizei vik::graphics::g_WindowWidth;
GLsizei vik::graphics::g_WindowHeight;

/* Cache the AnimationManager instance */
static AnimationManager *animationManager = AnimationManager::GetInstance();

/* Global graphics objects */
bool vik::graphics::g_PointerIsVisible;
bool vik::graphics::g_PointerIsPressed;
static FocusPoint pointerFocusPoint(0, 0, 30, 20, 20);

static RotaryLayout layout;

static animation_cookie notificationCookie = INVALID_ANIMATION_COOKIE;
static std::vector<Notification *> notifications;

ShaStation *thisStation;

/* Keep a map of request cookies to request information structures */
static std::map<request_cookie, RequestInfo> pendingRequests;

bool _itunesUp = false;;
static void _OnITunesClosed(void);



/*
 * Graphics Functions
 */
GLvoid vik::graphics::InitializeGraphics(_In_ GLsizei width, _In_ GLsizei height)
{
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glMatrixMode(GL_PROJECTION);
    glOrtho(0, width, height, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);

    g_WindowWidth = width;
    g_WindowHeight = height;

    // Setup global graphics objects
    g_PointerIsVisible = false;
    pointerFocusPoint.SetForegroundColor(ShaColors::PointerFocusPointColor);
    pointerFocusPoint.SetBackgroundColor(Colors::Transparent);

    layout.SetRect(Rect(0, 0, (GLfloat)width, (GLfloat)height));
    layout.SetForegroundColor(ShaColors::ThemeColor);
    layout.AddChild(new HomePage());

    // Setup Leap Motion Controller
    leap::InitializeLeap();

    // Initialize the host station
    thisStation = new ShaStation(SST_CONTROL_STATION, InetAddress(),
        ShaCommunicationManager::StationId(), ShaCommunicationManager::StationName());
}

GLvoid vik::graphics::OnClose(void)
{
    // When we delete all ShaStations, theres an odd situation where the RotaryLayout attempts to
    // deallocate the ShaStation's target element, resulting in a double delete bug. Therefore, we
    // must first remove all stations (that have element representations) from the RotaryLayout.
    ShaConnectionManager::ForEach([](_In_ ShaStation *station)
    {
        auto elem = station->GetElementRepresentation();
        if (elem)
        {
            layout.RemoveChild(elem);
        }
    });
    ShaConnectionManager::Destroy();

    // Destroy any active/pending animations
    for (auto notification : notifications)
    {
        delete notification;
    }
    notifications.clear();

    auto elem = thisStation->GetElementRepresentation();
    if (elem)
    {
        layout.RemoveChild(elem);
    }
    delete thisStation;
}

GLvoid vik::graphics::DrawSmartHomeAssistant(void)
{
    // First, set the background color
    ClearColor(ShaColors::BackgroundColor);
    glClear(GL_COLOR_BUFFER_BIT);

    // Update all animations
    animationManager->Update();

    layout.Draw(0, 0);

    // Draw the currently animating notification. Note that we always draw animations relative to
    // the lowest 100 pixels of the window.
    if (!notifications.empty())
    {
        notifications[0]->Draw(0, (GLfloat)g_WindowHeight - 100);
    }

    // Draw the focus point if it is on-screen
    if (g_PointerIsVisible)
    {
        pointerFocusPoint.Draw(0, 0);
    }
}

GLvoid vik::graphics::PostNotification(_In_ std::string message)
{
    Notification *notification = new Notification(message, 0, 100, (GLfloat)g_WindowWidth, 100);
    notification->SetBackgroundColor(ShaColors::ThemeColor);
    notification->SetForegroundColor(ShaColors::ForegroundColor);
    notifications.push_back(notification);

    // Animate up, delay, then down. Delete and remove notification on completion
    auto up = new LinearTranslationAnimation(notification, 0, 0, NOTIFICATION_MOVE_VELOCITY);
    auto delay = new DelayAnimation(notification, NOTIFICATION_DISPLAY_TIME);
    auto down = new LinearTranslationAnimation(notification, 0, 100, NOTIFICATION_MOVE_VELOCITY);
    auto remove = new DeletionAnimation(notification);
    down->SetAnimationCompletedCallback([](_In_ IAnimation *sender, _In_ IAnimatable *target)
    {
        (void)sender;
        (void)target;

        // Must be the first notification in the list
        assert(!notifications.empty());
        notifications.erase(std::begin(notifications));

        if (!notifications.empty())
        {
            notifications[0]->SetWidth((GLfloat)g_WindowWidth);
        }
    });

    notificationCookie = animationManager->SubmitPendingAnimation(up, notificationCookie);
    if (notificationCookie == INVALID_ANIMATION_COOKIE)
    {
        notificationCookie = animationManager->SubmitAnimation(up);
    }

    notificationCookie = animationManager->SubmitPendingAnimation(delay, notificationCookie);
    notificationCookie = animationManager->SubmitPendingAnimation(down, notificationCookie);
    notificationCookie = animationManager->SubmitPendingAnimation(remove, notificationCookie);
    assert(notificationCookie != INVALID_ANIMATION_COOKIE);
}



/*
 * Event/Message Handling Functions
 */
GLvoid vik::graphics::OnWindowResize(_In_ GLsizei width, _In_ GLsizei height)
{
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);

    g_WindowWidth = width;
    g_WindowHeight = height;

    layout.SetRect(Rect(0, 0, (GLfloat)width, (GLfloat)height));

    if (!notifications.empty())
    {
        notifications[0]->SetWidth((GLfloat)g_WindowWidth);
    }
}

GLvoid vik::graphics::OnKeyPressed(_In_ int keyCode)
{
#ifdef  WIN32
    switch (keyCode)
    {
    case VK_LEFT:
        layout.AdvanceLeft();
        break;

    case VK_RIGHT:
        layout.AdvanceRight();
        break;
    }
#else
    (void)keyCode;
#endif  /* WIN32 */
}

GLvoid vik::graphics::OnPointerEntered(_In_ GLfloat x, _In_ GLfloat y)
{
    assert(!g_PointerIsVisible);
    g_PointerIsVisible = true;
    pointerFocusPoint.SetLocation(Point(x, y));

    Point pt(x - layout.GetX(), y - layout.GetY());
    layout.OnPointerEntered(pt);
}

GLvoid vik::graphics::OnPointerExited(void)
{
    assert(g_PointerIsVisible);
    g_PointerIsVisible = false;
    pointerFocusPoint.CancelCurrentAnimation();

    layout.OnPointerExited();
}

GLvoid vik::graphics::OnPointerMoved(_In_ GLfloat x, _In_ GLfloat y)
{
    auto ani = new LinearTranslationAnimation(&pointerFocusPoint, x, y, POINTER_MOVE_VELOCITY);
    pointerFocusPoint.SubmitOverrideAnimation(ani);

    Point pt(x - layout.GetX(), y - layout.GetY());
    layout.OnPointerMoved(pt);
}

GLvoid vik::graphics::OnPointerTapped(_In_ GLfloat x, _In_ GLfloat y)
{
    // We use a static variable here as we need a reference later on when the DelayAnimation
    // completes. This can cause issues if the user quickly "double taps," but it's unlikely that
    // they will notice and just assume that the pointer moved while down.
    static Point pt(0, 0);
    pt = Point(x - layout.GetX(), y - layout.GetY());
    layout.OnPointerDown(pt);

    // Use a short delay so we get the pointer "down" effect
    DelayAnimation *ani = new DelayAnimation(&layout, POINTER_TAP_DELAY);
    ani->SetAnimationCompletedCallback([](_In_ IAnimation *sender, _In_ IAnimatable *target)
    {
        (void)sender;
        (void)target;
        layout.OnPointerUp(pt);
    });
    layout.SubmitOverrideAnimation(ani);
}

GLvoid vik::graphics::OnPointerPressed(_In_ GLfloat x, _In_ GLfloat y)
{
    Point pt(x - layout.GetX(), y - layout.GetY());
    layout.OnPointerDown(pt);

    g_PointerIsPressed = true;
}

GLvoid vik::graphics::OnPointerReleased(_In_ GLfloat x, _In_ GLfloat y)
{
    Point pt(x - layout.GetX(), y - layout.GetY());
    layout.OnPointerUp(pt);

    g_PointerIsPressed = false;
}

GLvoid vik::graphics::OnPinchGesture(_In_ GLfloat x, _In_ GLfloat y)
{
    (void)x;
    (void)y;
}

GLvoid vik::graphics::OnPinchReleasedGesture(_In_ GLfloat x, _In_ GLfloat y)
{
    (void)x;
    (void)y;
}

GLvoid vik::graphics::OnSwipeGesture(_In_ SwipeDirection swipeDirection)
{
    switch (swipeDirection)
    {
    case SD_LEFT:
        layout.AdvanceRight();
        break;

    case SD_RIGHT:
        layout.AdvanceLeft();
        break;
    }
}



static void _OnStationDisconnect(_In_ ShaStation *station)
{
    if (station->Type() == SST_APPLIANCE_STATION)
    {
        PostNotification(station->Name() + " has disconnected from Vik's Smart Home System");
    }

    auto elem = station->GetElementRepresentation();
    if (elem)
    {
        layout.RemoveChild(elem);
    }

    // Remove all pending messages from the message queue
    for (auto itr = std::begin(pendingRequests); itr != std::end(pendingRequests); )
    {
        auto remove = itr;
        ++itr;

        if (remove->second.targetId == station->Id())
        {
            pendingRequests.erase(remove);
        }
    }

    bool success = ShaConnectionManager::RemoveStation(station->Id());
    assert(success);
    (void)success;
}

GLvoid vik::graphics::CheckStationTimeouts(void)
{
    std::vector<ShaStation *> timedOut;

    // Need to get the status if we are running locally :P
    if (IsITunesRunning())
    {
        auto itunes = dynamic_cast<ITunes *>(thisStation);
        if (itunes)
        {
            itunes->PostGetStatusRequest();
        }
    }

    ShaConnectionManager::ForEach([&](_In_ ShaStation *station)
    {
        if (!station->IsAlive())
        {
            // We cannot do the removal here as ShaConnectionManager::ForEach runs a for-each loop
            // (and we cannot remove an item we hold an iterator to)
            timedOut.push_back(station);
        }
        else
        {
            station->SetAlive(false);

            // Send a heartbeat message to the station
            auto cookie = ShaCommunicationManager::PostHeartbeatMessage(station);

            if (cookie != INVALID_REQUEST_COOKIE)
            {
                SubmitStationRequest(cookie, RequestInfo{ station->Id(),
                    [](_In_ const RequestInfo &sender, _In_ const char *msg)
                {
                    // Check to see if the station has switched to iTunes
                    if (msg && strcmp(msg, "invalid"))
                    {
                        auto station = ShaConnectionManager::GetStation(sender.targetId);

                        if (station && station->Type() == SST_CONTROL_STATION)
                        {
                            auto newStation = new ShaStation(SST_APPLIANCE_STATION,
                                station->InetAddr(), station->Id(), station->Name());
                            ShaConnectionManager::RemoveStation(station->Id());

                            OnStationConnect(newStation);
                        }
                    }
                }});
            }
        }
    });

    for (auto &station : timedOut)
    {
        _OnStationDisconnect(station);
    }
}

GLvoid vik::graphics::OnStationConnect(_In_ ShaStation *station)
{
    if (ShaConnectionManager::HasStation(station->Id()))
    {
        // It is possible for a station to re-send its connection string (for various reasons), so
        // just ignore the connection if we already have the information cached
        delete station;
        return;
    }

    bool success = ShaConnectionManager::AddStation(station);
    assert(success);
    (void)success;

    auto cookie = ShaCommunicationManager::PostServerRequest(station, RT_GET, "info");
    SubmitStationRequest(cookie, RequestInfo{ station->Id(),
        [](_In_ const RequestInfo &sender, _In_ const char *msg)
    {
        if (!msg || !strcmp(msg, "invalid"))
        {
            return;
        }

        // Use the response to change the type of the station
        std::istringstream stream(msg);
        std::string type;
        std::vector<std::string> features;

        stream >> type;
        while (!stream.fail() && !stream.eof())
        {
            std::string arg;
            stream >> arg;
            features.push_back(arg);
        }

        ShaStation *station = ShaConnectionManager::GetStation(sender.targetId);
        if (station)
        {
            ShaStation *replaceStation = ShaStation::Convert(station, type, features);
            if (replaceStation)
            {
                // Specialized station; raise a notification and replace the object in the
                // ShaConnectionManager
                bool success = ShaConnectionManager::UpdateStation(replaceStation);
                assert(success);
                (void)success;

                layout.AddChild(replaceStation->GetElementRepresentation());
                PostNotification(replaceStation->Name() +
                    " has connected to Vik's Smart Home System");
            }
        }
    }});
}

GLvoid vik::graphics::OnStationDisconnect(_Inout_ char *id)
{
    // Ignore if the station does not exist (as it is possible for the UDP broadcast packet to get
    // dropped)
    auto station = ShaConnectionManager::GetStation(id);
    if (station)
    {
        _OnStationDisconnect(station);
    }

    // We are expected to handle resource management for the message
    delete[] id;
}

GLvoid vik::graphics::OnStationResponse(_In_ request_cookie cookie, _Inout_ char *msg)
{
    if (pendingRequests.find(cookie) != std::end(pendingRequests))
    {
        auto info = pendingRequests[cookie];

        auto station = ShaConnectionManager::GetStation(info.targetId);
        if (station)
        {
            station->SetAlive(true);

            if (info.func)
            {
                info.func(info, msg);
            }
        }

        pendingRequests.erase(cookie);
    }

    // We are expected to handle resource management for the message
    delete[] msg;
}

bool vik::graphics::SubmitStationRequest(_In_ request_cookie cookie, _In_ const RequestInfo &info)
{
    if (cookie != INVALID_REQUEST_COOKIE)
    {
        assert(pendingRequests.find(cookie) == std::end(pendingRequests));
        pendingRequests[cookie] = info;
        return true;
    }

    return false;
}



void vik::graphics::StartITunes(void)
{
    std::thread proc([](void)
    {
        if (!_itunesUp)
        {
            // Need to check to see which version of iTunes is installed
            DWORD dwAttr = GetFileAttributes(TEXT("C:\\Program Files\\iTunes"));
            LPCTSTR lpPath;

            if (dwAttr != INVALID_FILE_ATTRIBUTES && (dwAttr & FILE_ATTRIBUTE_DIRECTORY))
            {
                lpPath = TEXT("\"C:\\Program Files\\iTunes\\iTunes.exe\"");
            }
            else
            {
                lpPath = TEXT("\"C:\\Program Files (x86)\\iTunes\\iTunes.exe\"");
            }

            ShellExecute(GetDesktopWindow(), nullptr, lpPath, nullptr, nullptr, SW_SHOWMINIMIZED);

            HRESULT hr = ::CoInitialize(0);
            if (SUCCEEDED(hr))
            {
                ComPtr<IiTunes> iTunes;
                hr = ::CoCreateInstance(CLSID_iTunesApp, NULL, CLSCTX_LOCAL_SERVER,
                    IID_IiTunes, &iTunes);
                ::CoUninitialize();
            }

            if (SUCCEEDED(hr))
            {
                _itunesUp = true;
            }

            ShaPostMessage(AM_ITUNES_STARTED, SUCCEEDED(hr), 0);
        }
    });
    proc.detach();
}

void vik::graphics::OnITunesResponse(_In_ bool succeeded)
{
    if (!succeeded)
    {
        PostNotification("VikBEE Brand iTunes cannot launch.");
    }
    else
    {
        auto station = ShaStation::Convert(thisStation, ITUNES_TYPE, std::vector<std::string>());

        if (station)
        {
            delete thisStation;
            thisStation = station;

            auto elem = thisStation->GetElementRepresentation();
            assert(elem);
            layout.AddChild(elem);
        }
    }
}

std::string vik::graphics::ITunesTrackRequest(_In_ TrackRequest request)
{
    std::string response = "invalid\n";

    HRESULT hr = ::CoInitialize(0);
    if (SUCCEEDED(hr))
    {
        ComPtr<IiTunes> iTunes;
        hr = ::CoCreateInstance(CLSID_iTunesApp, NULL, CLSCTX_LOCAL_SERVER, IID_IiTunes,
            (PVOID *)&iTunes);
        if (SUCCEEDED(hr))
        {
            response = "succeeded\n";
            switch (request)
            {
            case TR_NEXT:
                hr = iTunes->NextTrack();
                break;

            case TR_PREVIOUS:
                hr = iTunes->PreviousTrack();
                break;
            }
        }
        else
        {
            // iTunes has been closed
            _OnITunesClosed();
            response = "failed\n";
        }

        ::CoUninitialize();
    }

    return response;
}

std::string vik::graphics::ITunesSongRequest(_In_ ITunesStatus::SongStatus request)
{
    std::string response = "invalid\n";

    HRESULT hr = ::CoInitialize(0);
    if (SUCCEEDED(hr))
    {
        ComPtr<IiTunes> iTunes;
        hr = ::CoCreateInstance(CLSID_iTunesApp, NULL, CLSCTX_LOCAL_SERVER, IID_IiTunes,
            (PVOID *)&iTunes);
        if (SUCCEEDED(hr))
        {
            response = "succeeded\n";
            switch (request)
            {
            case ITunesStatus::SS_PLAY:
                hr = iTunes->Play();
                break;

            case ITunesStatus::SS_PAUSE:
                hr = iTunes->Pause();
                break;
            }
        }
        else
        {
            // iTunes has been closed
            _OnITunesClosed();
            response = "failed\n";
        }

        ::CoUninitialize();
    }

    return response;
}

bool vik::graphics::GetITunesStatus(_Inout_ ITunesStatus &status)
{
    HRESULT hr = ::CoInitialize(0);
    if (SUCCEEDED(hr))
    {
        ComPtr<IiTunes> iTunes;
        hr = ::CoCreateInstance(CLSID_iTunesApp, NULL, CLSCTX_LOCAL_SERVER, IID_IiTunes,
            (PVOID *)&iTunes);
        if (SUCCEEDED(hr))
        {
            ComPtr<IITTrack> track;
            hr = iTunes->get_CurrentTrack(&track);
            if (SUCCEEDED(hr))
            {
                BSTR artist, album, song;

                hr = track->get_Artist(&artist);
                if (SUCCEEDED(hr))
                {
                    hr = track->get_Album(&album);
                    if (SUCCEEDED(hr))
                    {
                        hr = track->get_Name(&song);
                        if (SUCCEEDED(hr))
                        {
                            status.artistName.assign(artist, artist + SysStringLen(artist));
                            status.albumName.assign(album, album + SysStringLen(album));
                            status.trackName.assign(song, song + SysStringLen(song));

                            SysFreeString(song);
                        }
                        SysFreeString(album);
                    }
                    SysFreeString(artist);
                }
            }

            if (SUCCEEDED(hr))
            {
                ITPlayerState state;
                hr = iTunes->get_PlayerState(&state);
                if (SUCCEEDED(hr))
                {
                    status.songState = (state == ITPlayerStatePlaying) ? ITunesStatus::SS_PLAY :
                        ITunesStatus::SS_PAUSE;
                }
            }
        }

        ::CoUninitialize();
    }

    if (FAILED(hr))
    {
        _OnITunesClosed();
    }

    return !!SUCCEEDED(hr);
}

static void _OnITunesClosed(void)
{
    // Potentiall send update message?
    _itunesUp = false;
}

bool vik::graphics::IsITunesRunning(void)
{
    return _itunesUp;
}
