/*
 * ShaLeap.cpp
 *
 * Author(s):
 *      Jerry Lin
 *
 * The interface used by the Smart Home Assistant to coordinate between the Leap and the graphical
 * output.
 */

#include <algorithm>
#include <iomanip>

#include "ShaLeap.h"

using namespace Leap;

using namespace sha::leap;

static Controller leapController;
static ShaListener shaListener;

/*
 * Public Initializers
 */
void sha::leap::InitializeLeap(void)
{
    ConfigureController(leapController);
    leapController.addListener(shaListener);
    shaListener.EnableShaGesture(GT_PINCH);
    shaListener.EnableShaGesture(GT_SWIPE);
}

bool sha::leap::ConfigureController(Controller &controller)
{
    Config config = controller.config();
    //config.setFloat("Gesture.Circle.MinRadius", 5);
    config.setFloat("Gesture.ScreenTap.MinDistance", 2.0);
    return config.save();
}



ShaListener::ShaListener(void)
{
    this->_turnIncrementDegree = 15;
    this->_leapState = SLS_GESTURES;
    this->_turnDirection = TD_NONE;

    this->_shaGestures.push_back(new ShaPinchGesture{});
    this->_shaGestures.push_back(new ShaSwipeGesture{});

    this->_enabledGestures.emplace(GT_PINCH, false);
    this->_enabledGestures.emplace(GT_SWIPE, false);
}

/*
 * Inherited Leap Listener Callback Functions
 */
void ShaListener::onInit(const Controller &controller)
{
    (void)controller;
}

void ShaListener::onConnect(const Controller &controller)
{
    //controller.enableGesture(Gesture::TYPE_CIRCLE);
    //controller.enableGesture(Gesture::TYPE_SWIPE);
    controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
}

void ShaListener::onDisconnect(const Controller &controller)
{
    (void)controller;
}

void ShaListener::onExit(const Controller &controller)
{
    (void)controller;
}

void ShaListener::onFrame(const Controller &controller)
{
    this->_ProcessHands(controller);

    //if (this->_leapState == ShaLeapState::SLS_GESTURES)
    //{
        this->_ProcessGestures(controller);
    //}
}

void ShaListener::onFocusGained(const Controller &controller)
{
    (void)controller;
}

void ShaListener::onFocusLost(const Controller &controller)
{
    (void)controller;
}



int _GetSecondFingerIndexFromLeft(_In_ const Hand &hand)
{
    const FingerList &fingers = hand.fingers();
    int index = 0;
    float minX = fingers.rightmost().tipPosition().x;

    for (int i = 0; i < fingers.count(); i++)
    {
        const Finger &finger = fingers[i];
        if (finger != fingers.leftmost())
        {
            float x = finger.tipPosition().x;
            if (x <= minX)
            {
                minX = x;
                index = i;
            }
        }
    }

    return index;
}
void ShaListener::_ProcessHands(_In_ const Controller &controller)
{
    const Hand &hand = controller.frame(0).hands().frontmost();

    //std::cout << "Num Pointables: " << controller.frame(0).pointables().count() << std::endl;

    //std::cout << "Palm Velocity: " << hand.palmVelocity().magnitude() << std::endl;

    if (hand.isValid())
    {
        //TODO: REMOVE
        if (hand.fingers().count() > 1)
        {
            const Finger &firstFinger = hand.fingers().leftmost();
            const Finger &secondFinger = hand.fingers()[_GetSecondFingerIndexFromLeft(hand)];
            const Vector &firstPos = firstFinger.tipPosition();
            const Vector &secondPos = secondFinger.tipPosition();

            float distance = (secondPos - firstPos).magnitude();
            //std::cout << "Distance: " << distance << std::endl;
        }

        /*
        if (this->_IsKnobReleased(hand))
        {
            this->_leapState = SLS_GESTURES;

            Vector frontMost = hand.fingers().frontmost().tipPosition();
            this->_x = frontMost.x;
            this->_y = frontMost.y;
            this->_z = frontMost.z;
        }
        else
        {
            this->_ProcessTurning(hand);
        }
        */
    }
    else
    {
        this->_leapState = SLS_GESTURES;
    }
}

float _FindAverageFingerDistance(_In_ const Hand &hand)
{
    float averageFingerDistance = 0;

    for (auto &finger : hand.fingers())
    {
        const Vector &palmPosition = hand.palmPosition();
        const Vector &fingerTip = finger.tipPosition();

        averageFingerDistance += (fingerTip - palmPosition).magnitude();
    }

    return averageFingerDistance / hand.fingers().count();
}

float averageFingerDistance = 0;
bool ShaListener::_IsKnobReleased(_In_ const Hand &hand)
{

    bool result = true;

    const FingerList &fingers = hand.fingers();
    int numRadialPointers = 0;
    int numScreenPointers = 0;

    if (fingers.count() > 2)
    {
        const Vector &palmNormal = hand.palmNormal();
        for (auto &finger : fingers)
        {
            const Vector &fingerDirection = finger.direction();
            float palmNormalDotFingerDirection = palmNormal.dot(fingerDirection);

            if (palmNormalDotFingerDirection < 0.4)
            {
                numRadialPointers++;
            }
            else if (palmNormalDotFingerDirection > 0.55)
            {
                numScreenPointers++;
            }
        }
    }

    //std::cout << hand.palmVelocity().magnitude() << std::endl;

    if (numRadialPointers > fingers.count() / 2 || hand.palmVelocity().magnitude() > 200)
    {
        averageFingerDistance = std::max(averageFingerDistance, _FindAverageFingerDistance(hand));
        //std::cout << "Released: " << averageFingerDistance << std::endl;
        result = true;
    }
    /*else if (numScreenPointers > fingers.count() / 2)
    {
        result = false;
    }*/
    else if (averageFingerDistance - _FindAverageFingerDistance(hand) > 20 && 
        abs(hand.palmNormal().z) > abs(hand.palmNormal().x) && abs(hand.palmNormal().z) > abs(hand.palmNormal().y)
        && hand.palmVelocity().magnitude() < 200
        && fingers.count() > 1)
    {
        result = false;
    }
    else if (this->_leapState == SLS_GESTURES)
    {
        result = true;
    }
    else if (this->_leapState == SLS_TURNING)
    {
        result = false;
    }
    
    //std::cout << _FindAverageFingerDistance(hand) << std::endl;

    return result;
}

void ShaListener::_ProcessTurning(_In_ const Hand &hand)
{
    if (this->_leapState != SLS_TURNING)
    {
        this->_leapState = SLS_TURNING;
        this->_turnDirection = TD_NONE;
        this->_lastRoll = hand.palmNormal().roll();
        std::cout << "Grabbed" << "\n";
    }
    else
    {
        float roll = hand.palmNormal().roll();
        float rollDifference = _FindRollDifference(roll);
        bool isClockwise = rollDifference < 0;

        if (TO_DEGREES(roll) < 45 && TO_DEGREES(roll) > -45)
        {
            while (TO_DEGREES(abs(rollDifference)) > this->_turnIncrementDegree)
            {
                if (isClockwise)
                {
                    if (this->_turnDirection == TD_NONE)
                    {
                        this->_turnDirection = TD_CLOCKWISE;
                    }
                   /* else if (this->_turnDirection == TD_COUNTERCLOCKWISE)
                    {
                        break;
                    }*/
                    this->_turns++;
                    //std::cout << "\tRoll: +" << this->_turnIncrementDegree << " degree increment" << std::endl;
                }
                else
                {
                    if (this->_turnDirection == TD_NONE)
                    {
                        this->_turnDirection = TD_COUNTERCLOCKWISE;
                    }
                    /*else if (this->_turnDirection == TD_CLOCKWISE)
                    {
                        break;
                    }*/
                    this->_turns--;
                    //std::cout << "\tRoll: -" << this->_turnIncrementDegree << " degree increment" << std::endl;
                }
                std::cout << "\tTurns: " << this->_turns << std::endl;

                this->_lastRoll += (rollDifference)*
                    (TO_RADIANS(this->_turnIncrementDegree)) / abs(rollDifference);

                rollDifference = _FindRollDifference(roll);
            }
        }
        else
        {
            //std::cout << "OUT OF RANGE, SON!" << std::endl;
        }
    }
}

float ShaListener::_FindRollDifference(_In_ float roll)
{
    return roll - this->_lastRoll;
}

void ShaListener::_ProcessGestures(_In_ const Controller &controller)
{
    const Frame &frame = controller.frame(0);
    const GestureList gestures = frame.gestures();

    for (auto gesture : this->_shaGestures)
    {
        gesture->Update(frame);

        switch (gesture->GetType())
        {
        case GT_PINCH:
            if (this->_enabledGestures[GT_PINCH])
            {
                this->_ProcessShaPinchGesture(static_cast<ShaPinchGesture *>(gesture));
            }
            break;
        case GT_SWIPE:
            if (this->_enabledGestures[GT_SWIPE])
            {
                this->_ProcessShaSwipeGesture(static_cast<ShaSwipeGesture *>(gesture));
            }
            break;
        }
    }

    Gesture existingGesture = frame.gesture(this->_currentGestureId);

    if (!existingGesture.isValid())
    {
        this->_currentGestureId = -1;
    }

    for (int i = 0; i < gestures.count(); ++i)
    {
        const Gesture &gesture = gestures[i];

        switch (gesture.type())
        {
        case Gesture::TYPE_CIRCLE:
            this->_ProcessCircleGesture(gesture);
            break;

        case Gesture::TYPE_SCREEN_TAP:
            this->_ProcessScreenTap(gesture);
            break;

        default:
            break;
        }
    }
}

int traversals = 0;
void ShaListener::_ProcessCircleGesture(_In_ const CircleGesture &circle)
{
    if (this->_currentGestureId == -1)
    {
        this->_currentGestureId = circle.id();
        traversals = 0;
        std::cout << "Current Circle ID: " << this->_currentGestureId << std::endl;
    }
    else if (this->_currentGestureId == circle.id())
    {
        bool isClockwise;
        if (circle.pointable().direction().angleTo(circle.normal()) <= Leap::PI / 4) {
            isClockwise = true;
        }
        else {
            isClockwise = false;
        }

        float turns = circle.progress() + 0.25f;

        while (floor(turns) > floor(traversals))
        {
            traversals++;
            if (isClockwise)
            {
                this->_circleTraversals++;
            }
            else
            {
                this->_circleTraversals--;
            }
            std::cout << "\tCircle Traversals: " << this->_circleTraversals << std::endl;
            std::cout.flush();
        }
    }
}

int screenTapCt = 0;
void ShaListener::_ProcessScreenTap(_In_ const ScreenTapGesture &screenTap)
{
    screenTapCt++;

    const Vector &position = screenTap.position();
    std::cout << "\tScreen Tap: MEOW! " << screenTapCt << " (" << position.x << ", " << position.y << ")" << std::endl;
}

bool ShaListener::DisableShaGesture(GestureType gestureType)
{
    if (this->_ContainsGestureType(gestureType))
    {
        switch (gestureType)
        {
        case GT_PINCH:
            this->_enabledGestures[GT_PINCH] = false;
            break;
        case GT_SWIPE:
            this->_enabledGestures[GT_SWIPE] = false;
            break;
        default:
            break;
        }
        return true;
    }
    return false;
}

bool ShaListener::EnableShaGesture(GestureType gestureType)
{
    if (this->_ContainsGestureType(gestureType))
    {
        switch (gestureType)
        {
        case GT_PINCH:
            this->_enabledGestures[GT_PINCH] = true;
            break;
        case GT_SWIPE:
            this->_enabledGestures[GT_SWIPE] = true;
            break;
        default:
            break;
        }
        return true;
    }

    return false;
}

bool ShaListener::_ContainsGestureType(GestureType gestureType) const
{
    bool contains = false;

    for (auto gesture : _shaGestures)
    {
        if (gesture->GetType() == gestureType)
        {
            contains = true;
        }
    }

    return contains;
}

void ShaListener::_ProcessShaPinchGesture(ShaPinchGesture *pinchGesture)
{
    if (pinchGesture->IsPinched())
    {
        std::cout << "PINCHED" << std::endl;
    }
    else if (pinchGesture->IsReleased())
    {
        std::cout << "\tRELEASED" << std::endl;
    }
}

void ShaListener::_ProcessShaSwipeGesture(ShaSwipeGesture *swipeGesture)
{
    //std::cout << swipeGesture->GetId() << ": " << swipeGesture->GetState() << std::endl;
    if (swipeGesture->IsComplete() && this->_currentShaGestureId != swipeGesture->GetId())
    {
        this->_currentShaGestureId = swipeGesture->GetId();
        std::cout << "SWIPE ";
        switch (swipeGesture->GetDirection())
        {
        case SD_UP:
            std::cout << "UP\n";
            break;
        case SD_DOWN:
            std::cout << "DOWN\n";
            break;
        case SD_LEFT:
            std::cout << "LEFT\n";
            break;
        case SD_RIGHT:
            std::cout << "RIGHT\n";
            break;
        }
    }
}
