/*
 * Socket.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Useful classes for dealing with both server and client ports (for TCP only)
 */

#include <cassert>

#include "Socket.h"

using namespace vik::communication;

/*
 * SOCKET CLASS
 */
Socket::Socket(void) :
    _socket(INVALID_SOCKET),
    _nErrorCode(0),
    _state(SOCKET_DISCONNECTED)
{
    memset(&this->_sAddr, 0, sizeof(this->_sAddr));
}

Socket::Socket(_In_ const InetAddress &inetAddr) :
    Socket()
{
    this->Connect(inetAddr);
}



bool Socket::IsConnected(void) const
{
    return this->_state == SOCKET_CONNECTED;
}

int Socket::GetError(void) const
{
    return this->_nErrorCode;
}

bool Socket::Connect(_In_ const InetAddress &inetAddr)
{
    if (this->_state != SOCKET_DISCONNECTED)
    {
        this->_nErrorCode = WSAEISCONN;
        return false;
    }

    // First need to allocate a socket
    this->_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (this->_socket == INVALID_SOCKET)
    {
        this->_nErrorCode = WSAGetLastError();
        return false;
    }

    // Connect to the server
    this->_sAddr = inetAddr;
    if (connect(this->_socket, (sockaddr *)&this->_sAddr, sizeof(this->_sAddr)) == SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
        closesocket(this->_socket);
        return false;
    }

    this->_state = SOCKET_CONNECTED;
    return true;
}

bool Socket::Close(void)
{
    if (this->_state != SOCKET_CONNECTED)
    {
        this->_nErrorCode = WSAENOTCONN;
        return false;
    }

    this->_state = SOCKET_DISCONNECTED;

    if (shutdown(this->_socket, SD_BOTH) == SOCKET_ERROR)
    {
        // Even if shutdown fails, we still need to close the socket
        this->_nErrorCode = WSAGetLastError();
        closesocket(this->_socket);

        this->_socket = INVALID_SOCKET;
        return false;
    }

    if (closesocket(this->_socket) == SOCKET_ERROR)
    {
        // Still mark socket as invalid
        this->_nErrorCode = WSAGetLastError();
        this->_socket = INVALID_SOCKET;
        return false;
    }

    this->_socket = INVALID_SOCKET;
    return true;
}

std::string Socket::GetIpAddr(void) const
{
    return inet_ntoa(this->_sAddr.sin_addr);
}

InetAddress Socket::GetInetAddr(void) const
{
    return this->_sAddr;
}

_Success_(return >= 0)
int Socket::Send(_In_reads_(nLen) const void *pBuffer, _In_ int nLen)
{
    if (this->_state != SOCKET_CONNECTED)
    {
        this->_nErrorCode = WSAENOTCONN;
        return -1;
    }

    int nResult = send(this->_socket, (char *)pBuffer, nLen, 0);
    if (nResult == SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
    }

    return nResult;
}

_Success_(return >= 0)
int Socket::Receive(_Out_writes_bytes_to_(nLen, return) void *pBuffer, _In_ int nLen)
{
    if (this->_state != SOCKET_CONNECTED)
    {
        this->_nErrorCode = WSAENOTCONN;
        return -1;
    }

    int nResult = recv(this->_socket, (char *)pBuffer, nLen, 0);
    if (nResult == SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
    }
    else if (nResult == 0)
    {
        // Receiving 0 means the client on the other end closed the connection
        this->Close();
    }

    return nResult;
}






/*
 * SERVERSOCKET CLASS
 */
ServerSocket::ServerSocket(void) :
    _socket(INVALID_SOCKET),
    _nErrorCode(0),
    _state(SOCKET_DISCONNECTED)
{
    memset(&this->_sAddr, 0, sizeof(this->_sAddr));
}

ServerSocket::ServerSocket(_In_ USHORT usPort) :
    ServerSocket()
{
    this->Start(usPort);
}



bool ServerSocket::IsRunning(void) const
{
    return this->_state == SOCKET_CONNECTED;
}

int ServerSocket::GetError(void) const
{
    return this->_nErrorCode;
}

bool ServerSocket::Start(_In_ USHORT usPort)
{
    if (this->_state != SOCKET_DISCONNECTED)
    {
        this->_nErrorCode = WSAEISCONN;
        return false;
    }
    if (usPort == 0)
    {
        this->_nErrorCode = WSAEADDRNOTAVAIL;
        return false;
    }

    // Allocate the server socket
    this->_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (this->_socket == INVALID_SOCKET)
    {
        this->_nErrorCode = WSAGetLastError();
        return false;
    }

    // Bind to the requested port
    this->_sAddr.sin_family = AF_INET;
    this->_sAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    this->_sAddr.sin_port = htons(usPort);
    if (bind(this->_socket, (sockaddr *)&this->_sAddr, sizeof(this->_sAddr)) == SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
        closesocket(this->_socket);
        return false;
    }

    // Start listening for incoming connections
    if (listen(this->_socket, 5) == SOCKET_ERROR)
    {
        this->_nErrorCode = WSAGetLastError();
        closesocket(this->_socket);
        return false;
    }

    this->_state = SOCKET_CONNECTED;
    return true;
}

Socket ServerSocket::Accept(void)
{
    Socket connection;

    if (this->_state != SOCKET_CONNECTED)
    {
        this->_nErrorCode = WSAENOTCONN;
        return connection;
    }

    sockaddr_in sAddr;
    int nAddrLen = sizeof(sAddr);
    SOCKET s = accept(this->_socket, (sockaddr *)&sAddr, &nAddrLen);

    if (s == INVALID_SOCKET)
    {
        // accpet failed
        this->_nErrorCode = WSAGetLastError();
        return connection;
    }

    // accept succeeded
    connection._socket = s;
    connection._sAddr = sAddr;
    connection._state = SOCKET_CONNECTED;
    return connection;
}

bool ServerSocket::Close(void)
{
    if (this->_state != SOCKET_CONNECTED)
    {
        this->_nErrorCode = WSAENOTCONN;
        return false;
    }

    this->_state = SOCKET_DISCONNECTED;

    if (closesocket(this->_socket) == SOCKET_ERROR)
    {
        // Still mark socket as invalid
        this->_nErrorCode = WSAGetLastError();
        this->_socket = INVALID_SOCKET;
        return false;
    }

    this->_socket = INVALID_SOCKET;
    return true;
}
