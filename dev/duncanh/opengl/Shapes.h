/*
 * Shapes.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Functions used to draw shapes in OpenGL. In general, it is not recommended that you call these
 * functions directly unless you are creating a custom UIElement.
 *
 * Additionally, unless there is good reason not too, you should use the drawing functions that
 * accept an appropriate Graphics struct (e.g. Rect, Oval, etc.) as input. This is especially true
 * for drawing ovals as the points would otherwise need to be calculated each time.
 *
 * Finally, each function has a version that accepts a color and a version that does not accept a
 * color. For the functions that do not accept a color, the color set by glColor* is used. This can
 * allow for some (slight) optimizations to be done when drawing many elements with the same color.
 */
#pragma once

#include <cstdint>

#include "Colors.h"
#include "Graphics.h"

namespace sha
{
    namespace graphics
    {
        /* Rectangles */
        GLvoid DrawRect(GLfloat x, GLfloat y, GLfloat width, GLfloat height);
        GLvoid DrawRect(GLfloat x, GLfloat y, GLfloat width, GLfloat height, const Color &color);
        GLvoid DrawRect(const Rect &rc);
        GLvoid DrawRect(const Rect &rc, const Color &color);

        GLvoid FillRect(GLfloat x, GLfloat y, GLfloat width, GLfloat height);
        GLvoid FillRect(GLfloat x, GLfloat y, GLfloat width, GLfloat height, const Color &color);
        GLvoid FillRect(const Rect &rc);
        GLvoid FillRect(const Rect &rc, const Color &color);

        /* Ovals (and Circles) */
        GLvoid DrawOval(GLfloat x, GLfloat y, GLfloat width, GLfloat height, uint32_t res);
        GLvoid DrawOval(GLfloat x, GLfloat y, GLfloat width, GLfloat height, uint32_t res,
                        const Color &color);
        GLvoid DrawOval(const Oval &oval);
        GLvoid DrawOval(const Oval &oval, const Color &color);

        GLvoid FillOval(GLfloat x, GLfloat y, GLfloat width, GLfloat height, uint32_t res);
        GLvoid FillOval(GLfloat x, GLfloat y, GLfloat width, GLfloat height, uint32_t res,
                        const Color &color);
        GLvoid FillOval(const Oval &oval);
        GLvoid FillOval(const Oval &oval, const Color &color);

        /* Triangles; can't do width and height */
        //GLvoid DrawTriangle();
    }
}

