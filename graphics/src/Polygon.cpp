/*
 * Polygon.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a UIElement that can take the shape of any polygon (identified by a sequence of
 * points).
 */

#include "Polygon.h"

using namespace vik::graphics;
using namespace vik::graphics::shapes;

/*
 * Constructor(s)/Destructor
 */
Polygon::Polygon(void) :
    Polygon(Rect(0, 0, 0, 0))
{
}

Polygon::Polygon(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
    Polygon(Rect(x, y, width, height))
{
}

Polygon::Polygon(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    Polygon(Rect(pt, width, height))
{
}

Polygon::Polygon(_In_ const Rect &rc) :
    Shape(rc)
{
}



/*
 * IAnimatable
 */
void Polygon::SetWidth(_In_ GLfloat width)
{
    GLfloat prevWidth = this->GetWidth();
    GLfloat prevHeight = this->GetHeight();

    Shape::SetWidth(width);
    this->_OnResize(prevWidth, prevHeight);
}

void Polygon::SetHeight(_In_ GLfloat height)
{
    GLfloat prevWidth = this->GetWidth();
    GLfloat prevHeight = this->GetHeight();

    Shape::SetHeight(height);
    this->_OnResize(prevWidth, prevHeight);
}

void Polygon::SetRect(_In_ const Rect &rc)
{
    GLfloat prevWidth = this->GetWidth();
    GLfloat prevHeight = this->GetHeight();

    Shape::SetRect(rc);
    this->_OnResize(prevWidth, prevHeight);
}

GLfloat Polygon::IncreaseWidth(_In_ GLfloat deltaWidth)
{
    GLfloat prevWidth = this->GetWidth();
    GLfloat prevHeight = this->GetHeight();

    Shape::IncreaseWidth(deltaWidth);
    this->_OnResize(prevWidth, prevHeight);
    return this->GetWidth();
}

GLfloat Polygon::IncreaseHeight(_In_ GLfloat deltaHeight)
{
    GLfloat prevWidth = this->GetWidth();
    GLfloat prevHeight = this->GetHeight();

    Shape::IncreaseHeight(deltaHeight);
    this->_OnResize(prevWidth, prevHeight);
    return this->GetHeight();
}



/*
 * UIElement
 */
GLvoid Polygon::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    glColor4fv(this->_backgroundColor);
    glBegin(GL_POLYGON);

    for (Point pt : this->_points)
    {
        pt.x += offX;
        pt.y += offY;
        glVertex2fv(pt);
    }

    glEnd();

    // Draw an outline if the border thickness != 0
    if (this->_borderThickness > 0)
    {
        glLineWidth(this->_borderThickness);
        glColor4fv(this->_borderColor);
        glBegin(GL_LINE_LOOP);

        for (Point pt : this->_points)
        {
            pt.x += offX;
            pt.y += offY;
            glVertex2fv(pt);
        }

        glEnd();
    }
}



/*
 * Polygon Points
 */
void Polygon::AddPoint(_In_ GLfloat x, _In_ GLfloat y)
{
    this->_points.emplace_back(x, y);
}

void Polygon::AddPoint(_In_ const Point &pt)
{
    this->_points.push_back(pt);
}

size_t Polygon::Size(void) const
{
    return this->_points.size();
}

void Polygon::Clear(void)
{
    this->_points.clear();
}



/*
 * Private Functions
 */
void Polygon::_OnResize(_In_ GLfloat prevWidth, _In_ GLfloat prevHeight)
{
    GLfloat widthScale = this->GetWidth() / prevWidth;
    GLfloat heightScale = this->GetHeight() / prevHeight;

    for (auto &pt : this->_points)
    {
        pt.x *= widthScale;
        pt.y *= heightScale;
    }
}
