/*
 * WindowHelpers.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * The WindowInfo implementation for the Linux operating system.
 */

#include "ShaGraphics.h"
#include "WindowHelpers.h"

using namespace sha;
using namespace sha::graphics;

bool sha::InitWindow(const WindowInfo &info, const ShaArgs &args)
{
    // TODO
    return false;
}

int sha::MessagePump(void)
{
    // TODO
    return 1;
}
