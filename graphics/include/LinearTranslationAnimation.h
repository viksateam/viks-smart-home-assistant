/*
 * LinearTranslationAnimation.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines an animation that translates an IAnimatable object from one point to another target
 * point at a constant velocity. The velocity is in units of pixels-per-microsecond.
 */
#pragma once

#include "Animatable.h"
#include "AnimationBase.h"

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            class LinearTranslationAnimation final :
                public AnimationBase
            {
            public:
                LinearTranslationAnimation(
                    _In_ IAnimatable *target,
                    _In_ GLfloat endX,
                    _In_ GLfloat endY,
                    _In_ GLfloat vel);

                /* IAnimation */
                virtual void OnAnimationBegin(void);
                virtual bool OnAnimationUpdate(_In_ uint64_t timeMicro);
                virtual void OnAnimationCompleted(void);

            private:

                GLfloat _endX;
                GLfloat _endY;
                GLfloat _velocity;

                // Set during OnAnimationBegin
                GLfloat _deltaX;
                GLfloat _deltaY;
                GLfloat _duration;
            };
        }
    }
}
