/*
 * Server.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * An implementation of a server for the Tiva C Series TM4C123G LaunchPad Evaluation Kit with the
 * CC3000 Booster Pack. This server does little more than open up a listening socket on some
 * specified port, accept incoming connections, relay incoming messages, and send responses.
 */

#include <cassert>
#include <string.h>

#include <driverlib/interrupt.h>
#include <driverlib/rom_map.h>

#include "Console.h"
#include "Debug.h"
#include "ip.h"
#include "netapp.h"
#include "Server.h"
#include "socket.h"
#include "spi.h"
#include "sys_tick.h"

#ifdef  WIN32
#define strtok_r    strtok_s
#endif  /* WIN32 */

#define MAX_SOCKET_DESCRIPTOR                   (((unsigned int)-1) >> 1)
#define NETAPP_IPCONFIG_MAC_OFFSET              (20)

volatile bool Server::s_fCheckSocket = false;
volatile bool Server::s_fConfig = false;
volatile bool Server::s_fConnected = false;
volatile bool Server::s_fIPValid = false;
volatile bool Server::s_fRunning = false;
volatile bool Server::s_fShutdownOkay = false;

MessageHandler Server::s_pfnMessageHandler = NULL;
const char *Server::s_pszName;
const char *Server::s_pszId;

long Server::s_lSocketHandle = INVALID_SOCKET;
long Server::s_lBroadcastSocketHandle = INVALID_SOCKET;
int Server::s_nNumConnections = 0;
long Server::s_lBroadcastIPAddress = 0;

#ifdef __CCS__

volatile bool g_rgfClosedConnections[10];
char Server::s_rgszIPAddress[IP_ADDRESS_MAX_LEN];
Server::Connection Server::s_rgConnections[MAX_CONNECTIONS];
char Server::s_rgcBroadcastRecvbuf[BROADCAST_BUFFER_LEN];

#elif __IAR_SYSTEMS_ICC__

__no_init volatile bool g_rgfClosedConnections[10];
__no_init char Server::s_rgszIPAddress[IP_ADDRESS_MAX_LEN];
__no_init Server::Connection Server::s_rgConnections[MAX_CONNECTIONS];
__no_init char Server::s_rgcBroadcastRecvbuf[BROADCAST_BUFFER_LEN];

#elif __arm__

__attribute__((zero_init))
volatile bool g_rgfClosedConnections[10];
__attribute__((zero_init))
char Server::s_rgszIPAddress[IP_ADDRESS_MAX_LEN];
__attribute__((zero_init))
Server::Connection Server::s_rgConnections[MAX_CONNECTIONS];
__attribute__((zero_init))
char Server::s_rgcBroadcastRecvbuf[BROADCAST_BUFFER_LEN];

#elif WIN32

volatile bool g_rgfClosedConnections[10];
char Server::s_rgszIPAddress[IP_ADDRESS_MAX_LEN];
Server::Connection Server::s_rgConnections[MAX_CONNECTIONS];
char Server::s_rgcBroadcastRecvbuf[BROADCAST_BUFFER_LEN];

#else

#error Compiler not supported

#endif



/*
 * Since the Server includes some debugging features, it is required that Tiva::InitClock,
 * Console::Init, Debug::Init, and SysTick::Init have all been called prior to the Server
 * initialization function. Those functions are not called here as other modules may take a
 * dependency on those features and initializing more than once is undefined behavior.
 */
void Server::Init(_In_ MessageHandler pfnHandler, _In_ const char *pszName, _In_ const char *pszIdHeader)
{
    WlanSPI::Init();
    s_pfnMessageHandler = pfnHandler;

    Console::WriteLine("\r\nInitializing the server...");

    // Enable processor interrupts
    MAP_IntMasterEnable();

    // WLAN Initialization
    wlan_init(Server::_WlanCallback, Server::_SendNullPatch, Server::_SendNullPatch,
        Server::_SendNullPatch, WlanSPI::ReadWlanInterrupt, WlanSPI::WlanInterruptEnable,
        WlanSPI::WlanInterruptDisable, WlanSPI::WriteWlan);

    // Trigger a WLAN device
    wlan_start(0);

    // Mask out unnecessary events
    wlan_set_event_mask(HCI_EVNT_WLAN_KEEPALIVE | HCI_EVNT_WLAN_UNSOL_INIT |
        HCI_EVNT_WLAN_ASYNC_PING_REPORT);

    for (int i = 0; i < MAX_CONNECTIONS; i++)
    {
        s_rgConnections[i].reset();
    }

    for (int i = 0; i < 10; i++)
    {
        g_rgfClosedConnections[i] = false;
    }
    
    for (int i = 0; i < BROADCAST_BUFFER_LEN; i++)
    {
        Server::s_rgcBroadcastRecvbuf[i] = 0;
    }
    
    if (pszName == NULL)
    {
        Server::s_pszName = "Generic Appliance";
    }
    else
    {
        Server::s_pszName = pszName;
    }
    
    
    // To Do: Generate Unique 32-bit HEX ID and append to s_pszId
    if (pszIdHeader == NULL)
    {
        Server::s_pszId = "";
    }
    else
    {
        Server::s_pszId = pszIdHeader;
    }
    
    // Red LED indicates no connection
    Debug::SetLed(LED_RED);
    Console::WriteLine("Server Initialization complete!");
}

bool Server::Connect(
    _In_z_ char *pszSsid,
    _In_opt_ int nEncryptionType,
    _In_opt_ const char *pszPassword)
{
    if (s_fConnected || s_fConfig)
    {
        return false;
    }

    // Use a blue LED to indicate a pending connection
    Debug::SetLed(LED_BLUE);
    s_fConfig = true;

#ifndef CC3000_TINY_DRIVER
    if (nEncryptionType == WLAN_SEC_UNSEC || !pszPassword)
    {
        wlan_connect(nEncryptionType, pszSsid, strlen(pszSsid), NULL, NULL, 0);
    }
    else
    {
        wlan_connect(nEncryptionType, pszSsid, strlen(pszSsid), NULL, (unsigned char *)pszPassword,
            strlen(pszPassword));
    }
#else
    wlan_connect(pszSsid, strlen(pszSsid));
#endif

    // Block until connection is made and we have an ip, or the connection fails
    while (s_fConfig || (!s_fIPValid && s_fConnected))
    {
        // TODO: possibly wait for an interrupt
    }

    return s_fConnected && s_fIPValid;
}

bool Server::InitServerSocket(void)
{
    if (!s_fConnected || s_lSocketHandle != INVALID_SOCKET)
    {
        return false;
    }

    // First step is to create a socket
    s_lSocketHandle = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (s_lSocketHandle == INVALID_SOCKET)
    {
        return false;
    }

    // Once the socket is open, we bind it to the port (use 0.0.0.0 as the address as that
    // signifies the host ip)
    sockaddr_in addr = { 0 };
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SHA_PORT);

    if (bind(s_lSocketHandle, (sockaddr *)&addr, sizeof(addr)) == EFAIL)
    {
        CloseServerSocket();
        return false;
    }

    // Start listening for incoming messages
    if (listen(s_lSocketHandle, 5) == EFAIL)
    {
        CloseServerSocket();
        return false;
    }
    
    

    _ResetConnections();

    return true;
}

char rgszSendBuffer[256];
bool Server::CloseServerSocket(void)
{
    // Close all connections before shutting down
    _ResetConnections();

    bool fResult = closesocket(s_lSocketHandle) == 0;
    s_lSocketHandle = INVALID_SOCKET;

    return fResult;
}

bool Server::InitBroadcastSocket(void)
{
    if (s_lBroadcastSocketHandle != INVALID_SOCKET)
    {
        return false;
    }

    // First step is to create a socket
    s_lBroadcastSocketHandle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (s_lBroadcastSocketHandle == INVALID_SOCKET)
    {
        Console::WriteLine("ERROR: Could not create socket successfully");
        return false;
    }

    // Once the socket is open, we bind it to the port (use 0.0.0.0 as the address as that
    // signifies the host ip)
    sockaddr_in addr = { 0 };
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SHA_BROADCAST_PORT);
    if (bind(s_lBroadcastSocketHandle, (sockaddr *)&addr, sizeof(addr)) == EFAIL)
    {
        Console::WriteLine("ERROR: Could not bind socket successfully");
        CloseBroadcastSocket();
        return false;
    }
    
    unsigned long ulDHCPLeaseTimeout = DHCP_LEASE_TIMEOUT_INF;
    unsigned long ulARPRefreshTimeout = ARP_REFRESH_TIMEOUT_INF;
    unsigned long ulKeepAliveEventTimeout = KEEPALIVE_EVENT_TIMEOUT_INF;
    unsigned long ulSocketInactivityTimeout = SOCKET_INACTIVITY_TIMEOUT_INF;
    
    // Set socket timeout value to be infinity
    long lResult = netapp_timeout_values(&ulDHCPLeaseTimeout, &ulARPRefreshTimeout, &ulKeepAliveEventTimeout, &ulSocketInactivityTimeout);
    if (lResult == SOC_ERROR)
    {
        Console::Write("ERROR: Error setting socket timeout values");
        CloseBroadcastSocket();
        return false;
    }
    
    // Broadcast device to other devices on network
    // connect appliance vikbee.thermostat.1.4FEE6719 VikBEE Brand Thermostat -- noreply
    strcpy(rgszSendBuffer, "connect appliance ");
    strcat(rgszSendBuffer, Server::s_pszId);
    strcat(rgszSendBuffer, " ");
    strcat(rgszSendBuffer, Server::s_pszName);
    strcat(rgszSendBuffer, " -- noreply\n");
    
    // Setup broadcast IP
    s_lBroadcastIPAddress = string_to_ip(s_rgszIPAddress) & 0xFFFFFF00;
    s_lBroadcastIPAddress |= ~(0xFFFFFF00);
    
    sockaddr_in broadcastAddr = { 0 };
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_port = htons(SHA_BROADCAST_PORT);
    broadcastAddr.sin_addr.s_addr = htonl(s_lBroadcastIPAddress);
    
    int iResult = sendto(s_lBroadcastSocketHandle, rgszSendBuffer, strlen(rgszSendBuffer), 0, (sockaddr *)&broadcastAddr, sizeof(broadcastAddr));
    
    if (iResult == SOC_ERROR)
    {
        Console::Write("ERROR: Error sending broadcast data");
        CloseBroadcastSocket();
        return false;
    }
    
    strcpy(rgszSendBuffer, "info appliance ");
    strcat(rgszSendBuffer, Server::s_pszId);
    strcat(rgszSendBuffer, " ");
    strcat(rgszSendBuffer, Server::s_pszName);
    strcat(rgszSendBuffer, " -- reply\n");

    return true;
}

bool Server::BroadcastMessage(_In_ char *pszMessage)
{
    int nResult;
    
    // Setup broadcast address
    sockaddr_in broadcastAddr = { 0 };
    broadcastAddr.sin_family = AF_INET;
    broadcastAddr.sin_port = htons(SHA_BROADCAST_PORT);
    broadcastAddr.sin_addr.s_addr = htonl(s_lBroadcastIPAddress);
    
    // Setup update message
    strcpy(rgszSendBuffer, "update ");
    strcat(rgszSendBuffer, Server::s_pszId);
    strcat(rgszSendBuffer, " ");
    strcat(rgszSendBuffer, pszMessage);
    strcat(rgszSendBuffer, "\n");

    // Debug
    Console::Write("SendBuffer: ");
    Console::WriteLine(rgszSendBuffer);

    // Send broadcast message
    nResult = sendto(s_lBroadcastSocketHandle, rgszSendBuffer, strlen(rgszSendBuffer), 0, (sockaddr *)&broadcastAddr, sizeof(broadcastAddr));

    if (nResult == SOC_ERROR)
    {
        Console::WriteLine("ERROR: Error sending broadcast data");
        CloseBroadcastSocket();
        
        return false;
    }

    return true;
}

bool Server::CloseBroadcastSocket(void)
{
    // Send Disconnect broadcast?
    bool fResult = closesocket(s_lBroadcastSocketHandle) == 0;
    s_lBroadcastSocketHandle = INVALID_SOCKET;
    
    return fResult;
}

char rgszLine[CONSOLE_BUFFER_SIZE];
bool Server::Run(void)
{
    bool fResult = true;
    s_fRunning = true;

    // Need to wait until connected
    Sys_Tick::Sleep(5000);
    while (!s_fConnected || !s_fIPValid)
    {
        if (Console::HasNextLine())
        {
            char *pszLine = rgszLine;
            Console::ReadLine(pszLine);

            char *pszCommand = strtok_r(pszLine, " ", &pszLine);
            char *pszSsid = strtok_r(pszLine, " ", &pszLine);
            char *pszEncryption = strtok_r(pszLine, " ", &pszLine);
            char *pszPassword = strtok_r(pszLine, " ", &pszLine);

            if (strcmp(pszCommand, "connect") == 0)
            {
                Connect(pszSsid, *pszEncryption - '0', pszPassword);
            }
        }
        
        // TODO: Smart Config
        Connect("vik", 3, "jerryberry");
    }
    Console::Write("IP: ");
    Console::WriteLine(s_rgszIPAddress);

    // Open the server's listening socket if not already open
    if (s_lSocketHandle == INVALID_SOCKET)
    {
        if (!InitServerSocket())
        {
            Console::WriteLine("ERROR: Could not initialize server socket");
            return false;
        }
    }
    
    // Open the server's broadcast socket if not already open
    if (s_lBroadcastSocketHandle == INVALID_SOCKET)
    {
        if (!InitBroadcastSocket())
        {
            Console::WriteLine("ERROR: Could not initialize broadcast UDP socket");
            return false;
        }
    }

    // Loop forever until the server is shut down
    while (s_fRunning)
    {
        // First, check to see if any connections timed out (and close those that did)
        if (s_fCheckSocket)
        {
            _CloseTerminatedConnections();
            s_fCheckSocket = false;
        }

        // Process incoming UDP broadcasts (if any)
        if (s_lBroadcastSocketHandle != INVALID_SOCKET)
        {
            _ProcessIncomingBroadcasts();
        }
        
        // Accept incoming connections
        sockaddr_in clientAddr;
        socklen_t clientLen;
        long lClientSocketHandle = accept(s_lSocketHandle, (sockaddr *)&clientAddr, &clientLen);
        if (lClientSocketHandle >= 0)
        {
            char rgszAddr[IP_ADDRESS_MAX_LEN];
            ip_to_string(clientAddr.sin_addr.s_addr, rgszAddr);

            if (!_OnNewConnection(lClientSocketHandle, clientAddr))
            {
                Console::Write("ERROR: could not accept connection from ");
                Console::WriteLine(rgszAddr);

                closesocket(lClientSocketHandle);
            }
            else
            {
                Console::Write("New connection from ");
                Console::WriteLine(rgszAddr);
            }
        }

        // Final step is to process incoming messages (but only if there are any connections)
        if (s_nNumConnections > 0)
        {
            _ProcessIncomingMessages();
        }
    }

    // Shut the server down
    fResult = _ResetConnections();
    fResult = CloseServerSocket() && fResult;

    return fResult;
}

const char *Server::GetIpAddress(void)
{
    return s_rgszIPAddress;
}

bool Server::_CloseConnection(_Inout_ Connection &conn)
{
    bool fResult;

    if (conn.lSocketHandle == INVALID_SOCKET)
    {
        return false;
    }

    fResult = closesocket(conn.lSocketHandle) == 0;
    conn.reset();

    s_nNumConnections--;
    assert(s_nNumConnections >= 0);

    return fResult;
}

bool Server::_ResetConnections(void)
{
    bool fResult = true;
    for (int i = 0; i < MAX_CONNECTIONS; i++)
    {
        fResult = _CloseConnection(s_rgConnections[i]) && fResult;
    }

    assert(s_nNumConnections == 0);
    return fResult;
}

void Server::_CloseTerminatedConnections(void)
{
    for (int i = 0; i < MAX_CONNECTIONS; i++)
    {
        if (s_rgConnections[i].lSocketHandle != INVALID_SOCKET)
        {
            socklen_t val;
            socklen_t valLen;
            int nOpt = getsockopt(s_rgConnections[i].lSocketHandle, SOL_SOCKET,
                SOCKOPT_RECV_NONBLOCK, &val, &valLen);

            // Close if timed out or if closed
            if (nOpt == ERROR_SOCKET_INACTIVE ||
                g_rgfClosedConnections[s_rgConnections[i].lSocketHandle])
            {
                _CloseConnection(s_rgConnections[i]);
                g_rgfClosedConnections[s_rgConnections[i].lSocketHandle] = false;
            }
        }
    }
}

bool Server::_OnNewConnection(_In_ long lSocketHandle, _In_ sockaddr_in &addr)
{
    // Check to see if we are at our connection limit. If that's the case, try and close any
    // potentially terminated connections
    if (s_nNumConnections == MAX_CONNECTIONS)
    {
        _CloseTerminatedConnections();
        if (s_nNumConnections == MAX_CONNECTIONS)
        {
            // All clients are active so reject the new connection
            return false;
        }
    }

    // Use the first available slot
    for (int i = 0; i < MAX_CONNECTIONS; i++)
    {
        if (s_rgConnections[i].lSocketHandle == INVALID_SOCKET)
        {
            s_rgConnections[i].lSocketHandle = lSocketHandle;
            ip_to_string(addr.sin_addr.s_addr, s_rgConnections[i].rgszAddr);
            s_nNumConnections++;
            return true;
        }
    }

    // This should never be reached (as we have already verified a spot)
    assert(false);
    return false;
}

bool Server::_ProcessIncomingMessages(void)
{
    fd_set fdRead;
    fd_set fdError;
    timeval timeout;
    long lsdMax = -1;

    FD_ZERO(&fdRead);
    FD_ZERO(&fdError);
    timeout.tv_sec = 0;
    timeout.tv_usec = 5000;     // 5 ms is the minimum

    // Set based on the valid connections
    for (int i = 0; i < MAX_CONNECTIONS; i++)
    {
        if (s_rgConnections[i].lSocketHandle != INVALID_SOCKET)
        {
            FD_SET(s_rgConnections[i].lSocketHandle, &fdRead);
            FD_SET(s_rgConnections[i].lSocketHandle, &fdError);

            if (s_rgConnections[i].lSocketHandle + 1 > lsdMax)
            {
                lsdMax = s_rgConnections[i].lSocketHandle + 1;
            }
        }
    }

    int nUpdates = select(lsdMax, &fdRead, NULL, &fdError, &timeout);

    if (nUpdates == 0)
    {
        // No updates
        return true;
    }

    bool fResult = true;
    for (int i = 0; i < MAX_CONNECTIONS; i++)
    {
        // Skip inactive connections
        if (s_rgConnections[i].lSocketHandle == INVALID_SOCKET)
        {
            continue;
        }

        // Close sockets with errors and read messages for those that have data
        if (FD_ISSET(s_rgConnections[i].lSocketHandle, &fdError))
        {
            _CloseConnection(s_rgConnections[i]);
        }
        else if (FD_ISSET(s_rgConnections[i].lSocketHandle, &fdRead))
        {
            // The connection has data. When we read, we have to read relative to the pcBufferIn
            // pointer and NOT the rgcBuffer pointer as this may be a continuation of a previous
            // message
            Connection &c = s_rgConnections[i];
            int nBufferSize = CONNECTION_BUFFER_LEN - (c.pcBufferIn - c.rgcBuffer);

            int nSize = recv(c.lSocketHandle, c.pcBufferIn, nBufferSize, 0);
            if (nSize <= 0)
            {
                Console::Write("ERROR: Error receiving data on connection ");
                Console::WriteLine(s_rgConnections[i].rgszAddr);
                _CloseConnection(c);

                fResult = false;
            }
            else
            {
                // Advance both the input pointer as well as the length by the size read
                c.nBuffLen += nSize;
                fResult = _OnConnectionReceivedData(c) && fResult;
            }
        }
    }

    return fResult;
}

bool Server::_ProcessIncomingBroadcasts(void)
{
    fd_set fdRead;
    //fd_set fdError;
    timeval timeout;

    FD_ZERO(&fdRead);
    //FD_ZERO(&fdError);
    timeout.tv_sec = 0;
    timeout.tv_usec = 5000;     // 5 ms is the minimum

    if (s_lBroadcastSocketHandle != INVALID_SOCKET)
    {
        FD_SET(s_lBroadcastSocketHandle, &fdRead);
        //FD_SET(s_lBroadcastSocketHandle, &fdError);
    }
    
    //int nUpdates = select(s_lBroadcastSocketHandle + 1, &fdRead, NULL, &fdError, &timeout);
    int nUpdates = select(s_lBroadcastSocketHandle + 1, &fdRead, NULL, NULL, &timeout);

    if (nUpdates == 0)
    {
        // No updates
        return true;
    }

    bool fResult = true;
    
    // Read messages if data exists
    if (FD_ISSET(s_lBroadcastSocketHandle, &fdRead))
    {
        // The connection has data. When we read, we have to read relative to the pcBufferIn
        // pointer and NOT the rgcBuffer pointer as this may be a continuation of a previous
        // message
        
        sockaddr_in lSenderSocket;
        socklen_t nSenderAddrSize = sizeof (lSenderSocket);
        int nResult = recvfrom(s_lBroadcastSocketHandle, Server::s_rgcBroadcastRecvbuf, BROADCAST_BUFFER_LEN, 0, (sockaddr *)&lSenderSocket, &nSenderAddrSize);
        if (nResult == SOC_ERROR)
        {
            Console::Write("ERROR: Error receiving broadcast data");
            CloseBroadcastSocket();

            fResult = false;
        }
        else
        {
            char *pszToken;
            
            // Debug
            Console::Write("Receive Broadcast: ");
            Console::WriteLine(s_rgcBroadcastRecvbuf);
            
            pszToken = strtok(Server::s_rgcBroadcastRecvbuf, " ");
            
            while (pszToken != NULL)
            {
                // Ignore connect responses
                if (strncmp(pszToken, "info", 4) == 0)
                {
                    return false;
                }
                else if (strncmp(pszToken, "--", 2) == 0)
                {
                    pszToken = strtok(NULL, " ");
                    
                    if (strncmp(pszToken, "reply", 5) == 0)
                    {
                        nResult = sendto(s_lBroadcastSocketHandle, rgszSendBuffer, strlen(rgszSendBuffer), 0, (sockaddr *)&lSenderSocket, nSenderAddrSize);
                        
                        if (nResult == SOC_ERROR)
                        {
                            Console::WriteLine("ERROR: Error sending broadcast data");
                            CloseBroadcastSocket();
                            
                            return false;
                        }
                        
                        return true;
                    }
                }
                
                pszToken = strtok(NULL, " ");
            }
        }
    }

    return fResult;
}

bool Server::_OnConnectionReceivedData(_Inout_ Connection &conn)
{
    char *pszCommand;

    // Process all commands that are ready and available
    while ((pszCommand = _GetNextCommand(conn)) != NULL)
    {
        if (s_pfnMessageHandler)
        {
            char *pszResponse = s_pfnMessageHandler(pszCommand);
            if (pszResponse && send(conn.lSocketHandle, pszResponse, strlen(pszResponse), 0) < 0)
            {
                Console::Write("ERROR: Could not send response; closing connection ");
                Console::WriteLine(conn.rgszAddr);
                _CloseConnection(conn);
                return false;
            }
        }
    }

    // Need to "reset" the input pointer to the beginning which reqires copying data that is not
    // part of a full command
    if (conn.pcBufferIn != conn.rgcBuffer)
    {
        memcpy(conn.rgcBuffer, conn.pcBufferIn, conn.nBuffLen);
        conn.pcBufferIn = conn.rgcBuffer;
    }
    
    return true;
}

char *Server::_GetNextCommand(_Inout_ Connection &conn)
{
    char *psz = NULL;

    for (int i = 0; i < conn.nBuffLen; i++)
    {
        if ((conn.pcBufferIn[i] == SENTINEL_CHARACTER))
        {
            conn.pcBufferIn[i] = '\0';

            psz = conn.pcBufferIn;
            conn.nBuffLen -= i + 1;
            conn.pcBufferIn += i + 1;
            break;
        }
    }

    return psz;
}

void Server::_WlanCallback(_In_ long lEventType, char *pszData, unsigned char ucLength)
{
    switch (lEventType)
    {
    case HCI_EVNT_WLAN_ASYNC_SIMPLE_CONFIG_DONE:
        s_fConfig = false;
        break;

    case HCI_EVNT_WLAN_UNSOL_CONNECT:
        s_fConnected = true;
        s_fConfig = false;
        break;

    case HCI_EVNT_WLAN_UNSOL_DISCONNECT:
        s_fConnected = false;
        s_fConfig = false;
        s_fIPValid = false;
        Debug::SetLed(LED_RED);
        break;

    case HCI_EVNT_WLAN_UNSOL_DHCP:
        // Notes: 
        // 1) IP config parameters are received swapped
        // 2) IP config parameters are valid only if status is OK
        if (*(pszData + NETAPP_IPCONFIG_MAC_OFFSET) == 0)
        {
            ip_to_string(MAKE_IP(pszData[3], pszData[2], pszData[1], pszData[0]), s_rgszIPAddress);
            Debug::SetLed(LED_GREEN);
            s_fIPValid = true;
        }
        else
        {
            s_fIPValid = false;
        }
        break;

    case HCI_EVENT_CC3000_CAN_SHUT_DOWN:
        s_fShutdownOkay = true;
        break;

    case HCI_EVNT_BSD_TCP_CLOSE_WAIT:
        // pszData[0] is the socket number that was closed
        g_rgfClosedConnections[pszData[0]] = true;
        s_fCheckSocket = true;
        break;

    default:
        break;
    }
}

char *Server::_SendNullPatch(_Out_ unsigned long *pulLength)
{
    *pulLength = 0;
    return NULL;
}
