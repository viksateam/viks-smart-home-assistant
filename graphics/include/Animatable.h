/*
 * Animatable.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * The interface for all animatable objects. 
 */
#pragma once

#include "sha.h"

#include "AnimationManager.h"
#include "Shapes.h"

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            class IAnimatable
            {
            public:
                virtual ~IAnimatable(void) {};

                virtual GLfloat GetX(void) const = 0;
                virtual GLfloat GetY(void) const = 0;
                virtual Point   GetLocation(void) const = 0;
                virtual GLfloat GetWidth(void) const = 0;
                virtual GLfloat GetHeight(void) const = 0;
                virtual Rect    GetRect(void) const = 0;

                virtual void    SetX(_In_ GLfloat x) = 0;
                virtual void    SetY(_In_ GLfloat y) = 0;
                virtual void    SetLocation(_In_ const Point &pt) = 0;
                virtual void    SetWidth(_In_ GLfloat width) = 0;
                virtual void    SetHeight(_In_ GLfloat height) = 0;
                virtual void    SetRect(_In_ const Rect &rc) = 0;

                virtual GLfloat TranslateX(_In_ GLfloat deltaX) = 0;
                virtual GLfloat TranslateY(_In_ GLfloat delatY) = 0;
                virtual Point   Translate(_In_ GLfloat deltaX, _In_ GLfloat deltaY) = 0;
                virtual Point   Translate(_In_ const Point &pt) = 0;
                virtual GLfloat IncreaseWidth(_In_ GLfloat deltaWidth) = 0;
                virtual GLfloat IncreaseHeight(_In_ GLfloat deltaHeight) = 0;

                _Success_(return != INVALID_ANIMATION_COOKIE)
                virtual animation_cookie SubmitAnimation(_In_ IAnimation *animation) = 0;
                _Success_(return != INVALID_ANIMATION_COOKIE)
                virtual animation_cookie SubmitPendingAnimation(_In_ IAnimation *animation) = 0;
                _Success_(return != INVALID_ANIMATION_COOKIE)
                virtual animation_cookie SubmitOverrideAnimation(_In_ IAnimation *animation) = 0;
                virtual bool             CancelCurrentAnimation(void) = 0;
            };
        }
    }
}
