/*
 * ThermostatElement.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * The UIElement that graphically represents a Thermostat ShaStation object. When the Thermostat
 * class is updated (i.e. a new supported feature is added), this class needs to get updated in
 * order for the addition to make an effect graphically.
 */
#pragma once

#include "ShaStation.h"
#include "Layout.h"
#include "Polygon.h"
#include "Slider.h"
#include "SliderComboBox.h"

namespace vik
{
    namespace graphics
    {
        class ThermostatElement final :
            public Layout
        {
        public:
            ThermostatElement(_In_ Thermostat *target);
            ThermostatElement(
                _In_ Thermostat *target,
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height);
            ThermostatElement(
                _In_ Thermostat *target,
                _In_ const Point &pt,
                _In_ GLfloat width,
                _In_ GLfloat height);
            ThermostatElement(_In_ Thermostat *target, _In_ const Rect &rc);

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual bool   OnPointerMoved(_In_ Point pt);

            void OnStatusChanged(_In_ const ThermostatStatus &status);

        private:

            /* LayoutAwareElement */
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            void _RefreshLayout(void);
            void _OnManipulationCompleted(_In_ SliderComboBox *sender, _In_ int index);
            void _OnSliderManipulationCompleted(_In_ Slider *sender, _In_ GLfloat value);

            Thermostat *_target;

            shapes::Polygon _base;
            shapes::Polygon _body;
            GLfloat         _mainWidth;

            ThermostatStatus _status;
            char             _temperatureString[11];
            GLfloat          _temperatureStringLength;

            SliderComboBox  _systemComboBox;
            SliderComboBox  _fanComboBox;
            Slider          _thermostatSlider;

            static GLfloat  s_tempStringLength;
        };
    }
}
