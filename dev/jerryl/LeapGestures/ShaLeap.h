/*
 * ShaLeap.h
 *
 * Author(s):
 *      Jerry Lin
 *
 * The interface used by the Smart Home Assistant to coordinate between the Leap and the graphical
 * output.
 */
#pragma once

#include <map>
#include <vector>

#include "Leap.h"

#include "ShaGesture.h"
#include "ShaPinchGesture.h"
#include "ShaSwipeGesture.h"

using namespace Leap;

#define TO_DEGREES(x)       (x * 180 / Leap::PI)
#define TO_RADIANS(x)       (x * Leap::PI / 180)

namespace sha
{
    namespace leap
    {
        void InitializeLeap(void);
        bool ConfigureController(Controller &controller);

        class ShaListener final :
            public Listener
        {
        public:
            ShaListener(void);

            virtual void onInit(const Controller &controller);
            virtual void onConnect(const Controller &controller);
            virtual void onDisconnect(const Controller &controller);
            virtual void onExit(const Controller &controller);
            virtual void onFrame(const Controller &controller);
            virtual void onFocusGained(const Controller &controller);
            virtual void onFocusLost(const Controller &controller);

        private:

            enum ShaLeapState
            {
                SLS_GESTURES,
                SLS_TURNING
            };

            enum TurnDirection
            {
                TD_CLOCKWISE,
                TD_COUNTERCLOCKWISE,
                TD_NONE
            };

            void _ProcessHands(_In_ const Controller &controller);
            void _ProcessTurning(_In_ const Hand &hand);
            bool _IsKnobReleased(_In_ const Hand &hand);
            float _FindRollDifference(_In_ float roll);
            void _ProcessGestures(_In_ const Controller &controller);
            void _ProcessCircleGesture(_In_ const CircleGesture &circle);
            void _ProcessScreenTap(_In_ const ScreenTapGesture &screenTap);

            int32_t _currentGestureId = -1;
            float   _circleTraversals;
            float   _x;
            float   _y;
            float   _z;
            float   _lastRoll;
            float   _turnIncrementDegree;
            float   _turns;
            ShaLeapState  _leapState;
            TurnDirection _turnDirection;

        public:
            bool DisableShaGesture(GestureType gestureType);
            bool EnableShaGesture(GestureType gestureType);

        private:
            bool _ContainsGestureType(GestureType gestureType) const;
            void _ProcessShaPinchGesture(ShaPinchGesture *pinchGesture);
            void _ProcessShaSwipeGesture(ShaSwipeGesture *swipeGesture);

            int32_t                     _currentShaGestureId = -1;
            std::vector<ShaGesture *>   _shaGestures;
            std::map<GestureType, bool> _enabledGestures;
        };
    }
}
