/*
 * main.cpp
 *
 * Author(s)
 *      Duncan Horn
 *
 * Just some fun tests for various graphics APIs, etc.
 */

#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <iostream>

// OpenGL
#include <GL/glu.h>
#include <GL/glx.h>

// XLib
#include <X11/Xlib.h>

#include "Colors.h"
#include "Shapes.h"

using namespace sha::graphics;

Display              *disp;
Window               root;
GLint                glAttr[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
XVisualInfo          *xvi;
Colormap             cmap;
XSetWindowAttributes setAttr;
Window               wnd;
GLXContext           glc;
XWindowAttributes    attr;
XEvent               event;



int dir = 1;
float delta = 0.01;
float res = 3.0;
std::chrono::high_resolution_clock::time_point prev;
static GLvoid Draw(void)
{
    glClear(GL_COLOR_BUFFER_BIT);
    ClearColor(Colors::BackgroundColor);
    
    auto now = std::chrono::high_resolution_clock::now();
    long long duration = std::chrono::duration_cast<std::chrono::microseconds>(now - prev).count();
    prev = now;
    
    Oval oval(0, 0, .5, .5, (uint32_t)res);
    Oval oval2(-.5, .5, .5, .5, (uint32_t)res);
    DrawOval(oval, Colors::Green);
    FillOval(oval2, Colors::Red);
    
    res += delta * dir;
    if (res >= 50)
    {
        dir = -1;
    }
    else if (res <= 3)
    {
        dir = 1;
        res = 3;
    }
}

int main(int argc, char *argv[])
{
    disp = XOpenDisplay(nullptr);

    if(!disp)
    {
        std::cout << "ERROR: Cannot open display server" << std::endl;
        exit(1);
    }
    
    root = DefaultRootWindow(disp);
    
    xvi = glXChooseVisual(disp, 0, glAttr);
    if (!xvi)
    {
        std::cout << "ERROR: No appropriate visual found" << std::endl;
        exit(1);
    }
    
    cmap = XCreateColormap(disp, root, xvi->visual, AllocNone);
    setAttr.colormap = cmap;
    setAttr.event_mask = ExposureMask | KeyPressMask;
    
    wnd = XCreateWindow(disp, root, 0, 0, 800, 600, 0, xvi->depth, InputOutput, xvi->visual,
                CWColormap | CWEventMask, &setAttr);
    
    // Display the window
    XMapWindow(disp, wnd);
    XStoreName(disp, wnd, "Vik's Smart Home Assistant");
    
    glc = glXCreateContext(disp, xvi, nullptr, GL_TRUE);
    glXMakeCurrent(disp, wnd, glc);
    
    /* Allow drawing from arrays */
    glEnableClientState(GL_VERTEX_ARRAY);
    
    prev = std::chrono::high_resolution_clock::now();
    while (1)
    {
        if (XCheckWindowEvent(disp, wnd, KeyPressMask, &event))
        {
            switch (event.type)
            {
            case Expose:
                XGetWindowAttributes(disp, wnd, &attr);
                glViewport(0, 0, attr.width, attr.height);
                
                Draw();
                
                glXSwapBuffers(disp, wnd);
                break;
                
            case KeyPress:
                glXMakeCurrent(disp, None, nullptr);
                glXDestroyContext(disp, glc);
                XDestroyWindow(disp, wnd);
                XCloseDisplay(disp);
                exit(0);
                break;
            }
        }
        else
        {
            XGetWindowAttributes(disp, wnd, &attr);
            glViewport(0, 0, attr.width, attr.height);
            
            Draw();
            
            glXSwapBuffers(disp, wnd);
        }
    }
    
    glXMakeCurrent(disp, None, nullptr);
    glXDestroyContext(disp, glc);
    XDestroyWindow(disp, wnd);
    XCloseDisplay(disp);
    
    return 0;
}

