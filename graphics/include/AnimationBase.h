/*
 * AnimationBase.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Base class for animations which provides default implementations for some of the IAnimation
 * functions.
 */
#pragma once

#include "Animation.h"

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            class AnimationBase :
                public IAnimation
            {
            public:
                AnimationBase(void);
                AnimationBase(_In_ IAnimatable *target);

                /* IAnimation */
                virtual void OnAnimationBegin(void);
                virtual void OnAnimationCompleted(void);
                virtual void OnAnimationCancelled(void);
                virtual bool IsCancelled(void);
                virtual void SetParent(_In_ UIElement *parent);
                virtual UIElement *GetParent(void);
                virtual void SetAnimationBeginCallback(_In_ AnimationEventCallback func);
                virtual void SetAnimationCompletedCallback(_In_ AnimationEventCallback func);
                virtual void SetAnimationCancelledCallback(_In_ AnimationEventCallback func);
                virtual void AddPendingAnimation(_In_ IAnimation *ani);
                virtual const AnimationList &GetPendingAnimations(void);

            protected:

                IAnimatable   *_target;
                UIElement     *_parent;
                AnimationList _pending;

                bool _isCancelled;

                AnimationEventCallback _beginCallback;
                AnimationEventCallback _completedCallback;
                AnimationEventCallback _cancelledCallback;
            };
        }
    }
}
