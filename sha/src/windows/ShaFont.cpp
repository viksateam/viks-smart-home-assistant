/*
 * ShaFont.h
 *
 * Author(s):
 *      Duncan Horn
 *
 *
 */

#include "ShaFont.h"

using namespace vik::graphics;

#define MAKE_FONT_BASE(x)           (1000 + (x * 256))

#define TITLE_FONT_BASE             MAKE_FONT_BASE(0)
#define APPLIANCE_FONT_BASE         MAKE_FONT_BASE(1)
#define NOTIFICATION_FONT_BASE      MAKE_FONT_BASE(2)
#define LARGE_FONT_BASE             MAKE_FONT_BASE(3)
#define ARTIST_FONT_BASE            MAKE_FONT_BASE(4)

ShaFont Fonts::TitleFont =
{
    40, false, false, false, TEXT("Segoe UI Semilight"), TITLE_FONT_BASE
};

ShaFont Fonts::ApplianceFont =
{
    34, false, false, false, TEXT("Segoe UI Semilight"), APPLIANCE_FONT_BASE
};

ShaFont Fonts::NotificationFont =
{
    11, false, false, false, TEXT("Segoe UI Semilight"), NOTIFICATION_FONT_BASE
};

ShaFont Fonts::LargeFont =
{
    86, false, false, false, TEXT("Segoe UI Light"), LARGE_FONT_BASE
};

ShaFont Fonts::ArtistFont =
{
    60, false, false, false, TEXT("Segoe UI Light"), ARTIST_FONT_BASE
};
