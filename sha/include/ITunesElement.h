/*
 * ITunesElement.h
 *
 * Author(s):
 *      Jerry Lin
 *
 * UIElement for controlling iTunes.
 */
#pragma once

#include "Button.h"
#include "Layout.h"
#include "Polygon.h"
#include "ShaStation.h"

namespace vik
{
    namespace graphics
    {
        class ITunesElement final :
            public Layout
        {
        public:
            ITunesElement(_In_ ITunes *target);
            ITunesElement(
                _In_ ITunes *target,
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height);
            ITunesElement(
                _In_ ITunes *target,
                _In_ const Point &pt,
                _In_ GLfloat width,
                _In_ GLfloat height);
            ITunesElement(_In_ ITunes *target, _In_ const Rect &rc);

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual bool   OnPointerMoved(_In_ Point pt);

            void OnStatusChanged(_In_ ITunesStatus status);

        private:

            /* LayoutAwareElement */
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            virtual bool _OnButtonPressed(_In_ ButtonBase *sender, _In_ const Point &pt);

            ITunesStatus    _status;
            shapes::Polygon _image;

            ITunes  *_target;
            Button  _nextTrackButton;
            Button  _previousTrackButton;
            Button  _songStatusButton;
            GLfloat _statusWidth;
        };
    }
}
