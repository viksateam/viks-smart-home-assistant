/*
 * Ellipse.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines a UIElement for an Ellipse. The Ellipse UIElement is similar to vik::graphics::Oval in
 * the sense that it caches the points necessary for drawing, but differs in the fact that the
 * Ellipse UIElement has a border. Thus, the Ellipse must cache its own points. For this reason,
 * resizing ellipses is very expensive, so it is not recommended that you animate them much if at
 * all
 */
#pragma once

#include "Shape.h"

namespace vik
{
    namespace graphics
    {
        namespace shapes
        {
            class Ellipse :
                public Shape
            {
            public:
                Ellipse(_In_ int res = 100);
                Ellipse(
                    _In_ GLfloat x,
                    _In_ GLfloat y,
                    _In_ GLfloat width,
                    _In_ GLfloat height,
                    _In_ int res = 100);
                Ellipse(
                    _In_ const Point &pt,
                    _In_ GLfloat width,
                    _In_ GLfloat height,
                    _In_ int res = 100);
                Ellipse(_In_ const Rect &rc, _In_ int res = 100);

                /* IAnimatable */
                virtual void    SetWidth(_In_ GLfloat width);
                virtual void    SetHeight(_In_ GLfloat height);
                virtual void    SetRect(_In_ const Rect &rc);
                virtual GLfloat IncreaseWidth(_In_ GLfloat deltaWidth);
                virtual GLfloat IncreaseHeight(_In_ GLfloat deltaHeight);

                /* UIElement */
                virtual GLvoid  Draw(_In_ GLfloat offX, _In_ GLfloat offY);

                /* Shape */
                virtual void    SetBorderThickness(_In_ GLfloat thickness);

            protected:

                virtual void _OnResize(void);

                int _res;
                std::vector<Point> _points;
            };
        }
    }
}
