/*
 * MockSal.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Mock definitions for SAL annotations used when developing outside of Windows/Visual Studio. Add
 * as needed. It should go without saying, but DO NOT include this file in a WIN32 project.
 * Instead, use the proper sal.h header file.
 */
#pragma once

#ifdef  WIN32
static_assert(false, "ERROR: Do not include MockSal.h in a Win32 project");
#endif  /* WIN32 */

/* INPUT */
#define _In_
#define _In_opt_

#define _In_z_
#define _In_opt_z_

#define _In_reads_(size)
#define _In_reads_opt_(size)
#define _In_reads_bytes_(size)
#define _In_reads_bytes_opt_(size)
#define _In_reads_z_(size)
#define _In_reads_opt_z_(size)
#define _In_reads_or_z_(size)
#define _In_reads_or_z_opt_(size)

#define _In_reads_to_ptr_(ptr)
#define _In_reads_to_ptr_opt_(ptr)
#define _In_reads_to_ptr_z_(ptr)
#define _In_reads_to_ptr_opt_z_(ptr)



/* INPUT/OUTPUT */
#define _Inout_
#define _Inout_opt_

#define _Inout_z_
#define _Inout_opt_z_

#define _Inout_updates_(size)
#define _Inout_updates_opt_(size)
#define _Inout_updates_z_(size)
#define _Inout_updates_opt_z_(size)

#define _Inout_updates_to_(size,count)
#define _Inout_updates_to_opt_(size,count)

#define _Inout_updates_all_(size)
#define _Inout_updates_all_opt_(size)

#define _Inout_updates_bytes_(size)
#define _Inout_updates_bytes_opt_(size)

#define _Inout_updates_bytes_to_(size,count)
#define _Inout_updates_bytes_to_opt_(size,count)

#define _Inout_updates_bytes_all_(size)
#define _Inout_updates_bytes_all_opt_(size)



/* OUTPUT */
#define _Out_
#define _Out_opt_

#define _Out_writes_(size)
#define _Out_writes_opt_(size)
#define _Out_writes_bytes_(size)
#define _Out_writes_bytes_opt_(size)
#define _Out_writes_z_(size)
#define _Out_writes_opt_z_(size)

#define _Out_writes_to_(size,count)
#define _Out_writes_to_opt_(size,count)
#define _Out_writes_all_(size)
#define _Out_writes_all_opt_(size)

#define _Out_writes_bytes_to_(size,count)
#define _Out_writes_bytes_to_opt_(size,count)
#define _Out_writes_bytes_all_(size)
#define _Out_writes_bytes_all_opt_(size)

#define _Out_writes_to_ptr_(ptr)
#define _Out_writes_to_ptr_opt_(ptr)
#define _Out_writes_to_ptr_z_(ptr)
#define _Out_writes_to_ptr_opt_z_(ptr)

#define _Outptr_
#define _Outptr_result_maybenull_
#define _Outptr_opt_
#define _Outptr_opt_result_maybenull_

#define _Outptr_result_z_
#define _Outptr_opt_result_z_
#define _Outptr_result_maybenull_z_
#define _Outptr_opt_result_maybenull_z_

#define _Outptr_result_nullonfailure_
#define _Outptr_opt_result_nullonfailure_

#define _Outptr_result_buffer_(size)
#define _Outptr_opt_result_buffer_(size)
#define _Outptr_result_buffer_to_(size, count)
#define _Outptr_opt_result_buffer_to_(size, count)

#define _Outptr_result_buffer_all_(size)
#define _Outptr_opt_result_buffer_all_(size)

#define _Outptr_result_buffer_maybenull_(size)
#define _Outptr_opt_result_buffer_maybenull_(size)
#define _Outptr_result_buffer_to_maybenull_(size, count)
#define _Outptr_opt_result_buffer_to_maybenull_(size, count)

#define _Outptr_result_buffer_all_maybenull_(size)
#define _Outptr_opt_result_buffer_all_maybenull_(size)

#define _Outptr_result_bytebuffer_(size)
#define _Outptr_opt_result_bytebuffer_(size)
#define _Outptr_result_bytebuffer_to_(size, count)
#define _Outptr_opt_result_bytebuffer_to_(size, count)

#define _Outptr_result_bytebuffer_all_(size)
#define _Outptr_opt_result_bytebuffer_all_(size)

#define _Outptr_result_bytebuffer_maybenull_(size)
#define _Outptr_opt_result_bytebuffer_maybenull_(size)
#define _Outptr_result_bytebuffer_to_maybenull_(size, count)
#define _Outptr_opt_result_bytebuffer_to_maybenull_(size, count)

#define _Outptr_result_bytebuffer_all_maybenull_(size)
#define _Outptr_opt_result_bytebuffer_all_maybenull_(size)



/* RETURN */
#define _Success_(x)
#define _Check_return_
#define _Must_inspect_result_
