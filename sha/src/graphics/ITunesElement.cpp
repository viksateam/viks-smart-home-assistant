/*
 * ITunesElement.cpp
 *
 * Author(s):
 *      Jerry Lin
 *
 * UIElement for controlling iTunes.
 */

#include "HomePage.h"
#include "ITunesElement.h"
#include "ShaColors.h"
#include "ShaFont.h"
#include "WindowHelpers.h"

using namespace vik;
using namespace vik::graphics;

#define BUTTON_HEIGHT           (125)
#define BUTTON_PADDING          (30)

#define LOWER_STATUS_PADDING    (100)

#define IMAGE_WIDTH             (1.8f)
#define IMAGE_HEIGHT            (1.97f)

ITunesElement::ITunesElement(_In_ ITunes *target) :
    ITunesElement(target, Rect(0, 0, 0, 0))
{
}

ITunesElement::ITunesElement(
    _In_ ITunes *target,
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    ITunesElement(target, Rect(x, y, width, height))
{
}

ITunesElement::ITunesElement(
    _In_ ITunes *target,
    _In_ const Point &pt,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    ITunesElement(target, Rect(pt, width, height))
{
}

ITunesElement::ITunesElement(_In_ ITunes *target, _In_ const Rect &rc) :
    Layout(rc),
    _target(target),
    _image(0, 0, IMAGE_WIDTH, IMAGE_HEIGHT)
{
    // Initialize the image
    Point start(0.64f, 0.41f);
    for (auto &pt : itunesPoints)
    {
        this->_image.AddPoint(pt - start);
    }
    this->_image.SetBorderColor(ShaColors::ForegroundColor);
    this->_image.SetBorderThickness(3);

    // Initialize the buttons
    this->_nextTrackButton.SetPreferredColors(ShaColors::ForegroundColor,
        ShaColors::BackgroundColor, ShaColors::ButtonHoverColor, ShaColors::ForegroundColor);
    this->_nextTrackButton.OnButtonUp(this,
        (ButtonBase::ButtonEventCallback)&ITunesElement::_OnButtonPressed);
    this->_nextTrackButton.SetText(">>");

    this->_previousTrackButton.SetPreferredColors(ShaColors::ForegroundColor,
        ShaColors::BackgroundColor, ShaColors::ButtonHoverColor, ShaColors::ForegroundColor);
    this->_previousTrackButton.OnButtonUp(this,
        (ButtonBase::ButtonEventCallback)&ITunesElement::_OnButtonPressed);
    this->_previousTrackButton.SetText("<<");

    this->_songStatusButton.SetPreferredColors(ShaColors::ForegroundColor,
        ShaColors::BackgroundColor, ShaColors::ButtonHoverColor, ShaColors::ForegroundColor);
    this->_songStatusButton.OnButtonUp(this,
        (ButtonBase::ButtonEventCallback)&ITunesElement::_OnButtonPressed);

    this->_status = ITunesStatus{ ITunesStatus::SS_PLAY };
    this->OnStatusChanged(this->_status);

    if (rc.width != 0 && rc.height != 0)
    {
        this->_OnSizeUpdated(rc.width, rc.height);
    }
}



/*
 * UIElement
 */
GLvoid ITunesElement::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    this->_image.Draw(offX, offY);
    WindowDrawText(offX + TITLE_X_OFFSET, offY + TITLE_Y_OFFSET, this->_target->Name().c_str(),
        Fonts::TitleFont, ShaColors::ForegroundColor);

    // Draw the buttons
    this->_nextTrackButton.Draw(offX, offY);
    this->_previousTrackButton.Draw(offX, offY);
    this->_songStatusButton.Draw(offX, offY);

    // Draw the song information
    GLfloat x = this->GetWidth() - SIDE_PADDING - this->_statusWidth;
    GLfloat y = TOP_PADDING;
    WindowDrawText(x + offX, y + offY, this->_status.artistName.c_str(), Fonts::ArtistFont,
        ShaColors::ForegroundColor);
    y += Fonts::ArtistFont.height * 2.0f;
    WindowDrawText(x + offX, y + offY, this->_status.albumName.c_str(), Fonts::TitleFont,
        ShaColors::ForegroundColor);
    y += Fonts::TitleFont.height * 2.0f;
    WindowDrawText(x + offX, y + offY, this->_status.trackName.c_str(), Fonts::ApplianceFont,
        ShaColors::ForegroundColor);
}

bool ITunesElement::OnPointerMoved(_In_ Point pt)
{
    if (Layout::OnPointerMoved(pt))
    {
        return true;
    }

    // Now, check our children
    if (this->_nextTrackButton.GetRect().Contains(pt) &&
        this->_nextTrackButton.OnPointerEntered(pt - this->_nextTrackButton.GetLocation()))
    {
        this->_pointerTarget = &this->_nextTrackButton;
        return true;
    }
    else if (this->_previousTrackButton.GetRect().Contains(pt) &&
        this->_previousTrackButton.OnPointerEntered(pt - this->_previousTrackButton.GetLocation()))
    {
        this->_pointerTarget = &this->_previousTrackButton;
        return true;
    }
    else if (this->_songStatusButton.GetRect().Contains(pt) &&
        this->_songStatusButton.OnPointerEntered(pt - this->_songStatusButton.GetLocation()))
    {
        this->_pointerTarget = &this->_songStatusButton;
        return true;
    }

    return false;
}



void ITunesElement::OnStatusChanged(_In_ ITunesStatus status)
{
    this->_status = status;

    switch (status.songState)
    {
    case ITunesStatus::SS_PLAY:
        this->_songStatusButton.SetText("Pause");
        break;

    case ITunesStatus::SS_PAUSE:
        this->_songStatusButton.SetText("Play");
        break;

    default:
        // Not reached
        abort();
        break;
    }
}



/*
 * LayoutAwareElement
 */
void ITunesElement::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    Layout::_OnSizeUpdated(width, height);
    GLfloat mainHeight = height - (TOP_PADDING + BOTTOM_PADDING);

    GLfloat mainWidth = width - 2 * SIDE_PADDING;
    this->_statusWidth = mainWidth / 2;

    // Handle buttons' sizes/locations
    GLfloat x = this->_statusWidth + BUTTON_PADDING;
    GLfloat y = height - BOTTOM_PADDING - BUTTON_HEIGHT - (BUTTON_HEIGHT / 2) -
        LOWER_STATUS_PADDING;
    GLfloat buttonWidth = (this->_statusWidth - (4 * BUTTON_PADDING)) / 3;
    this->_previousTrackButton.SetRect(Rect(x, y, buttonWidth, BUTTON_HEIGHT));

    x += buttonWidth + BUTTON_PADDING;
    y -= (BUTTON_HEIGHT / 2);
    this->_songStatusButton.SetRect(Rect(x, y, buttonWidth, BUTTON_HEIGHT * 2));

    x += buttonWidth + BUTTON_PADDING;
    y += (BUTTON_HEIGHT / 2);
    this->_nextTrackButton.SetRect(Rect(x, y, buttonWidth, BUTTON_HEIGHT));

    // Handle the "picture" size/location
    if (mainHeight < 0 || this->_statusWidth < 0)
    {
        // Prevent setting rectangle widths to zero
        this->_image.SetRect(Rect(0, 0, 3, 4));
    }
    else
    {
        GLfloat dim = std::min(mainHeight / IMAGE_HEIGHT, this->_statusWidth / IMAGE_WIDTH);
        x = SIDE_PADDING + (this->_statusWidth - dim * IMAGE_WIDTH) / 2;
        y = TOP_PADDING + (mainHeight - dim * IMAGE_HEIGHT) / 2;

        this->_image.SetRect(Rect(x, y, dim * IMAGE_WIDTH, dim * IMAGE_HEIGHT));
    }
}



bool ITunesElement::_OnButtonPressed(_In_ ButtonBase *sender, _In_ const Point &pt)
{
    (void)pt;

    if (sender == &this->_previousTrackButton)
    {
        this->_target->PostStatusRequest(TR_PREVIOUS);
    }
    else if (sender == &this->_songStatusButton)
    {
        ITunesStatus::SongStatus status = this->_status.songState;
        if (status == ITunesStatus::SS_PLAY)
        {
            status = ITunesStatus::SS_PAUSE;
        }
        else
        {
            status = ITunesStatus::SS_PLAY;
        }

        this->_target->PostStatusRequest(status);
    }
    else if (sender == &this->_nextTrackButton)
    {
        this->_target->PostStatusRequest(TR_NEXT);
    }

    return true;
}
