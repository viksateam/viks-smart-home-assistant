/*
 * Drawing.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Functions used to draw shapes in OpenGL. In general, it is not recommended that you call these
 * functions directly unless you are creating a custom UIElement.
 */

#include "Drawing.h"

using namespace vik::graphics;

/*
 * Rectangles
 */
static GLvoid _DrawRect(_In_ GLenum mode, _In_ const Rect &rc)
{
    glBegin(mode);

    glVertex2f(rc.pt.x, rc.pt.y);
    glVertex2f(rc.pt.x + rc.width, rc.pt.y);
    glVertex2f(rc.pt.x + rc.width, rc.pt.y + rc.height);
    glVertex2f(rc.pt.x, rc.pt.y + rc.height);

    glEnd();
}

GLvoid vik::graphics::DrawRect(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height)
{
    Rect rc(x, y, width, height);
    DrawRect(rc);
}

GLvoid vik::graphics::DrawRect(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ const Color &color)
{
    glColor4fv(color);
    DrawRect(x, y, width, height);
}

GLvoid vik::graphics::DrawRect(_In_ const Rect &rc)
{
    _DrawRect(GL_LINE_LOOP, rc);
}

GLvoid vik::graphics::DrawRect(_In_ const Rect &rc, _In_ const Color &color)
{
    glColor4fv(color);
    DrawRect(rc);
}



GLvoid vik::graphics::FillRect(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height)
{
    Rect rc(x, y, width, height);
    FillRect(rc);
}

GLvoid vik::graphics::FillRect(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ const Color &color)
{
    glColor4fv(color);
    FillRect(x, y, width, height);
}

GLvoid vik::graphics::FillRect(_In_ const Rect &rc)
{
    _DrawRect(GL_QUADS, rc);
}

GLvoid vik::graphics::FillRect(_In_ const Rect &rc, _In_ const Color &color)
{
    glColor4fv(color);
    FillRect(rc);
}



/*
 * Ovals (and Circles)
 */
static GLvoid _DrawOval(_In_ GLenum mode, _In_ const Oval &oval)
{
    //glVertexPointer(2, GL_FLOAT, 0, &oval.vertices[0]);
    //glDrawArrays(mode, 0, oval.vertices.size());

    glBegin(mode);

    for (auto &pt : oval.vertices)
    {
        glVertex2fv(pt.vertex);
    }

    glEnd();
}

GLvoid vik::graphics::DrawOval(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ uint32_t res)
{
    Oval oval(x, y, width, height, res);
    DrawOval(oval);
}

GLvoid vik::graphics::DrawOval(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ uint32_t res,
    _In_ const Color &color)
{
    glColor4fv(color);
    DrawOval(x, y, width, height, res);
}

GLvoid vik::graphics::DrawOval(_In_ const Oval &oval)
{
    _DrawOval(GL_LINE_LOOP, oval);
}

GLvoid vik::graphics::DrawOval(_In_ const Oval &oval, _In_ const Color &color)
{
    glColor4fv(color);
    DrawOval(oval);
}



GLvoid vik::graphics::FillOval(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ uint32_t res)
{
    Oval oval(x, y, width, height, res);
    FillOval(oval);
}

GLvoid vik::graphics::FillOval(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ uint32_t res,
    _In_ const Color &color)
{
    glColor4fv(color);
    FillOval(x, y, width, height, res);
}

GLvoid vik::graphics::FillOval(_In_ const Oval &oval)
{
    _DrawOval(GL_POLYGON, oval);
}

GLvoid vik::graphics::FillOval(_In_ const Oval &oval, _In_ const Color &color)
{
    glColor4fv(color);
    FillOval(oval);
}
