/*
 * ShaStation.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents the relevant information needed for communicating with either an appliance or a
 * control station. Please note that properties pertaining to the control/appliance station are not
 * stored in this structure. Instead, please use separate derived structures for keeping track of
 * relevant information.
 */
#pragma once

#include <string>
#include <vector>

#include "InetAddress.h"
#include "UIElement.h"

#define COFFEE_MAKER_TYPE           ("coffee_machine")
#define CONTROL_STATION_TYPE        ("control_station")
#define LIGHT_TYPE                  ("light")
#define THERMOSTAT_TYPE             ("thermostat")
#define ITUNES_TYPE                 ("itunes")

#define TITLE_X_OFFSET              (140)
#define TITLE_Y_OFFSET              (40)

#define TOP_PADDING                 (150)
#define BOTTOM_PADDING              (40)
#define SIDE_PADDING                (70)

namespace vik
{
    enum ShaStationType
    {
        SST_CONTROL_STATION,
        SST_APPLIANCE_STATION
    };

    class ShaStation
    {
    public:
        ShaStation(
            _In_ ShaStationType type,
            _In_ const communication::InetAddress &addr,
            _In_ const std::string &id,
            _In_ const std::string &name);
        virtual ~ShaStation(void);

        static ShaStation *Convert(
            _In_ const ShaStation *prev,
            _In_ const std::string &type,
            _In_ const std::vector<std::string> &args);

        virtual const ShaStationType             &Type(void) const;
        virtual const communication::InetAddress &InetAddr(void) const;
        virtual const std::string                &Id(void) const;
        virtual const std::string                &Name(void) const;

        virtual bool IsAlive(void) const;
        virtual void SetAlive(_In_ bool isAlive);

        virtual bool PostGetStatusRequest(void);
        virtual void PostStationRequest(_In_ const std::string &msg) const;

        virtual graphics::UIElement *GetElementRepresentation(void);

    protected:

        virtual graphics::UIElement *_CreateElementRepresentation(void);

        ShaStationType             _type;
        communication::InetAddress _addr;
        std::string                _id;
        std::string                _name;
        int                        _alive;
        graphics::UIElement        *_element;
    };



    /*
     * Coffee Machine
     */
    struct CoffeeMachineStatus
    {
        enum CoffeeMachineState
        {
            COFFEE_OFF,
            COFFEE_ON,
            COFFEE_DONE
        };

        CoffeeMachineState state;
        int cups;
    };

    class CoffeeMachine final :
        public ShaStation
    {
    public:
        CoffeeMachine(_In_ const ShaStation &station, _In_ const std::vector<std::string> &args);

        const CoffeeMachineStatus &GetStatus(void) const;
        void                      SetStatus(_In_ const CoffeeMachineStatus &status);

        const std::vector<std::string> &GetOptions(void) const;

        virtual bool PostGetStatusRequest(void);
        void         PostStatusRequest(_In_ const CoffeeMachineStatus &status) const;

    protected:

        virtual graphics::UIElement *_CreateElementRepresentation(void);

        CoffeeMachineStatus      _status;
        std::vector<std::string> _options;
    };



    /*
     * ITunes
     */
    struct ITunesStatus
    {
        enum SongStatus
        {
            SS_PLAY,
            SS_PAUSE
        };

        SongStatus songState;
        std::string artistName;
        std::string albumName;
        std::string trackName;
    };

    enum TrackRequest
    {
        TR_NEXT,
        TR_PREVIOUS
    };

    class ITunes final :
        public ShaStation
    {
    public:
        ITunes(_In_ const ShaStation &station, _In_ const std::vector<std::string> &args);

        const ITunesStatus &GetStatus(void) const;
        void                SetStatus(_In_ const ITunesStatus &status);

        virtual bool PostGetStatusRequest(void);
        void         PostStatusRequest(_In_ ITunesStatus::SongStatus status);
        void         PostStatusRequest(_In_ TrackRequest status);

    protected:

        virtual graphics::UIElement *_CreateElementRepresentation(void);

        ITunesStatus _status;
    };



    /*
     * Light
     */
    enum LightStatus
    {
        LIGHT_OFF,
        LIGHT_ON
    };

    class Light final :
        public ShaStation
    {
    public:
        Light(_In_ const ShaStation &station, _In_ const std::vector<std::string> &args);

        LightStatus GetStatus(void) const;
        void        SetStatus(_In_ LightStatus status);

        virtual bool PostGetStatusRequest(void);
        void         PostStatusRequest(_In_ LightStatus status) const;

    protected:

        virtual graphics::UIElement *_CreateElementRepresentation(void);

        LightStatus _status;
    };



    /*
     * Thermostat
     */
    struct ThermostatStatus
    {
        enum SystemStatus
        {
            SS_OFF  = 0,
            SS_COOL = 1,
            SS_HEAT = 2
        };

        enum FanStatus
        {
            FS_ON   = 0,
            FS_AUTO = 1
        };

        SystemStatus system;
        FanStatus fan;
        int currTemp;
        int setTemp;
    };

    class Thermostat final :
        public ShaStation
    {
    public:
        Thermostat(_In_ const ShaStation &station, _In_ const std::vector<std::string> &args);

        const ThermostatStatus &GetStatus(void) const;
        void                   SetStatus(_In_ const ThermostatStatus &status);

        int          GetMinTemp(void) const;
        int          GetMaxTemp(void) const;

        virtual bool PostGetStatusRequest(void);
        void         PostStatusRequest(_In_ const ThermostatStatus &status) const;

    protected:

        virtual graphics::UIElement *_CreateElementRepresentation(void);

        void _PostSystemStatusRequest(_In_ ThermostatStatus::SystemStatus status) const;
        void _PostFanStatusRequest(_In_ ThermostatStatus::FanStatus status) const;
        void _PostTempRequest(_In_ int temp) const;

        ThermostatStatus _status;

        int _minTemp;
        int _maxTemp;
    };
}
