/*
 * Button.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a button 
 */
#pragma once

#include "ButtonBase.h"
#include "Rectangle.h"
#include "ShaFont.h"

namespace vik
{
    namespace graphics
    {
        class Button :
            public ButtonBase
        {
            enum ButtonState
            {
                BS_NORMAL,
                BS_HOVER,
                BS_PRESSED,
                BS_EXITED
            };

        public:
            Button(void);
            Button(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
            Button(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            Button(_In_ const Rect &rc);

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual void   SetBackgroundColor(_In_ const Color &color);
            virtual void   SetForegroundColor(_In_ const Color &color);
            virtual bool   OnPointerEntered(_In_ Point pt);
            virtual bool   OnPointerExited(void);
            virtual bool   OnPointerDown(_In_ Point pt);
            virtual bool   OnPointerUp(_In_ Point pt);

            /* Text */
            void              SetFont(_In_ const ShaFont &font);
            void              SetText(_In_ const std::string &str);
            const ShaFont     &GetFont(void) const;
            const std::string &GetText(void) const;

            /* Button Colors */
            void SetBorderColor(_In_ const Color &color);
            void SetHoverBackgroundColor(_In_ const Color &color);
            void SetHoverForegroundColor(_In_ const Color &color);
            void SetHoverBorderColor(_In_ const Color &color);
            void SetPressedBackgroundColor(_In_ const Color &color);
            void SetPressedForegroundColor(_In_ const Color &color);
            void SetPressedBorderColor(_In_ const Color &color);
            void SetPreferredColors(
                _In_ const Color &textColor,
                _In_ const Color &fillColor,
                _In_ const Color &hoverColor,
                _In_ const Color &borderColor);

        private:

            /* LayoutAwareElement */
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            void _OnStateChanged(_In_ ButtonState prevState);
            void _CalculateTextLength(void);

            ShaFont           _font;
            std::string       _text;
            GLfloat           _textLength;
            shapes::Rectangle _content;
            ButtonState       _state;

            Color _borderColor;
            Color _hoverBackgroundColor;
            Color _hoverForegroundColor;
            Color _hoverBorderColor;
            Color _pressedBackgroundColor;
            Color _pressedForegroundColor;
            Color _pressedBorderColor;
        };
    }
}
