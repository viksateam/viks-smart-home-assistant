/*
 * ShaLeap.h
 *
 * Author(s):
 *      Jerry Lin
 *
 * The interface used by the Smart Home Assistant to coordinate between the Leap and the graphical
 * output.
 */
#pragma once

#include <map>
#include <vector>

#include "Leap.h"

#include "sha.h"
#include "ShaGesture.h"
#include "ShaPinchGesture.h"
#include "ShaSwipeGesture.h"

#define LEAP_DEFAULT_X_MIN      -180
#define LEAP_DEFAULT_X_MAX      180
#define LEAP_DEFAULT_Y_MIN      200
#define LEAP_DEFAULT_Y_MAX      470
#define LEAP_DEFAULT_Z_MIN      -150
#define LEAP_DEFAULT_Z_MAX      250

#define INVALID_LEAP_ID         -1

using namespace Leap;

namespace vik
{
    namespace leap
    {
        void InitializeLeap(void);
        bool ConfigureController(_Inout_ Controller &controller);

        class ShaListener final :
            public Listener
        {
        public:
            ShaListener(void);
            ~ShaListener(void);

            virtual void onInit(_In_ const Controller &controller);
            virtual void onConnect(_In_ const Controller &controller);
            virtual void onDisconnect(_In_ const Controller &controller);
            virtual void onExit(_In_ const Controller &controller);
            virtual void onFrame(_In_ const Controller &controller);
            virtual void onFocusGained(_In_ const Controller &controller);
            virtual void onFocusLost(_In_ const Controller &controller);

            GLfloat GetXMin(void) const;
            GLfloat GetXMax(void) const;
            GLfloat GetYMin(void) const;
            GLfloat GetYMax(void) const;
            GLfloat GetLeapX(void) const;
            GLfloat GetLeapY(void) const;
            void Calibrate(
                _In_ GLfloat xMin,
                _In_ GLfloat xMax,
                _In_ GLfloat yMin,
                _In_ GLfloat yMax,
                _In_ GLfloat zMin,
                _In_ GLfloat zMax);

            /* Sha-specific Gestures */
            bool DisableShaGesture(GestureType gestureType);
            bool EnableShaGesture(GestureType gestureType);

        private:

            enum PointerType
            {
                PT_FINGER,
                PT_PALM,
                PT_PINCH
            };

            GLfloat _GetWindowX(_In_ GLfloat x) const;
            GLfloat _GetWindowY(_In_ GLfloat y) const;
            Vector  _GetTransformedPosition(_In_ const Vector &position) const;
            void _ProcessHands(_In_ const Controller &controller);
            void _ProcessGestures(_In_ const Controller &controller);
            void _ProcessScreenTap(_In_ const ScreenTapGesture &screenTap);
            void _ProcessPointerMoved(_In_ const Vector &position);
            void _ProcessPointerExited(void);
            void _ShiftBoundaries(_In_ const Vector &position);
            bool _IsInBounds(_In_ GLfloat windowX, _In_ GLfloat windowY, _In_ GLfloat leapZ) const;

            /* Sha-specific Gestures */
            void _ProcessShaGestures(_In_ const Controller &controller);
            void _ProcessShaPinchGesture(_In_ ShaPinchGesture *pinchGesture);
            void _ProcessShaSwipeGesture(_In_ ShaSwipeGesture *swipeGesture);
            bool _ContainsGestureType(_In_ GestureType gestureType) const;
            bool _IsValidShaGesture(_In_ int gestureId) const;

            GLfloat _x;
            GLfloat _y;
            GLfloat _z;
            GLfloat _xMin;
            GLfloat _xMax;
            GLfloat _yMin;
            GLfloat _yMax;
            GLfloat _baseXMin;
            GLfloat _baseXMax;
            GLfloat _baseYMin;
            GLfloat _baseYMax;
            GLfloat _baseZMin;
            GLfloat _baseZMax;
            bool    _isExited;

            /* Sha-specific Variables */
            int                         _currentShaGestureId;
            bool                        _isPinching;
            bool                        _isSwiping;
            bool                        _isShaGestureHandled;
            PointerType                 _pointerType;
            Vector                      _pinchPosition;
            std::vector<ShaGesture *>   _shaGestures;
            std::map<GestureType, bool> _enabledGestures;
        };
    }
}
