/*
 * main.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * A simple test client using WinSock
 */

#include <iostream>
#include <string>

#include "Socket.h"

using namespace vik::communication;

int main(void)
{
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData))
    {
        std::cout << "ERROR: Could not initialize WinSock (" << WSAGetLastError() << ")" << std::endl;
        return 1;
    }

    // Get host information
    std::string ip;
    USHORT port;

    std::cout << "Enter host ip address: ";
    std::cin >> ip;
    std::cout << "Enter host port number: ";
    std::cin >> port;

    // Send message
    std::string message;

    // Need to flush the buffer
    std::getline(std::cin, message);
    std::cout << "Enter message: ";
    std::getline(std::cin, message);

    while (message != "exit")
    {
        // Open the socket and attempt to send a packet
        Socket client(InetAddress(ip.c_str(), port));
        if (!client.IsConnected())
        {
            std::cout << "ERROR: Failed to connect to host (" << client.GetError() << ")" << std::endl;
            WSACleanup();
            return 1;
        }

        message += "\n";
        if (client.Send(message.c_str(), message.length()) < 0)
        {
            std::cout << "ERROR: Failed to send message to host (" << client.GetError() << ")" << std::endl;
            client.Close();
            WSACleanup();
            return 1;
        }

        // Get response
        char rgszBuffer[1000];
        int nLen;
        if ((nLen = client.Receive(rgszBuffer, sizeof(rgszBuffer) - 1)) < 0)
        {
            std::cout << "ERROR: Failed to receive message from host (" << client.GetError() << ")" << std::endl;
            client.Close();
            WSACleanup();
            return 1;
        }
        else
        {
            rgszBuffer[nLen] = '\0';
            std::cout << rgszBuffer;
        }

        client.Close();
        std::cout << "Enter message: ";
        std::getline(std::cin, message);
    }

    if (WSACleanup())
    {
        std::cout << "ERROR: Could not close WinSock (" << WSAGetLastError() << ")" << std::endl;
    }
}
