/*
 * ConcurrentAnimation.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a "group" of animations that begin execution at the same time. This allows for
 * (1) multiple animations to wait on another at the same time, or (2) one (or more) animations to
 * wait on multiple to finish.
 *
 * By default, all child animations are begun at the same time and completed/cancelled at the same
 * time. However, it may be desired that pending animations of our children begin immediately after
 * that child has completed. If this is the case, the ConcurrentAnimation class provides the
 * GetCompletedAnimations funciton. When called, all completed animations that have yet to be
 * claimed are marked as completed (by calling OnAnimationCompleted), removed from the collection
 * of completed animations, and returned to the calling function. Thus, the responsibility of
 * destroying these animations and propertly handling their pending animations is handed over to
 * the calling function. Note that it is considered unsafe to call this function after a call to
 * OnAnimationCompleted or OnAnimationCancelled has been made.
 *
 * Calling SetPendingAnimation on a ConcurrentAnimation where the pending animation has already
 * been set causes the current pending animation and the new pending animation to get "joined"
 * together into one ConcurrentAnimation. Additionally, once OnAnimationCompleted or
 * OnAnimationCancelled gets called, the pending animations of all unclaimed children are "joined"
 * with the current pending animation as well.
 */

#include <cassert>
#include <vector>

#include "ConcurrentAnimation.h"

using namespace vik::graphics::animations;

/*
 * Constructors/Destructors
 */
ConcurrentAnimation::ConcurrentAnimation(void) :
    _started(false),
    _completed(false)
{
}

ConcurrentAnimation::~ConcurrentAnimation(void)
{
    assert(this->_completed);

    // There should be no more animations remaining. However, it could be the case that the
    // animation was cancelled (not recommended). In that case, go ahead and delete those animation
    // resources
    for (auto ani : this->_animations)
    {
        delete ani;
    }

    for (auto ani : this->_completedAnimations)
    {
        delete ani;
    }
}



/*
 * IAnimation
 */
void ConcurrentAnimation::OnAnimationBegin(void)
{
    assert(!this->_started);
    assert(!this->_completed);
    assert(this->_completedAnimations.empty());
    AnimationBase::OnAnimationBegin();

    this->_started = true;

    for (auto ani : this->_animations)
    {
        ani->OnAnimationBegin();
    }
}

bool ConcurrentAnimation::OnAnimationUpdate(_In_ uint64_t timeMicro)
{
    assert(this->_started);
    assert(!this->_completed);

    for (auto itr = std::begin(this->_animations); itr != std::end(this->_animations); )
    {
        if ((*itr)->OnAnimationUpdate(timeMicro))
        {
            this->_completedAnimations.push_back(*itr);
            itr = this->_animations.erase(itr);
        }
        else
        {
            ++itr;
        }
    }

    return this->_animations.empty();
}

void ConcurrentAnimation::OnAnimationCompleted(void)
{
    assert(this->_started);
    assert(!this->_completed);
    assert(this->_animations.empty());
    AnimationBase::OnAnimationCompleted();

    this->_completed = true;

    for (auto ani : this->_completedAnimations)
    {
        ani->OnAnimationCompleted();
    }

    this->_CollectPendingAnimations();
}

void ConcurrentAnimation::OnAnimationCancelled(void)
{
    assert(!this->_completed);
    AnimationBase::OnAnimationCancelled();

    this->_completed = true;

    // When a ConcurrentAnimation is cancelled, it may be the case that there are animations that
    // have already finished. This fact needs to be ignored as any potential animations that are
    // pending for those IAnimations become our reponsibility.
    for (auto ani : this->_animations)
    {
        ani->OnAnimationCancelled();
    }
    for (auto ani : this->_completedAnimations)
    {
        ani->OnAnimationCancelled();
    }

    this->_CollectPendingAnimations();
}

void ConcurrentAnimation::AddPendingAnimation(_In_ IAnimation *ani)
{
    assert(!this->_completed);
    AnimationBase::AddPendingAnimation(ani);
}



/*
 * ConcurrentAnimation
 */
void ConcurrentAnimation::AddAnimation(_In_ IAnimation *ani)
{
    assert(!this->_completed);
    this->_animations.push_back(ani);

    if (this->_started)
    {
        ani->OnAnimationBegin();
    }
}

bool ConcurrentAnimation::HasCompletedAnimations(void) const
{
    return !this->_completedAnimations.empty();
}

std::vector<IAnimation *> ConcurrentAnimation::ClaimCompletedAnimations(void)
{
    assert(!this->_completed);
    std::vector<IAnimation *> completed;

    for (auto ani : this->_completedAnimations)
    {
        // Caller expects these animations to be completed
        ani->OnAnimationCompleted();
        completed.push_back(ani);
    }

    // All IAnimations that were previously in our completed animations list are now the
    // responsibility of the calling function. Therefore, we must clear all references to them.
    this->_completedAnimations.clear();
    return completed;
}

std::vector<IAnimation *> ConcurrentAnimation::ClaimAllAnimations(void)
{
    assert(!this->_completed);
    std::vector<IAnimation *> animations;

    for (auto ani : this->_animations)
    {
        animations.push_back(ani);
    }
    for (auto ani : this->_completedAnimations)
    {
        // Unlike with ClaimCompletedAnimations, we don't mark animations as completed as the
        // caller does not know that they have completed
        animations.push_back(ani);
    }

    // All IAnimation chlidren are no longer our responsibility
    this->_animations.clear();
    this->_completedAnimations.clear();
    return animations;
}



/*
 * Private Functions
 */
void ConcurrentAnimation::_CollectPendingAnimations(void)
{
    assert(this->_completed);

    for (auto ani : this->_animations)
    {
        const AnimationList &pending = ani->GetPendingAnimations();
        this->_pending.insert(std::end(this->_pending), std::begin(pending), std::end(pending));
    }
    for (auto ani : this->_completedAnimations)
    {
        const AnimationList &pending = ani->GetPendingAnimations();
        this->_pending.insert(std::end(this->_pending), std::begin(pending), std::end(pending));
    }
}
