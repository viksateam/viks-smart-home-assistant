/*
 * ShaCommunicationManager.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Used to facilitate communication over the network between the client/server threads and the main
 * UI thread (or other worker threads).
 */
#pragma once

#include <cstdint>
#include <string>

#include "InetAddress.h"
#include "ShaServer.h"
#include "ShaStation.h"

namespace vik
{
    namespace communication
    {
        enum RequestType
        {
            RT_GET,
            RT_POST,
        };

        inline std::ostream &operator<<(std::ostream &stream, RequestType type)
        {
            stream << ((type == RT_GET) ? "get" : "post");
            return stream;
        }

        class ShaCommunicationManager
        {
        public:
            ShaCommunicationManager(void) = delete;

            static const std::string &StationId(void);
            static const std::string &StationName(void);
            static std::string       GetInfoString(void);

            /* Incoming messages */
            static void OnBroadcastMessageReceived(
                _In_ const std::string &msg,
                _In_ const InetAddress &addr);
            static std::string OnConnectionMessageReceived(
                _In_ const std::string &msg,
                _In_ const InetAddress &addr);

            /* Outgoing messages */
            _Success_(return != INVALID_REQUEST_COOKIE)
            static request_cookie PostHeartbeatMessage(_In_ vik::ShaStation *station);
            _Success_(return != INVALID_REQUEST_COOKIE)
            static request_cookie PostServerRequest(
                _In_ const vik::ShaStation *station,
                _In_ RequestType type,
                _In_ const std::string &command,
                _In_ const std::vector<std::string> &args = std::vector<std::string>());

        private:

            static std::string s_stationId;
            static std::string s_stationName;

            static request_cookie s_nextCookie;
        };
    }
}
