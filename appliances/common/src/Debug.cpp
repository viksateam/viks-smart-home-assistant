/*
 * Debug.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Functions that are useful for debugging with the Tiva C Series TM4C123G LaunchPad Evaluation Kit
 * microcontroller. These functions are mostly used to set the on-board LEDs
 */

#include <cassert>

#include <driverlib/gpio.h>
#include <driverlib/rom_map.h>
#include <driverlib/sysctl.h>
#include <inc/hw_memmap.h>

#include "Debug.h"

#define VALID_LED_MASK(mask)        ((mask & ~LED_WHITE) == 0)

void Debug::Init(void)
{
    // Enable use of PORTF to toggle LED and disable interrupt on this port
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    MAP_GPIOIntDisable(GPIO_PORTF_BASE, 0xFF);

    // Configure Red LED
    MAP_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, LED_RED);
    MAP_GPIOPinWrite(GPIO_PORTF_BASE, LED_RED, 0x00);

    // Configure Blue LED
    MAP_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, LED_BLUE);
    MAP_GPIOPinWrite(GPIO_PORTF_BASE, LED_BLUE, 0x00);

    // Configure Green LED
    MAP_GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, LED_GREEN);
    MAP_GPIOPinWrite(GPIO_PORTF_BASE, LED_GREEN, 0x00);
}

void Debug::TurnOnLed(_In_ led_t uLedMask)
{
    assert(VALID_LED_MASK(uLedMask));
    MAP_GPIOPinWrite(GPIO_PORTF_BASE, uLedMask, uLedMask);
}

void Debug::TurnOffLed(_In_ led_t uLedMask)
{
    assert(VALID_LED_MASK(uLedMask));

    MAP_GPIOPinWrite(GPIO_PORTF_BASE, uLedMask, 0x00);
}

void Debug::ToggleLed(_In_ led_t uLedMask)
{
    assert(VALID_LED_MASK(uLedMask));
    MAP_GPIOPinWrite(GPIO_PORTF_BASE, uLedMask,
        MAP_GPIOPinRead(GPIO_PORTF_BASE, uLedMask) ^ uLedMask);
}

void Debug::SetLed(_In_ led_t uLedMask)
{
    assert(VALID_LED_MASK(uLedMask));

    // Writes to all LED ports (which is the same mask as LED_WHITE)
    MAP_GPIOPinWrite(GPIO_PORTF_BASE, LED_WHITE, uLedMask);
}
