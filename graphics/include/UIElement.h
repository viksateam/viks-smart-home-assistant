/*
 * UIElement.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines the base class for all UI Elements - UIElement. In general, you should not need to
 * define your own derivative UIElement. Instead consider creating a Component derivative and
 * setting its Template to get the desired look and feel using existing UIElements.
 */
#pragma once

#include "Animatable.h"
#include "Colors.h"

namespace vik
{
    namespace graphics
    {
        struct Padding
        {
            Padding(void) : Padding(0, 0, 0, 0) {}
            Padding(_In_ GLfloat padding) : Padding(padding, padding, padding, padding) {}
            Padding(_In_ GLfloat sides, _In_ GLfloat verticals) :
                Padding(sides, verticals, sides, verticals)
            {
            }
            Padding(_In_ GLfloat left, _In_ GLfloat top, _In_ GLfloat right, _In_ GLfloat bottom) :
                left(left),
                top(top),
                right(right),
                bottom(bottom)
            {
            }

            GLfloat top;
            GLfloat bottom;
            GLfloat left;
            GLfloat right;
        };

        enum Orientation
        {
            HORIZONTAL,
            VERTICAL
        };

        class UIElement :
            public animations::IAnimatable
        {
        public:
            UIElement(void);
            UIElement(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
            UIElement(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            UIElement(_In_ const Rect &rc);
            virtual ~UIElement(void);

            /* IAnimatable */
            virtual GLfloat GetX(void) const;
            virtual GLfloat GetY(void) const;
            virtual Point   GetLocation(void) const;
            virtual GLfloat GetWidth(void) const;
            virtual GLfloat GetHeight(void) const;
            virtual Rect    GetRect(void) const;
            virtual void    SetX(_In_ GLfloat x);
            virtual void    SetY(_In_ GLfloat y);
            virtual void    SetLocation(_In_ const Point &pt);
            virtual void    SetWidth(_In_ GLfloat width);
            virtual void    SetHeight(_In_ GLfloat height);
            virtual void    SetRect(_In_ const Rect &rc);
            virtual GLfloat TranslateX(_In_ GLfloat deltaX);
            virtual GLfloat TranslateY(_In_ GLfloat deltaY);
            virtual Point   Translate(_In_ GLfloat deltaX, _In_ GLfloat deltaY);
            virtual Point   Translate(_In_ const Point &pt);
            virtual GLfloat IncreaseWidth(_In_ GLfloat deltaWidth);
            virtual GLfloat IncreaseHeight(_In_ GLfloat deltaHeight);
            _Success_(return != INVALID_ANIMATION_COOKIE)
            virtual animations::animation_cookie SubmitAnimation(
                _In_ animations::IAnimation *animation);
            _Success_(return != INVALID_ANIMATION_COOKIE)
            virtual animations::animation_cookie SubmitPendingAnimation(
                _In_ animations::IAnimation *animation);
            _Success_(return != INVALID_ANIMATION_COOKIE)
            virtual animations::animation_cookie SubmitOverrideAnimation(
                _In_ animations::IAnimation *animation);
            virtual bool    CancelCurrentAnimation(void);

            /* Colors */
            virtual const Color &GetBackgroundColor(void) const;
            virtual const Color &GetForegroundColor(void) const;
            virtual void        SetBackgroundColor(_In_ const Color &color);
            virtual void        SetForegroundColor(_In_ const Color &color);

            /* "Abstract" functions */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY) = 0;

            /* Pointer Input */
            virtual bool OnPointerEntered(_In_ Point pt);
            virtual bool OnPointerExited(void);
            virtual bool OnPointerMoved(_In_ Point pt);
            virtual bool OnPointerDown(_In_ Point pt);
            virtual bool OnPointerUp(_In_ Point pt);

        protected:

            Rect    _rect;
            animations::animation_cookie _animationCookie;

            Color   _backgroundColor;
            Color   _foregroundColor;
        };
    }
}
