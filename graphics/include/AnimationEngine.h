/*
 * AnimationEngine.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines animation engines that are used for dynamically creating IAnimation instances. These
 * engines are mainly intended to be used by Layouts for defining how children are added,
 * repositioned, resized, and removed.
 */
#pragma once

#include "sha.h"

#include "Animatable.h"
#include "Animation.h"
#include "LinearGrowthAnimation.h"
#include "LinearTranslationAnimation.h"

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            /*
             * Insertion Animations
             */
            class IInsertionAnimationEngine
            {
            public:
                virtual ~IInsertionAnimationEngine(void) {}

                virtual IAnimation *CreateAnimation(_In_ IAnimatable *target) = 0;
            };



            /*
             * Reposition Animations
             */
            class IRepositionAnimationEngine
            {
            public:
                virtual ~IRepositionAnimationEngine(void) {}

                virtual IAnimation *CreateAnimation(
                    _In_ IAnimatable *target,
                    _In_ GLfloat endX,
                    _In_ GLfloat endY) = 0;
            };

            class LinearTranslationAnimationEngine :
                public IRepositionAnimationEngine
            {
            public:
                LinearTranslationAnimationEngine(_In_ GLfloat vel) :
                    _vel(vel)
                {
                }

                /*
                 * IRepositionAnimationEngine
                 */
                virtual IAnimation *CreateAnimation(
                    _In_ IAnimatable *target,
                    _In_ GLfloat endX,
                    _In_ GLfloat endY)
                {
                    return new LinearTranslationAnimation(target, endX, endY, this->_vel);
                }

            private:

                GLfloat _vel;
            };



            /*
             * Growth Animations
             */
            class IGrowthAnimationEngine
            {
            public:
                virtual ~IGrowthAnimationEngine(void) {}

                virtual IAnimation *CreateAnimation(
                    _In_ IAnimatable *target,
                    _In_ GLfloat endX,
                    _In_ GLfloat endY,
                    _In_ GLfloat endWidth,
                    _In_ GLfloat endHeight) = 0;
                virtual IAnimation *CreateAnimation(
                    _In_ IAnimatable *target,
                    _In_ const Rect &rc) = 0;
            };

            class LinearGrowthAnimationEngine final :
                public IGrowthAnimationEngine
            {
            public:
                LinearGrowthAnimationEngine(_In_ GLfloat vel) :
                    _vel(vel)
                {
                }

                /*
                 * IGrowthAnimationEngine
                 */
                virtual IAnimation *CreateAnimation(
                    _In_ IAnimatable *target,
                    _In_ GLfloat endX,
                    _In_ GLfloat endY,
                    _In_ GLfloat endWidth,
                    _In_ GLfloat endHeight)
                {
                    return new LinearGrowthAnimation(target, endX, endY, endWidth, endHeight,
                        this->_vel);
                }

                virtual IAnimation *CreateAnimation(_In_ IAnimatable *target, _In_ const Rect &rc)
                {
                    return new LinearGrowthAnimation(target, rc, this->_vel);
                }

            private:

                GLfloat _vel;
            };



            /*
             * Removal Animations
             */
            class IRemovalAnimationEngine
            {
            public:
                virtual ~IRemovalAnimationEngine(void) {}

                virtual IAnimation *CreateAnimation(_In_ IAnimatable *target);
            };
        }
    }
}
