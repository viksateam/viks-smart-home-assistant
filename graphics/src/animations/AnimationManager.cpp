/*
 * AnimationManager.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * 
 */

#include <cassert>

#include "AnimationManager.h"
#include "ConcurrentAnimation.h"

using namespace vik::graphics::animations;

/*
 * Constructors/Destructor
 */
AnimationManager::AnimationManager(void) :
    _nextCookie(0),
    _prevTime(std::chrono::high_resolution_clock::now())
{
}

AnimationManager::~AnimationManager(void)
{
    // There is no guarantee that the target objects still exist, so the best that we can do is
    // delete the animation (i.e. we don't want to call OnAnimationCancelled, etc.)

    for (auto ani : this->_runningAnimations)
    {
        delete ani.first;
    }
    for (auto ani : this->_pendingAnimations)
    {
        delete ani.first;
    }
    for (auto ani : this->_readyAnimations)
    {
        delete ani.first;
    }
}



/*
 * Static Instance Functions
 */
AnimationManager *AnimationManager::GetInstance(void)
{
    static AnimationManager instance;

    return &instance;
}



/*
 * Animation Managing
 */
void AnimationManager::Update(void)
{
    using namespace std::chrono;

    // Calculate the elapsed time since the previous update
    auto now = high_resolution_clock::now();
    auto dur = duration_cast<microseconds>(now - this->_prevTime);
    this->_prevTime = now;

    // Begin all animations in the ready list and add them to the running list
    if (!this->_readyAnimations.empty())
    {
        for (auto ani : this->_readyAnimations)
        {
            ani.first->OnAnimationBegin();
            this->_runningAnimations.insert(ani);
        }
        this->_readyAnimations.clear();
    }

    // Update all animations that are currently updating
    for (auto it = std::begin(this->_runningAnimations); it != std::end(this->_runningAnimations);)
    {
        IAnimation *ani = it->first;
        assert(ani);

        if (ani->IsCancelled())
        {
            ++it;
            continue;
        }

        if (ani->OnAnimationUpdate(dur.count()))
        {
            // Remove all references. We must do this before calling OnAnimationComplete since that
            // function may cause the target IAnimatable object to get destroyed, which will cause
            // the object to attempt to remove any currently running animations (a double delete)
            if (it->second != INVALID_ANIMATION_COOKIE)
            {
                this->_animations.erase(it->second);
            }
            it = this->_runningAnimations.erase(it);

            // Animation has completed. Mark as completed, claim pending animations, and destroy
            ani->OnAnimationCompleted();
            this->_ClaimPendingAnimations(ani);
            delete ani;
        }
        else
        {
            // If the animation is not done, still check to see if it is a concurrent animation. If
            // it is, there may be some completed animations that we can claim now
            ConcurrentAnimation *conc = dynamic_cast<ConcurrentAnimation *>(ani);
            if (conc && conc->HasCompletedAnimations())
            {
                for (auto animation : conc->ClaimCompletedAnimations())
                {
                    assert(animation);

                    // The animation is guaranteed to be untracked (has no animation_cookie
                    // associated with it). Therefore, all that needs to be done is claim all
                    // pending animations and destroy the animation
                    this->_ClaimPendingAnimations(animation);
                    delete animation;
                }
            }

            ++it;
        }
    }

    for (auto pair : this->_cancelledAnimations)
    {
        // Do a "sweeping" removal; it would be more costly to check and then erase
        this->_animations.erase(pair.second);
        this->_pendingAnimations.erase(pair.first);
        this->_readyAnimations.erase(pair.first);
        this->_runningAnimations.erase(pair.first);

        delete pair.first;
    }
    this->_cancelledAnimations.clear();
}

_Success_(return != INVALID_ANIMATION_COOKIE)
animation_cookie AnimationManager::SubmitAnimation(_In_ IAnimation *ani)
{
    animation_cookie cookie = this->_nextCookie++;

    this->_animations.insert(std::make_pair(cookie, ani));
    this->_readyAnimations.insert(std::make_pair(ani, cookie));

    return cookie;
}

_Success_(return != INVALID_ANIMATION_COOKIE)
animation_cookie AnimationManager::SubmitPendingAnimation(
    _In_ IAnimation *ani,
    _In_ animation_cookie parentCookie)
{
    animation_cookie cookie = INVALID_ANIMATION_COOKIE;

    if (parentCookie != INVALID_ANIMATION_COOKIE)
    {
        auto itr = this->_animations.find(parentCookie);
        if (itr != std::end(this->_animations))
        {
            // Parent exists; add as pending and update the cookie
            cookie = this->_nextCookie++;

            itr->second->AddPendingAnimation(ani);
            this->_animations.insert(std::make_pair(cookie, ani));
            this->_pendingAnimations.insert(std::make_pair(ani, cookie));
        }
    }

    return cookie;
}

bool AnimationManager::CancelAnimation(_In_ animation_cookie cookie)
{
    if (cookie == INVALID_ANIMATION_COOKIE)
    {
        return false;
    }

    auto itr = this->_animations.find(cookie);
    if (itr == std::end(this->_animations) || itr->second->IsCancelled())
    {
        // Animation does not exist; fail right away
        return false;
    }

    // Animation exists. Cancel the animation, claim all pending animations, destroy the animation,
    // and remove all references to it
    IAnimation *ani = itr->second;

    ani->OnAnimationCancelled();
    this->_ClaimPendingAnimations(ani);
    this->_cancelledAnimations.insert(std::make_pair(ani, cookie));

    return true;
}



/*
 * Private Functions
 */
void AnimationManager::_ClaimPendingAnimations(_In_ IAnimation *ani)
{
    const IAnimation::AnimationList &list = ani->GetPendingAnimations();

    for (auto animation : list)
    {
        // Move each animation to the ready list. It is not guaranteed that each animation be
        // tracked (some may have come from ConcurrentAnimations), so insert them into the ready
        // list with INVALID_ANIMATION_COOKIE if that is the case
        auto itr = this->_pendingAnimations.find(animation);
        if (itr != std::end(this->_pendingAnimations))
        {
            this->_readyAnimations.insert(*itr);
            this->_pendingAnimations.erase(itr);
        }
        else
        {
            this->_readyAnimations.insert(std::make_pair(animation, INVALID_ANIMATION_COOKIE));
        }
    }
}
