/*
 * Ellipse.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines a UIElement for an Ellipse. The Ellipse UIElement is similar to vik::graphics::Oval in
 * the sense that it caches the points necessary for drawing, but differs in the fact that the
 * Ellipse UIElement has a border. Thus, the Ellipse must cache its own points. For this reason,
 * resizing ellipses is very expensive, so it is not recommended that you animate them much if at
 * all
 */

#include "Ellipse.h"

using namespace vik::graphics;
using namespace vik::graphics::shapes;

/*
* Constructors/Destructors
*/
Ellipse::Ellipse(_In_ int res) :
    _res(res)
{
    this->_OnResize();
}

Ellipse::Ellipse(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ int res) :
    Shape(x, y, width, height),
    _res(res)
{
    this->_OnResize();
}

Ellipse::Ellipse(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height, _In_ int res) :
    Shape(pt, width, height),
    _res(res)
{
    this->_OnResize();
}

Ellipse::Ellipse(_In_ const Rect &rc, _In_ int res) :
    Shape(rc),
    _res(res)
{
    this->_OnResize();
}



/*
 * IAnimatable
 */
#pragma region IAnimatable
void Ellipse::SetWidth(_In_ GLfloat width)
{
    Shape::SetWidth(width);
    this->_OnResize();
}

void Ellipse::SetHeight(_In_ GLfloat height)
{
    Shape::SetHeight(height);
    this->_OnResize();
}

void Ellipse::SetRect(_In_ const Rect &rc)
{
    Shape::SetRect(rc);
    this->_OnResize();
}

GLfloat Ellipse::IncreaseWidth(_In_ GLfloat deltaWidth)
{
    auto val = Shape::IncreaseWidth(deltaWidth);
    this->_OnResize();

    return val;
}

GLfloat Ellipse::IncreaseHeight(_In_ GLfloat deltaHeight)
{
    auto val = Shape::IncreaseHeight(deltaHeight);
    this->_OnResize();

    return val;
}
#pragma endregion



/*
 * UIElement
 */
GLvoid Ellipse::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    // Drawing the border simply consists of drawing the points in a triangle strip
    if (this->GetBorderThickness() > 0)
    {
        glColor4fv(this->GetBorderColor());
        glBegin(GL_TRIANGLE_STRIP);

        for (auto pt : this->_points)
        {
            glVertex2f(pt.x + offX, pt.y + offY);
        }

        glEnd();
    }

    // Drawing the background simply requires us to skip every odd entry in the points vector
    glColor4fv(this->GetBackgroundColor());
    glBegin(GL_POLYGON);

    for (size_t i = 0; i < this->_points.size(); i += 2)
    {
        const Point &pt = this->_points[i];
        glVertex2f(pt.x + offX, pt.y + offY);
    }

    glEnd();
}


/*
 * Shape
 */
void Ellipse::SetBorderThickness(_In_ GLfloat thickness)
{
    Shape::SetBorderThickness(thickness);
    this->_OnResize();
}



/*
 * Private Functions
 */
void Ellipse::_OnResize(void)
{
    assert(this->_res >= 3);
    assert(this->GetBorderThickness() >= 0);
    assert(this->GetBorderThickness() * 2 < this->GetWidth());
    assert(this->GetBorderThickness() * 2 < this->GetHeight());

    this->_points.clear();

    GLfloat border = this->GetBorderThickness();
    GLfloat outer_width = this->GetWidth();
    GLfloat inner_width = outer_width - (2 * border);
    GLfloat outer_height = this->GetHeight();
    GLfloat inner_height = outer_height - (2 * border);

    // It is more convenient for us to describe all points relative to the center
    GLfloat center_x = outer_width / 2.0f;
    GLfloat center_y = outer_height / 2.0f;

    // res is the number of points to draw. Therefore, each angle increment is (2 * PI) / res
    GLfloat angle_delta = (float)(2.0f * M_PI) / this->_res;
    GLfloat angle = 0;

    // Need one extra iteration because GL_TRIANGLE_STRIP does not loop
    for (int i = 0; i <= this->_res; i++)
    {
        float cos_angle = cos(angle);
        float sin_angle = sin(angle);

        // We need to generate two points: one for the border, and one for the background
        GLfloat x_outer = center_x + (outer_width * cos_angle / 2);
        GLfloat x_inner = center_x + (inner_width * cos_angle / 2);

        GLfloat y_outer = center_y + (outer_height * sin_angle / 2);
        GLfloat y_inner = center_y + (inner_height * sin_angle / 2);

        this->_points.push_back(Point(x_inner, y_inner));
        this->_points.push_back(Point(x_outer, y_outer));

        angle += angle_delta;
    }
}
