/*
 * Debug.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Functions that are useful for debugging with the Tiva C Series TM4C123G LaunchPad Evaluation Kit
 * microcontroller. These functions are mostly used to set the on-board LEDs
 */
#pragma once

#include "sha.h"

#include <cstdint>

#define LED_BLACK           (0x00)
#define LED_RED             (0x02)
#define LED_GREEN           (0x08)
#define LED_BLUE            (0x04)
#define LED_MAGENTA         (LED_RED | LED_BLUE)
#define LED_CYAN            (LED_GREEN | LED_BLUE)
#define LED_YELLOW          (LED_RED | LED_GREEN)
#define LED_WHITE           (LED_RED | LED_GREEN | LED_BLUE)

typedef uint8_t led_t;

class Debug
{
private:
    /* Can't instantiate an instance of a Debug object */
    Debug(void) {}

public:

    static void Init(void);
    static void TurnOnLed(_In_ led_t uLedMask);
    static void TurnOffLed(_In_ led_t uLedMask);
    static void ToggleLed(_In_ led_t uLedMask);
    static void SetLed(_In_ led_t uLedMask);
};
