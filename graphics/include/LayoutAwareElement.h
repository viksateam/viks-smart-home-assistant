/*
 * LayoutAwareElement.h
 *
 * Authors(s):
 *      Duncan Horn
 *
 * This class defines the functions _OnPositionUpdated and _OnSizeUpdated which get called whenever
 * any Set* function is called.
 */
#pragma once

#include "UIElement.h"

namespace vik
{
    namespace graphics
    {
        class LayoutAwareElement :
            public UIElement
        {
        public:
            LayoutAwareElement(void);
            LayoutAwareElement(_In_ GLfloat width, _In_ GLfloat height);
            LayoutAwareElement(
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height);
            LayoutAwareElement(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            LayoutAwareElement(_In_ const Rect &rc);

            /* IAnimatable */
            virtual void    SetX(_In_ GLfloat x);
            virtual void    SetY(_In_ GLfloat y);
            virtual void    SetLocation(_In_ const Point &pt);
            virtual void    SetWidth(_In_ GLfloat width);
            virtual void    SetHeight(_In_ GLfloat height);
            virtual void    SetRect(_In_ const Rect &rc);
            virtual GLfloat TranslateX(_In_ GLfloat deltaX);
            virtual GLfloat TranslateY(_In_ GLfloat deltaY);
            virtual Point   Translate(_In_ GLfloat deltaX, _In_ GLfloat deltaY);
            virtual Point   Translate(_In_ const Point &pt);
            virtual GLfloat IncreaseWidth(_In_ GLfloat deltaWidth);
            virtual GLfloat IncreaseHeight(_In_ GLfloat deltaHeight);

        protected:

            virtual void _OnPositionUpdated(_In_ GLfloat x, _In_ GLfloat y);
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);
        };
    }
}
