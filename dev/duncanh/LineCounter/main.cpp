
#include <fstream>
#include <iostream>
#include <string>
#include <Windows.h>

static const std::wstring rootDir = L"..\\..\\..\\";

template <typename _Func>
static void iterateDir(_In_ const std::wstring &dir, _In_ bool onlyDir, _In_ _Func func)
{
    WIN32_FIND_DATA fd;
    HANDLE h = FindFirstFile(dir.c_str(), &fd);
    if (h != INVALID_HANDLE_VALUE)
    {
        do
        {
            // Make sure it is a directory
            if ((!onlyDir || (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) && fd.cFileName[0] != '.')
            {
                func(fd.cFileName);
            }
        }
        while (FindNextFile(h, &fd));

        // Close the search handle
        FindClose(h);
    }
}

struct LinesInfo
{
    int numHeaders;
    int numSource;
    int numHeaderLines;
    int numSourceLines;

    LinesInfo(void) : numHeaders(0), numSource(0), numHeaderLines(0), numSourceLines(0)
    {
    }
    LinesInfo(int numHeaders, int numSource, int numHeaderLines, int numSourceLines) :
        numHeaders(numHeaders), numSource(numSource), numHeaderLines(numHeaderLines), numSourceLines(numSourceLines)
    {
    }

    LinesInfo operator+(_In_ const LinesInfo &other)
    {
        return LinesInfo(this->numHeaders + other.numHeaders, this->numSource + other.numSource,
            this->numHeaderLines + other.numHeaderLines, this->numSourceLines + other.numSourceLines);
    }
    LinesInfo &operator+=(_In_ const LinesInfo &other)
    {
        *this = *this + other;
        return *this;
    }
};

LinesInfo recursiveIterate(_In_ const std::wstring &base)
{
    LinesInfo info;

    // First, recursively iterate all directories
    iterateDir(base + L"*", true, [&](_In_ const std::wstring &str)
    {
        info += recursiveIterate(base + str + L"\\");
    });

    // Iterate all .cpp files
    iterateDir(base + L"*.cpp", false, [&](_In_ const std::wstring &str)
    {
        info.numSource++;

        std::ifstream file(base + str);
        while (!file.bad() && !file.eof())
        {
            info.numSourceLines++;

            std::string line;
            std::getline(file, line);
        }
    });

    // Iterate all .h files
    iterateDir(base + L"*.h", false, [&](_In_ const std::wstring &str)
    {
        info.numHeaders++;

        std::ifstream file(base + str);
        while (!file.bad() && !file.eof())
        {
            info.numHeaderLines++;

            std::string line;
            std::getline(file, line);
        }
    });

    return info;
}

int main(void)
{
    // Iterate all directories in the main root directory
    iterateDir(rootDir + L"*", true, [](_In_ const std::wstring &str)
    {
        std::wcout << str << ":" << std::endl;
        LinesInfo info = recursiveIterate(rootDir + str + L"\\");

        std::cout << "    Header Files: " << info.numHeaders << std::endl;
        std::cout << "    Source Files: " << info.numSource << std::endl;
        std::cout << "    Header Lines: " << info.numHeaderLines << std::endl;
        std::cout << "    Source Lines: " << info.numSourceLines << std::endl << std::endl;
    });
}
