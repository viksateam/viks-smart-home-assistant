/*
 * main.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Entry point for the Smart Home Assistant. This file does its best to remain platform independent
 * calling external functions for window creation, etc. There are obvious exceptions (e.g. main vs
 * WinMain), but you can't say we did not try.
 */

#include "sha.h"

#include "ShaArgs.h"
#include "WindowHelpers.h"

using namespace vik;

#ifdef  WIN32
int CALLBACK WinMain(
    _In_ HINSTANCE hInstance,
    _In_opt_ HINSTANCE hPrevInstance,
    _In_ LPSTR lpCmdLine,
    _In_ int nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // Enable run-time memory check for debug builds
#if defined(DEBUG) || defined(_DEBUG)
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

    // We need to modify lpCmdLine to the argc/argv format... Ugh!
    int argc;
    char **argv;

    LPWSTR *szArgv = CommandLineToArgvW(GetCommandLine(), &argc);
    argv = (char **)szArgv;     // Cheat a little and re-use the buffer

    if (!szArgv)
    {
        // Unknown error
        MessageBox(nullptr, L"ERROR: Could not load command line arguments", nullptr, 0);
        return false;
    }

    for (int i = 0; i < argc; i++)
    {
        int j;
        LPWSTR lpwsz = szArgv[i];
        for (j = 0; lpwsz[j]; j++)
        {
            argv[i][j] = (char)lpwsz[j];
        }
        argv[i][j] = '\0';
    }

    // Set information for initializing and showing the window
    WindowInfo wndInfo;
    wndInfo.x = CW_USEDEFAULT;
    wndInfo.y = CW_USEDEFAULT;
    wndInfo.width = 800;
    wndInfo.height = 600;
    wndInfo.className = TEXT("SmartHomeAssistant");
    wndInfo.windowName = TEXT("Vik's Smart Home Assistant");
    wndInfo.hInstance = hInstance;
    wndInfo.cmdShow = nCmdShow;
#else
int main(_In_ int argc, _In_ char *argv[])
{
    WindowInfo wndInfo;
    wndInfo.x = 0;
    wndInfo.y = 0;
    wndInfo.width = 800;
    wndInfo.height = 600;
    wndInfo.windowName = "Vik's Smart Home Assistant";
#endif  /* WIN32 */

    ShaArgs args(argc, argv);

#ifdef  WIN32
    // Done with arguments; we can now free the resources
    LocalFree(szArgv);
#endif  /* WIN32 */

    // Create and show the window
    if (InitWindow(wndInfo, args))
    {
        // From here, the message pump takes over. This is again operating system dependent, so we
        // again offload the work to a separate function. This function continuously reads
        // events/messages, performs any operating system specific work where needed, and forwards
        // relevant events/messages on to common Smart Home Assistant code where needed.
        return MessagePump();
    }

    return false;
}
