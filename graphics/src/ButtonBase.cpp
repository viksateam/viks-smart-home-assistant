/*
 * ButtonBase.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * A useful base class for button-like UIElements.
 */

#include "ButtonBase.h"

using namespace vik::graphics;

/*
 * Constructor(s)/Destructor
 */
ButtonBase::ButtonBase(void) :
    ButtonBase(Rect(0, 0, 0, 0))
{
}

ButtonBase::ButtonBase(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
    ButtonBase(Rect(x, y, width, height))
{
}

ButtonBase::ButtonBase(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    ButtonBase(Rect(pt, width, height))
{
}

ButtonBase::ButtonBase(_In_ const Rect &rc) :
    LayoutAwareElement(rc),
    _buttonEnteredCallback(nullptr),
    _buttonExitedCallback(nullptr),
    _buttonDownCallback(nullptr),
    _buttonUpCallback(nullptr),
    _buttonEnteredTarget(nullptr),
    _buttonExitedTarget(nullptr),
    _buttonDownTarget(nullptr),
    _buttonUpTarget(nullptr)
{
}



/* UIElement */
bool ButtonBase::OnPointerEntered(_In_ Point pt)
{
    if (this->_buttonEnteredCallback)
    {
        return (this->_buttonEnteredTarget->*_buttonEnteredCallback)(this, pt);
    }

    return UIElement::OnPointerEntered(pt);
}

bool ButtonBase::OnPointerExited(void)
{
    if (this->_buttonExitedCallback)
    {
        // The point for OnPointerExited is always bogus
        Point pt(0, 0);
        return (this->_buttonExitedTarget->*_buttonExitedCallback)(this, pt);
    }

    return UIElement::OnPointerExited();
}

bool ButtonBase::OnPointerDown(_In_ Point pt)
{
    if (this->_buttonDownCallback)
    {
        return (this->_buttonDownTarget->*_buttonDownCallback)(this, pt);
    }

    return UIElement::OnPointerDown(pt);
}

bool ButtonBase::OnPointerUp(_In_ Point pt)
{
    if (this->_buttonUpCallback)
    {
        return (this->_buttonUpTarget->*_buttonUpCallback)(this, pt);
    }

    return UIElement::OnPointerUp(pt);
}



/*
 * Button Callbacks
 */
void ButtonBase::OnButtonEntered(_In_ UIElement *target, _In_ ButtonEventCallback callback)
{
    this->_buttonEnteredTarget = target;
    this->_buttonEnteredCallback = callback;
}

void ButtonBase::OnButtonExited(_In_ UIElement *target, _In_ ButtonEventCallback callback)
{
    this->_buttonExitedTarget = target;
    this->_buttonExitedCallback = callback;
}

void ButtonBase::OnButtonDown(_In_ UIElement *target, _In_ ButtonEventCallback callback)
{
    this->_buttonDownTarget = target;
    this->_buttonDownCallback = callback;
}

void ButtonBase::OnButtonUp(_In_ UIElement *target, _In_ ButtonEventCallback callback)
{
    this->_buttonUpTarget = target;
    this->_buttonUpCallback = callback;
}
