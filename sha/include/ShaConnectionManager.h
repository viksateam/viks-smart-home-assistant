/*
 * ShaConnectionManager.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Mainly used to map station id's to their ShaStation object
 */
#pragma once

#include <map>
#include <string>

#include "ShaStation.h"

namespace vik
{
    class ShaConnectionManager final
    {
    private:
        ShaConnectionManager(void) {}

    public:

        static void Destroy(void);

        /* Modification/Indexing */
        static bool       HasStation(_In_ const std::string &id);
        static ShaStation *GetStation(_In_ const std::string &id);
        static bool       AddStation(_In_ ShaStation *station);
        static bool       UpdateStation(_In_ ShaStation *station);
        static bool       RemoveStation(_In_ const std::string &id);

        template <typename _Func>
        static void ForEach(_In_ _Func func)
        {
            for (auto &pair : s_map)
            {
                func(pair.second);
            }
        }

    private:

        static std::map<std::string, ShaStation *> s_map;
    };
}
