/*
 * UIElement.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines the base class for all UI Elements - UIElement. In general, you should not need to
 * define your own derivative UIElement. Instead consider creating a Component derivative and
 * setting its Template to get the desired look and feel using existing UIElements.
 */

#include "UIElement.h"

using namespace vik::graphics;
using namespace vik::graphics::animations;

/*
 * Constructors/Destructors
 */
UIElement::UIElement(void) :
    _rect(0, 0, 0, 0),
    _animationCookie(INVALID_ANIMATION_COOKIE),
    _backgroundColor(Colors::Transparent),
    _foregroundColor(Colors::Transparent)
{
}

UIElement::UIElement(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
    _rect(x, y, width, height),
    _animationCookie(INVALID_ANIMATION_COOKIE),
    _backgroundColor(Colors::Transparent),
    _foregroundColor(Colors::Transparent)
{
}

UIElement::UIElement(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    _rect(pt, width, height),
    _animationCookie(INVALID_ANIMATION_COOKIE),
    _backgroundColor(Colors::Transparent),
    _foregroundColor(Colors::Transparent)
{
}

UIElement::UIElement(_In_ const Rect &rc) :
    _rect(rc),
    _animationCookie(INVALID_ANIMATION_COOKIE),
    _backgroundColor(Colors::Transparent),
    _foregroundColor(Colors::Transparent)
{
}

UIElement::~UIElement(void)
{
    this->CancelCurrentAnimation();
}



/*
 * IAnimatable
 */
#pragma region IAnimatable
GLfloat UIElement::GetX(void) const
{
    return this->_rect.pt.x;
}

GLfloat UIElement::GetY(void) const
{
    return this->_rect.pt.y;
}

Point UIElement::GetLocation(void) const
{
    return this->_rect.pt;
}

GLfloat UIElement::GetWidth(void) const
{
    return this->_rect.width;
}

GLfloat UIElement::GetHeight(void) const
{
    return this->_rect.height;
}

Rect UIElement::GetRect(void) const
{
    return this->_rect;
}

void UIElement::SetX(_In_ GLfloat x)
{
    this->_rect.pt.x = x;
}

void UIElement::SetY(_In_ GLfloat y)
{
    this->_rect.pt.y = y;
}

void UIElement::SetLocation(_In_ const Point &pt)
{
    this->_rect.pt = pt;
}

void UIElement::SetWidth(_In_ GLfloat width)
{
    assert(width >= 0);
    this->_rect.width = width;
}

void UIElement::SetHeight(_In_ GLfloat height)
{
    assert(height >= 0);
    this->_rect.height = height;
}

void UIElement::SetRect(_In_ const Rect &rc)
{
    this->_rect = rc;
}

GLfloat UIElement::TranslateX(_In_ GLfloat deltaX)
{
    this->_rect.pt.x += deltaX;
    return this->_rect.pt.x;
}

GLfloat UIElement::TranslateY(_In_ GLfloat deltaY)
{
    this->_rect.pt.y += deltaY;
    return this->_rect.pt.y;
}

Point UIElement::Translate(_In_ GLfloat deltaX, _In_ GLfloat deltaY)
{
    return this->Translate(Point(deltaX, deltaY));
}

Point UIElement::Translate(_In_ const Point &pt)
{
    this->_rect.pt += pt;
    return this->_rect.pt;
}

GLfloat UIElement::IncreaseWidth(_In_ GLfloat deltaWidth)
{
    this->_rect.width += deltaWidth;

    assert(this->_rect.width >= 0);
    return this->_rect.width;
}

GLfloat UIElement::IncreaseHeight(_In_ GLfloat deltaHeight)
{
    this->_rect.height += deltaHeight;

    assert(this->_rect.height >= 0);
    return this->_rect.height;
}

_Success_(return != INVALID_ANIMATION_COOKIE)
animation_cookie UIElement::SubmitAnimation(_In_ IAnimation *animation)
{
    this->_animationCookie = AnimationManager::GetInstance()->SubmitAnimation(animation);
    return this->_animationCookie;
}

_Success_(return != INVALID_ANIMATION_COOKIE)
animation_cookie UIElement::SubmitPendingAnimation(_In_ IAnimation *animation)
{
    auto mgr = AnimationManager::GetInstance();
    animation_cookie cookie = mgr->SubmitPendingAnimation(animation, this->_animationCookie);

    if (cookie == INVALID_ANIMATION_COOKIE)
    {
        // Current animation invalid/cancelled/completed so just submit normally
        cookie = mgr->SubmitAnimation(animation);
    }

    this->_animationCookie = cookie;
    return cookie;
}

_Success_(return != INVALID_ANIMATION_COOKIE)
animation_cookie UIElement::SubmitOverrideAnimation(_In_ IAnimation *animation)
{
    // Ignore failures (as they don't matter)
    AnimationManager::GetInstance()->CancelAnimation(this->_animationCookie);
    return this->SubmitAnimation(animation);
}

bool UIElement::CancelCurrentAnimation(void)
{
    return AnimationManager::GetInstance()->CancelAnimation(this->_animationCookie);
}
#pragma endregion



/*
 * Colors
 */
#pragma region Colors
const Color &UIElement::GetBackgroundColor(void) const
{
    return this->_backgroundColor;
}

const Color &UIElement::GetForegroundColor(void) const
{
    return this->_foregroundColor;
}

void UIElement::SetBackgroundColor(_In_ const Color &color)
{
    this->_backgroundColor = color;
}

void UIElement::SetForegroundColor(_In_ const Color &color)
{
    this->_foregroundColor = color;
}
#pragma endregion



/* Pointer Input */
#pragma region PointerInput
bool UIElement::OnPointerEntered(_In_ Point pt)
{
    // By default, report all events as handled
    (void)pt;

    return true;
}

bool UIElement::OnPointerExited(void)
{
    // By default, report all events as handled
    return true;
}

bool UIElement::OnPointerMoved(_In_ Point pt)
{
    // By default, report all events as handled
    (void)pt;

    return true;
}

bool UIElement::OnPointerDown(_In_ Point pt)
{
    // By default, report all events as handled
    (void)pt;

    return true;
}

bool UIElement::OnPointerUp(_In_ Point pt)
{
    // By default, report all events as handled
    (void)pt;

    return true;
}
#pragma endregion
