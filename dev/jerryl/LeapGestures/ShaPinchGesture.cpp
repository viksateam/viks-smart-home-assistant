/*
 * ShaPinchGesture.cpp
 *
 * Author(s):
 *      Jerry Lin
 *
 * Detects right-handed, thumb-to-pointer finger pinch and release motions. The other three fingers
 * must be tucked away and not shown.
 */

#include "ShaPinchGesture.h"

using namespace sha;
using namespace sha::leap;

/*
 * Constructors / Destructors
 */
ShaPinchGesture::ShaPinchGesture(void)
{
    this->_pinchThreshold = DEFAULT_PINCH_THRESHOLD;
    this->_releaseThreshold = DEFAULT_RELEASE_THRESHOLD;
    this->_Reset();
}



/*
 * Public funcions
 */
Vector ShaPinchGesture::GetPinchLocation(void) const
{
    if (this->_state == GS_UPDATE)
    {
        return this->_lastPalmPosition + this->_palmToPinchPosition;
    }
    else
    {
        return Vector::zero();
    }
}

GestureType ShaPinchGesture::GetType(void) const
{
    return GT_PINCH;
}

bool ShaPinchGesture::IsPinched(void) const
{
    return this->_state == GS_UPDATE && !this->_isReleased;
}

bool ShaPinchGesture::IsReleased(void) const
{
    return this->_state == GS_UPDATE && this->_isReleased;
}

void ShaPinchGesture::Update(_In_ const Frame &frame)
{
    const HandList &hands = frame.hands();
    if (!hands.isEmpty())
    {
        const Hand &hand = hands.frontmost();
        const FingerList &fingers = hand.fingers();

        switch (this->_state)
        {
        case GS_STOP:
        {
            if (fingers.count() == 2)
            {
                const Finger &firstFinger = fingers.leftmost();
                const Finger &secondFinger = fingers[this->_GetSecondFingerIndex(fingers)];
                const Vector &firstPos = firstFinger.stabilizedTipPosition();
                const Vector &secondPos = secondFinger.stabilizedTipPosition();

                float distance = (secondPos - firstPos).magnitude();
                Vector pinchPos = this->_GetEstimatedPinchPosition(firstPos, secondPos);
                Vector estimatedPinchPos = this->_GetLastPinchPosition(hand);

                if (distance > this->_pinchThreshold)
                {
                    if (this->_lastDistance > distance &&
                        (estimatedPinchPos - pinchPos).magnitude() < this->_lastDistance / 2)
                    {
                        this->_state = GS_START;
                        this->_id = ShaGesture::CreateID();
                    }

                    this->_UpdateHandInformation(hand, pinchPos);
                    this->_lastDistance = distance;
                }
            }
        }
            break;
        case GS_START:
        {
            if (fingers.count() > 1)
            {
                auto firstFinger = fingers.leftmost();
                auto secondFinger = fingers[this->_GetSecondFingerIndex(fingers)];
                auto firstPos = firstFinger.stabilizedTipPosition();
                auto secondPos = secondFinger.stabilizedTipPosition();

                float distance = (secondPos - firstPos).magnitude();
                Vector pinchPos = this->_GetEstimatedPinchPosition(firstPos, secondPos);
                Vector lastPinchPos = this->_GetLastPinchPosition(hand);

                //Do not need to check for threshold
                if ((lastPinchPos - pinchPos).magnitude() < this->_lastDistance / 2)
                {
                    if (this->_lastDistance > distance)
                    {
                        this->_UpdateHandInformation(hand, pinchPos);
                        this->_lastDistance = distance;
                    }
                    else if ((lastPinchPos - firstPos).magnitude() < this->_lastDistance &&
                        (lastPinchPos - secondPos).magnitude() > this->_lastDistance)
                    {
                        //ONE FINGER in pinch
                        this->_palmToPinchPosition =
                            this->_GetLastPinchPosition(hand) - hand.palmPosition();
                        this->_lastDistance = 0;
                        this->_state = GS_UPDATE;
                    }
                    else
                    {
                        this->_Reset();
                    }
                }
                else
                {
                    //NO FINGERS in pinch
                    this->_palmToPinchPosition = 
                        this->_GetLastPinchPosition(hand) - hand.palmPosition();
                    this->_lastDistance = 0;
                    this->_state = GS_UPDATE;
                }
            }
            else
            {
                //One or no fingers
                this->_palmToPinchPosition = this->_GetLastPinchPosition(hand) - hand.palmPosition();
                this->_lastDistance = 0;
                this->_state = GS_UPDATE;
            }
        }
            break;
        case GS_UPDATE:
        {
            if (fingers.count() > 1)
            {
                auto firstPos = fingers.leftmost().tipPosition();
                auto secondPos = fingers[this->_GetSecondFingerIndex(fingers)].tipPosition();
                auto medianPos = this->_GetEstimatedPinchPosition(firstPos, secondPos);
                float distance = (secondPos - firstPos).magnitude();

                Vector guessedPosition = this->_GetLastPinchPosition(hand);
                float distanceToGuessedPos = (medianPos - guessedPosition).magnitude();

                if (this->_lastDistance == 0)
                {
                    if (distanceToGuessedPos < this->_pinchThreshold)
                    {
                        this->_UpdateHandInformation(hand, medianPos);
                        this->_lastDistance = distance;
                    }
                }
                else
                {
                    if (distanceToGuessedPos < this->_lastDistance / 2 &&
                        distance > this->_lastDistance)
                    {
                        this->_UpdateHandInformation(hand, medianPos);
                        this->_lastDistance = distance;

                        if (distance > this->_releaseThreshold)
                        {
                            this->_isReleased = true;
                        }
                    }
                    else
                    {
                        this->_Reset();
                    }
                }
            }

            this->_lastPalmPosition = hand.palmPosition();
        }
            break;
        default:
            this->_Reset();
            break;
        }
    }
    else
    {
        this->_Reset();
    }
}



/*
 * Private helper functions
 */
bool ShaPinchGesture::_IsPinchGestureStarting(_In_ const Hand &hand) const
{

    return false;
}

Vector ShaPinchGesture::_GetLastPinchPosition(_In_ const Hand &hand) const
{
    return hand.palmPosition() + this->_palmToPinchPosition;
}

Vector ShaPinchGesture::_GetEstimatedPinchPosition(_In_ const Vector &pos1, _In_ const Vector &pos2) const
{
    return pos1 + ((pos2 - pos1) / 2);
}

int ShaPinchGesture::_GetSecondFingerIndex(_In_ const FingerList &fingers) const
{
    int index = 0;
    float minX = fingers.rightmost().tipPosition().x;

    for (int i = 0; i < fingers.count(); i++)
    {
        const Finger &finger = fingers[i];
        if (finger != fingers.leftmost())
        {
            float x = finger.tipPosition().x;
            if (x <= minX)
            {
                minX = x;
                index = i;
            }
        }
    }

    return index;
}

void ShaPinchGesture::_UpdateHandInformation(
    _In_ const Hand &hand,
    _In_ const Vector &pinchPosition)
{
    Vector palmToMedian = pinchPosition - hand.palmPosition();

    this->_lastPalmPosition = hand.palmPosition();
    this->_palmToPinchPosition = palmToMedian;
}

void ShaPinchGesture::_Reset(void)
{
    this->_lastDistance = -1;
    this->_lastPalmPosition = Vector::zero();
    this->_palmToPinchPosition = Vector::zero();
    this->_state = GS_STOP;
    this->_isReleased = false;
}
