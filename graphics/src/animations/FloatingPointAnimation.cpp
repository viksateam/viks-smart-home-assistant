/*
 * FloatingPointAnimation.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * This class is mostly a hack. In some cases we wish to animate some arbitrary property of an
 * element (e.g. the red value in its color). Animation classes could be written for specific
 * situations, but then we end up with a bunch of highly specialized classes (and a bunch of
 * classes touching other classes' privates.
 */

#include <cassert>

#include "FloatingPointAnimation.h"

using namespace vik::graphics::animations;

/*
 * Constructor(s)/Destructor
 */
FloatingPointAnimation::FloatingPointAnimation(
    _In_ GLfloat *target,
    _In_ GLfloat end,
    _In_ GLfloat vel) :
    _target(target),
    _end(end),
    _vel(vel)
{
    assert(target);
    assert(vel > 0);
}



/* IAnimation */
void FloatingPointAnimation::OnAnimationBegin(void)
{
    AnimationBase::OnAnimationBegin();

    // Need to calculate update information based off the current value
    GLfloat distance = this->_end - *this->_target;

    // Duration (us) is distance (px) / velocity (px / us)
    this->_duration = std::abs(distance) / this->_vel;

    // Protect against the possibility of a division by zero
    if (this->_duration == 0)
    {
        this->_duration = 1e-6f;
    }

    // _deltaX and _deltaY are really velocities (change in position (px) per update unit (us))
    this->_delta = distance / this->_duration;
}

bool FloatingPointAnimation::OnAnimationUpdate(_In_ uint64_t timeMicro)
{
    // Need to convert update time to GLfloat (for more accuracy in position updates)
    GLfloat time = static_cast<GLfloat>(timeMicro);

    // Ensure we don't move too far
    if (time > this->_duration)
    {
        time = this->_duration;
    }

    *this->_target += this->_delta * time;

    this->_duration -= time;
    return this->_duration <= 0;
}

void FloatingPointAnimation::OnAnimationCompleted(void)
{
    AnimationBase::OnAnimationCompleted();

    // No action needs to be taken. Assert that the target value is in the correct location for
    // debugging purposes
    assert(std::abs(*this->_target - this->_end) < 1e-2);
}
