/*
 * ConcurrentAnimation.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a "group" of animations that begin execution at the same time. This allows for
 * (1) multiple animations to wait on another at the same time, or (2) one (or more) animations to
 * wait on multiple to finish.
 *
 * By default, all child animations are begun at the same time and completed/cancelled at the same
 * time. However, it may be desired that pending animations of our children begin immediately after
 * that child has completed. If this is the case, the ConcurrentAnimation class provides the
 * GetCompletedAnimations function. When called, all completed animations that have yet to be
 * claimed are marked as completed (by calling OnAnimationCompleted), removed from the collection
 * of completed animations, and returned to the calling function. Thus, the responsibility of
 * destroying these animations and properly handling their pending animations is handed over to
 * the calling function. Note that it is considered unsafe to call this function after a call to
 * OnAnimationCompleted or OnAnimationCancelled has been made.
 *
 * Calling SetPendingAnimation on a ConcurrentAnimation where the pending animation has already
 * been set causes the current pending animation and the new pending animation to get "joined"
 * together into one ConcurrentAnimation. Additionally, once OnAnimationCompleted or
 * OnAnimationCancelled gets called, the pending animations of all unclaimed children are "joined"
 * with the current pending animation as well.
 */
#pragma once

#include <list>
#include <vector>

#include "AnimationBase.h"

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            class ConcurrentAnimation final :
                public AnimationBase
            {
            public:
                ConcurrentAnimation(void);
                virtual ~ConcurrentAnimation(void);

                /* IAnimation */
                virtual void OnAnimationBegin(void);
                virtual bool OnAnimationUpdate(_In_ uint64_t timeMicro);
                virtual void OnAnimationCompleted(void);
                virtual void OnAnimationCancelled(void);
                virtual void AddPendingAnimation(_In_ IAnimation *ani);

                /* Animation Collection */
                void AddAnimation(_In_ IAnimation *ani);

                /* Querying and retrieving animations */
                bool                      HasCompletedAnimations(void) const;
                std::vector<IAnimation *> ClaimCompletedAnimations(void);
                std::vector<IAnimation *> ClaimAllAnimations(void);

            protected:

                void _CollectPendingAnimations(void);

                typedef std::list<IAnimation *> AnimationCollection;

                AnimationCollection _animations;
                AnimationCollection _completedAnimations;

                bool          _started;
                bool          _completed;
            };
        }
    }
}
