/*
 * ShaGesture.h
 * 
 * Author(s):
 *      Jerry Lin
 * 
 * Provides underlying structure for Sha-specific gestures.
 */
#pragma once

#include "Leap.h"

using namespace Leap;

namespace vik
{
    namespace leap
    {
        enum GestureType
        {
            GT_PINCH,
            GT_SWIPE,
            GT_INVALID
        };

        enum GestureState
        {
            GS_START,
            GS_UPDATE,
            GS_STOP
        };

        class ShaGesture
        {
        protected:

            static int CreateID(void);

        public:
            ShaGesture(void);

            int                 GetId(void) const;
            virtual GestureType GetType(void) const;
            GestureState        GetState(void) const;
            virtual void        Update(_In_ const Frame &frame);

        protected:

            int          _id;
            GestureState _state;
        };
    }
}
