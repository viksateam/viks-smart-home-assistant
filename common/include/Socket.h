/*
 * Socket.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Useful classes for dealing with both server and client ports (for TCP only)
 */
#pragma once

#include <string>
#include <WinSock2.h>

#include "InetAddress.h"

namespace vik
{
    namespace communication
    {
        enum SocketState
        {
            SOCKET_DISCONNECTED,
            SOCKET_CONNECTED
        };

        // Represents a client (non-listening) socket
        class Socket
        {
        public:
            Socket(void);
            Socket(_In_ const InetAddress &inetAddr);

            bool IsConnected(void) const;
            int  GetError(void) const;

            bool Connect(_In_ const InetAddress &inetAddr);
            bool Close(void);

            std::string GetIpAddr(void) const;
            InetAddress GetInetAddr(void) const;

            _Success_(return >= 0)
            int Send(_In_reads_(nLen) const void *pBuffer, _In_ int nLen);
            _Success_(return >= 0)
            int Receive(_Out_writes_bytes_to_(nLen, return) void *pBuffer, _In_ int nLen);

        private:

            // Connection information
            SOCKET      _socket;
            sockaddr_in _sAddr;
            int         _nErrorCode;

            // State information
            SocketState _state;

            friend class ServerSocket;
        };

        class ServerSocket
        {
        public:
            ServerSocket(void);
            ServerSocket(_In_ USHORT usPort);

            bool IsRunning(void) const;
            int  GetError(void) const;

            bool   Start(_In_ USHORT usPort);
            Socket Accept(void);
            bool   Close(void);

        private:

            // Server information
            SOCKET      _socket;
            sockaddr_in _sAddr;
            int         _nErrorCode;

            // State information
            SocketState _state;
        };
    }
}
