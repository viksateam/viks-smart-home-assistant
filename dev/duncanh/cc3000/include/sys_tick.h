/*
 * SysTick.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Implementation of SysTick methods to be used with the CC3000
 */
#pragma once

#include <cstdint>

#include "sha.h"

class SysTick
{
private:
    /* Can't instantiate an instance of a SysTick object */
    SysTick(void) {}

public:

    static void Init(void);
    static void Sleep(_In_ uint64_t uMilliseconds);

    /* Increments every ms */
    static volatile uint64_t s_uTickCount;
};
