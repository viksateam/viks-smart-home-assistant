/*
 * ShaLeap.cpp
 *
 * Author(s):
 *      Jerry Lin
 *
 * The interface used by the Smart Home Assistant to coordinate between the Leap and the graphical
 * output.
 */

#include <algorithm>

#include "ShaGraphics.h"
#include "ShaLeap.h"
#include "WindowHelpers.h"

using namespace Leap;
using namespace vik::graphics;
using namespace vik::leap;

static Controller leapController;
static ShaListener shaListener;

/*
 * Public Initializers
 */
void vik::leap::InitializeLeap(void)
{
    ConfigureController(leapController);
    leapController.addListener(shaListener);
    shaListener.EnableShaGesture(GT_PINCH);
    shaListener.EnableShaGesture(GT_SWIPE);
}

bool vik::leap::ConfigureController(_Inout_ Controller &controller)
{
    Config config = controller.config();
    config.setFloat("Gesture.ScreenTap.MinDistance", 2.0f);
    config.setFloat("Gesture.ScreenTap.MinForwardVelocity", 30.0f);
    return config.save();
}



/*
 * ShaListener Constructor(s)
 */
ShaListener::ShaListener(void) : Listener()
{
    this->Calibrate(LEAP_DEFAULT_X_MIN, LEAP_DEFAULT_X_MAX,
                    LEAP_DEFAULT_Y_MIN, LEAP_DEFAULT_Y_MAX,
                    LEAP_DEFAULT_Z_MIN, LEAP_DEFAULT_Z_MAX);
    this->_xMin = LEAP_DEFAULT_X_MIN;
    this->_xMax = LEAP_DEFAULT_X_MAX;
    this->_yMin = LEAP_DEFAULT_Y_MIN;
    this->_yMax = LEAP_DEFAULT_Y_MAX;

    this->_isExited = true;
    this->_isPinching = false;
    this->_pointerType = PT_FINGER;
    this->_pinchPosition = Vector::zero();
    this->_isSwiping = false;
    this->_isShaGestureHandled = false;
    this->_currentShaGestureId = INVALID_LEAP_ID;
    this->_shaGestures.push_back(new ShaPinchGesture{});
    this->_shaGestures.push_back(new ShaSwipeGesture{});
    this->_enabledGestures.emplace(GT_PINCH, false);
    this->_enabledGestures.emplace(GT_SWIPE, false);
}

ShaListener::~ShaListener(void)
{
    for (auto gesture : this->_shaGestures)
    {
        delete gesture;
    }
}



/*
 * Inherited Leap Listener Callback Functions
 */
void ShaListener::onInit(_In_ const Controller &controller)
{
    (void)controller;
}

void ShaListener::onConnect(_In_ const Controller &controller)
{
    controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
}

void ShaListener::onDisconnect(_In_ const Controller &controller)
{
    (void)controller;
}

void ShaListener::onExit(_In_ const Controller &controller)
{
    (void)controller;
}

void ShaListener::onFrame(_In_ const Controller &controller)
{
    this->_ProcessShaGestures(controller);
    this->_ProcessGestures(controller);
    this->_ProcessHands(controller);
}

void ShaListener::onFocusGained(_In_ const Controller &controller)
{
    (void)controller;
}

void ShaListener::onFocusLost(_In_ const Controller &controller)
{
    (void)controller;
}



/*
 * Public ShaListener Functions
 */
GLfloat ShaListener::GetXMin(void) const
{
    return this->_xMin;
}

GLfloat ShaListener::GetXMax(void) const
{
    return this->_xMax;
}

GLfloat ShaListener::GetYMin(void) const
{
    return this->_yMin;
}

GLfloat ShaListener::GetYMax(void) const
{
    return this->_yMax;
}

GLfloat ShaListener::GetLeapX(void) const
{
    return this->_x;
}

GLfloat ShaListener::GetLeapY(void) const
{
    return this->_y;
}

void ShaListener::Calibrate(
    _In_ GLfloat xMin,
    _In_ GLfloat xMax,
    _In_ GLfloat yMin,
    _In_ GLfloat yMax,
    _In_ GLfloat zMin,
    _In_ GLfloat zMax)
{
    this->_baseXMin = xMin;
    this->_baseXMax = xMax;
    this->_baseYMin = yMin;
    this->_baseYMax = yMax;
    this->_baseZMin = zMin;
    this->_baseZMax = zMax;
}



/*
 * Public Sha-specific Gesture Functions
 */
bool ShaListener::DisableShaGesture(GestureType gestureType)
{
    if (this->_ContainsGestureType(gestureType))
    {
        this->_enabledGestures[gestureType] = false;
        return true;
    }
    return false;
}

bool ShaListener::EnableShaGesture(GestureType gestureType)
{
    if (this->_ContainsGestureType(gestureType))
    {
        this->_enabledGestures[gestureType] = true;
        return true;
    }
    return false;
}



/*
 * ShaListener Private Helper Functions
 */
GLfloat ShaListener::_GetWindowX(_In_ GLfloat x) const
{
    return ((x - this->_xMin) * g_WindowWidth) / (this->_xMax - this->_xMin);
}

GLfloat ShaListener::_GetWindowY(_In_ GLfloat y) const
{
    return g_WindowHeight -
            (((y - this->_yMin) * g_WindowHeight) / (this->_yMax - this->_yMin));
}

Vector ShaListener::_GetTransformedPosition(_In_ const Vector &position) const
{
    Vector transformedPosition = position;
    GLfloat maxDifference = std::max(
        abs(transformedPosition.x - this->_x),
        abs(transformedPosition.y - this->_y));
    transformedPosition.z = this->_z - maxDifference - 1;
    return transformedPosition;
}

void ShaListener::_ProcessHands(_In_ const Controller &controller)
{
    const HandList &hands = controller.frame(0).hands();

    if (!hands.isEmpty())
    {
        const Hand &hand = hands[0];
        const FingerList &fingers = hand.fingers();
        if (hands.count() == 1)
        {
            if (this->_isPinching)
            {
                if (this->_pointerType != PT_PINCH)
                {
                    this->_pointerType = PT_PINCH;
                    this->_ProcessPointerMoved(this->_GetTransformedPosition(this->_pinchPosition));
                }
                else
                {
                    this->_ProcessPointerMoved(this->_pinchPosition);
                }
            }
            else
            {
                if (this->_isSwiping || fingers.isEmpty())
                {
                    if (this->_pointerType != PT_PALM)
                    {
                        this->_pointerType = PT_PALM;
                        this->_ProcessPointerMoved(this->_GetTransformedPosition(hand.palmPosition()));
                    }
                    else
                    {
                        this->_ProcessPointerMoved(hand.palmPosition());
                    }
                }
                else
                {
                    auto fingerPosition = fingers.frontmost().tipPosition();
                    if (fingers.count() == 2)
                    {
                        fingerPosition = fingers.rightmost().tipPosition();
                    }
                    if (this->_pointerType != PT_FINGER)
                    {
                        this->_pointerType = PT_FINGER;
                        this->_ProcessPointerMoved(
                            this->_GetTransformedPosition(fingerPosition));
                    }
                    else
                    {
                        this->_ProcessPointerMoved(fingerPosition);
                    }
                }
            }
        }
        else
        {
            this->_ProcessPointerExited();
        }
    }
    else
    {
        this->_ProcessPointerExited();
    }
}

void ShaListener::_ProcessGestures(_In_ const Controller &controller)
{
    const GestureList gestures = controller.frame(0).gestures();

    for (int i = 0; i < gestures.count(); ++i)
    {
        const Gesture gesture = gestures[i];

        switch (gesture.type())
        {
        case Gesture::TYPE_SCREEN_TAP:
            this->_ProcessScreenTap(gesture);
            break;

        default:
            break;
        }
    }
}

void ShaListener::_ProcessScreenTap(_In_ const ScreenTapGesture &screenTap)
{
    const Vector &position = screenTap.position();
    GLfloat windowX = this->_GetWindowX(position.x);
    GLfloat windowY = this->_GetWindowY(position.y);

    if (this->_IsInBounds(windowX, windowY, position.z))
    {
        ShaPostMessage(AM_POINTER_TAPPED, 0, MAKELPARAM(round(windowX), round(windowY)));
    }
}

void ShaListener::_ProcessPointerMoved(_In_ const Vector &position)
{
    this->_ShiftBoundaries(position);

    GLfloat windowX = this->_GetWindowX(position.x);
    GLfloat windowY = this->_GetWindowY(position.y);

    if (this->_IsInBounds(windowX, windowY, position.z))
    {
        ShaPostMessage(AM_POINTER_MOVED, 0, MAKELPARAM(round(windowX), round(windowY)));
        this->_isExited = false;
    }
    else
    {
        this->_ProcessPointerExited();
    }

    this->_x = position.x;
    this->_y = position.y;
    this->_z = position.z;
}

void ShaListener::_ProcessPointerExited(void)
{
    if (!this->_isExited)
    {
        ShaPostMessage(AM_POINTER_EXITED, 0, 0);
        this->_isExited = true;

        this->_xMin = this->_baseXMin;
        this->_xMax = this->_baseXMax;
        this->_yMin = this->_baseYMin;
        this->_yMax = this->_baseYMax;
    }
}

void ShaListener::_ShiftBoundaries(_In_ const Vector &position)
{
    GLfloat windowX = this->_GetWindowX(position.x);
    GLfloat windowY = this->_GetWindowY(position.y);

    if (this->_IsInBounds(windowX, windowY, position.z))
    {
        GLfloat xDeltaMagnitude = abs(position.x - this->_x);
        GLfloat yDeltaMagnitude = abs(position.y - this->_y);
        GLfloat zDeltaMagnitude = abs(position.z - this->_z);
        GLfloat shiftWeight = 0.5f;

        if (this->_z > position.z &&
            zDeltaMagnitude > xDeltaMagnitude &&
            zDeltaMagnitude > yDeltaMagnitude)
        {
            this->_xMin += (position.x - this->_x);
            this->_xMax += (position.x - this->_x);
            this->_yMin += (position.y - this->_y);
            this->_yMax += (position.y - this->_y);
        }
        else if (position.z > this->_z &&
            zDeltaMagnitude > xDeltaMagnitude &&
            zDeltaMagnitude > yDeltaMagnitude)
        {
            if (this->_xMin != this->_baseXMin)
            {
                if (zDeltaMagnitude * shiftWeight > abs(this->_baseXMin - this->_xMin))
                {
                    this->_xMin = this->_baseXMin;
                    this->_xMax = this->_baseXMax;
                }
                else
                {
                    GLfloat shift = zDeltaMagnitude * shiftWeight *
                        ((this->_baseXMin - this->_xMin) / abs(this->_baseXMin - this->_xMin));
                    this->_xMin += shift;
                    this->_xMax += shift;
                }
            }

            if (this->_yMin != this->_baseYMin)
            {
                if (zDeltaMagnitude * shiftWeight > abs(this->_baseYMin - this->_yMin))
                {
                    this->_yMin = this->_baseYMin;
                    this->_yMax = this->_baseYMax;
                }
                else
                {
                    GLfloat shift = zDeltaMagnitude * shiftWeight *
                        ((this->_baseYMin - this->_yMin) / abs(this->_baseYMin - this->_yMin));
                    this->_yMin += shift;
                    this->_yMax += shift;
                }
            }
        }
        else
        {
            if (this->_xMin != this->_baseXMin)
            {
                if (xDeltaMagnitude * shiftWeight > abs(this->_baseXMin - this->_xMin))
                {
                    this->_xMin = this->_baseXMin;
                    this->_xMax = this->_baseXMax;
                }
                else
                {
                    GLfloat xShift = xDeltaMagnitude * shiftWeight *
                        ((this->_baseXMin - this->_xMin) / abs(this->_baseXMin - this->_xMin));
                    this->_xMin += xShift;
                    this->_xMax += xShift;
                }
            }

            if (this->_yMin != this->_baseYMin)
            {
                if (yDeltaMagnitude * shiftWeight > abs(this->_baseYMin - this->_yMin))
                {
                    this->_yMin = this->_baseYMin;
                    this->_yMax = this->_baseYMax;
                }
                else
                {
                    GLfloat yShift = yDeltaMagnitude * shiftWeight *
                        ((this->_baseYMin - this->_yMin) / abs(this->_baseYMin - this->_yMin));
                    this->_yMin += yShift;
                    this->_yMax += yShift;
                }
            }
        }
    }
}

bool ShaListener::_IsInBounds(_In_ GLfloat windowX, _In_ GLfloat windowY, _In_ GLfloat leapZ) const
{
    return windowX < g_WindowWidth && windowX > 0 &&
        windowY < g_WindowHeight && windowY > 0 &&
        leapZ < this->_baseZMax && leapZ > this->_baseZMin;
}



/*
 * Private Functions for Sha-specific Gestures
 */
void ShaListener::_ProcessShaGestures(_In_ const Controller &controller)
{
    auto frame = controller.frame(0);

    if (frame.hands().count() > 0)
    {
        auto hand = frame.hands().frontmost();
        auto palmNormal = hand.palmNormal();

        if (!this->_IsValidShaGesture(this->_currentShaGestureId))
        {
            this->_currentShaGestureId = INVALID_LEAP_ID;
        }

        for (auto gesture : this->_shaGestures)
        {
            gesture->Update(frame);

            switch (gesture->GetType())
            {
            case GT_PINCH:
                if (this->_enabledGestures[GT_PINCH])
                {
                    this->_ProcessShaPinchGesture(static_cast<ShaPinchGesture *>(gesture));
                }
                break;

            case GT_SWIPE:
                if (this->_enabledGestures[GT_SWIPE])
                {
                    this->_ProcessShaSwipeGesture(static_cast<ShaSwipeGesture *>(gesture));
                }
                break;
            }
        }
    }
}

void ShaListener::_ProcessShaPinchGesture(_In_ ShaPinchGesture *pinchGesture)
{
    if (this->_currentShaGestureId == INVALID_LEAP_ID && pinchGesture->GetState() != GS_STOP)
    {
        this->_currentShaGestureId = pinchGesture->GetId();
        this->_isShaGestureHandled = false;
    }

    auto pinchPosition = pinchGesture->GetPinchPosition();
    GLfloat windowX = this->_GetWindowX(pinchPosition.x);
    GLfloat windowY = this->_GetWindowY(pinchPosition.y);

    // Post Sha Message on Pinch and Release
    if (this->_currentShaGestureId != pinchGesture->GetId() || pinchGesture->GetState() == GS_STOP)
    {
        if (this->_isPinching)
        {
            this->_isPinching = false;
            this->_pinchPosition = pinchPosition;
            if (g_PointerIsPressed)
            {
                ShaPostMessage(AM_POINTER_RELEASED, 0, MAKELPARAM(round(windowX), round(windowY)));
            }
        }
    }
    else if (!this->_isShaGestureHandled)
    {
        if (pinchGesture->IsPinched() && !this->_isPinching)
        {
            this->_isPinching = true;
            this->_pinchPosition = pinchPosition;
            if (!g_PointerIsPressed)
            {
                ShaPostMessage(AM_POINTER_PRESSED, 0, MAKELPARAM(round(windowX), round(windowY)));
            }
        }
        else if (pinchGesture->IsReleased() && this->_isPinching)
        {
            this->_isPinching = false;
            if (g_PointerIsPressed)
            {
                ShaPostMessage(AM_POINTER_RELEASED, 0, MAKELPARAM(round(windowX), round(windowY)));
            }
            this->_isShaGestureHandled = true;
        }
    }

    if (this->_isPinching)
    {
        this->_pinchPosition = pinchPosition;
    }
}

void ShaListener::_ProcessShaSwipeGesture(_In_ ShaSwipeGesture *swipeGesture)
{
    if (this->_currentShaGestureId == INVALID_LEAP_ID && swipeGesture->GetState() != GS_STOP)
    {
        this->_currentShaGestureId = swipeGesture->GetId();
        this->_isShaGestureHandled = false;
    }

    if (this->_currentShaGestureId == swipeGesture->GetId())
    {
        this->_isSwiping = (swipeGesture->GetState() == GS_UPDATE);
    }

    if (swipeGesture->IsComplete() && !this->_isShaGestureHandled &&
        this->_currentShaGestureId == swipeGesture->GetId())
    {
        switch (swipeGesture->GetDirection())
        {
        case SD_LEFT:
            ShaPostMessage(AM_SWIPE_GESTURE, SD_LEFT, 0);
            break;

        case SD_RIGHT:
            ShaPostMessage(AM_SWIPE_GESTURE, SD_RIGHT, 0);
            break;
        }

        this->_isShaGestureHandled = true;
    }
}

bool ShaListener::_ContainsGestureType(_In_ GestureType gestureType) const
{
    bool contains = false;
    for (auto gesture : _shaGestures)
    {
        if (gesture->GetType() == gestureType)
        {
            contains = true;
        }
    }
    return contains;
}

bool ShaListener::_IsValidShaGesture(_In_ int gestureId) const
{
    for (auto gesture : this->_shaGestures)
    {
        if (gesture->GetId() == gestureId && gesture->GetState() != GS_STOP)
        {
            return true;
        }
    }
    return false;
}
