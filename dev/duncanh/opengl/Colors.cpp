/*
 * Colors.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines the values for the colors in the sha::graphics::Colors class
 */

#include "Colors.h"

using namespace sha::graphics;

/* Standard Colors */
const Color Colors::Red = { { { 1.0, 0.0, 0.0, 1.0 } } };
const Color Colors::Green = { { { 0.0, 1.0, 0.0, 1.0 } } };
const Color Colors::Blue = { { { 0.0, 0.0, 1.0, 1.0 } } };
const Color Colors::Magenta = { { { 1.0, 0.0, 1.0, 1.0 } } };
const Color Colors::Cyan = { { { 0.0, 1.0, 1.0, 1.0 } } };
const Color Colors::Yellow = { { { 1.0, 1.0, 0.0, 1.0 } } };
const Color Colors::Black = { { { 0.0, 0.0, 0.0, 1.0 } } };
const Color Colors::White = { { { 1.0, 1.0, 1.0, 1.0 } } };
const Color Colors::Transparent = { { { 0.0, 0.0, 0.0, 0.0 } } };

/* "Theme" Colors */
const Color Colors::BackgroundColor = { { { 0.1098039, 0.1098039, 0.1098039, 1.0 } } };
