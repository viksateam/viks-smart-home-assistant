/*
 * Shape.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * A simple extension to the UIElement class that adds a border thickness and color
 */

#include "Shape.h"

using namespace vik::graphics;
using namespace vik::graphics::shapes;

/*
* Constructors/Destructors
*/
Shape::Shape(void) :
    _borderThickness(0),
    _borderColor(Colors::Transparent)
{
}

Shape::Shape(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
    UIElement(x, y, width, height),
    _borderThickness(0),
    _borderColor(Colors::Transparent)
{
}

Shape::Shape(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    UIElement(pt, width, height),
    _borderThickness(0),
    _borderColor(Colors::Transparent)
{
}

Shape::Shape(_In_ const Rect &rc) :
    UIElement(rc),
    _borderThickness(0),
    _borderColor(Colors::Transparent)
{
}



/*
 * Border
 */
GLfloat Shape::GetBorderThickness(void) const
{
    return this->_borderThickness;
}

void Shape::SetBorderThickness(_In_ GLfloat thickness)
{
    assert(thickness >= 0);
    this->_borderThickness = thickness;
}

const Color &Shape::GetBorderColor(void) const
{
    return this->_borderColor;
}

void Shape::SetBorderColor(_In_ const Color &color)
{
    this->_borderColor = color;
}
