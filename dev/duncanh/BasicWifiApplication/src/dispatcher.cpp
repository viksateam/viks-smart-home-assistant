/*****************************************************************************
*
*  dispatcher.c  - CC3000 Host Driver Implementation.
*  Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/
*
*  Redistribution and use in source and binary forms, with or without
*  modification, are permitted provided that the following conditions
*  are met:
*
*    Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*****************************************************************************/

//*****************************************************************************
//
//! \addtogroup dispatcher_api
//! @{
//
//*****************************************************************************

#include <cstring>
#include <stdint.h>

#include "dispatcher.h"

#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"


/*
* __no_init is used to prevent the buffer initialization in order to prevent hardware WDT
* expiration before entering to 'main()'. For every IDE, different syntax exists :
*      1.  __CCS__ for CCS v5
*      2.  __IAR_SYSTEMS_ICC__ for IAR Embedded Workbench
*      3.  __arm__ for Keil
*/
#ifdef __CCS__

unsigned char g_ucUARTBuffer[UART_IF_BUFFER + 1];

#elif __IAR_SYSTEMS_ICC__

__no_init unsigned char g_ucUARTBuffer[UART_IF_BUFFER + 1];

#elif __arm__

__attribute__((zero_init))
unsigned char g_ucUARTBuffer[UART_IF_BUFFER + 1];

#elif WIN32     /* Ignore attributes for Visual Studio */

unsigned char g_ucUARTBuffer[UART_IF_BUFFER + 1];

#endif

volatile unsigned char uart_have_cmd = 0;
volatile unsigned long g_ulRxBuffCount = 0;
volatile unsigned long g_ulRxBuffIndex = 0;

unsigned long g_uluDMAErrCount = 0;



enum ARROWKEY_DIR
{
    AKD_UP      = 'A',
    AKD_DOWN    = 'B',
    AKD_RIGHT   = 'C',
    AKD_LEFT    = 'D',
};
static void DispatcherPrintArrowKey(ARROWKEY_DIR dir)
{
    UARTCharPut(UART0_BASE, 0x1B);
    UARTCharPut(UART0_BASE, '[');
    UARTCharPut(UART0_BASE, dir);
}

//*****************************************************************************
//
//! UARTIntHandler
//!
//!  \param  buffer
//!
//!  \return none
//!
//!  \brief  Handles RX and TX interrupts
//
//*****************************************************************************
#ifdef  __cplusplus
extern "C" {
#endif  /* __cplusplus */
void UART0_Handler(void)
{
    if (UARTIntStatus(UART0_BASE, TRUE))
    {
        int32_t nChar = UARTCharGet(UART0_BASE);

        // Need to handle a few special cases first (such as backspace, etc). Do so by handling any
        // character less than space (0x20; the first non-special character) separately than all
        // other characters. Additionally, make sure the character is not the delete character
        // (0x7F) as we handle that the same as backspace
        if (nChar < ' ' || nChar == 0x7F)
        {
            if (nChar == '\r')
            {
                // The user pressed ENTER. Update that the command is ready and output the
                // character. Note that we have to output \r\n for new lines
                UARTCharPut(UART0_BASE, nChar);
                UARTCharPut(UART0_BASE, '\n');
                g_ulRxBuffCount = 0;
                g_ulRxBuffIndex = 0;
                uart_have_cmd = 1;
            }
            else if (nChar == '\b' || nChar == 0x7F)
            {
                // The user pressed backspace. Move everything from the current position in the
                // buffer to the left
                if (g_ulRxBuffIndex > 0)
                {
                    // Shift the buffer
                    for (unsigned long i = g_ulRxBuffIndex; i < g_ulRxBuffCount; i++)
                    {
                        g_ucUARTBuffer[i - 1] = g_ucUARTBuffer[i];
                    }

                    // Print the characters in their new locations
                    DispatcherPrintArrowKey(AKD_LEFT);
                    for (unsigned long i = g_ulRxBuffIndex; i < g_ulRxBuffCount; i++)
                    {
                        UARTCharPut(UART0_BASE, g_ucUARTBuffer[i - 1]);
                    }

                    // Get rid of the previous last character
                    UARTCharPut(UART0_BASE, ' ');

                    // Move the cursor back
                    for (unsigned long i = g_ulRxBuffIndex; i <= g_ulRxBuffCount; i++)
                    {
                        DispatcherPrintArrowKey(AKD_LEFT);
                    }

                    g_ulRxBuffIndex--;
                    g_ulRxBuffCount--;
                }
            }
            else if (nChar == 0x1B)
            {
                // Escape sequence. Need to determine if it was the escape key or an arrow key
                int32_t nEscapeType = UARTCharGetNonBlocking(UART0_BASE);

                if (nEscapeType == '[')
                {
                    int32_t nDir = UARTCharGet(UART0_BASE);
                    bool fShouldPrint = false;

                    switch (nDir)
                    {
                    case 'C':       // Right
                        if (g_ulRxBuffIndex < g_ulRxBuffCount)
                        {
                            g_ulRxBuffIndex++;
                            fShouldPrint = true;
                        }
                        break;

                    case 'D':       // Left
                        if (g_ulRxBuffIndex > 0)
                        {
                            g_ulRxBuffIndex--;
                            fShouldPrint = true;
                        }
                        break;

                    case 'A':
                    case 'B':
                        break;

                    default:
                        while ((nChar = UARTCharGet(UART0_BASE)) != '~');
                        break;
                    }

                    if (fShouldPrint)
                    {
                        DispatcherPrintArrowKey((ARROWKEY_DIR)nDir);
                    }
                }
            }
        }
        else if (g_ulRxBuffCount < UART_IF_BUFFER)
        {
            // Non-special character; need to move all characters to the right of the index over
            // one slot; this means we need to print all characters at their new position...
            if (g_ulRxBuffIndex < g_ulRxBuffCount)
            {
                for (unsigned long i = g_ulRxBuffCount; i > g_ulRxBuffIndex; i--)
                {
                    g_ucUARTBuffer[i] = g_ucUARTBuffer[i - 1];
                }
            }

            // Now save and print the new character regardless
            g_ucUARTBuffer[g_ulRxBuffIndex] = nChar;
            g_ulRxBuffCount++;
            g_ulRxBuffIndex++;

            // Echo all non-special characters
            UARTCharPut(UART0_BASE, nChar);

            // Finally, print the moved characters if needed
            if (g_ulRxBuffIndex < g_ulRxBuffCount)
            {
                for (unsigned long i = g_ulRxBuffIndex; i < g_ulRxBuffCount; i++)
                {
                    UARTCharPut(UART0_BASE, g_ucUARTBuffer[i]);
                }

                // Need to move cursor back by the same amount
                for (unsigned long i = g_ulRxBuffIndex; i < g_ulRxBuffCount; i++)
                {
                    DispatcherPrintArrowKey(AKD_LEFT);
                }
            }
        }
    }
}
#ifdef  __cplusplus
}
#endif  /* __cplusplus */

//*****************************************************************************
//
//! DispatcherUartSendPacket
//!
//!  \param  inBuff    pointer to the UART input buffer
//!  \param  usLength  buffer length
//!
//!  \return none
//!
//!  \brief  The function sends to UART a buffer of given length 
//
//*****************************************************************************
void DispatcherUartSendPacket(const unsigned char *inBuff, unsigned short usLength)
{
    unsigned long ulIndex = 0;
    for (ulIndex = 0; ulIndex < usLength; ulIndex++)
    {
        MAP_UARTCharPut(UART0_BASE, *inBuff++);
    }
}

//*****************************************************************************
//
//! Cofigure the UART
//!
//!  @param  none
//!
//!  @return none
//!
//!  @brief  Cofigure the UART
//
//*****************************************************************************
void DispatcherUARTConfigure(unsigned long ucSysClock)
{
    g_ulRxBuffCount = 0;

    // Enable corresponding peripherals modules
    // The UART Init shall be in test code
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    MAP_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    //
    // Set GPIO A0 and A1 as UART pins
    //
    GPIOPadConfigSet(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1, GPIO_STRENGTH_2MA,
        GPIO_PIN_TYPE_STD_WPU);
    GPIOPinConfigure(0x00000001); //GPIO_PA0_U0RX
    GPIOPinConfigure(0x00000401); //GPIO_PA1_U0TX
    MAP_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    //
    // Configure the UART for 115,200, 8-N-1 operation
    //
    MAP_UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
        (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

    MAP_UARTIntDisable(UART0_BASE, 0xFFFFFFFF);

    // Set both the TX and RX trigger thresholds to 4.  This will be used by
    // the uDMA controller to signal when more data should be transferred.  The
    // uDMA TX and RX channels will be configured so that it can transfer 2
    // bytes in a burst when the UART is ready to transfer more data.
    UARTFIFOLevelSet(UART0_BASE, UART_FIFO_TX1_8, UART_FIFO_RX1_8);

    //
    // Enable the UART interrupt but first clear all the pending interrupts
    //
    MAP_UARTIntClear(UART0_BASE, 0xFFFFFFFF);
    MAP_UARTIntEnable(UART0_BASE, UART_INT_RT | UART_INT_RX);
    MAP_IntEnable(INT_UART0);
}

//*****************************************************************************
//
// Close the Doxygen group.
//! @}
//
//*****************************************************************************
