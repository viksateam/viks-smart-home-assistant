/*
 * Console.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Implements a simple Console interface for the Tiva C Series TM4C123G LaunchPad Evaluation Kit
 */
#pragma once

#include "sha.h"

#include <stdint.h>

#define CONSOLE_BUFFER_SIZE             (128)

class Console
{
private:
    /* Can't instantiate an instance of a Console object */
    Console(void) {}

    enum SEEK_DIR
    {
        SEEK_DIR_UP     = 'A',
        SEEK_DIR_DOWN   = 'B',
        SEEK_DIR_RIGHT  = 'C',
        SEEK_DIR_LEFT   = 'D'
    };

public:

    static void Init(void);
    static void Enable(void);
    static void Disable(void);
    static void StartTrace(void);
    static void EndTrace(void);

    static bool HasNext(void);
    static bool HasNextLine(void);

    static void OnInput(_In_ int32_t nKey);

    /* Input */
    static void Read(_Out_ char *pszBuff);
    static void ReadLine(_Out_ char *pszBuff);

    /* Output */
    static void WriteByte(_In_ char ch);
    static void Write(_In_z_ const char *psz);
    static void Write(_In_ uint32_t uVal);
    static void WriteBuffer(_In_reads_(uLen) char *psz, _In_ uint32_t uLen);
    static void WriteLine(_In_z_ const char *psz);
    static void WriteLine(_In_ uint32_t uVal);
    static void Trace(_In_z_ const char *psz);
    static void TraceLine(_In_z_ const char *psz);

private:

    /* Various key press handlers */
    static void _OnNormalKeyPressed(_In_ int32_t nKey);
    static void _OnReturnPressed(void);
    static void _OnBackspacePressed(void);
    static void _OnEscapeSequence(void);

    static inline void _Seek(_In_ SEEK_DIR dir);
    static inline int _NextIndex(_In_ int nIndex);
    static inline int _PrevIndex(_In_ int nIndex);

    static inline void _Block(void);

    /* Console Buffer */
    static volatile char s_rgszBuffer[CONSOLE_BUFFER_SIZE];
    static volatile int  s_nStartOfBuffer;
    static volatile int  s_nEndOfBuffer;
    static volatile int  s_nSeekIndex;

    static bool s_fEnabled;
    static bool s_fTrace;
};
