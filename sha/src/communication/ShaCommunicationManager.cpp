/*
 * ShaCommunicationManager.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Used to facilitate communication over the network between the client/server threads and the main
 * UI thread (or other worker threads).
 */

#include <algorithm>
#include <cctype>
#include <functional>
#include <locale>
#include <random>
#include <sstream>

#include "ShaCommunicationManager.h"
#include "ShaGraphics.h"
#include "ShaServer.h"
#include "ShaStation.h"
#include "WindowHelpers.h"

using namespace vik;
using namespace vik::communication;
using namespace vik::graphics;

static void _HandleNewConnection(_Inout_ std::istringstream &stream, _In_ const InetAddress &addr);
static void _HandleDisconnect(_Inout_ std::istringstream &stream);
static std::string _HandleCommand(_Inout_ std::istringstream &stream);

static ShaStation *_MakeStation(
    _In_ const std::string &type,
    _In_ const InetAddress &addr,
    _In_ const std::string &id,
    _In_ const std::string &name);

/*
 * Variable Declarations
 */
std::string ShaCommunicationManager::s_stationId;
std::string ShaCommunicationManager::s_stationName = "Vik's iTunes";
request_cookie ShaCommunicationManager::s_nextCookie = 0;



/* NOTE: Although there appears to be a race condition here, ShaServer::Start calls the StationId
   function when it sends the initial broadcast message, which will initialize the StationId (and
   thus there is no race condition to worry about) */
const std::string &ShaCommunicationManager::StationId(void)
{
    // Only assign the id once
    static bool isInitialized = false;

    if (!isInitialized)
    {
        // TODO: load from file?
        std::random_device eng;
        std::stringstream stream;
        stream << "control_station-" << std::hex << std::uppercase << eng();

        s_stationId = stream.str();
        isInitialized = true;
    }

    return s_stationId;
}

const std::string &ShaCommunicationManager::StationName(void)
{
    return s_stationName;
}

std::string ShaCommunicationManager::GetInfoString(void)
{
    std::stringstream stream;
    stream << "control " << StationId() << " " << s_stationName;

    return stream.str();
}



/*
 * Incoming Messages
 */
void ShaCommunicationManager::OnBroadcastMessageReceived(
    _In_ const std::string &msg,
    _In_ const InetAddress &addr)
{
    // There are four types of broadcast messages:
    //      (1) connect
    //      (2) info
    //      (3) disconnect
    //      (4) update
    std::istringstream stream(msg);

    std::string command;
    stream >> command;
    if (command == "connect" || command == "info")
    {
        _HandleNewConnection(stream, addr);
    }
    else if (command == "disconnect")
    {
        _HandleDisconnect(stream);
    }
    else if (command == "update")
    {
        // TODO
    }
    else
    {
        // Invalid command
    }
}

std::string ShaCommunicationManager::OnConnectionMessageReceived(
    _In_ const std::string &msg,
    _In_ const InetAddress &addr)
{
    // There are _____ types of connection messages:
    //      (1) heartbeat
    //      (2) request
    std::istringstream stream(msg);

    std::string command;
    stream >> command;
    if (command == "heartbeat")
    {
        // On heartbeat, just acknowledge
        return "ack\n";
    }
    else if (command == "info")
    {
        // No expected response for info commands
        _HandleNewConnection(stream, addr);
        return std::string();
    }
    else if (command == "request")
    {
        return _HandleCommand(stream);
    }

    // Otherwise, invalid message
    return "invalid\n";
}

static void _HandleNewConnection(
    _Inout_ std::istringstream &stream,
    _In_ const InetAddress &addr)
{
    // Format (from here) is: [control|appliance] [id] [name] -- <reply|noreply>
    std::string type, id, name;
    stream >> type;
    stream >> id;

    // Ignore if the message was from us
    if (id == ShaCommunicationManager::StationId())
    {
        return;
    }

    // Read until "--"
    char *prefix = "";
    do
    {
        std::string arg;
        std::getline(stream, arg, '-');

        name += prefix + arg;
        prefix = "-";
    }
    while (!stream.eof() && !stream.fail() && stream.peek() != '-');

    // Need to trim the whitespace from the name
    name.erase(std::begin(name), std::find_if(std::begin(name), std::end(name),
        std::not1(std::ptr_fun<int, int>(std::isspace))));
    name.erase(std::find_if(std::rbegin(name), std::rend(name),
        std::not1(std::ptr_fun<int, int>(std::isspace))).base(), std::end(name));

    // Assume this is a new connection; the UI thread can determine if it is or not
    ShaStation *station = _MakeStation(type, addr, id, name);
    if (station)
    {
        bool success = ShaPostMessage(AM_STATION_CONNECTED, 0, (uintptr_t)station);

        // Remove the last '-' from the stream
        stream.get();

        if (!stream.eof() && !stream.fail())
        {
            std::string command;
            stream >> command;
            if (command == "reply")
            {
                std::string msg = "info " + ShaCommunicationManager::GetInfoString() + "\n";
                ShaServer::SendSingleMessage(station->InetAddr(), msg);
            }
        }

        if (!success)
        {
            delete station;
        }
    }
}

static void _HandleDisconnect(_Inout_ std::istringstream &stream)
{
    std::string id;
    stream >> id;

    // Need to create a copy of the string to post the message
    if (!id.empty() && id != ShaCommunicationManager::StationId())
    {
        char *msg = new char[id.size() + 1];
        strcpy_s(msg, id.size() + 1, id.c_str());

        if (!ShaPostMessage(AM_STATION_DISCONNECTED, 0, (intptr_t)msg))
        {
            delete[] msg;
        }
    }
}

static std::string _HandleCommand(_Inout_ std::istringstream &stream)
{
    // Format (from here) is: [get|post] [command] <args...>
    std::string type, command;
    stream >> type;
    stream >> command;

    if (type == "get")
    {
        // Control stations currently only accept the info command
        if (command == "info")
        {
            if (IsITunesRunning())
            {
                return "itunes\n";
            }
            else
            {
                return "control_station\n";
            }
        }
        else if (command == "status" && IsITunesRunning())
        {
            ITunesStatus status;
            if (GetITunesStatus(status))
            {
                std::string response;
                response = status.songState == ITunesStatus::SS_PLAY ? "play " : "pause ";

                std::replace(std::begin(status.artistName), std::end(status.artistName), ' ', '_');
                std::replace(std::begin(status.albumName), std::end(status.albumName), ' ', '_');
                std::replace(std::begin(status.trackName), std::end(status.trackName), ' ', '_');

                response += status.artistName + " " + status.albumName + " " + status.trackName;
                return response + "\n";
            }
        }
    }
    else if (type == "post" && IsITunesRunning())
    {
        std::string arg;
        stream >> arg;

        if (command == "song")
        {
            if (arg == "play")
            {
                return ITunesSongRequest(ITunesStatus::SS_PLAY);
            }
            else if (arg == "pause")
            {
                return ITunesSongRequest(ITunesStatus::SS_PAUSE);
            }
        }
        else if (command == "track")
        {
            if (arg == "previous")
            {
                return ITunesTrackRequest(TR_PREVIOUS);
            }
            else if (arg == "next")
            {
                return ITunesTrackRequest(TR_NEXT);
            }
        }
    }

    // Otherwise, invalid type
    return "invalid\n";
}

static ShaStation *_MakeStation(
    _In_ const std::string &type,
    _In_ const InetAddress &addr,
    _In_ const std::string &id,
    _In_ const std::string &name)
{
    ShaStationType sst;

    if (type == "control")
    {
        sst = SST_CONTROL_STATION;
    }
    else if (type == "appliance")
    {
        sst = SST_APPLIANCE_STATION;
    }
    else
    {
        return nullptr;
    }

    // Always use SHA_PORT as the port number
    InetAddress realAddress(addr.GetIpAddress(), SHA_PORT);
    return new ShaStation(sst, realAddress, id, name);
}



/*
 * Outgoing Messages
 */
_Success_(return != INVALID_REQUEST_COOKIE)
request_cookie ShaCommunicationManager::PostHeartbeatMessage(_In_ vik::ShaStation *station)
{
    if (!station->PostGetStatusRequest())
    {
        return PostServerRequest(station, RT_GET, "status");
    }

    return INVALID_REQUEST_COOKIE;
}

_Success_(return != INVALID_REQUEST_COOKIE)
request_cookie ShaCommunicationManager::PostServerRequest(
    _In_ const ShaStation *station,
    _In_ RequestType type,
    _In_ const std::string &command,
    _In_ const std::vector<std::string> &args)
{
    // Make the message string
    std::stringstream stream;
    stream << "request " << type << " " << command;
    for (auto &arg : args)
    {
        stream << " " << arg;
    }
    stream << "\n";

    // Post the message asynchronously
    std::string message = stream.str();
    if (ShaServer::StartConnection(station->InetAddr(), message, s_nextCookie))
    {
        return s_nextCookie++;
    }

    return INVALID_REQUEST_COOKIE;
}
