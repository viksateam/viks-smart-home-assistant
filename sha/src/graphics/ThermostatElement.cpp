/*
 * ThermostatElement.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * The UIElement that graphically represents a Thermostat ShaStation object. When the Thermostat
 * class is updated (i.e. a new supported feature is added), this class needs to get updated in
 * order for the addition to make an effect graphically.
 */

#include "Drawing.h"
#include "ShaColors.h"
#include "ShaFont.h"
#include "ThermostatElement.h"
#include "WindowHelpers.h"

using namespace vik;
using namespace vik::graphics;

#define THERMOSTAT_LEFT         (1.0f)
#define THERMOSTAT_RIGHT        (1.6f)
#define THERMOSTAT_LO           (2.6f)
#define THERMOSTAT_HI           (0.3f)

#define THUMB_SIZE              (15)

#define ACTUAL_TEMP_STRING      "Actual Temp."

GLfloat ThermostatElement::s_tempStringLength = 0;

static const std::vector<Point> _bodyPoints
{
    Point(1.0f, 2.88038f), Point(1.0f, 0.3f), Point(1.00576f, 0.24147f), Point(1.02283f, 0.18519f),
    Point(1.05055f, 0.13332f), Point(1.08786f, 0.08786f), Point(1.13332f, 0.05055f),
    Point(1.18519f, 0.02283f), Point(1.24147f, 0.00576f), Point(1.3f, 0.0f),
    Point(1.35852f, 0.00576f), Point(1.4148f, 0.02283f), Point(1.46667f, 0.05055f),
    Point(1.51213f, 0.08786f), Point(1.54944f, 0.13332f), Point(1.57716f, 0.18519f),
    Point(1.59423f, 0.24147f), Point(1.6f, 0.3f), Point(1.6f, 2.88038f),
};

static const std::vector<Point> _basePoints
{
    Point(1.3f, 2.8f), Point(1.41705f, 2.81152f), Point(1.52961f, 2.84567f),
    Point(1.63334f, 2.90111f), Point(1.72426f, 2.97573f), Point(1.79888f, 3.06665f),
    Point(1.85432f, 3.17038f), Point(1.88847f, 3.28294f), Point(1.90000f, 3.4f),
    Point(1.88847f, 3.51705f), Point(1.85432f, 3.62961f), Point(1.79888f, 3.73334f),
    Point(1.72426f, 3.82426f), Point(1.63334f, 3.89888f), Point(1.52961f, 3.95432f),
    Point(1.41705f, 3.98847f), Point(1.3f, 4.0f), Point(1.18294f, 3.98847f),
    Point(1.07038f, 3.95432f), Point(0.96665f, 3.89888f), Point(0.87573f, 3.82426f),
    Point(0.80111f, 3.73334f), Point(0.74567f, 3.62961f), Point(0.71152f, 3.51705f),
    Point(0.7f, 3.4f), Point(0.71152f, 3.28294f), Point(0.74567f, 3.17038f),
    Point(0.80111f, 3.06665f), Point(0.87573f, 2.97573f), Point(0.96665f, 2.90111f),
    Point(1.07038f, 2.84567f), Point(1.18294f, 2.81152f),
};

static const std::vector<std::string> _systemOptions{ "Off", "Cool", "Heat" };
static const std::vector<std::string> _fanOptions{ "On", "Auto" };

/*
* Constructor(s)/Destructor
*/
ThermostatElement::ThermostatElement(_In_ Thermostat *target) :
    ThermostatElement(target, Rect(0, 0, 0, 0))
{
}

ThermostatElement::ThermostatElement(
    _In_ Thermostat *target,
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    ThermostatElement(target, Rect(x, y, width, height))
{
}

ThermostatElement::ThermostatElement(
    _In_ Thermostat *target,
    _In_ const Point &pt,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    ThermostatElement(target, Rect(pt, width, height))
{
}

ThermostatElement::ThermostatElement(_In_ Thermostat *target, _In_ const Rect &rc) :
    Layout(rc),
    _target(target),
    _base(0, 0, 3, 4),
    _body(0, 0, 3, 4),
    _temperatureStringLength(0),
    _thermostatSlider(rc, VERTICAL, (GLfloat)target->GetMinTemp(), (GLfloat)target->GetMaxTemp(),
        (GLfloat)target->GetMinTemp())
{
    // Only calculate the width once
    if (s_tempStringLength == 0)
    {
        s_tempStringLength = GetStringWidth(Fonts::ApplianceFont, ACTUAL_TEMP_STRING);
    }

    // Initialize the combo boxes
    this->_systemComboBox.SetFont(Fonts::ApplianceFont);
    this->_systemComboBox.SetBorderThickness(2);
    this->_systemComboBox.SetBorderColor(ShaColors::ForegroundColor);
    this->_systemComboBox.SetForegroundColor(ShaColors::ForegroundColor);
    this->_systemComboBox.SetPadding(Padding(20));
    this->_systemComboBox.OnManipulationCompleted(
        (SliderComboBox::ManipulationEventCallback)&ThermostatElement::_OnManipulationCompleted,
        this);
    for (auto &opt : _systemOptions)
    {
        this->_systemComboBox.AddOption(opt);
    }

    this->_fanComboBox.SetFont(Fonts::ApplianceFont);
    this->_fanComboBox.SetBorderThickness(2);
    this->_fanComboBox.SetBorderColor(ShaColors::ForegroundColor);
    this->_fanComboBox.SetForegroundColor(ShaColors::ForegroundColor);
    this->_fanComboBox.SetPadding(Padding(20));
    this->_fanComboBox.OnManipulationCompleted(
        (SliderComboBox::ManipulationEventCallback)&ThermostatElement::_OnManipulationCompleted,
        this);
    for (auto &opt : _fanOptions)
    {
        this->_fanComboBox.AddOption(opt);
    }

    // Initialize the slider
    this->_thermostatSlider.SetLoColor(Colors::Red);
    this->_thermostatSlider.SetThumbSize(THUMB_SIZE);
    this->_thermostatSlider.SetThumbBorderThickness(2);
    this->_thermostatSlider.SetBorderColor(ShaColors::ForegroundColor);
    this->_thermostatSlider.SetHoverColor(ShaColors::ButtonHoverColor);
    this->_thermostatSlider.OnManipulationCompleted(
        (Slider::ManipulationEventCallback)&ThermostatElement::_OnSliderManipulationCompleted,
        this);

    // Initialize the "image"
    this->_body.AddPoints(std::begin(_bodyPoints), std::end(_bodyPoints));
    this->_body.SetBorderColor(ShaColors::ForegroundColor);
    this->_body.SetBorderThickness(3);

    this->_base.AddPoints(std::begin(_basePoints), std::end(_basePoints));
    this->_base.SetBorderColor(ShaColors::ForegroundColor);
    this->_base.SetBackgroundColor(Colors::Red);
    this->_base.SetBorderThickness(3);

    // Current Temperature
    this->_temperatureString[0] = '\0';
}



/*
 * UIElement
 */
static const char *commonVals[]
{
    "0",  "5",  "10", "15", "20", "25", "30", "35", "40", "45", "50",
    "55", "60", "65", "70", "75", "80", "85", "90", "95", "100"
};
GLvoid ThermostatElement::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    // Draw the images
    this->_base.Draw(offX, offY);
    this->_thermostatSlider.Draw(offX, offY);
    this->_body.Draw(offX, offY);

    // This is a hack: We need to draw over the outlines, so manually draw a rectangle
    GLfloat x = this->_thermostatSlider.GetX() + 1;
    GLfloat y = this->_thermostatSlider.GetY() + this->_thermostatSlider.GetHeight();
    GLfloat width = this->_thermostatSlider.GetWidth() - 2 * this->_body.GetBorderThickness() + 3;
    GLfloat height = this->_base.GetHeight() * 0.6f / 4;
    FillRect(x + offX, y + offY, width, height, Colors::Red);

    // Use different colors to represent lower/higher set temperatures
    y -= THUMB_SIZE / 2;
    int min = this->_target->GetMinTemp();
    int max = this->_target->GetMaxTemp();
    GLfloat delta = (this->_thermostatSlider.GetHeight() - THUMB_SIZE) / (max - min);

    GLfloat setY = y - (this->_thermostatSlider.GetValue() - min) * delta;
    y -= (this->_status.currTemp - min) * delta;

    if (y - setY > THUMB_SIZE / 2)
    {
        // Temperature is set above the current temperature
        FillRect(x + offX, setY + offY + THUMB_SIZE / 2, width, y - setY - THUMB_SIZE / 2,
            Colors::Orange);
    }
    else if (setY - y > THUMB_SIZE / 2)
    {
        // Temperature is set below the current temperature
        FillRect(x + offX, y + offY, width, setY - y - THUMB_SIZE / 2, Colors::Blue);
    }

    // Draw the temperature "notches"
    y = this->_thermostatSlider.GetY() + this->_thermostatSlider.GetHeight() - THUMB_SIZE / 2;
    for (int i = min; i <= max; i++)
    {
        GLfloat notchWidth = this->_thermostatSlider.GetWidth() / 8;

        if (i % 10 == 0)
        {
            notchWidth *= 2;
        }
        if (i % 5 == 0)
        {
            notchWidth *= 2;

            if (i >= 0 && i <= 100)
            {
                if (delta * 3 > Fonts::ApplianceFont.height)
                {
                    WindowDrawText(x - 55 + offX, y - Fonts::ApplianceFont.height / 2 + offY,
                        commonVals[i / 5], Fonts::ApplianceFont, ShaColors::ForegroundColor);
                }
                else
                {
                    WindowDrawText(x - 23 + offX, y - Fonts::NotificationFont.height / 2 + offY,
                        commonVals[i / 5], Fonts::NotificationFont, ShaColors::ForegroundColor);
                }
            }
        }

        FillRect(x + offX, y + offY, notchWidth, 2, ShaColors::ForegroundColor);
        y -= delta;
    }

    // Draw the actual temperature
    y = this->_thermostatSlider.GetY() + this->_thermostatSlider.GetHeight() - THUMB_SIZE / 2 -
        (this->_status.currTemp - min) * delta;
    width = (this->GetWidth() / 2) - this->_thermostatSlider.GetX();
    FillRect(x + offX, y + offY, width, 2, ShaColors::ForegroundColor);

    x += width - s_tempStringLength;
    GLfloat tempY = y - Fonts::ApplianceFont.height - 5;
    WindowDrawText(x + offX, tempY + offY, ACTUAL_TEMP_STRING, Fonts::ApplianceFont,
        ShaColors::ForegroundColor);

    y += 26;
    x += s_tempStringLength - this->_temperatureStringLength - 100;
    WindowDrawText(x + offX, y + offY, this->_temperatureString, Fonts::LargeFont,
        ShaColors::ForegroundColor);

    x += this->_temperatureStringLength + 20;
    WindowDrawText(x + offX, y - 5 + offY, "o", Fonts::ApplianceFont, ShaColors::ForegroundColor);

    x += 20;
    WindowDrawText(x + offX, y + offY, "F", Fonts::LargeFont, ShaColors::ForegroundColor);

    // Draw the Combo combo boxes
    this->_systemComboBox.Draw(offX, offY);
    this->_fanComboBox.Draw(offX, offY);

    height = this->GetHeight() - TOP_PADDING - BOTTOM_PADDING;
    x = this->GetWidth() - SIDE_PADDING - this->_mainWidth * 3 / 4;
    y = TOP_PADDING + (height / 4) - 2.0f * Fonts::ApplianceFont.height;
    WindowDrawText(offX + x, offY + y, "System", Fonts::ApplianceFont, ShaColors::ForegroundColor);

    y += height / 2;
    WindowDrawText(offX + x, offY + y, "Fan", Fonts::ApplianceFont, ShaColors::ForegroundColor);

    WindowDrawText(offX + TITLE_X_OFFSET, offY + TITLE_Y_OFFSET, this->_target->Name().c_str(),
        Fonts::TitleFont, ShaColors::ForegroundColor);
}

bool ThermostatElement::OnPointerMoved(_In_ Point pt)
{
    if (Layout::OnPointerMoved(pt))
    {
        return true;
    }

    if (this->_systemComboBox.GetRect().Contains(pt) &&
        this->_systemComboBox.OnPointerEntered(pt - this->_systemComboBox.GetLocation()))
    {
        this->_pointerTarget = &this->_systemComboBox;
        return true;
    }
    else if (this->_fanComboBox.GetRect().Contains(pt) &&
        this->_fanComboBox.OnPointerEntered(pt - this->_fanComboBox.GetLocation()))
    {
        this->_pointerTarget = &this->_fanComboBox;
        return true;
    }
    else if (this->_thermostatSlider.GetRect().Contains(pt) &&
        this->_thermostatSlider.OnPointerEntered(pt - this->_thermostatSlider.GetLocation()))
    {
        this->_pointerTarget = &this->_thermostatSlider;
        return true;
    }

    return false;
}



void ThermostatElement::OnStatusChanged(_In_ const ThermostatStatus &status)
{
    if (this->_thermostatSlider.IsManipulating())
    {
        // Still update the current temp
        this->_status.currTemp = status.currTemp;
        return;
    }

    this->_thermostatSlider.SetValue((GLfloat)status.setTemp);

    _itoa_s(status.currTemp, this->_temperatureString, 10);
    this->_temperatureStringLength = GetStringWidth(Fonts::LargeFont, this->_temperatureString);

    this->_status = status;
    this->_RefreshLayout();
}



/*
 * LayoutAwareElement
 */
void ThermostatElement::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    Layout::_OnSizeUpdated(width, height);
    this->_RefreshLayout();
}

void ThermostatElement::_RefreshLayout(void)
{
    GLfloat width = this->GetWidth();
    GLfloat height = this->GetHeight();

    this->_mainWidth = (width - 2 * SIDE_PADDING) / 2;
    GLfloat mainHeight = height - (TOP_PADDING + BOTTOM_PADDING);

    GLfloat imageWidth = this->_mainWidth;

    // Handle the "picture" size/location
    if (mainHeight < 0 || imageWidth < 0)
    {
        // Prevent setting rectangle widths to zero
        this->_body.SetRect(Rect(0, 0, 1, 1));
        this->_base.SetRect(Rect(0, 0, 1, 1));
    }
    else
    {
        GLfloat dim = std::min(mainHeight / 4, imageWidth / 3);
        GLfloat x = SIDE_PADDING + (imageWidth - dim * 3) / 2;
        GLfloat y = TOP_PADDING + (mainHeight - dim * 4) / 2;

        this->_body.SetRect(Rect(x, y, dim * 3, dim * 4));
        this->_base.SetRect(Rect(x, y, dim * 3, dim * 4));

        // Set the size of the slider
        width = (THERMOSTAT_RIGHT - THERMOSTAT_LEFT) * dim;
        height = (THERMOSTAT_LO - THERMOSTAT_HI) * dim;
        x += THERMOSTAT_LEFT * dim;
        y += THERMOSTAT_HI * dim;
        this->_thermostatSlider.SetRect(Rect(x, y, width, height));

        // Set the size of the combo boxes
        x = this->GetWidth() - SIDE_PADDING - this->_mainWidth;
        y = TOP_PADDING + (mainHeight / 4);
        this->_systemComboBox.SetRect(Rect(x, y, this->_mainWidth, 100));

        y = TOP_PADDING + (mainHeight * 3 / 4);
        this->_fanComboBox.SetRect(Rect(x, y, this->_mainWidth, 100));

        switch (this->_status.system)
        {
        case ThermostatStatus::SS_OFF:
            this->_systemComboBox.JumpToOption("Off");
            break;

        case ThermostatStatus::SS_COOL:
            this->_systemComboBox.JumpToOption("Cool");
            break;

        case ThermostatStatus::SS_HEAT:
            this->_systemComboBox.JumpToOption("Heat");
            break;
        }

        switch (this->_status.fan)
        {
        case ThermostatStatus::FS_ON:
            this->_fanComboBox.JumpToOption("On");
            break;

        case ThermostatStatus::FS_AUTO:
            this->_fanComboBox.JumpToOption("Auto");
            break;
        }
    }
}

void ThermostatElement::_OnManipulationCompleted(_In_ SliderComboBox *sender, _In_ int index)
{
    ThermostatStatus status = this->_status;

    if (sender == &this->_systemComboBox)
    {
        status.system = (ThermostatStatus::SystemStatus)index;
    }
    else if (sender == &this->_fanComboBox)
    {
        status.fan = (ThermostatStatus::FanStatus)index;
    }

    this->_target->PostStatusRequest(status);
}

void ThermostatElement::_OnSliderManipulationCompleted(_In_ Slider *sender, _In_ GLfloat value)
{
    ThermostatStatus status = this->_status;

    if (sender == &this->_thermostatSlider)
    {
        status.setTemp = (int)std::round(value);
        this->_thermostatSlider.SetValue((GLfloat)status.setTemp);
    }

    this->_target->PostStatusRequest(status);
}
