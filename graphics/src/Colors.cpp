/*
 * Colors.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines the values for the colors in the vik::graphics::Colors class
 */

#include "Colors.h"

using namespace vik::graphics;

#define FROM_RGBA(r, g, b, a)       { { { r, g, b, a } } }

/* Standard Colors */
const Color Colors::Red         = FROM_RGBA(1.0, 0.0, 0.0, 1.0);
const Color Colors::Green       = FROM_RGBA(0.0, 1.0, 0.0, 1.0);
const Color Colors::Blue        = FROM_RGBA(0.0, 0.0, 1.0, 1.0);
const Color Colors::Magenta     = FROM_RGBA(1.0, 0.0, 1.0, 1.0);
const Color Colors::Cyan        = FROM_RGBA(0.0, 1.0, 1.0, 1.0);
const Color Colors::Yellow      = FROM_RGBA(1.0, 1.0, 0.0, 1.0);
const Color Colors::Orange      = FROM_RGBA(1.0, 0.64706f, 0.0, 1.0);
const Color Colors::Brown       = FROM_RGBA(0.58834f, 0.29412f, 0.0, 1.0);
const Color Colors::Black       = FROM_RGBA(0.0, 0.0, 0.0, 1.0);
const Color Colors::White       = FROM_RGBA(1.0, 1.0, 1.0, 1.0);
const Color Colors::Transparent = FROM_RGBA(0.0, 0.0, 0.0, 0.0);
