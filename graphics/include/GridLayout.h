/*
 * GridLayout.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a Layout that positions children in a grid-like fashion. Elements added to the
 * GridLayout are automatically resized to match the dimensions of the cell they occupy. By
 * default, each cell is given a width and height of 1. The width and height are described as
 * percentages as opposed to absolute sizes.
 */
#pragma once

#include "Layout.h"

namespace vik
{
    namespace graphics
    {
        class GridLayout final :
            public Layout
        {
        public:
            GridLayout(void);
            GridLayout(_In_ GLfloat width, _In_ GLfloat height);
            GridLayout(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
            GridLayout(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            GridLayout(_In_ const Rect &rc);

            /* UIElement */
            virtual void Draw(_In_ GLfloat offX, _In_ GLfloat offY);

            /* Layout */
            size_t GetNumCols(void) const;
            size_t GetNumRows(void) const;
            size_t GetBoundarySize(void) const;
            size_t GetMarginSize(void) const;
            void   SetNumCols(_In_ size_t cols);
            void   SetNumRows(_In_ size_t rows);
            void   SetBoundarySize(_In_ size_t boundarySize);
            void   SetMarginSize(_In_ size_t marginSize);

            /* Children */
            virtual void AddChild(_In_ UIElement *elem);
            bool         RemoveChild(_In_ UIElement *elem);
            bool         RemoveChild(_In_ size_t index);
            UIElement    *GetChild(_In_ size_t index) const;
            size_t       Size(void) const;

        private:

            std::vector<UIElement *> _children;
            size_t _cols = 4;                   // default to 4 columnns
            size_t _rows = 3;                   // default to 3 rows

            size_t _boundarySize = 20;
            size_t _marginSize = 10;

            /* Layout */
            size_t _GetColFromIndex(_In_ size_t index) const;
            size_t _GetRowFromIndex(_In_ size_t index) const;
        };
    }
}
