/*
 * AbsoluteLayout.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a Layout where children are places absolutely with respect to their desired widths
 * and heights. Note that this layout does not respect width/height restrictions. That is, children
 * are drawn regardless of whether or not whole or part of the element exists outside the bounds of
 * the layout.
 */

#include "AbsoluteLayout.h"
#include "DeletionAnimation.h"

using namespace vik::graphics;
using namespace vik::graphics::animations;

/*
 * Constructors/Destructor
 */
AbsoluteLayout::AbsoluteLayout(void) :
    Layout()
{
}

AbsoluteLayout::AbsoluteLayout(_In_ GLfloat width, _In_ GLfloat height) :
    Layout(width, height)
{
}

AbsoluteLayout::AbsoluteLayout(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    Layout(x, y, width, height)
{
}

AbsoluteLayout::AbsoluteLayout(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    Layout(pt, width, height)
{
}

AbsoluteLayout::AbsoluteLayout(_In_ const Rect &rc) :
    Layout(rc)
{
}



/*
 * UIElement
 */
void AbsoluteLayout::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    for (auto elem : this->_children)
    {
        elem->Draw(offX, offY);
    }
}



/*
 * Layout
 */
void AbsoluteLayout::AddChild(_In_ UIElement *elem)
{
    this->_children.push_back(elem);

    // Submit the insertion animation if there is one
    if (this->_insertionAnimationEngine)
    {
        elem->SubmitAnimation(this->_insertionAnimationEngine->CreateAnimation(elem));
    }
}



/*
 * Children
 */
bool AbsoluteLayout::RemoveChild(_In_ UIElement *elem)
{
    for (auto itr = std::begin(this->_children); itr != std::end(this->_children); ++itr)
    {
        if (*itr == elem)
        {
            // Since the calling function holds a reference to the UIElement child, it is their
            // responsibility to deallocate the object
            this->_children.erase(itr);

            // Submit a removal animation if there is an engine
            if (this->_removalAnimationEngine)
            {
                elem->SubmitOverrideAnimation(this->_removalAnimationEngine->CreateAnimation(elem));
            }

            return true;
        }
    }

    return false;
}

bool AbsoluteLayout::RemoveChild(_In_ size_t index)
{
    if (index >= this->_children.size())
    {
        return false;
    }

    UIElement *elem = this->_children[index];
    this->_children.erase(std::begin(this->_children) + index);

    // Submit a removal animation if there is an engine
    if (this->_removalAnimationEngine)
    {
        elem->SubmitOverrideAnimation(this->_removalAnimationEngine->CreateAnimation(elem));
    }

    // Since the calling function is removing by index, it is our responsibility to deallocate the
    // element on removal
    elem->SubmitPendingAnimation(new DeletionAnimation(elem));

    return true;
}

UIElement *AbsoluteLayout::GetChild(_In_ size_t index) const
{
    if (index >= this->_children.size())
    {
        return nullptr;
    }

    return this->_children[index];
}

size_t AbsoluteLayout::Size(void) const
{
    return this->_children.size();
}
