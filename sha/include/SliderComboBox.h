/*
 * SliderComboBox.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a combo box-like element that, instead of expanding down when clicked, functions much
 * more like the RotaryElement where it can be manipulated left and right. This functionality can
 * most accurately be compared to the Windows Phone time picker.
 */
#pragma once

#include <string>
#include <vector>

#include "AnimationManager.h"
#include "LayoutAwareElement.h"
#include "Rectangle.h"
#include "ShaFont.h"
#include "Slider.h"

namespace vik
{
    namespace graphics
    {
        class SliderComboBox final :
            public LayoutAwareElement
        {
        public:
            SliderComboBox(void);
            SliderComboBox(_In_ GLfloat width, _In_ GLfloat height, _In_ Orientation orientation);
            SliderComboBox(
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height,
                _In_ Orientation orientation);
            SliderComboBox(
                _In_ const Point &pt,
                _In_ GLfloat width,
                _In_ GLfloat height,
                _In_ Orientation orientation);
            SliderComboBox(_In_ const Rect &rc, _In_ Orientation orientation);

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual bool   OnPointerEntered(_In_ Point pt);
            virtual bool   OnPointerExited(void);
            virtual bool   OnPointerMoved(_In_ Point pt);
            virtual bool   OnPointerDown(_In_ Point pt);
            virtual bool   OnPointerUp(_In_ Point pt);

            /* Options */
            void              AddOption(_In_ const std::string &opt);
            bool              RemoveOption(_In_ const std::string &opt);
            bool              RemoveOption(_In_ size_t index);
            int               GetSelectedIndex(void) const;
            const std::string &GetSelectedOption(void) const;
            bool              JumpToOption(_In_ size_t index);
            bool              JumpToOption(_In_ const std::string &opt);

            /* UI Properties */
            void   SetOrientation(_In_ Orientation orientation);
            void   SetFont(_In_ const ShaFont &font);
            void   SetPadding(_In_ const Padding &padding);
            void   SetBorderColor(_In_ const Color &color);
            void   SetBorderThickness(_In_ GLfloat thickness);
            size_t Size(void) const;

            /* Events */
            typedef void (UIElement::*ManipulationEventCallback)(
                _In_ SliderComboBox *sender,
                _In_ int index);
            void OnManipulationStarted(
                _In_ ManipulationEventCallback callback,
                _In_ UIElement *target);
            void OnManipulationCompleted(
                _In_ ManipulationEventCallback callback,
                _In_ UIElement *target);

        protected:

            /* LayoutAwareElement */
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            void    _RefreshLayout(void);
            GLfloat _CalculateWidth(
                _In_ const std::string &str,
                _In_reads_(256) const int *widths) const;
            GLfloat _CalculateWidth(_In_ const std::string &str) const;
            void    _UpdateWidths(void);
            void    _SetElementWidth(_In_ GLfloat width);
            void    _UpdateElementHeight(void);
            void    _ResetOffsets(bool resetY = true, bool resetX = true);
            void    _SnapToIndex(_In_ int index);

            std::vector<std::string> _elements;
            std::vector<GLfloat>     _widths;

            Orientation _orientation;
            ShaFont     _font;
            GLfloat     _offX;
            GLfloat     _offY;
            GLfloat     _elemWidth;
            GLfloat     _elemHeight;
            Padding     _elemPadding;
            shapes::Rectangle _border;

            SliderState _state;
            Point       _manipulatePoint;
            animations::animation_cookie _manipulateCookie;

            ManipulationEventCallback _manipulationStartedCallback;
            ManipulationEventCallback _manipulationCompletedCallback;
            UIElement *_manipulationStartedTarget;
            UIElement *_manipulationCompletedTarget;
        };
    }
}
