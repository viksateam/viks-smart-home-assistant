/*
 * ImageButton.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a button with an image and a label
 */

#include "Drawing.h"
#include "ImageButton.h"
#include "ShaFont.h"
#include "WindowHelpers.h"

using namespace vik::graphics;

#define TEXT_PADDING        (8)

/*
 * Constructor(s)/Destructor
 */
ImageButton::ImageButton(void) :
    ImageButton(Rect(0, 0, 0, 0))
{
}

ImageButton::ImageButton(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
    ImageButton(Rect(x, y, width, height))
{
}

ImageButton::ImageButton(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    ImageButton(Rect(pt, width, height))
{
}

ImageButton::ImageButton(_In_ const Rect &rc) :
    ButtonBase(rc),
    _labelColor(Colors::Transparent),
    _textColor(Colors::Transparent),
    _image(rc),
    _textWidth(0),
    _state(BS_NORMAL)
{
}



/*
 * UIElement
 */
GLvoid ImageButton::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    GLfloat height = (GLfloat)Fonts::NotificationFont.height + 2 * TEXT_PADDING;
    FillRect(offX, this->GetHeight() - height + offY, this->GetWidth(), height, this->_labelColor);

    this->_bkgnd.Draw(offX, offY);
    this->_image.Draw(offX, offY);
    if (!this->_text.empty())
    {
        GLfloat x = (this->GetWidth() - this->_textWidth) / 2;
        WindowDrawText(x + offX, this->GetHeight() - height + TEXT_PADDING + offY,
            this->_text.c_str(), Fonts::NotificationFont, this->_textColor);
    }
}

bool ImageButton::OnPointerEntered(_In_ Point pt)
{
    assert(this->_state == BS_NORMAL || this->_state == BS_EXITED);
    this->_UpdateState((this->_state == BS_NORMAL) ? BS_HOVER : BS_PRESSED);

    return ButtonBase::OnPointerEntered(pt);
}

bool ImageButton::OnPointerExited(void)
{
    this->_UpdateState((this->_state == BS_PRESSED) ? BS_EXITED : BS_NORMAL);

    return ButtonBase::OnPointerExited();
}

bool ImageButton::OnPointerDown(_In_ Point pt)
{
    assert(this->_state == BS_HOVER);
    this->_UpdateState(BS_PRESSED);

    return ButtonBase::OnPointerDown(pt);
}

bool ImageButton::OnPointerUp(_In_ Point pt)
{
    ButtonState state = this->_state;
    assert(this->_state == BS_PRESSED || this->_state == BS_EXITED);
    this->_UpdateState((this->_state == BS_PRESSED) ? BS_HOVER : BS_NORMAL);

    if (state == BS_PRESSED)
    {
        return ButtonBase::OnPointerUp(pt);
    }

    return true;
}

void ImageButton::SetForegroundColor(_In_ const Color &color)
{
    ButtonBase::SetForegroundColor(color);
    this->_bkgnd.SetBorderColor(color);
    this->_image.SetBorderColor(color);
    this->_UpdateState(this->_state);
}



/*
 * Button Properties
 */
void ImageButton::SetText(_In_ const std::string &text)
{
    this->_text = text;
    this->_textWidth = GetStringWidth(Fonts::NotificationFont, text.c_str());

    this->_RefreshLayout();
}

void ImageButton::SetLabelColor(_In_ const Color &color)
{
    this->_labelColor = color;
}

void ImageButton::SetThickness(_In_ const GLfloat thickness)
{
    this->_image.SetBorderThickness(thickness);
    this->_bkgnd.SetBorderThickness(thickness);
}



/*
 * Image
 */
void ImageButton::AddPoint(_In_ const Point &pt)
{
    this->_image.AddPoint(pt);
}



/*
 * LayoutAwareElement
 */
void ImageButton::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    ButtonBase::_OnSizeUpdated(width, height);

    this->_bkgnd.SetRect(Rect(0, 0, width, height));
    this->_image.SetRect(Rect(0, 0, width, height));
}

void ImageButton::_UpdateState(_In_ ButtonState state)
{
    this->_state = state;

    switch (state)
    {
    case BS_NORMAL:
    case BS_EXITED:
        this->_bkgnd.SetBackgroundColor(Colors::Transparent);
        this->_image.SetBorderColor(this->_foregroundColor);
        this->_textColor = this->_foregroundColor;
        break;

    case BS_HOVER:
        this->_bkgnd.SetBackgroundColor(this->_labelColor);
        this->_image.SetBorderColor(this->_foregroundColor);
        this->_textColor = this->_foregroundColor;
        break;

    case BS_PRESSED:
        this->_bkgnd.SetBackgroundColor(this->_foregroundColor);
        this->_image.SetBorderColor(this->_backgroundColor);
        this->_textColor = this->_backgroundColor;
        break;
    }
}

void ImageButton::_RefreshLayout(void)
{

}
