/*
 * ShaSwipeGesture.h
 * 
 * Author(s):
 *      Jerry Lin
 *
 * Detects and reports a swipe gesture based on palm velocity (in mm/s).
 */
#pragma once

#include "Leap.h"

#include "ShaGesture.h"

using namespace Leap;

#define DEFAULT_SWIPE_MIN_DISTANCE  150
#define DEFAULT_SWIPE_MIN_VELOCITY  1200

namespace vik
{
    namespace leap
    {
        enum SwipeDirection
        {
            SD_UP,
            SD_DOWN,
            SD_LEFT,
            SD_RIGHT,
            SD_NONE
        };

        class ShaSwipeGesture final :
            public ShaGesture
        {
        public:
            ShaSwipeGesture(void);

            SwipeDirection      GetDirection(void) const;
            virtual GestureType GetType(void) const;
            bool                IsComplete(void) const;
            bool                SetMinDistance(_In_ float minDistance);
            bool                SetMinVelocity(_In_ float minVelocity);
            virtual void        Update(_In_ const Frame &frame);

        private:

            Vector         _GetDirectionalUnitVector(_In_ SwipeDirection swipeDirection) const;
            SwipeDirection _GetSwipeDirection(_In_ const Vector &direction) const;
            void           _Reset(void);

            float          _distanceTravelled;
            float          _minDistance;
            float          _minVelocity;
            SwipeDirection _swipeDirection;
            Vector         _lastPalmPosition;
            Vector         _lastPalmVelocity;
        };
    }
}
