/*
 * ShaServer.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * The implementation of the server for the Smart Home Assistant
 */
#pragma once

#include "Socket.h"
#include "UDPSocket.h"

#define SHA_PORT                (1992)
#define SHA_BROADCAST_PORT      (12392)

#define SENTINEL_CHARACTER      ('\n')

#define INVALID_REQUEST_COOKIE  (-1)

namespace vik
{
    namespace communication
    {
        typedef long request_cookie;

        class ShaServer
        {
        private:
            /* Cannot initialize a ShaServer object */
            ShaServer(void) {}

        public:

            static bool Connect(void);
            static bool Start(void);
            static bool Close(void);

            static bool SendSingleMessage(
                _In_ const InetAddress &addr,
                _In_ const std::string &message);
            static bool StartConnection(
                _In_ const InetAddress &addr,
                _In_ const std::string &message,
                _In_ request_cookie cookie);

        private:

            static ServerSocket s_socket;
            static UDPSocket    s_broadcastSocket;

#ifdef WIN32
            static HANDLE       s_hServerThread;
            static HANDLE       s_hBroadcastThread;
#endif  /* WIN32 */
        };
    }
}
