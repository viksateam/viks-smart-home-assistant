This is the "main" directory for the Smart Home Assistant. The entry into the application is
(suitably) located in main.cpp in the src\ directory.

For Leap SDK support in Debug mode, copy the "Leapd.dll", "msvcp100d.dll" and "msvcr100d.dll"
files from "..\leap\LeapDeveloperKit\LeapSDK\lib\x86" into the \Debug folder.
