/*
 * DeletionAnimation.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * The existence of this class is mostly a hack. The intent of this animation is to delete the
 * target IAnimatable object when the animation is run. Thus, when this object is submitted, it is
 * assumed that all references to the target object have been removed and it is okay to destroy the
 * object.
 */
#pragma once

#include "Animatable.h"
#include "AnimationBase.h"

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            class DeletionAnimation final :
                public AnimationBase
            {
            public:
                DeletionAnimation(_In_ IAnimatable *target);

                /* IAnimation */
                virtual bool OnAnimationUpdate(_In_ uint64_t timeMicro);
                virtual void OnAnimationCompleted(void);
                virtual void OnAnimationCancelled(void);
            };
        }
    }
}
