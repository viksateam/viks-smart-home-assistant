/*
 * GPIO.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Definition of GPIO ports for the Tiva C Series TM4C123G Launchpad (chip TM4C123GH6PM)
 */
#pragma once

#include <stdint.h>

#include <driverlib/gpio.h>
#include <TM4C123.h>

#include "Bitwise.h"

/*
 * GPIO Offsets; Note: you should use GPIOX->* instead if possible
 */
#define GPIO_DATA_OFFSET        (0x000)         /* GPIO Data */
#define GPIO_DIR_OFFSET         (0x400)         /* GPIO Direction */
#define GPIO_IS_OFFSET          (0x404)         /* GPIO Interrupt State */
#define GPIO_IBE_OFFSET         (0x408)         /* GPIO Interrupt Both Edges */
#define GPIO_IEV_OFFSET         (0x40C)         /* GPIO Interrupt Event */
#define GPIO_IM_OFFSET          (0x410)         /* GPIO Interrupt Mask */
#define GPIO_RIS_OFFSET         (0x414)         /* GPIO Raw Interrupt Status */
#define GPIO_MIS_OFFSET         (0x418)         /* GPIO Masked Interrupt Status */
#define GPIO_ICR_OFFSET         (0x41C)         /* GPIO Interrupt Clear */
#define GPIO_AFSEL_OFFSET       (0x420)         /* GPIO Alternative Function Select */
#define GPIO_DR2R_OFFSET        (0x500)         /* GPIO 2-mA Drive Select */
#define GPIO_DR4R_OFFSET        (0x504)         /* GPIO 4-mA Drive Select */
#define GPIO_DR8R_OFFSET        (0x508)         /* GPIO 8-mA Drive Select */
#define GPIO_ODR_OFFSET         (0x50C)         /* GPIO Open Drain Select */
#define GPIO_PUR_OFFSET         (0x510)         /* GPIO Pull-Up Select */
#define GPIO_PDR_OFFSET         (0x514)         /* GPIO Pull-Down Select */
#define GPIO_SLR_OFFSET         (0x518)         /* GPIO Slew Rate Control Select */
#define GPIO_DEN_OFFSET         (0x51C)         /* GPIO Digital Enable */
#define GPIO_LOCK_OFFSET        (0x520)         /* GPIO Lock */
#define GPIO_CR_OFFSET          (0x524)         /* GPIO Commit */
#define GPIO_AMSEL_OFFSET       (0x528)         /* GPIO Analog Mode Select */
#define GPIO_PCTL_OFFSET        (0x52C)         /* GPIO Port Control */
#define GPIO_ADCCTL_OFFSET      (0x530)         /* GPIO ADC Control */
#define GPIO_DMACTL_OFFSET      (0x534)         /* GPIO DMA Control */
#define GPIO_PeriphlD4_OFFSET   (0xFD0)         /* GPIO Peripheral Identification 4 */
#define GPIO_PeriphlD5_OFFSET   (0xFD4)         /* GPIO Peripheral Identification 5 */
#define GPIO_PeriphlD6_OFFSET   (0xFD8)         /* GPIO Peripheral Identification 6 */
#define GPIO_PeriphlD7_OFFSET   (0xFDC)         /* GPIO Peripheral Identification 7 */
#define GPIO_PeriphlD0_OFFSET   (0xFE0)         /* GPIO Peripheral Identification 0 */
#define GPIO_PeriphlD1_OFFSET   (0xFE4)         /* GPIO Peripheral Identification 1 */
#define GPIO_PeriphlD2_OFFSET   (0xFE8)         /* GPIO Peripheral Identification 2 */
#define GPIO_PeriphlD3_OFFSET   (0xFEC)         /* GPIO Peripheral Identification 3 */
#define GPIO_PCellD0_OFFSET     (0xFF0)         /* GPIO PrimeCell Identification 0 */
#define GPIO_PCellD1_OFFSET     (0xFF4)         /* GPIO PrimeCell Identification 1 */
#define GPIO_PCellD2_OFFSET     (0xFF8)         /* GPIO PrimeCell Identification 2 */
#define GPIO_PCellD3_OFFSET     (0xFFC)         /* GPIO PrimeCell Identification 3 */



/*
 * Defines for easy access to specific bit numbers. Bitwise-OR them together to reference multiple
 * bits at once.
 */
#define GPIO_BIT_0          (0x01)
#define GPIO_BIT_1          (0x02)
#define GPIO_BIT_2          (0x04)
#define GPIO_BIT_3          (0x08)
#define GPIO_BIT_4          (0x10)
#define GPIO_BIT_5          (0x20)
#define GPIO_BIT_6          (0x40)
#define GPIO_BIT_7          (0x80)
#define GPIO_BITS_LO        (0x0F)
#define GPIO_BITS_HI        (0xF0)
#define GPIO_BITS_ALL       (0xFF)



/*
 * Pointers to GPIO ports
 */
#define GPIO_PTR(base, mask)    ((volatile uint32_t *)((base) | ((mask) << 2)))

// GPIO Port A
#define GPIO_A_PTR_(mask)   GPIO_PTR(GPIOA_BASE, mask)
#define GPIO_A_PTR          GPIO_A_PTR_(GPIO_BITS_ALL)
#define GPIO_A0_PTR         GPIO_A_PTR_(GPIO_BIT_0)
#define GPIO_A1_PTR         GPIO_A_PTR_(GPIO_BIT_1)
#define GPIO_A2_PTR         GPIO_A_PTR_(GPIO_BIT_2)
#define GPIO_A3_PTR         GPIO_A_PTR_(GPIO_BIT_3)
#define GPIO_A4_PTR         GPIO_A_PTR_(GPIO_BIT_4)
#define GPIO_A5_PTR         GPIO_A_PTR_(GPIO_BIT_5)
#define GPIO_A6_PTR         GPIO_A_PTR_(GPIO_BIT_6)
#define GPIO_A7_PTR         GPIO_A_PTR_(GPIO_BIT_7)

// GPIO Port B
#define GPIO_B_PTR_(mask)   GPIO_PTR(GPIOB_BASE, mask)
#define GPIO_B_PTR          GPIO_B_PTR_(GPIO_BITS_ALL)
#define GPIO_B0_PTR         GPIO_B_PTR_(GPIO_BIT_0)
#define GPIO_B1_PTR         GPIO_B_PTR_(GPIO_BIT_1)
#define GPIO_B2_PTR         GPIO_B_PTR_(GPIO_BIT_2)
#define GPIO_B3_PTR         GPIO_B_PTR_(GPIO_BIT_3)
#define GPIO_B4_PTR         GPIO_B_PTR_(GPIO_BIT_4)
#define GPIO_B5_PTR         GPIO_B_PTR_(GPIO_BIT_5)
#define GPIO_B6_PTR         GPIO_B_PTR_(GPIO_BIT_6)
#define GPIO_B7_PTR         GPIO_B_PTR_(GPIO_BIT_7)

// GPIO Port C
#define GPIO_C_PTR_(mask)   GPIO_PTR(GPIOC_BASE, mask)
#define GPIO_C_PTR          GPIO_C_PTR_(GPIO_BITS_ALL)
#define GPIO_C0_PTR         GPIO_C_PTR_(GPIO_BIT_0)
#define GPIO_C1_PTR         GPIO_C_PTR_(GPIO_BIT_1)
#define GPIO_C2_PTR         GPIO_C_PTR_(GPIO_BIT_2)
#define GPIO_C3_PTR         GPIO_C_PTR_(GPIO_BIT_3)
#define GPIO_C4_PTR         GPIO_C_PTR_(GPIO_BIT_4)
#define GPIO_C5_PTR         GPIO_C_PTR_(GPIO_BIT_5)
#define GPIO_C6_PTR         GPIO_C_PTR_(GPIO_BIT_6)
#define GPIO_C7_PTR         GPIO_C_PTR_(GPIO_BIT_7)

// GPIO Port D
#define GPIO_D_PTR_(mask)   GPIO_PTR(GPIOD_BASE, mask)
#define GPIO_D_PTR          GPIO_D_PTR_(GPIO_BITS_ALL)
#define GPIO_D0_PTR         GPIO_D_PTR_(GPIO_BIT_0)
#define GPIO_D1_PTR         GPIO_D_PTR_(GPIO_BIT_1)
#define GPIO_D2_PTR         GPIO_D_PTR_(GPIO_BIT_2)
#define GPIO_D3_PTR         GPIO_D_PTR_(GPIO_BIT_3)
#define GPIO_D4_PTR         GPIO_D_PTR_(GPIO_BIT_4)
#define GPIO_D5_PTR         GPIO_D_PTR_(GPIO_BIT_5)
#define GPIO_D6_PTR         GPIO_D_PTR_(GPIO_BIT_6)
#define GPIO_D7_PTR         GPIO_D_PTR_(GPIO_BIT_7)

// GPIO Port E
#define GPIO_E_PTR_(mask)   GPIO_PTR(GPIOE_BASE, mask)
#define GPIO_E_PTR          GPIO_E_PTR_(GPIO_BITS_ALL)
#define GPIO_E0_PTR         GPIO_E_PTR_(GPIO_BIT_0)
#define GPIO_E1_PTR         GPIO_E_PTR_(GPIO_BIT_1)
#define GPIO_E2_PTR         GPIO_E_PTR_(GPIO_BIT_2)
#define GPIO_E3_PTR         GPIO_E_PTR_(GPIO_BIT_3)
#define GPIO_E4_PTR         GPIO_E_PTR_(GPIO_BIT_4)
#define GPIO_E5_PTR         GPIO_E_PTR_(GPIO_BIT_5)
#define GPIO_E6_PTR         GPIO_E_PTR_(GPIO_BIT_6)
#define GPIO_E7_PTR         GPIO_E_PTR_(GPIO_BIT_7)

// GPIO Port F
#define GPIO_F_PTR_(mask)   GPIO_PTR(GPIOF_BASE, mask)
#define GPIO_F_PTR          GPIO_F_PTR_(GPIO_BITS_ALL)
#define GPIO_F0_PTR         GPIO_F_PTR_(GPIO_BIT_0)
#define GPIO_F1_PTR         GPIO_F_PTR_(GPIO_BIT_1)
#define GPIO_F2_PTR         GPIO_F_PTR_(GPIO_BIT_2)
#define GPIO_F3_PTR         GPIO_F_PTR_(GPIO_BIT_3)
#define GPIO_F4_PTR         GPIO_F_PTR_(GPIO_BIT_4)
#define GPIO_F5_PTR         GPIO_F_PTR_(GPIO_BIT_5)
#define GPIO_F6_PTR         GPIO_F_PTR_(GPIO_BIT_6)
#define GPIO_F7_PTR         GPIO_F_PTR_(GPIO_BIT_7)

// GPIO Port G
#define GPIO_G_PTR_(mask)   GPIO_PTR(GPIOG_BASE, mask)
#define GPIO_G_PTR          GPIO_G_PTR_(GPIO_BITS_ALL)
#define GPIO_G0_PTR         GPIO_G_PTR_(GPIO_BIT_0)
#define GPIO_G1_PTR         GPIO_G_PTR_(GPIO_BIT_1)
#define GPIO_G2_PTR         GPIO_G_PTR_(GPIO_BIT_2)
#define GPIO_G3_PTR         GPIO_G_PTR_(GPIO_BIT_3)
#define GPIO_G4_PTR         GPIO_G_PTR_(GPIO_BIT_4)
#define GPIO_G5_PTR         GPIO_G_PTR_(GPIO_BIT_5)
#define GPIO_G6_PTR         GPIO_G_PTR_(GPIO_BIT_6)
#define GPIO_G7_PTR         GPIO_G_PTR_(GPIO_BIT_7)

// GPIO Port H
#define GPIO_H_PTR_(mask)   GPIO_PTR(GPIOH_BASE, mask)
#define GPIO_H_PTR          GPIO_H_PTR_(GPIO_BITS_ALL)
#define GPIO_H0_PTR         GPIO_H_PTR_(GPIO_BIT_0)
#define GPIO_H1_PTR         GPIO_H_PTR_(GPIO_BIT_1)
#define GPIO_H2_PTR         GPIO_H_PTR_(GPIO_BIT_2)
#define GPIO_H3_PTR         GPIO_H_PTR_(GPIO_BIT_3)
#define GPIO_H4_PTR         GPIO_H_PTR_(GPIO_BIT_4)
#define GPIO_H5_PTR         GPIO_H_PTR_(GPIO_BIT_5)
#define GPIO_H6_PTR         GPIO_H_PTR_(GPIO_BIT_6)
#define GPIO_H7_PTR         GPIO_H_PTR_(GPIO_BIT_7)



/*
 * GPIO Registers; Note: you should use GPIOX->* instead if possible
 */
#define GPIO_REGISTER(base, offset)     ((unsigned volatile uint32_t *)(base + offset))

#define GPIO_DATA(base)         GPIO_REGISTER(base, GPIO_DATA_OFFSET)
#define GPIO_DIR(base)          GPIO_REGISTER(base, GPIO_DIR_OFFSET)
#define GPIO_IS(base)           GPIO_REGISTER(base, GPIO_IS_OFFSET)
#define GPIO_IBE(base)          GPIO_REGISTER(base, GPIO_IBE_OFFSET)
#define GPIO_IEV(base)          GPIO_REGISTER(base, GPIO_IEV_OFFSET)
#define GPIO_IM(base)           GPIO_REGISTER(base, GPIO_IM_OFFSET)
#define GPIO_RIS(base)          GPIO_REGISTER(base, GPIO_RIS_OFFSET)
#define GPIO_MIS(base)          GPIO_REGISTER(base, GPIO_MIS_OFFSET)
#define GPIO_ICR(base)          GPIO_REGISTER(base, GPIO_ICR_OFFSET)
#define GPIO_AFSEL(base)        GPIO_REGISTER(base, GPIO_AFSEL_OFFSET)
#define GPIO_DR2R(base)         GPIO_REGISTER(base, GPIO_DR2R_OFFSET)
#define GPIO_DR4R(base)         GPIO_REGISTER(base, GPIO_DR4R_OFFSET)
#define GPIO_DR8R(base)         GPIO_REGISTER(base, GPIO_DR8R_OFFSET)
#define GPIO_ODR(base)          GPIO_REGISTER(base, GPIO_ODR_OFFSET)
#define GPIO_PUR(base)          GPIO_REGISTER(base, GPIO_PUR_OFFSET)
#define GPIO_PDR(base)          GPIO_REGISTER(base, GPIO_PDR_OFFSET)
#define GPIO_SLR(base)          GPIO_REGISTER(base, GPIO_SLR_OFFSET)
#define GPIO_DEN(base)          GPIO_REGISTER(base, GPIO_DEN_OFFSET)
#define GPIO_LOCK(base)         GPIO_REGISTER(base, GPIO_LOCK_OFFSET)
#define GPIO_OCR(base)          GPIO_REGISTER(base, GPIO_OCR_OFFSET)
#define GPIO_AMSEL(base)        GPIO_REGISTER(base, GPIO_AMSEL_OFFSET)
#define GPIO_PCTL(base)         GPIO_REGISTER(base, GPIO_PCTL_OFFSET)
#define GPIO_ADCCTL(base)       GPIO_REGISTER(base, GPIO_ADCCTL_OFFSET)
#define GPIO_DMACTL(base)       GPIO_REGISTER(base, GPIO_DMACTL_OFFSET)
#define GPIO_PeriphlD4(base)    GPIO_REGISTER(base, GPIO_PeriphlD4_OFFSET)
#define GPIO_PeriphlD5(base)    GPIO_REGISTER(base, GPIO_PeriphlD5_OFFSET)
#define GPIO_PeriphlD6(base)    GPIO_REGISTER(base, GPIO_PeriphlD6_OFFSET)
#define GPIO_PeriphlD7(base)    GPIO_REGISTER(base, GPIO_PeriphlD7_OFFSET)
#define GPIO_PeriphlD0(base)    GPIO_REGISTER(base, GPIO_PeriphlD0_OFFSET)
#define GPIO_PeriphlD1(base)    GPIO_REGISTER(base, GPIO_PeriphlD1_OFFSET)
#define GPIO_PeriphlD2(base)    GPIO_REGISTER(base, GPIO_PeriphlD2_OFFSET)
#define GPIO_PeriphlD3(base)    GPIO_REGISTER(base, GPIO_PeriphlD3_OFFSET)
#define GPIO_PCellD0(base)      GPIO_REGISTER(base, GPIO_PCellD0_OFFSET)
#define GPIO_PCellD1(base)      GPIO_REGISTER(base, GPIO_PCellD1_OFFSET)
#define GPIO_PCellD2(base)      GPIO_REGISTER(base, GPIO_PCellD2_OFFSET)
#define GPIO_PCellD3(base)      GPIO_REGISTER(base, GPIO_PCellD3_OFFSET)



/*
 * Gating Conrol Indices
 */
#define GPIO_A_GATING_CTRL_INDEX    (0x01)
#define GPIO_B_GATING_CTRL_INDEX    (0x02)
#define GPIO_C_GATING_CTRL_INDEX    (0x04)
#define GPIO_D_GATING_CTRL_INDEX    (0x08)
#define GPIO_E_GATING_CTRL_INDEX    (0x10)
#define GPIO_F_GATING_CTRL_INDEX    (0x20)



/*
 * GPIO Class. Note that it is not possible to construct a GPIO object, and therefore no way to
 * pass one as an argument. This is by design. If you need to write a function that operates
 * independently of the GPIO port, then I suggest you write that function in any of the following
 * ways:
 *
 * template <typename _GPIO>
 * void Foo(void)
 * {
 *     // Use _GPIO::*
 * }
 * ...
 * Foo<GPIO_A>();
 *
 *
 *
 * template <uintptr_t _Base, uint32_t _GCI>
 * void Foo(void)
 * {
 *     // Use GPIO<_Base, _GCI>::*;
 * }
 * ...
 * Foo<GPIO_A::base, GPIO_A::index>();
 *
 * Or by passing in function pointers
 */
 
#ifdef __cplusplus

enum GPIO_Dir
{
    DIR_INPUT,
    DIR_OUTPUT
};

enum GPIO_DigIO
{
    DEN_DISABLE,
    DEN_ENABLE
};

enum GPIO_IntSense
{
    IS_EDGE,
    IS_LEVEL
};

enum GPIO_IntBothEdges
{
    IBE_BOTH,
    IBE_SINGLE
};

enum GPIO_IntEvent
{
    IEV_LOW, // low level or falling edge
    IEV_HIGH // high level or rising edge
};

template <uintptr_t _Base, uint32_t _GatingCtrlIndex>
class GPIO
{
private:
    /* It does not make sense to construct a GPIO object */
    GPIO(void) {}

public:

    typedef GPIOA_Type *value_type;

    static GPIOA_Type *const value;

    static const uint32_t     index = _GatingCtrlIndex;
    static const uintptr_t    base = _Base;

    /*
     * GPIO Functions
     */
    static void Init(uint32_t mask, GPIO_Dir dir, GPIO_DigIO den)
    {
        // Activate the port
        SET_FLAGS(SYSCTL->RCGCGPIO, index);

        if (dir == DIR_OUTPUT)
        {
            // Set flags to make output
            SET_FLAGS(value->DIR, mask);
        }
        else // DIR_INPUT
        {
            // Clear flags to make input
            CLEAR_FLAGS(value->DIR, mask);
        }

        if (den == DEN_ENABLE)
        {
            // Set flags to enable digital I/O
            SET_FLAGS(value->DEN, mask);
        }
        else // DEN_DISABLE
        {
            // Set flags to disable digital I/O
            CLEAR_FLAGS(value->DEN, mask);
        }
    }
    
    static void InitInterrupt(uint32_t mask, GPIO_IntSense sense, GPIO_IntBothEdges whichEdges,
                              GPIO_IntEvent event, void(*pfnIntHandler)(void))
    {
        // If SW1 and/or SW2 on TM4C123GH6PM is used
        if ((base == GPIOF_BASE) && ((mask & 0x11) > 0))
        {
            // Unlock PortF
            SET_FLAGS(value->LOCK, 0x4C4F434B);
            
            // Enable commit
            SET_FLAGS(value->CR, mask);
            
            // Enable weak pull-up resistor
            SET_FLAGS(value->PUR, mask);
        }
        
        // Initialize GPIO direction and digital I/O
        Init(mask, DIR_INPUT, DEN_ENABLE);
        
        // Disable alternate function
        CLEAR_FLAGS(value->AFSEL, mask);
        
        // Disable analog functionality
        CLEAR_FLAGS(value->AMSEL, mask);
        
        if (sense == IS_LEVEL)
        {
            // Set flags to trigger on level
            SET_FLAGS(value->IS, mask);
        }
        else // IS_EDGE
        {
            // Clear flags to trigger on edge
            CLEAR_FLAGS(value->IS, mask);
        }
        
        if (whichEdges == IBE_SINGLE)
        {
            // Set flags to trigger on both edges
            SET_FLAGS(value->IBE, mask);
        }
        else // IBE_BOTH
        {
            // Clear flags to trigger on single edge
            CLEAR_FLAGS(value->IBE, mask);
        }
        
        if (event == IEV_HIGH)
        {
            // Set flags to trigger on high level or rising edge
            SET_FLAGS(value->IEV, mask);
        }
        else // IEV_LOW
        {
            // Clear flags to trigger on low level or falling edge
            CLEAR_FLAGS(value->IEV, mask);
        }
        
        // Clear interrupt flag
        SET_FLAGS(value->ICR, mask);
        
        // Arm interrupt
        SET_FLAGS(value->IM, mask);
        
        // Enable interrupt in Nested Vector Interrupt Control (NVIC)
        // - doesn't set NVIC priority
        // - modifies the NVIC_VTABLE register
        GPIOIntRegister(base, pfnIntHandler);
    }
    
    static void Set(uint32_t mask)
    {
        SET_FLAGS(*GPIO_PTR(base, mask), mask);
    }
    
    static void Clear(uint32_t mask)
    {
        CLEAR_FLAGS(*GPIO_PTR(base, mask), mask);
    }
    
    static void Assign(uint32_t mask, uint32_t value)
    {
        SET_BITS(*GPIO_PTR(base, mask), mask, value);
    }
    
    static void Toggle(uint32_t mask)
    {
        TOGGLE_FLAGS(*GPIO_PTR(base, mask), mask);
    }
    
    static uint32_t Get(uint32_t mask)
    {
        return IS_ANY_FLAG_SET(value->DATA, mask);
    }
    
    static uint32_t IsSet(uint32_t mask)
    {
        return IS_ANY_FLAG_SET(value->RIS, mask);
    }
    
    static void AcknowledgeInt(uint32_t mask)
    {
        SET_FLAGS(value->ICR, mask);
    }
    
    static void DisableInt(uint32_t mask)
    {
        CLEAR_FLAGS(value->IM, mask);
    }
    
    static void EnableInt(uint32_t mask)
    {
        SET_FLAGS(value->IM, mask);
    }
};

/*
 * Definitions for GPIO<...>::value
 */
template <uintptr_t _Base, uint32_t _GCI>
GPIOA_Type *const GPIO<_Base, _GCI>::value = (GPIOA_Type *)_Base;

/*
 * GPIO Typedefs
 */
typedef GPIO<GPIOA_BASE, GPIO_A_GATING_CTRL_INDEX> GPIO_A;
typedef GPIO<GPIOB_BASE, GPIO_B_GATING_CTRL_INDEX> GPIO_B;
typedef GPIO<GPIOC_BASE, GPIO_C_GATING_CTRL_INDEX> GPIO_C;
typedef GPIO<GPIOD_BASE, GPIO_D_GATING_CTRL_INDEX> GPIO_D;
typedef GPIO<GPIOE_BASE, GPIO_E_GATING_CTRL_INDEX> GPIO_E;
typedef GPIO<GPIOF_BASE, GPIO_F_GATING_CTRL_INDEX> GPIO_F;

#endif  /* __cplusplus */
