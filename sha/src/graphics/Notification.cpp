/*
 * Notification.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a notification that can be displayed by the Smart Home Assistant. Currently,
 * notifications are simple rectangles with text wrapping across multiple lines. The amount of text
 * that gets displayed is directly proportional to the size of the Notification, and thus clipping
 * of text may occur if there is too much text, or if the Notification is too small. In the event
 * that text does need to be clipped, the text is terminated by an ellipsis.
 */

#include "Drawing.h"
#include "Notification.h"
#include "ShaFont.h"
#include "WindowHelpers.h"

#define TOP_PADDING         (10)
#define SIDE_PADDING        (20)
#define LINE_HEIGHT         (20)

using namespace vik::graphics;

/*
 * Constructors/Destructor
 */
Notification::Notification(_In_ const std::string &message) :
    _message(message)
{
    this->_OnSizeChanged();
}

Notification::Notification(
    _In_ const std::string &message,
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    UIElement(x, y, width, height),
    _message(message)
{
    this->_OnSizeChanged();
}

Notification::Notification(
    _In_ const std::string message,
    _In_ const Point &pt,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    UIElement(pt, width, height),
    _message(message)
{
    this->_OnSizeChanged();
}

Notification::Notification(_In_ const std::string &message, _In_ const Rect &rc) :
    UIElement(rc),
    _message(message)
{
    this->_OnSizeChanged();
}



/*
 * UIElement
 */
void Notification::SetWidth(_In_ GLfloat width)
{
    UIElement::SetWidth(width);
    this->_OnSizeChanged();
}

void Notification::SetHeight(_In_ GLfloat height)
{
    UIElement::SetHeight(height);
    this->_OnSizeChanged();
}

void Notification::SetRect(_In_ const Rect &rc)
{
    UIElement::SetRect(rc);
    this->_OnSizeChanged();
}

GLfloat Notification::IncreaseWidth(_In_ GLfloat deltaWidth)
{
    GLfloat result = UIElement::IncreaseWidth(deltaWidth);
    this->_OnSizeChanged();

    return result;
}

GLfloat Notification::IncreaseHeight(_In_ GLfloat deltaHeight)
{
    GLfloat result = UIElement::IncreaseHeight(deltaHeight);
    this->_OnSizeChanged();

    return result;
}

GLvoid Notification::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    // First, draw the rectangle
    FillRect(offX, offY, this->GetWidth(), this->GetHeight(), this->_backgroundColor);

    // Now draw the message
    offX += SIDE_PADDING;
    offY += TOP_PADDING;
    for (auto &str : this->_displayedText)
    {
        WindowDrawText(offX, offY, str.c_str(), Fonts::NotificationFont, this->_foregroundColor);
        offY += LINE_HEIGHT;
    }
}



/*
 * Private Functions
 */
void Notification::_OnSizeChanged(void)
{
    this->_displayedText.clear();
    GLfloat width = this->GetWidth();
    GLfloat height = this->GetHeight();

    width -= 2 * SIDE_PADDING;
    height -= 2 * TOP_PADDING;
    size_t lines = (size_t)(height / LINE_HEIGHT);

    std::string msg(this->_message);
    int charWidths[256];

    if (vik::GetCharacterWidths(Fonts::NotificationFont, charWidths))
    {
        for (size_t i = 0; (i < lines) && !msg.empty(); i++)
        {
            size_t lineLength = 0;
            GLfloat lineWidth = width;

            // Move past all initial whitespace
            for (; (lineLength < msg.size()) && (isspace((int)msg[lineLength])); lineLength++);
            msg.erase(std::begin(msg), std::begin(msg) + lineLength);

            // Calculate the length of the line
            for (lineLength = 0; (lineLength < msg.size()) &&
                ((lineWidth - charWidths[lineLength] >= 0)); lineLength++)
            {
                if (msg[lineLength] == '\n')
                {
                    // Special case: a new line is desired
                    break;
                }

                lineWidth -= charWidths[msg[lineLength]];
            }

            // There are now two cases for us to consider:
            //      1. Cut line off at the last whitespace
            //      2. There is only one word on the line and it needs to wrap
            size_t maxLineLength = lineLength;
            if (lineLength < msg.size() && !isspace((int)msg[lineLength]))
            {
                while ((lineLength > 0) && !isspace((int)msg[lineLength - 1]))
                {
                    lineLength--;
                }
            }

            if (lineLength == 0)
            {
                // Case 2
                lineLength = maxLineLength;
            }

            std::string line(std::begin(msg), std::begin(msg) + lineLength);
            msg.erase(std::begin(msg), std::begin(msg) + lineLength);
            this->_displayedText.push_back(line);
        }

        // Need to put an ellipsis on the end of the last line if it is too long
        if (!msg.empty() && this->_displayedText.size() > 0)
        {
            std::string &lastString = this->_displayedText[this->_displayedText.size() - 1];
            lastString[lastString.size() - 1] = '.';
            lastString[lastString.size() - 2] = '.';
            lastString[lastString.size() - 3] = '.';
        }
    }
}
