/*
 * Slider.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a slider that can take a variable width, height, orientation, and range that is
 * manipulatable by the user.
 */
#pragma once

#include "LayoutAwareElement.h"
#include "Rectangle.h"

namespace vik
{
    namespace graphics
    {
        enum SliderState
        {
            SS_NORMAL,
            SS_HOVER,
            SS_MANIPULATING
        };

        class Slider final :
            public LayoutAwareElement
        {
            class SliderAnimation :
                public animations::AnimationBase
            {
            public:
                SliderAnimation(_In_ Slider *target, _In_ GLfloat end, _In_ GLfloat vel);

                /* IAnimation */
                virtual void OnAnimationBegin(void);
                virtual bool OnAnimationUpdate(_In_ uint64_t timeMicro);
                virtual void OnAnimationCompleted(void);

                Slider  *target;
                GLfloat end;
                GLfloat vel;
                GLfloat delta;
                GLfloat duration;
            };

        public:
            Slider(void);
            Slider(
                _In_ GLfloat width,
                _In_ GLfloat height,
                _In_ Orientation orientation,
                _In_ GLfloat min,
                _In_ GLfloat max,
                _In_ GLfloat initVal);
            Slider(
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height,
                _In_ Orientation orientation,
                _In_ GLfloat min,
                _In_ GLfloat max,
                _In_ GLfloat initVal);
            Slider(
                _In_ const Point &pt,
                _In_ GLfloat width,
                _In_ GLfloat height,
                _In_ Orientation orientation,
                _In_ GLfloat min,
                _In_ GLfloat max,
                _In_ GLfloat initVal);
            Slider(
                _In_ const Rect &rc,
                _In_ Orientation orientation,
                _In_ GLfloat min,
                _In_ GLfloat max,
                _In_ GLfloat initVal);

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual bool   OnPointerEntered(_In_ Point pt);
            virtual bool   OnPointerExited(void);
            virtual bool   OnPointerMoved(_In_ Point pt);
            virtual bool   OnPointerDown(_In_ Point pt);
            virtual bool   OnPointerUp(_In_ Point pt);

            /* UI Properties */
            void SetLoColor(_In_ const Color &color);
            void SetHiColor(_In_ const Color &color);
            void SetHoverColor(_In_ const Color &color);
            void SetBorderColor(_In_ const Color &color);
            void SetBorderThickness(_In_ GLfloat thickness);
            void SetThumbBorderThickness(_In_ GLfloat thickness);
            void SetThumbSize(_In_ GLfloat thumbSize);

            /* Manipulation */
            typedef void (UIElement::*ManipulationEventCallback)(
                _In_ Slider *sender,
                _In_ GLfloat val);
            void OnManipulationStarted(
                _In_ ManipulationEventCallback callback,
                _In_ UIElement *target);
            void OnManipulationCompleted(
                _In_ ManipulationEventCallback callback,
                _In_ UIElement *target);
            bool IsManipulating(void) const;
            void SetValue(_In_ GLfloat value);
            GLfloat GetValue(void) const;

        private:

            /* LayoutAwareElement */
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            void    _RefreshLayout(void);
            GLfloat _SizeRatio(void) const;
            void    _SnapToValue(_In_ GLfloat value);

            Orientation _orientation;
            GLfloat     _loValue;
            GLfloat     _hiValue;
            GLfloat     _value;
            GLfloat     _stopValue;
            SliderState _state;

            shapes::Rectangle _loRect;
            shapes::Rectangle _hiRect;
            shapes::Rectangle _thumb;
            GLfloat           _thumbSize;
            Color             _hoverColor;

            animations::animation_cookie _valueCookie;

            ManipulationEventCallback _manipulationStartedCallback;
            ManipulationEventCallback _manipulationCompletedCallback;
            UIElement *_manipulationStartedTarget;
            UIElement *_manipulationCompletedTarget;
        };
    }
}
