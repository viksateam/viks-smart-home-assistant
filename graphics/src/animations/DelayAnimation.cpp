/*
 * DelayAnimation.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Used to represent a delay between two separate animations. The duration of the delay is
 * specified in units of microseconds.
 */

#include "DelayAnimation.h"

using namespace vik::graphics::animations;

/*
 * Constructor(s)/Destructor
 */
DelayAnimation::DelayAnimation(_In_ IAnimatable *target, _In_ int64_t durationMicro) :
    AnimationBase(target),
    _duration(durationMicro)
{
}



/*
 * IAnimation
 */
bool DelayAnimation::OnAnimationUpdate(_In_ uint64_t timeMicro)
{
    this->_duration -= timeMicro;
    return this->_duration <= 0;
}
