/*
 * Polygon.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a UIElement that can take the shape of any polygon (identified by a sequence of
 * points).
 */
#pragma once

#include <algorithm>
#include <vector>

#include "Shape.h"

namespace vik
{
    namespace graphics
    {
        namespace shapes
        {
            class Polygon final :
                public Shape
            {
            public:
                Polygon(void);
                Polygon(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
                Polygon(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
                Polygon(_In_ const Rect &rc);

                /* IAnimatable */
                virtual void    SetWidth(_In_ GLfloat width);
                virtual void    SetHeight(_In_ GLfloat height);
                virtual void    SetRect(_In_ const Rect &rc);
                virtual GLfloat IncreaseWidth(_In_ GLfloat deltaWidth);
                virtual GLfloat IncreaseHeight(_In_ GLfloat deltaHeight);

                /* UIElement */
                virtual GLvoid  Draw(_In_ GLfloat offX, _In_ GLfloat offY);

                /* Shape */
                //virtual void    SetBorderThickness(_In_ GLfloat thickness);

                /* Polygon Points */
                void AddPoint(_In_ GLfloat x, _In_ GLfloat y);
                void AddPoint(_In_ const Point &pt);

                template <typename _It>
                void AddPoints(_In_ const _It &begin, _In_ const _It &end)
                {
                    using _Type = decltype(*begin);
                    std::for_each(begin, end, [&](_In_ const _Type &pt)
                    {
                        this->_points.push_back(pt);
                    });
                }

                size_t Size(void) const;
                void   Clear(void);

                typedef std::vector<Point>::iterator iterator;
                iterator begin(void) { return std::begin(this->_points); }
                iterator end(void) { return std::end(this->_points); }

            private:

                void _OnResize(_In_ GLfloat prevWidth, _In_ GLfloat prevHeight);

                std::vector<Point> _points;
            };
        }
    }
}
