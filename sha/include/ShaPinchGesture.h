/*
* ShaPinchGesture.h
*
* Author(s):
*      Jerry Lin
*
* Detects right-handed, thumb-to-pointer finger pinch and release motions. The other three fingers
* must be tucked away and not shown.
*/
#pragma once

#include "ShaGesture.h"

namespace vik
{
    namespace leap
    {
        class ShaPinchGesture final :
            public ShaGesture
        {
        public:
            ShaPinchGesture(void);

            Vector              GetPinchPosition(void) const;
            virtual GestureType GetType(void) const;
            bool                IsPinched(void) const;
            bool                IsReleased(void) const;
            virtual void        Update(_In_ const Frame &frame);

        private:

            Vector _GetEstimatedPinchPosition(_In_ const Vector &pos1, _In_ const Vector &pos2) const;
            Vector _GetLastPinchPosition(_In_ const Hand &hand) const;
            int    _GetSecondFingerIndex(_In_ const FingerList &fingers) const;
            void   _UpdateHandInformation(
                _In_ const Hand &hand,
                _In_ const Vector &pinchPosition);
            void   _Reset(void);

            bool   _isReleased;
            float  _lastDistance;
            Vector _lastPalmPosition;
            Vector _palmToPinchPosition;
        };
    }
}
