/*
 * StringHelpers.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines useful functions for string handling and manipulation. Most of these are simple wrappers
 * around common functions defined by the STL
 */
#pragma once

#include <iterator>
#include <string>

#include "sha.h"

namespace vik
{
    /* Conversion between different types of strings */
    template <typename _From, typename _To>
    _To convert_string(_In_ const _From &str)
    {
        return _To(std::begin(str), std::end(str));
    }

    /* Gets the length of a string type */
    template <typename _Ty>
    size_t string_length(_In_ const _Ty &str)
    {
        return std::char_traits<_Ty>::length(str);
    }
}
