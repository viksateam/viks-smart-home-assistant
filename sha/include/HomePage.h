/*
 * HomePage.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents the home page that is visible on the far left of Vik's Smart Home Assistant and is
 * always present in the Rotary Layout.
 */
#pragma once

#include "Button.h"
#include "ImageButton.h"
#include "Layout.h"

namespace vik
{
    namespace graphics
    {
        extern const std::vector<Point> itunesPoints;

        class HomePage final :
            public Layout
        {
        public:
            HomePage(void);
            HomePage(_In_ GLfloat width, _In_ GLfloat height);
            HomePage(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
            HomePage(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            HomePage(_In_ const Rect &rc);
            virtual ~HomePage(void);

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual bool   OnPointerMoved(_In_ Point pt);

        protected:

            /* LayoutAwareElement */
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            void _OnItunesButtonPressed(_In_ ButtonBase *sender, _In_ const Point &pt);
            void _RefreshLayout(void);

            std::vector<ImageButton *> _buttons;
        };
    }
}
