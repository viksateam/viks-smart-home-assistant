/*
 * UDPSocket.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Useful class for dealing with UDP sockets
 */
#pragma once

#include <string>
#include <WinSock2.h>

#include "InetAddress.h"

namespace vik
{
    namespace communication
    {
        enum UDPSocketState
        {
            UDPSOCKET_CLOSED,
            UDPSOCKET_OPEN,
            UDPSOCKET_BOUND,
        };

        class UDPSocket
        {
        public:
            UDPSocket(void);
            UDPSocket(_In_ USHORT usPort);

            bool IsConnected(void) const;
            int  GetError(void) const;

            bool Open(_In_ USHORT usPort);
            bool Close(void);

            _Success_(return >= 0)
            int Send(
                _In_ const InetAddress &inetAddr,
                _In_reads_(nLen) const void *pBuffer,
                _In_ int nLen);
            _Success_(return >= 0)
            int Broadcast(_In_ USHORT usPort, _In_reads_(nLen) const void *pBuffer, _In_ int nLen);
            _Success_(return >= 0)
            int Receive(
                _Out_writes_bytes_to_(nLen, return) void *pBuffer,
                _In_ int nLen,
                _Out_opt_ InetAddress *pInetAddr);

        private:

            bool _OpenSocket(void);

            // Socket information
            SOCKET _socket;
            int    _nErrorCode;

            UDPSocketState _state;
        };
    }
}
