/*
 * ShaConnectionManager.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Mainly used to map station id's to their ShaStation object
 */

#include <cassert>

#include "ShaConnectionManager.h"

using namespace vik;

std::map<std::string, ShaStation *> ShaConnectionManager::s_map;

void ShaConnectionManager::Destroy(void)
{
    for (auto &pair : s_map)
    {
        delete pair.second;
    }

    s_map.clear();
}

/*
 * Modification/Indexing
 */
bool ShaConnectionManager::HasStation(_In_ const std::string &id)
{
    auto it = s_map.find(id);
    return it != std::end(s_map);
}

ShaStation *ShaConnectionManager::GetStation(_In_ const std::string &id)
{
    auto it = s_map.find(id);

    if (it == std::end(s_map))
    {
        return nullptr;
    }

    return it->second;
}

bool ShaConnectionManager::AddStation(_In_ ShaStation *station)
{
    // AddStation fails if the station already exists
    return s_map.insert(std::make_pair(station->Id(), station)).second;
}

bool ShaConnectionManager::UpdateStation(_In_ ShaStation *station)
{
    // Function fails if station already exists
    if (!HasStation(station->Id()))
    {
        return false;
    }

    // We assume that all references to the station in question have been removed.
    auto &pos = s_map[station->Id()];
    delete pos;
    pos = station;
    return true;
}

bool ShaConnectionManager::RemoveStation(_In_ const std::string &id)
{
    if (!HasStation(id))
    {
        return false;
    }

    auto station = s_map[id];
    size_t erased = s_map.erase(id);

    assert(erased == 1);
    delete station;

    return erased == 1;
}
