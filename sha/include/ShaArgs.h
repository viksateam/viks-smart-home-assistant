/*
 * ShaArgs.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Convenience class for parsing command-line options and setting variables to be used later.
 * Currently, there are no command-line options.
 */
#pragma once

#include "sha.h"

namespace vik
{
    class ShaArgs
    {
    public:
        ShaArgs(_In_ int argc, _In_ char *argv[]);

        bool IsFullScreen(void) const;
        bool ShowDebugInfo(void) const;

    private:

        bool _fullScreen;
        bool _debugInfo;
    };
}
