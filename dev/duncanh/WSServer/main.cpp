/*
 * main.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * A simple test server using WinSock
 */

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>

#include "Socket.h"

using namespace vik::communication;

static DWORD WINAPI ServerThreadProc(_In_ LPVOID lpParam);
static DWORD WINAPI ConnectionThreadProc(_In_ LPVOID lpParam);

static ServerSocket serverSocket;
static volatile bool g_fRunning = true;

int main(void)
{
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData))
    {
        std::cout << "ERROR: Could not initialize WinSock (" << WSAGetLastError() << ")" << std::endl;
        return 1;
    }

    // Get the desired port number
    USHORT usPort;
    std::cout << "Enter server port number: ";
    std::cin >> usPort;

    // Start the server thread and wait for it to terminate
    if (serverSocket.Start(usPort))
    {
        HANDLE hServerThread = CreateThread(NULL, 0, ServerThreadProc, NULL, 0, NULL);
        if (hServerThread)
        {
            std::string line;

            while (line != "exit" && g_fRunning)
            {
                std::cin >> line;
            }

            // Wait for server to terminate
            g_fRunning = false;
            serverSocket.Close();
            if (WaitForSingleObject(hServerThread, INFINITE) != WAIT_OBJECT_0)
            {
                std::cout << "ERROR: Could not close server thread" << std::endl;
            }
        }
        else
        {
            std::cout << "ERROR: Could not start server thread" << std::endl;
            serverSocket.Close();
            WSACleanup();
            return 1;
        }
    }
    else
    {
        std::cout << "ERROR: Failed to start server (" << serverSocket.GetError() << ")" << std::endl;
    }

    if (WSACleanup())
    {
        std::cout << "ERROR: Could not close WinSock (" << WSAGetLastError() << ")" << std::endl;
    }
}

DWORD WINAPI ServerThreadProc(_In_ LPVOID lpParam)
{
    UNREFERENCED_PARAMETER(lpParam);

    while (g_fRunning)
    {
        Socket newConnection = serverSocket.Accept();
        if (newConnection.IsConnected())
        {
            std::cout << "New connection from " << newConnection.GetIpAddr() << std::endl;

            // Start a new thread for the connection
            Socket *pSock = new Socket(newConnection);
            HANDLE hClient = CreateThread(NULL, 0, ConnectionThreadProc, pSock, 0, NULL);
            if (!hClient)
            {
                std::cout << "ERROR: Could not create connection thread" << std::endl;
                newConnection.Close();
                delete pSock;
            }
            else
            {
                CloseHandle(hClient);
            }
        }
    }

    // Ignore failures as the main thread closing the socket could be the reason for the failure
    serverSocket.Close();
    return 0;
}

static DWORD WINAPI ConnectionThreadProc(_In_ LPVOID lpParam)
{
    Socket sock = *(Socket *)lpParam;
    std::stringstream stream;
    delete lpParam;

    // While the client is active, continuously field their requests
    while (sock.IsConnected())
    {
        char rgszBuffer[1024];
        int nLen = sock.Receive(rgszBuffer, sizeof(rgszBuffer)-1);

        if (nLen >= 0)
        {
            rgszBuffer[nLen] = '\0';
            std::cout << rgszBuffer;
            stream << rgszBuffer;

            // TODO: Check for commands

            sock.Send(rgszBuffer, strlen(rgszBuffer));
        }
        else
        {
            break;
        }
    }

    std::cout << "Connection from " << sock.GetIpAddr() << " closed" << std::endl;
    if (!sock.Close())
    {
        std::cout << "ERROR: Could not close socket for client " << sock.GetIpAddr() << std::endl;
    }
    return 0;
}
