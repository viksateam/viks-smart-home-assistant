/*
 * LightElement.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * The UIElement that graphically represents a Light ShaStation object. When the Light class is
 * updated (i.e. a new supported feature is added), this class needs to get updated in order for
 * the addition to make an effect graphically.
 *
 * The LightElement has two main parts: A "picture" on the left and a status/button on the right.
 * There is a constant 100px padding at the top for the appliance name, a constant 20px padding at
 * the bottom, and a constant 50px margin on both the left and right. The remainder of the space is
 * split into two parts. The right side is dedicated to the status and on/off button. This space
 * has a minimum width of 200px, though if the total width of the whole space is greater than
 * 2 * 200px, the right side takes up half of the whole space. The remainder of the space is
 * allocated to the lamp picture.
 */

#include "LightElement.h"
#include "ShaColors.h"
#include "ShaFont.h"
#include "WindowHelpers.h"

using namespace vik;
using namespace vik::graphics;

#define MIN_STATUS_WIDTH        (300)
#define STATUS_STRING           "Status: "

#define BUTTON_WIDTH            (300)
#define BUTTON_HEIGHT           (125)

#define SHADE_TOP               (1.0f / 6.0f)
#define SHADE_BOTTOM            (1.0f / 3.0f)
#define SHADE_TOP_WIDTH         (1.0f / 4.0f)
#define SHADE_BOTTOM_WIDTH      (1.0f / 2.0f)
#define SHADE_TOP_LEFT          (0.5f - SHADE_TOP_WIDTH / 2)
#define SHADE_TOP_RIGHT         (0.5f + SHADE_TOP_WIDTH / 2)
#define SHADE_BOTTOM_LEFT       (0.5f - SHADE_BOTTOM_WIDTH / 2)
#define SHADE_BOTTOM_RIGHT      (0.5f + SHADE_BOTTOM_WIDTH / 2)

#define POLE_WIDTH              (1.0f / 50.0f)
#define POLE_HEIGHT             (1.0f / 2.0f)
#define BASE_TOP_WIDTH          (5.0f / 32.0f)
#define BASE_BOTTOM_WIDTH       (3.0f / 16.0f)
#define POLE_TOP                (SHADE_BOTTOM)
#define POLE_BOTTOM             (POLE_TOP + POLE_HEIGHT)
#define POLE_LEFT               (0.5f - POLE_WIDTH / 2)
#define POLE_RIGHT              (0.5f + POLE_WIDTH / 2)
#define BASE_TOP                (POLE_BOTTOM)
#define BASE_BOTTOM             (BASE_TOP + POLE_WIDTH)
#define BASE_TOP_LEFT           (0.5f - BASE_TOP_WIDTH / 2)
#define BASE_TOP_RIGHT          (0.5f + BASE_TOP_WIDTH / 2)
#define BASE_BOTTOM_LEFT        (0.5f - BASE_BOTTOM_WIDTH / 2)
#define BASE_BOTTOM_RIGHT       (0.5f + BASE_BOTTOM_WIDTH / 2)

GLfloat LightElement::s_statusWidth = 0;

/*
 * Constructor(s)/Destructor
 */
LightElement::LightElement(_In_ Light *target) :
    LightElement(target, Rect(0, 0, 0, 0))
{
}

LightElement::LightElement(
    _In_ Light *target,
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    LightElement(target, Rect(x, y, width, height))
{
}

LightElement::LightElement(
    _In_ Light *target,
    _In_ const Point &pt,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    LightElement(target, Rect(pt, width, height))
{
}

LightElement::LightElement(_In_ Light *target, _In_ const Rect &rc) :
    Layout(rc),
    _lampShade(0, 0, 1, 1),
    _lampBase(0, 0, 1, 1),
    _button(0, 0, BUTTON_WIDTH, BUTTON_HEIGHT),
    _target(target)
{
    // Only calculate the width once
    if (s_statusWidth == 0)
    {
        s_statusWidth = GetStringWidth(Fonts::ApplianceFont, STATUS_STRING);
    }

    // Initialize the button
    this->_button.SetPreferredColors(ShaColors::ForegroundColor, ShaColors::BackgroundColor,
        ShaColors::ButtonHoverColor, ShaColors::ForegroundColor);
    this->_button.OnButtonUp(this,
        (ButtonBase::ButtonEventCallback)&LightElement::_OnButtonPressed);

    // Initialize lamp "picture"
    static const std::vector<Point> lampShadePoints
    {
        Point(SHADE_TOP_LEFT, SHADE_TOP), Point(SHADE_BOTTOM_LEFT, SHADE_BOTTOM),
        Point(SHADE_BOTTOM_RIGHT, SHADE_BOTTOM), Point(SHADE_TOP_RIGHT, SHADE_TOP)
    };
    this->_lampShade.AddPoints(std::begin(lampShadePoints), std::end(lampShadePoints));
    this->_lampShade.SetBorderColor(ShaColors::ForegroundColor);
    this->_lampShade.SetBorderThickness(3);

    static const std::vector<Point> lampBasePoints
    {
        Point(POLE_LEFT, POLE_TOP), Point(POLE_LEFT, POLE_BOTTOM),
        Point(BASE_TOP_LEFT, BASE_TOP), Point(BASE_BOTTOM_LEFT, BASE_BOTTOM),
        Point(BASE_BOTTOM_RIGHT, BASE_BOTTOM), Point(BASE_TOP_RIGHT, BASE_TOP),
        Point(POLE_RIGHT, POLE_BOTTOM), Point(POLE_RIGHT, POLE_TOP)
    };
    this->_lampBase.AddPoints(std::begin(lampBasePoints), std::end(lampBasePoints));
    this->_lampBase.SetBorderColor(ShaColors::ForegroundColor);
    this->_lampBase.SetBorderThickness(3);

    this->OnStatusChanged(this->_target->GetStatus());
    this->_OnSizeUpdated(rc.width, rc.height);
}



/*
 * UIElement
 */
GLvoid LightElement::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    WindowDrawText(offX + TITLE_X_OFFSET, offY + TITLE_Y_OFFSET, this->_target->Name().c_str(),
        Fonts::TitleFont, ShaColors::ForegroundColor);

    // Draw the status and button
    GLfloat x = offX + this->GetWidth() - this->_statusWidth +
        (BUTTON_WIDTH - s_statusWidth * 1.6f) / 2;
    GLfloat y = offY + TOP_PADDING - Fonts::ApplianceFont.height - 50 +
        (this->GetHeight() - TOP_PADDING - BOTTOM_PADDING - BUTTON_HEIGHT) / 2;

    WindowDrawText(x, y, STATUS_STRING, Fonts::ApplianceFont, ShaColors::ForegroundColor);
    WindowDrawText(x + s_statusWidth, y, this->_statusString.c_str(), Fonts::ApplianceFont,
        this->_statusColor);

    // Draw the lamp "picture"
    this->_lampBase.Draw(offX, offY);
    this->_lampShade.Draw(offX, offY);

    // Finally, draw the button
    this->_button.Draw(offX, offY);
}

bool LightElement::OnPointerMoved(_In_ Point pt)
{
    if (Layout::OnPointerMoved(pt))
    {
        return true;
    }

    // Now, check our children
    if (this->_button.GetRect().Contains(pt) &&
        this->_button.OnPointerEntered(pt - this->_button.GetLocation()))
    {
        this->_pointerTarget = &this->_button;
        return true;
    }

    return false;
}



void LightElement::OnStatusChanged(_In_ LightStatus status)
{
    this->_status = status;

    switch (status)
    {
    case LIGHT_ON:
        this->_statusString = "ON";
        this->_statusColor = Colors::Green;
        this->_button.SetText("Turn Off");
        this->_lampShade.SetBackgroundColor(Colors::Yellow);
        break;

    case LIGHT_OFF:
        this->_statusString = "OFF";
        this->_statusColor = Colors::Red;
        this->_button.SetText("Turn On");
        this->_lampShade.SetBackgroundColor(Colors::Transparent);
        break;

    default:
        // Not reached
        abort();
        break;
    }
}



/*
 * LayoutAwareElement
 */
void LightElement::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    LayoutAwareElement::_OnSizeUpdated(width, height);

    GLfloat mainWidth = width - 2 * SIDE_PADDING;
    GLfloat mainHeight = height - (TOP_PADDING + BOTTOM_PADDING);

    this->_statusWidth = std::max(mainWidth / 2, (GLfloat)MIN_STATUS_WIDTH);
    GLfloat lampWidth = mainWidth - this->_statusWidth;

    // Handle the "picture" size/location
    if (mainHeight < 0 || lampWidth < 0)
    {
        // Prevent setting rectangle widths to zero
        this->_lampBase.SetRect(Rect(0, 0, 1, 1));
        this->_lampShade.SetRect(Rect(0, 0, 1, 1));
    }
    else
    {
        GLfloat dim = std::min(mainHeight, lampWidth);
        GLfloat x = SIDE_PADDING + (lampWidth - dim) / 2;
        GLfloat y = TOP_PADDING + (mainHeight - dim) / 2;

        this->_lampBase.SetRect(Rect(x, y, dim, dim));
        this->_lampShade.SetRect(Rect(x, y, dim, dim));
    }

    // Handle the button size/location
    GLfloat x = width - this->_statusWidth;
    GLfloat y = TOP_PADDING + (mainHeight - BUTTON_HEIGHT) / 2;
    this->_button.SetLocation(Point(x, y));
}



bool LightElement::_OnButtonPressed(_In_ ButtonBase *sender, _In_ const Point &pt)
{
    (void)sender;
    (void)pt;

    LightStatus status = (this->_status == LIGHT_ON) ? LIGHT_OFF : LIGHT_ON;

    this->_target->PostStatusRequest(status);
    return true;
}
