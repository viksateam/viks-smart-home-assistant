/*
 * Bitwise.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Useful macros/defines for performing common bitwise operations
 */
#pragma once

#define SET_FLAGS(value, flags)             (value |= flags)
#define SET_BITS(value, mask, flags)        (value = (value & ~mask) | flags)
#define CLEAR_FLAGS(value, flags)           (value &= (~flags))
#define TOGGLE_FLAGS(value, flags)          (value ^= flags)

#define IS_ANY_FLAG_SET(value, mask)        (value & mask)
#define ARE_ALL_FLAGS_SET(value, mask)      ((value & mask) == mask)

#define BYTE_0(value)   (value & 0x000000FF)
#define BYTE_1(value)   (value & 0x0000FF00)
#define BYTE_2(value)   (value & 0x00FF0000)
#define BYTE_3(value)   (value & 0xFF000000)
