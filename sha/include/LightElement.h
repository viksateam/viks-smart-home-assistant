/*
 * LightElement.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * The UIElement that graphically represents a Light ShaStation object. When the Light class is
 * updated (i.e. a new supported feature is added), this class needs to get updated in order for
 * the addition to make an effect graphically.
 *
 * The LightElement has two main parts: A "picture" on the left and a status/button on the right.
 * There is a constant 100px padding at the top for the appliance name, a constant 20px padding at
 * the bottom, and a constant 50px margin on both the left and right. The remainder of the space is
 * split into two parts. The right side is dedicated to the status and on/off button. This space
 * has a minimum width of XXXpx, though if the total width of the whole space is greater than
 * 2 * XXXpx, the right side takes up half of the whole space. The remainder of the space is
 * allocated to the lamp picture.
 */
#pragma once

#include "Button.h"
#include "ShaStation.h"
#include "Layout.h"
#include "Polygon.h"
#include "Rectangle.h"

namespace vik
{
    namespace graphics
    {
        class LightElement final :
            public Layout
        {
        public:
            LightElement(_In_ Light *target);
            LightElement(
                _In_ Light *target,
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height);
            LightElement(
                _In_ Light *target,
                _In_ const Point &pt,
                _In_ GLfloat width,
                _In_ GLfloat height);
            LightElement(_In_ Light *target, _In_ const Rect &rc);

            /* UIElement */
            virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            virtual bool   OnPointerMoved(_In_ Point pt);

            void OnStatusChanged(_In_ LightStatus status);

        private:

            /* LayoutAwareElement */
            virtual void _OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height);

            virtual bool _OnButtonPressed(_In_ ButtonBase *sender, _In_ const Point &pt);

            Light  *_target;
            Button _button;

            /* Picture */
            shapes::Polygon _lampShade;
            shapes::Polygon _lampBase;
            GLfloat         _statusWidth;
            LightStatus     _status;
            std::string     _statusString;
            Color           _statusColor;

            static GLfloat s_statusWidth;
        };
    }
}
