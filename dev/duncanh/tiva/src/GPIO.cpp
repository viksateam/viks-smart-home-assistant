/*
 * GPIO.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Definition of GPIO ports for the Tiva C Series TM4C123G Launchpad (chip TM4C123GH6PM)
 */

#include "GPIO.h"

/*
 * Definitions for GPIO<...>::value
 */
//template <>
//GPIOA_Type * const GPIO_A::value = (GPIOA_Type *)GPIO_A::base;

//template <>
//GPIOA_Type * const GPIO_B::value = (GPIOA_Type *)GPIO_B::base;

//template <>
//GPIOA_Type * const GPIO_C::value = (GPIOA_Type *)GPIO_C::base;

//template <>
//GPIOA_Type * const GPIO_D::value = (GPIOA_Type *)GPIO_D::base;

//template <>
//GPIOA_Type * const GPIO_E::value = (GPIOA_Type *)GPIO_E::base;

//template <>
//GPIOA_Type * const GPIO_F::value = (GPIOA_Type *)GPIO_F::base;
