/*
 * LayoutAwareElement.cpp
 *
 * Authors(s):
 *      Duncan Horn
 *
 * This class defines the functions _OnPositionUpdated and _OnSizeUpdated which get called whenever
 * any Set* function is called.
 */

#include "LayoutAwareElement.h"

using namespace vik::graphics;

/*
 * Constructor(s)/Destructor
 */
LayoutAwareElement::LayoutAwareElement(void)
{
}

LayoutAwareElement::LayoutAwareElement(_In_ GLfloat width, _In_ GLfloat height) :
    UIElement(0, 0, width, height)
{
}

LayoutAwareElement::LayoutAwareElement(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    UIElement(x, y, width, height)
{
}

LayoutAwareElement::LayoutAwareElement(
    _In_ const Point &pt,
    _In_ GLfloat width,
    _In_ GLfloat height) :
    UIElement(pt, width, height)
{
}

LayoutAwareElement::LayoutAwareElement(_In_ const Rect &rc) :
    UIElement(rc)
{
}



/*
* IAnimatable
*/
void LayoutAwareElement::SetX(_In_ GLfloat x)
{
    this->_OnPositionUpdated(x, this->GetY());
}

void LayoutAwareElement::SetY(_In_ GLfloat y)
{
    this->_OnPositionUpdated(this->GetX(), y);
}

void LayoutAwareElement::SetLocation(_In_ const Point &pt)
{
    this->_OnPositionUpdated(pt.x, pt.y);
}

void LayoutAwareElement::SetWidth(_In_ GLfloat width)
{
    this->_OnSizeUpdated(width, this->GetHeight());
}

void LayoutAwareElement::SetHeight(_In_ GLfloat height)
{
    this->_OnSizeUpdated(this->GetWidth(), height);
}

void LayoutAwareElement::SetRect(_In_ const Rect &rc)
{
    this->_OnPositionUpdated(rc.pt.x, rc.pt.y);
    this->_OnSizeUpdated(rc.width, rc.height);
}

GLfloat LayoutAwareElement::TranslateX(_In_ GLfloat deltaX)
{
    this->_OnPositionUpdated(this->GetX() + deltaX, this->GetY());
    return this->GetX();
}

GLfloat LayoutAwareElement::TranslateY(_In_ GLfloat deltaY)
{
    this->_OnPositionUpdated(this->GetX(), this->GetY() + deltaY);
    return this->GetY();
}

Point LayoutAwareElement::Translate(_In_ GLfloat deltaX, _In_ GLfloat deltaY)
{
    this->_OnPositionUpdated(this->GetX() + deltaX, this->GetY() + deltaY);
    return this->GetLocation();
}

Point LayoutAwareElement::Translate(_In_ const Point &pt)
{
    this->_OnPositionUpdated(this->GetX() + pt.x, this->GetY() + pt.y);
    return this->GetLocation();
}

GLfloat LayoutAwareElement::IncreaseWidth(_In_ GLfloat deltaWidth)
{
    this->_OnSizeUpdated(this->GetWidth() + deltaWidth, this->GetHeight());
    return this->GetWidth();
}

GLfloat LayoutAwareElement::IncreaseHeight(_In_ GLfloat deltaHeight)
{
    this->_OnSizeUpdated(this->GetWidth(), this->GetHeight() + deltaHeight);
    return this->GetHeight();
}



/*
 * Protected Functions
 */
void LayoutAwareElement::_OnPositionUpdated(_In_ GLfloat x, _In_ GLfloat y)
{
    // By default, fulfill the request
    UIElement::SetLocation(Point(x, y));
}

void LayoutAwareElement::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    // By default, fulfill the request
    UIElement::SetWidth(width);
    UIElement::SetHeight(height);
}
