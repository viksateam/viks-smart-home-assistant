/*
 * Rectangle.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines a UIElement for a Rectangle. Rectangles are just very simple wrappers around the
 * UIElement with the addition that they define the Draw function (i.e. you can actually draw
 * Rectangles whereas you cannot draw base UIElements).
 */

#include "Drawing.h"
#include "Rectangle.h"

using namespace vik::graphics;
using namespace vik::graphics::shapes;

/*
 * Constructors/Destructors
 */
Rectangle::Rectangle(void)
{
}

Rectangle::Rectangle(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height) :
    Shape(x, y, width, height)
{
}

Rectangle::Rectangle(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height) :
    Shape(pt, width, height)
{
}

Rectangle::Rectangle(_In_ const Rect &rc) :
    Shape(rc)
{
}



/*
 * UIElement
 */
GLvoid Rectangle::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    GLfloat x = this->GetX() + offX;
    GLfloat y = this->GetY() + offY;
    GLfloat width = this->GetWidth();
    GLfloat height = this->GetHeight();
    GLfloat borderThickness = this->GetBorderThickness();

    // First, draw the border, but only if it exists. Drawing the border can get a little
    // complicated as we need to maintain a "hole" in the middle. We can't just draw over with the
    // background color because the background may not be opaque
    if (borderThickness > 0)
    {
        glColor4fv(this->GetBorderColor());

        FillRect(x, y, borderThickness, height);                            // Left border
        FillRect(x, y, width, borderThickness);                             // Top border
        FillRect(x + width - borderThickness, y, borderThickness, height);  // Right border
        FillRect(x, y + height - borderThickness, width, borderThickness);  // Bottom border
    }

    // Now, fill in the rectangle
    x += borderThickness;
    y += borderThickness;
    width -= borderThickness * 2;
    height -= borderThickness * 2;
    assert(width >= -1e-2);
    assert(height >= -1e-2);

    FillRect(x, y, width, height, this->GetBackgroundColor());
}
