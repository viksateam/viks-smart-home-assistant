/*
 * Animation.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Definition of the IAnimation interface. This defines the various functions that are used by the
 * AnimationManager when an animation begins, updates, and when the animation is removed from the
 * animation list.
 *
 * Using an IAnimation object requires following special rules that, when broken, can lead to bugs
 * such as memory leaks, double deletes, use after free, etc. These rules revolve around when, and
 * how frequently each of the functions can/should be called.
 *
 * When an animation is about to begin (i.e. all of its dependent animations have completed), the
 * AnimationManager must call the IAnimation's OnAnimationBegin function. This function is intended
 * to allow the IAnimation to setup any necessary internal state e.g. query starting location,
 * calculate duration, etc. This function must only be called once, and must be called before any
 * calls to OnAnimationUpdated or OnAnimationCompleted.
 *
 * After an animation has begun (i.e. after the initial call to OnAnimationBegin), the
 * AnimationManager will update the IAnimation by making calls to OnAnimationUpdate, supplying the
 * amount of time since the previous update, in units of microseconds, as input. The
 * OnAnimationUpdate function will return true once the animation has completed and false
 * otherwise. It is considered safe, though not recommended, to call OnAnimationUpdate after it has
 * returned true on a previous call.
 *
 * Once an animation has completed, the AnimationManager must call the OnAnimationCompleted
 * function. Once the OnAnimationCompleted function has been called, no calls other than a call to
 * GetPendingAnimation are allowed to be made. This is to allow the IAnimation to "setup" its
 * pending animation if needed (e.g. ConcurrentAnimation needs to do this). Additionally, only one
 * call to OnAnimationCompleted is allowed to be made on each IAnimation object, and that call must
 * be made exclusive of any calls to OnAnimationCancelled (i.e. only one is allowed to be called).
 *
 * Alternatively, any time after the creation of the IAnimation up until OnAnimationCompleted gets
 * called, an animation can be cancelled with a call to OnAnimationCancelled. This call is
 * effectively the same as a call to OnAnimationCompleted, and all rules for that function apply,
 * only no modifications to any underlying animatable objects should be made once the animation is
 * cancelled.
 *
 * Any time from creation to completion/cancellation, a pending animation can be set for the
 * IAnimation object with a call to SetPendingAnimation. Once the call to SetPendingAnimation has
 * been made, the IAnimation argument is placed under control of the IAnimation object being
 * invoked. I.e. the IAnimation in question is responsible for correct memory management of that
 * object from that point on. Calling SetPendingAnimation after either OnAnimationCompleted or
 * OnAnimationCancelled is not considered safe.
 *
 * When OnAnimationCompleted or OnAnimationCancelled gets called, the IAnimation's pending
 * animation is placed in control of the calling function/object. Because it is required that
 * either OnAnimationCompleted or OnAnimationCancelled get called before the destruction of the
 * IAnimation object, IAnimation objects are never responsible for the final memory management of
 * its pending animations. IAnimation objects are, however, reponsible for the intermediate memory
 * managment of pending animations when SetPendingAnimation gets called.
 *
 * An IAnimation's pending animation can be retrieved by a call to its GetPendingAnimation
 * function. It is considered safe to call an IAnimation's GetPendingAnimation function before a
 * call to either OnAnimationCompleted or OnAnimationCancelled, though it is not guaranteed that
 * the returned value is up to date.
 */
#pragma once

#include "sha.h"

#include <cstdint>
#include <vector>

#define US_IN_MS        (1000)              /* Microseconds in one millisecond */
#define US_IN_SEC       (US_IN_MS * 1000)   /* Microseconds in one second */

namespace vik
{
    namespace graphics
    {
        class UIElement;

        namespace animations
        {
            class IAnimatable;

            class IAnimation
            {
            public:
                virtual ~IAnimation(void) {}

                virtual void OnAnimationBegin(void) = 0;
                virtual bool OnAnimationUpdate(_In_ uint64_t timeMicro) = 0;
                virtual void OnAnimationCompleted(void) = 0;
                virtual void OnAnimationCancelled(void) = 0;

                virtual bool IsCancelled(void) = 0;

                virtual void      SetParent(_In_ UIElement *parent) = 0;
                virtual UIElement *GetParent(void) = 0;

                typedef void (*AnimationEventCallback)(IAnimation *sender, IAnimatable *target);
                virtual void SetAnimationBeginCallback(_In_ AnimationEventCallback func) = 0;
                virtual void SetAnimationCompletedCallback(_In_ AnimationEventCallback func) = 0;
                virtual void SetAnimationCancelledCallback(_In_ AnimationEventCallback func) = 0;

                typedef std::vector<IAnimation *> AnimationList;
                virtual void                AddPendingAnimation(_In_ IAnimation *ani) = 0;
                virtual const AnimationList &GetPendingAnimations(void) = 0;
            };
        }
    }
}
