/*
 * AbsoluteLayout.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a Layout where children are places absolutely with respect to their desired widths
 * and heights. Note that this layout does not respect width/height restrictions. That is, children
 * are drawn regardless of whether or not whole or part of the element exists outside the bounds of
 * the layout.
 */
#pragma once

#include "Layout.h"

namespace vik
{
    namespace graphics
    {
        class AbsoluteLayout final :
            public Layout
        {
        public:
            AbsoluteLayout(void);
            AbsoluteLayout(_In_ GLfloat width, _In_ GLfloat height);
            AbsoluteLayout(
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height);
            AbsoluteLayout(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
            AbsoluteLayout(_In_ const Rect &rc);

            /* UIElement */
            virtual void Draw(_In_ GLfloat offX, _In_ GLfloat offY);

            /* Layout */
            virtual void AddChild(_In_ UIElement *elem);

            /* Children */
            bool      RemoveChild(_In_ UIElement *elem);
            bool      RemoveChild(_In_ size_t index);
            UIElement *GetChild(_In_ size_t index) const;
            size_t    Size(void) const;

        private:

            std::vector<UIElement *> _children;
        };
    }
}
