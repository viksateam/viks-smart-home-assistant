/*
 * Rectangle.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Defines a UIElement for a Rectangle. Rectangles are just very simple wrappers around the
 * UIElement with the addition that they define the Draw function (i.e. you can actually draw
 * Rectangles whereas you cannot draw base UIElements).
 */
#pragma once

#include "Shape.h"

namespace vik
{
    namespace graphics
    {
        namespace shapes
        {
            class Rectangle :
                public Shape
            {
            public:
                Rectangle(void);
                Rectangle(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
                Rectangle(_In_ const Point &pt, _In_ GLfloat width, _In_ GLfloat height);
                Rectangle(_In_ const Rect &rc);

                /* UIElement */
                virtual GLvoid Draw(_In_ GLfloat offX, _In_ GLfloat offY);
            };
        }
    }
}
