/*
 * AnimationManager.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * 
 */
#pragma once

#include <chrono>
#include <map>

#include "Animation.h"

#define INVALID_ANIMATION_COOKIE        (-1)

namespace vik
{
    namespace graphics
    {
        namespace animations
        {
            // Used for identifying animations
            typedef long animation_cookie;

            class AnimationManager final
            {
            private:
                AnimationManager(void);
                ~AnimationManager(void);

            public:
                static AnimationManager *GetInstance(void);

                void Update(void);

                _Success_(return != INVALID_ANIMATION_COOKIE)
                animation_cookie SubmitAnimation(_In_ IAnimation *ani);
                _Success_(return != INVALID_ANIMATION_COOKIE)
                animation_cookie SubmitPendingAnimation(
                    _In_ IAnimation *ani,
                    _In_ animation_cookie parentCookie);
                bool             CancelAnimation(_In_ animation_cookie cookie);

            private:

                void _ClaimPendingAnimations(_In_ IAnimation *ani);

                animation_cookie _nextCookie;
                std::map<animation_cookie, IAnimation *> _animations;
                std::map<IAnimation *, animation_cookie> _pendingAnimations;
                std::map<IAnimation *, animation_cookie> _readyAnimations;
                std::map<IAnimation *, animation_cookie> _runningAnimations;
                std::map<IAnimation *, animation_cookie> _cancelledAnimations;

                std::chrono::high_resolution_clock::time_point _prevTime;
            };
        }
    }
}
