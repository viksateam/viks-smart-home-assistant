/*
 * InetAddress.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Class that is useful for dealing with internet addresses. 
 */
#pragma once

#include <WinSock2.h>

namespace vik
{
    namespace communication
    {
        class InetAddress
        {
        public:
            InetAddress(void);
            InetAddress(_In_ const sockaddr_in &addr);
            InetAddress(_In_ ULONG ulIpAddr, _In_ USHORT usPort);
            InetAddress(_In_ const char *pszIpAddr, _In_ USHORT usPort);

            InetAddress &operator=(_In_ const sockaddr_in &addr);
            operator sockaddr_in(void) const;

            USHORT      GetPort(void) const;
            ULONG       GetIpAddress(void) const;
            const char  *GetIpAddressAsString(void) const;

        private:

            USHORT _usPort;
            ULONG  _ulIpAddr;
        };
    }
}
