/*
 * Notification.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a notification that can be displayed by the Smart Home Assistant. Currently,
 * notifications are simple rectangles with text wrapping across multiple lines. The amount of text
 * that gets displayed is directly proportional to the size of the Notification, and thus clipping
 * of text may occur if there is too much text, or if the Notification is too small. In the event
 * that text does need to be clipped, the text is terminated by an ellipsis.
 */
#pragma once

#include "UIElement.h"

namespace vik
{
    namespace graphics
    {
        class Notification final :
            public UIElement
        {
        public:
            Notification(_In_ const std::string &message);
            Notification(
                _In_ const std::string &message,
                _In_ GLfloat x,
                _In_ GLfloat y,
                _In_ GLfloat width,
                _In_ GLfloat height);
            Notification(
                _In_ const std::string message,
                _In_ const Point &pt,
                _In_ GLfloat width,
                _In_ GLfloat height);
            Notification(_In_ const std::string &message, _In_ const Rect &rc);

            /* UIElement */
            virtual void    SetWidth(_In_ GLfloat width);
            virtual void    SetHeight(_In_ GLfloat height);
            virtual void    SetRect(_In_ const Rect &rc);
            virtual GLfloat IncreaseWidth(_In_ GLfloat deltaWidth);
            virtual GLfloat IncreaseHeight(_In_ GLfloat deltaHeight);
            virtual GLvoid  Draw(_In_ GLfloat offX, _In_ GLfloat offY);

        private:

            void _OnSizeChanged(void);

            std::string              _message;
            std::vector<std::string> _displayedText;
        };
    }
}
