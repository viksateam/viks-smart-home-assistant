/*
 * SliderComboBox.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * Represents a combo box-like element that, instead of expanding down when clicked, functions much
 * more like the RotaryElement where it can be manipulated left and right. This functionality can
 * most accurately be compared to the Windows Phone time picker.
 */

#include <algorithm>

#include "FloatingPointAnimation.h"
#include "SliderComboBox.h"
#include "WindowHelpers.h"

using namespace vik::graphics;
using namespace vik::graphics::animations;

#define SNAP_VELOCITY       (.0005f)

/*
 * Constructor(s)/Destructor
 */
SliderComboBox::SliderComboBox(void) :
    SliderComboBox(Rect(0, 0, 0, 0), Orientation::HORIZONTAL)
{
}

SliderComboBox::SliderComboBox(
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ Orientation orientation) :
    SliderComboBox(Rect(0, 0, width, height), orientation)
{
}

SliderComboBox::SliderComboBox(
    _In_ GLfloat x,
    _In_ GLfloat y,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ Orientation orientation) :
    SliderComboBox(Rect(x, y, width, height), orientation)
{
}

SliderComboBox::SliderComboBox(
    _In_ const Point &pt,
    _In_ GLfloat width,
    _In_ GLfloat height,
    _In_ Orientation orientation) :
    SliderComboBox(Rect(pt, width, height), orientation)
{
}

SliderComboBox::SliderComboBox(_In_ const Rect &rc, _In_ Orientation orientation) :
    LayoutAwareElement(rc),
    _orientation(orientation),
    _font(Fonts::ApplianceFont),
    _offX(0),
    _offY(0),
    _elemWidth(0),
    _elemHeight((GLfloat)Fonts::ApplianceFont.height),
    _state(SS_NORMAL),
    _manipulatePoint(0, 0),
    _manipulateCookie(INVALID_ANIMATION_COOKIE),
    _manipulationStartedCallback(nullptr),
    _manipulationCompletedCallback(nullptr),
    _manipulationStartedTarget(nullptr),
    _manipulationCompletedTarget(nullptr)
{
    this->_ResetOffsets();
    this->_RefreshLayout();
}



/*
 * UIElement
 */
#pragma region UIElement
GLvoid SliderComboBox::Draw(_In_ GLfloat offX, _In_ GLfloat offY)
{
    offX += this->GetX();
    offY += this->GetY();

    GLfloat x = this->_offX;
    GLfloat y = this->_offY;
    for (size_t i = 0; i < this->_elements.size(); i++)
    {
        GLfloat right = x;
        GLfloat bottom = y;

        if (this->_orientation == HORIZONTAL)
        {
            right += this->_elemWidth;
        }
        else
        {
            bottom += this->_elemHeight;
        }

        if (right > this->GetWidth() || bottom > this->GetHeight())
        {
            // Impossible to draw any additional elements
            break;
        }
        else if (x >= 0 && y >= 0)
        {
            GLfloat ratio;
            if (this->_orientation == HORIZONTAL)
            {
                ratio = 1 - (2 * std::abs(this->_border.GetX() - x) /
                    (this->GetWidth() - this->_elemWidth));
            }
            else
            {
                ratio = 1 - (2 * std::abs(this->_border.GetY() - y) /
                    (this->GetHeight() - this->_elemHeight));
            }

            assert(ratio <= 1);
            Color c = this->_foregroundColor;
            c.a *= ratio;

            // We wish to draw the string in the middle of the "area" it occupies
            GLfloat deltaX = (this->_elemWidth - this->_widths[i]) / 2;
            WindowDrawText(x + deltaX + offX + this->_elemPadding.left,
                y + offY + this->_elemPadding.top, this->_elements[i].c_str(), this->_font, c);
        }

        x = right;
        y = bottom;
    }

    // Draw the outline rectangle on top of everything else
    this->_border.Draw(offX, offY);
}

bool SliderComboBox::OnPointerEntered(_In_ Point pt)
{
    (void)pt;
    assert(this->_state != SS_HOVER);

    if (this->_state == SS_NORMAL)
    {
        this->_state = SS_HOVER;
    }

    return true;
}

bool SliderComboBox::OnPointerExited(void)
{
    if (this->_state == SS_HOVER)
    {
        this->_state = SS_NORMAL;
    }

    return true;
}

bool SliderComboBox::OnPointerMoved(_In_ Point pt)
{
    assert(this->_state != SS_NORMAL);

    if (this->_state == SS_MANIPULATING)
    {
        GLfloat deltaX = pt.x - this->_manipulatePoint.x;
        GLfloat deltaY = pt.y - this->_manipulatePoint.y;

        switch (this->_orientation)
        {
        case HORIZONTAL:
            this->_offX += deltaX;

            if (this->_offX > this->_border.GetX())
            {
                this->_offX = this->_border.GetX();
            }
            else if (this->_offX + this->_elemWidth * this->_elements.size() <
                this->_border.GetX() + this->_border.GetWidth())
            {
                this->_offX = this->_border.GetX() + this->_border.GetWidth() -
                    this->_elemWidth * this->_elements.size();
            }
            break;

        case VERTICAL:
            this->_offY += deltaY;

            if (this->_offY > this->_border.GetY())
            {
                this->_offY = this->_border.GetY();
            }
            else if (this->_offY + this->_elemHeight * this->_elements.size() <
                this->_border.GetY() + this->_border.GetHeight())
            {
                this->_offY = this->_border.GetY() + this->_border.GetHeight() -
                    this->_elemHeight * this->_elements.size();
            }
            break;
        }

        this->_manipulatePoint = pt;
    }

    return true;
}

bool SliderComboBox::OnPointerDown(_In_ Point pt)
{
    assert(this->_state == SS_HOVER);
    AnimationManager::GetInstance()->CancelAnimation(this->_manipulateCookie);

    this->_state = SS_MANIPULATING;
    this->_manipulatePoint = pt;

    if (this->_manipulationStartedCallback)
    {
        (this->_manipulationStartedTarget->*this->_manipulationStartedCallback)(this,
            this->GetSelectedIndex());
    }

    return true;
}

bool SliderComboBox::OnPointerUp(_In_ Point pt)
{
    assert(this->_state == SS_MANIPULATING);
    this->OnPointerMoved(pt);

    if ((this->GetRect() - this->GetLocation()).Contains(pt))
    {
        this->_state = SS_HOVER;
    }
    else
    {
        this->_state = SS_NORMAL;
    }

    if (this->_manipulationCompletedCallback)
    {
        (this->_manipulationCompletedTarget->*this->_manipulationCompletedCallback)(this,
            this->GetSelectedIndex());
    }

    this->_SnapToIndex(this->GetSelectedIndex());
    return true;
}
#pragma endregion



/*
 * Options
 */
void SliderComboBox::AddOption(_In_ const std::string &opt)
{
    GLfloat width = this->_CalculateWidth(opt);
    if (width > this->_elemWidth)
    {
        this->_SetElementWidth(width);
    }

    this->_elements.push_back(opt);
    this->_widths.push_back(width);

    // Only need to update if orientation is horizontal as all elements have the same height
    if (this->_orientation == HORIZONTAL)
    {
        this->_RefreshLayout();
    }
}

bool SliderComboBox::RemoveOption(_In_ const std::string &opt)
{
    auto itr = std::find(std::begin(this->_elements), std::end(this->_elements), opt);
    if (itr != std::end(this->_elements))
    {
        auto result = this->RemoveOption(std::distance(std::begin(this->_elements), itr));
        assert(result);

        return result;
    }

    return false;
}

bool SliderComboBox::RemoveOption(_In_ size_t index)
{
    assert(this->_elements.size() == this->_widths.size());

    if (index >= this->_elements.size())
    {
        return false;
    }

    GLfloat width = this->_widths[index];
    this->_elements.erase(std::begin(this->_elements) + index);
    this->_widths.erase(std::begin(this->_widths) + index);

    // Possibly need to re-calculate the largest width
    if (width >= this->_elemWidth)
    {
        auto itr = std::max(std::begin(this->_widths), std::end(this->_widths));

        if (itr != std::end(this->_widths))
        {
            this->_SetElementWidth(*itr);
        }
        else
        {
            this->_SetElementWidth(this->_elemPadding.left + this->_elemPadding.right);
        }

        if (this->_orientation == HORIZONTAL)
        {
            this->_RefreshLayout();
        }
    }

    return true;
}

int SliderComboBox::GetSelectedIndex(void) const
{
    int index;

    if (this->_orientation == HORIZONTAL)
    {
        index = (int)std::round((this->_border.GetX() - this->_offX) / this->_elemWidth);
    }
    else
    {
        index = (int)std::round((this->_border.GetY() - this->_offY) / (this->_elemHeight));
    }

    assert((size_t)index < this->_elements.size());
    return index;
}

const std::string &SliderComboBox::GetSelectedOption(void) const
{
    int index = this->GetSelectedIndex();

    assert((size_t)index < this->_elements.size());
    return this->_elements[index];
}

bool SliderComboBox::JumpToOption(_In_ size_t index)
{
    if (this->_state != SS_MANIPULATING && index < this->_elements.size())
    {
        this->_SnapToIndex(index);
        return true;
    }

    return true;
}

bool SliderComboBox::JumpToOption(_In_ const std::string &opt)
{
    auto itr = std::find(std::begin(this->_elements), std::end(this->_elements), opt);
    if (itr != std::end(this->_elements))
    {
        return this->JumpToOption(std::distance(std::begin(this->_elements), itr));
    }

    return false;
}



/*
 * UI Properties
 */
void SliderComboBox::SetOrientation(_In_ Orientation orientation)
{
    if (orientation == this->_orientation)
    {
        // Don't "jump back"
        return;
    }

    this->_orientation = orientation;

    this->_ResetOffsets();
    this->_RefreshLayout();
}

void SliderComboBox::SetFont(_In_ const ShaFont &font)
{
    this->_font = font;
    this->_UpdateElementHeight();
    this->_UpdateWidths();

    this->_RefreshLayout();
}

void SliderComboBox::SetPadding(_In_ const Padding &padding)
{
    this->_elemPadding = padding;
    this->_UpdateElementHeight();
    this->_UpdateWidths();

    this->_RefreshLayout();
}

void SliderComboBox::SetBorderColor(_In_ const Color &color)
{
    this->_border.SetBorderColor(color);
}

void SliderComboBox::SetBorderThickness(_In_ GLfloat thickness)
{
    this->_border.SetBorderThickness(thickness);
}

size_t SliderComboBox::Size(void) const
{
    return this->_elements.size();
}



/*
 * Manipulation Events
 */
void SliderComboBox::OnManipulationStarted(
    _In_ ManipulationEventCallback callback,
    _In_ UIElement *target)
{
    this->_manipulationStartedCallback = callback;
    this->_manipulationStartedTarget = target;
}

void SliderComboBox::OnManipulationCompleted(
    _In_ ManipulationEventCallback callback,
    _In_ UIElement *target)
{
    this->_manipulationCompletedCallback = callback;
    this->_manipulationCompletedTarget = target;
}



/*
 * LayoutAwareElement
 */
void SliderComboBox::_OnSizeUpdated(_In_ GLfloat width, _In_ GLfloat height)
{
    GLfloat deltaX = (this->GetWidth() / 2) - this->_offX;
    GLfloat deltaY = (this->GetHeight() / 2) - this->_offY;
    this->_offX = (width / 2) - deltaX;
    this->_offY = (height / 2) - deltaY;

    LayoutAwareElement::_OnSizeUpdated(width, height);

    this->_RefreshLayout();
}



void SliderComboBox::_RefreshLayout(void)
{
    // First, calculate the size and position of the outline rectangle
    GLfloat x = (this->GetWidth() - this->_elemWidth) / 2;
    GLfloat y = (this->GetHeight() - this->_elemHeight) / 2;
    this->_border.SetRect(Rect(x, y, this->_elemWidth, this->_elemHeight));
}

GLfloat SliderComboBox::_CalculateWidth(
    _In_ const std::string &str,
    _In_reads_(256) const int *widths) const
{
    GLfloat width = 0;

    for (auto ch : str)
    {
        width += widths[ch];
    }

    return width + this->_elemPadding.left + this->_elemPadding.right;
}

GLfloat SliderComboBox::_CalculateWidth(_In_ const std::string &str) const
{
    int charWidths[256];

    if (GetCharacterWidths(this->_font, charWidths))
    {
        // For some reason, the null character has non-zero width
        charWidths[0] = 0;

        return this->_CalculateWidth(str, charWidths);
    }

    return 0;
}

void SliderComboBox::_UpdateWidths(void)
{
    assert(this->_elements.size() == this->_widths.size());
    int charWidths[256];

    this->_SetElementWidth(this->_elemPadding.left + this->_elemPadding.right);

    if (!GetCharacterWidths(this->_font, charWidths))
    {
        return;
    }

    // For some reason, the null character has non-zero width
    charWidths[0] = 0;

    for (size_t i = 0; i < this->_elements.size(); i++)
    {
        this->_widths[i] = this->_CalculateWidth(this->_elements[i], charWidths);

        if (this->_widths[i] > this->_elemWidth)
        {
            this->_SetElementWidth(this->_widths[i]);
        }
    }
}

void SliderComboBox::_SetElementWidth(_In_ GLfloat width)
{
    GLfloat delta = (this->_elemWidth - width) / 2;
    this->_offX += delta;

    this->_elemWidth = width;
}

void SliderComboBox::_UpdateElementHeight(void)
{
    GLfloat height = this->_font.height + this->_elemPadding.top + this->_elemPadding.bottom;
    GLfloat delta = (this->_elemHeight - height) / 2;
    this->_offY += delta;

    this->_elemHeight = this->_font.height + this->_elemPadding.top + this->_elemPadding.bottom;
}

void SliderComboBox::_ResetOffsets(bool resetY, bool resetX)
{
    if (resetY)
    {
        this->_offY = (this->GetHeight() - this->_elemHeight) / 2;
    }
    if (resetX)
    {
        this->_offX = (this->GetWidth() - this->_elemWidth) / 2;
    }
}

void SliderComboBox::_SnapToIndex(_In_ int index)
{
    assert((size_t)index < this->_elements.size());
    GLfloat end;
    GLfloat *target;

    if (this->_orientation == HORIZONTAL)
    {
        end = this->_border.GetX() - (this->_elemWidth * index);
        target = &this->_offX;
    }
    else
    {
        end = this->_border.GetY() - (this->_elemHeight * index);
        target = &this->_offY;
    }

    auto ani = new FloatingPointAnimation(target, end, SNAP_VELOCITY);
    AnimationManager::GetInstance()->CancelAnimation(this->_manipulateCookie);
    this->_manipulateCookie = AnimationManager::GetInstance()->SubmitAnimation(ani);
}
