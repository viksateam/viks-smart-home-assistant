
#include <WinSock2.h>
#include <Windows.h>

#include <cassert>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "Socket.h"
#include "UDPSocket.h"

using namespace vik::communication;

// Thread Procedures
static DWORD WINAPI BroadcastThreadProc(_In_ LPVOID lpAux);
static DWORD WINAPI ServerThreadProc(_In_ LPVOID lpAux);
static DWORD WINAPI ClientThreadProc(_In_ LPVOID lpAux);

// Message Handling
static void _OnConnectMessageReceived(
    _Inout_ std::istringstream &stream,
    _In_ const InetAddress &addr);
static std::string _HandleGetRequest(_Inout_ std::istringstream &stream);
static std::string _HandlePostRequest(_Inout_ std::istringstream &stream);

static ServerSocket s_serverSocket;
static UDPSocket s_broadcastSocket;

static std::string s_station = "appliance";
static std::string s_type    = "coffee_machine";
static std::string s_id      = "vikbee.coffee_machine";
static std::string s_name    = "VikBEE Brand Coffee Machine";
static std::string s_ops     = "";

#define SHA_PORT                (1992)
#define SHA_BROADCAST_PORT      (12392)

#define BUFFER_SIZE             (1024)

static bool _ValidApplianceType(void);

std::map<std::string, std::string> _attributes;

int main(int argc, char **argv)
{
    UNREFERENCED_PARAMETER(argc);
    UNREFERENCED_PARAMETER(argv);

    _attributes["light"] = "off";

    _attributes["system"] = "cool";
    _attributes["fan"] = "auto";
    _attributes["set"] = "71";
    _attributes["actual"] = "71";

    _attributes["coffee"] = "off";
    _attributes["cups"] = "0";

    _attributes["artist"] = "JerryBerry";
    _attributes["album"] = "Vik's_Greatest_Hits";
    _attributes["track"] = "VikBEE";
    _attributes["song"] = "play";

    WSADATA wsaData;
    int nError = WSAStartup(MAKEWORD(2, 2), &wsaData);

    // Get appliance information
    do
    {
        std::cout << "Enter appliance type: ";
        std::getline(std::cin, s_type);
    }
    while (!_ValidApplianceType());

    std::cout << "Enter id: ";
    std::cin >> s_id;

    std::cout << "Enter name: ";
    std::getline(std::cin, s_name);
    std::getline(std::cin, s_name);

    std::cout << "Enter supported operations: ";
    std::getline(std::cin, s_ops);

    // Initialize WinSock
    if (nError)
    {
        std::cout << "Could not start Winsock. Error: " << nError << std::endl;
        return 1;
    }

    // Start the server socket and server thread
    if (!s_serverSocket.Start(SHA_PORT))
    {
        std::cout << "Could not start server. Error: " << s_serverSocket.GetError() << std::endl;
        WSACleanup();
        return 1;
    }

    HANDLE hServer = CreateThread(nullptr, 0, ServerThreadProc, nullptr, 0, nullptr);
    if (!hServer)
    {
        std::cout << "Failed to start server thread" << std::endl;
        s_serverSocket.Close();
        WSACleanup();
        return 1;
    }

    // Start the broadcast socket and broadcast thread
    if (!s_broadcastSocket.Open(SHA_BROADCAST_PORT))
    {
        std::cout << "Could not start broadcast server. Error: " << s_broadcastSocket.GetError() <<
            std::endl;
        s_serverSocket.Close();
        WSACleanup();
        return 1;
    }

    HANDLE hBroadcast = CreateThread(nullptr, 0, BroadcastThreadProc, nullptr, 0, nullptr);
    if (!hBroadcast)
    {
        std::cout << "Failed to start broadcast thread" << std::endl;
        s_serverSocket.Close();
        s_broadcastSocket.Close();
        WSACleanup();
        return 1;
    }

    std::string cmd;
    do
    {
        std::cin >> cmd;

        auto itr = std::find(std::begin(cmd), std::end(cmd), '=');
        if (itr != std::end(cmd))
        {
            std::string key(std::begin(cmd), itr);
            std::string val(itr + 1, std::end(cmd));
            _attributes[key] = val;
        }
    }
    while (cmd != "exit");

    s_serverSocket.Close();
    s_broadcastSocket.Close();

    WaitForSingleObject(hServer, INFINITE);
    WaitForSingleObject(hBroadcast, INFINITE);
    CloseHandle(hServer);
    CloseHandle(hBroadcast);

    WSACleanup();
}

static std::vector<std::string> _OnMessageReceived(
    _Inout_updates_(nLen + 1) char *psz,
    _In_ int nLen,
    _Inout_ std::stringstream &stream)
{
    std::vector<std::string> result;
    psz[nLen] = '\0';
    stream << psz;

    int nCount = std::count(psz, psz + nLen, '\n');
    for (int i = 0; i < nCount; i++)
    {
        std::string msg;
        std::getline(stream, msg);

        result.push_back(msg);
    }

    return result;
}

static char rgszBroadcastBuffer[BUFFER_SIZE + 1];
static DWORD WINAPI BroadcastThreadProc(_In_ LPVOID lpAux)
{
    UNREFERENCED_PARAMETER(lpAux);

    // On start, send the connect string
    std::string msg = "connect " + s_station + " " + s_id + " " + s_name + "\n";
    s_broadcastSocket.Broadcast(SHA_BROADCAST_PORT, msg.c_str(), msg.size());
    std::cout << "Sending broadcast string: " << msg;

    std::stringstream stream;
    while (s_broadcastSocket.IsConnected())
    {
        InetAddress addr;
        int nLen = s_broadcastSocket.Receive(rgszBroadcastBuffer, BUFFER_SIZE, &addr);
        if (nLen > 0)
        {
            auto messages = _OnMessageReceived(rgszBroadcastBuffer, nLen, stream);

            for (auto &msg : messages)
            {
                std::istringstream cmdStream(msg);
                std::string command;

                std::cout << "Receied broadcast message from " << addr.GetIpAddressAsString() << 
                    std::endl;
                std::cout << "\tMessage: " << msg << std::endl;
                cmdStream >> command;

                if (command == "connect")
                {
                    _OnConnectMessageReceived(cmdStream, addr);
                }
                else
                {
                    // Ignore disconnect and update messages
                }
            }
        }
        else
        {
            // Error
            s_broadcastSocket.Close();
        }
    }

    std::cout << "Closing broadcast thread" << std::endl;
    return 0;
}

static DWORD WINAPI ServerThreadProc(_In_ LPVOID lpAux)
{
    UNREFERENCED_PARAMETER(lpAux);

    while (s_serverSocket.IsRunning())
    {
        Socket sock = s_serverSocket.Accept();
        if (sock.IsConnected())
        {
            Socket *pSock = new Socket(sock);
            HANDLE hChild = CreateThread(nullptr, 0, ClientThreadProc, pSock, 0, nullptr);
            if (!hChild)
            {
                delete pSock;
            }
            else
            {
                CloseHandle(hChild);
            }
        }
    }

    std::cout << "Closing server thread" << std::endl;
    return 0;
}

static DWORD WINAPI ClientThreadProc(_In_ LPVOID lpAux)
{
    Socket *pSock = (Socket *)lpAux;
    std::cout << "Connection from " << pSock->GetIpAddr() << std::endl;

    char rgszBuffer[BUFFER_SIZE + 1];
    std::stringstream stream;

    while (pSock->IsConnected())
    {
        int nLen = pSock->Receive(rgszBuffer, BUFFER_SIZE);
        if (nLen > 0)
        {
            auto messages = _OnMessageReceived(rgszBuffer, nLen, stream);
            for (auto &msg : messages)
            {
                std::istringstream msgStream(msg);
                std::string command;

                std::cout << "\tInput: " << msg << std::endl;
                msgStream >> command;

                std::string response = "invalid\n";
                if (command == "heartbeat")
                {
                    response = "ack\n";
                }
                else if (command == "request")
                {
                    std::string type;
                    msgStream >> type;
                    if (type == "get")
                    {
                        response = _HandleGetRequest(msgStream);
                    }
                    else if (type == "post")
                    {
                        response = _HandlePostRequest(msgStream);
                    }
                }

                assert(response.back() == '\n');
                std::cout << "\tOutput: " << response;
                pSock->Send(response.c_str(), response.size());
            }
        }
        else
        {
            pSock->Close();
        }
    }

    std::cout << "Connection with " << pSock->GetIpAddr() << " closed" << std::endl;
    delete pSock;
    return 0;
}



static DWORD WINAPI InfoResponseThreadProc(_In_ LPVOID pAux)
{
    InetAddress *pAddr = (InetAddress *)pAux;

    Socket sock(*pAddr);
    if (sock.IsConnected())
    {
        std::string response = "info " + s_station + " " + s_id + " " + s_name + "\n";
        sock.Send(response.c_str(), response.size());
        sock.Close();
    }

    delete pAddr;
    return 0;
}

static void _OnConnectMessageReceived(
    _Inout_ std::istringstream &stream,
    _In_ const InetAddress &addr)
{
    // Since we are imitating an appliance station, we don't care about id or name. All we care
    // about is whether or not the command has a reply request
    std::string str;
    while (!stream.eof() && !stream.bad() && str != "--")
    {
        stream >> str;
    }

    while (!stream.eof() && !stream.bad())
    {
        std::string arg;
        stream >> arg;

        if (arg == "reply")
        {
            InetAddress *pAddr = new InetAddress(addr.GetIpAddress(), SHA_PORT);
            HANDLE hProc =
                CreateThread(nullptr, 0, InfoResponseThreadProc, pAddr, 0, nullptr);

            if (!hProc)
            {
                delete pAddr;
            }
            else
            {
                CloseHandle(hProc);
            }
        }
    }
}



static std::string _HandleLightGetRequest(
    _Inout_ std::istringstream &stream,
    _In_ const std::string &cmd)
{
    (void)stream;
    if (cmd == "status")
    {
        return (_attributes["light"] == "on") ? "on\n" : "off\n";
    }

    return "invalid\n";
}

static std::string _HandleCoffeeMachineGetRequest(
    _Inout_ std::istringstream &stream,
    _In_ const std::string &cmd)
{
    (void)stream;
    if (cmd == "status")
    {
        std::string response = _attributes["coffee"];
        if (_attributes["coffee"] != "off")
        {
            if (_attributes["cups"] != "0")
            {
                response += " cups=" + _attributes["cups"];
            }
        }

        return response + "\n";
    }

    return "invalid\n";
}

static std::string _HandleThermostatGetRequest(
    _Inout_ std::istringstream &stream,
    _In_ const std::string &cmd)
{
    (void)stream;

    if (cmd == "system")
    {
        return _attributes[cmd] + "\n";
    }
    else if (cmd == "fan")
    {
        return _attributes[cmd] + "\n";
    }
    else if (cmd == "temp")
    {
        std::string type;
        stream >> type;

        if (type != "set" && type != "actual")
        {
            return "invalid\n";
        }

        return _attributes[type] + "\n";
    }
    else if (cmd == "status")
    {
        if (rand() <= 100)
        {
            // Move the actual temp towards the set temp
            int actual = atoi(_attributes["actual"].c_str());
            int set = atoi(_attributes["set"].c_str());

            if (actual != set)
            {
                actual += (actual > set) ? -1 : 1;
            }

            std::stringstream ss;
            ss << actual;
            ss >> _attributes["actual"];
        }

        return _attributes["system"] + " " + _attributes["fan"] + " " + _attributes["set"] + " " +
            _attributes["actual"] + "\n";
    }

    return "invalid\n";
}

static std::string _HandleItunesGetRequest(
    _Inout_ std::istringstream &stream,
    _In_ const std::string &cmd)
{
    (void)stream;

    if (cmd == "status")
    {
        return _attributes["song"] + " " + _attributes["artist"] + " " + _attributes["album"] + " " +
            _attributes["track"] + "\n";
    }

    return "invalid\n";
}

static std::string _HandleGetRequest(_Inout_ std::istringstream &stream)
{
    std::string command;
    stream >> command;

    if (command == "info")
    {
        std::string response = s_type;
        if (!s_ops.empty())
        {
            response += " " + s_ops;
        }

        return response + "\n";
    }
    else if (s_type == "light")
    {
        return _HandleLightGetRequest(stream, command);
    }
    else if (s_type == "coffee_machine")
    {
        return _HandleCoffeeMachineGetRequest(stream, command);
    }
    else if (s_type == "thermostat")
    {
        return _HandleThermostatGetRequest(stream, command);
    }
    else if (s_type == "itunes")
    {
        return _HandleItunesGetRequest(stream, command);
    }

    return "invalid\n";
}



static std::string _HandleLightReqeust(
    _Inout_ std::istringstream &stream,
    _In_ const std::string cmd)
{
    std::string arg;
    stream >> arg;

    if (arg == "on")
    {
        if (_attributes[cmd] != "on")
        {
            _attributes[cmd] = "on";
            return "succeeded\n";
        }
        else
        {
            return "failed\n";
        }
    }
    else if (arg == "off")
    {
        if (_attributes[cmd] != "off")
        {
            _attributes[cmd] = "off";
            return "succeeded\n";
        }
        else
        {
            return "failed\n";
        }
    }

    return "invalid\n";
}

static std::string _HandleCoffeeMachineRequest(
    _Inout_ std::istringstream &stream,
    _In_ const std::string &cmd)
{
    if (cmd == "coffee")
    {
        std::string arg;
        stream >> arg;
        if (arg == "on")
        {
            stream >> arg;

            if (_attributes["coffee"] != "on")
            {
                if (!arg.empty())
                {
                    auto itr = std::find(std::begin(arg), std::end(arg), '=');
                    std::string key(std::begin(arg), itr);
                    if (itr != std::end(arg) && key == "cups")
                    {
                        std::string amt(itr + 1, std::end(arg));
                        _attributes["cups"] = amt;
                    }
                }

                _attributes["coffee"] = "on";
                return "succeeded\n";
            }
            else
            {
                return "failed\n";
            }
        }
        else if (arg == "off")
        {
            if (_attributes["coffee"] != "off")
            {
                _attributes["coffee"] = "off";
                _attributes["cups"] = "0";
                return "succeeded\n";
            }
            else
            {
                return "failed\n";
            }
        }
    }

    return "invalid\n";
}

static std::string _HandleThermostatRequest(
    _Inout_ std::istringstream &stream,
    _In_ const std::string &cmd)
{
    if (cmd == "system")
    {
        std::string system;
        stream >> system;

        if (system == "heat" || system == "cool" || system == "off")
        {
            _attributes[cmd] = system;
            return "succeeded\n";
        }
    }
    else if (cmd == "fan")
    {
        std::string fan;
        stream >> fan;

        if (fan == "on" || fan == "auto")
        {
            _attributes[cmd] = fan;
            return "succeeded\n";
        }
    }
    else if (cmd == "set")
    {
        std::string temp;
        stream >> temp;

        _attributes["set"] = temp;
        return "succeeded\n";
    }

    return "invalid\n";
}

static std::string _HandleItunesRequest(
    _Inout_ std::istringstream &stream,
    _In_ const std::string &cmd)
{
    if (cmd == "song")
    {
        std::string arg;
        stream >> arg;

        if (arg == "play" || arg == "pause")
        {
            _attributes[cmd] = arg;
            return "succeeded\n";
        }
    }

    return "invalid\n";
}

static std::string _HandlePostRequest(_Inout_ std::istringstream &stream)
{
    std::string cmd;
    stream >> cmd;

    if (s_type == "light")
    {
        return _HandleLightReqeust(stream, cmd);
    }
    else if (s_type == "coffee_machine")
    {
        return _HandleCoffeeMachineRequest(stream, cmd);
    }
    else if (s_type == "thermostat")
    {
        return _HandleThermostatRequest(stream, cmd);
    }
    else if (s_type == "itunes")
    {
        return _HandleItunesRequest(stream, cmd);
    }

    return "invalid\n";
}



static bool _ValidApplianceType(void)
{
    if (s_type != "coffee_machine" && s_type != "light" && s_type != "thermostat" && s_type != "itunes")
    {
        std::cout << "ERROR: supported appliance types are\n\tcoffee_machine\n\tlight\n\tthermostat\n\titunes\n";
        return false;
    }

    return true;
}
