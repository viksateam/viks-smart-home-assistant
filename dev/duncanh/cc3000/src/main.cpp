/*
 * main.cpp
 *
 * Author(s):
 *      Duncan Horn
 *
 * A simple project that can be used to debug/test the CC3000 wireless module with the Tiva C
 * Series TM4C123G LaunchPad Evaluation Kit
 */

#include <ctype.h>
#include <string.h>

#include <driverlib/interrupt.h>
#include <driverlib/rom_map.h>
#include <driverlib/sysctl.h>

#include <cc3000_common.h>
#include <evnt_handler.h>
#include <nvmem.h>
#include <security.h>
#include <wlan.h>

#include "Bitwise.h"
#include "Console.h"
#include "Debug.h"
#include "sys_tick.h"
#include "wlan_spi.h"
#include "tiva.h"

#ifdef  WIN32
#define strtok_r    strtok_s
#endif  /* WIN32 */

#define SMART_CONFIG_KEY_MAX_LEN                (32)
#define SSID_MAX_LEN                            (32)
#define PASSWORD_MAX_LEN                        (32)
#define IP_ADDRESS_MAX_LEN                      (16)        // 4 bytes, 3 periods, and 1 null
#define RECEIVE_BUFFER_SIZE                     (128)

#define NETAPP_IPCONFIG_MAC_OFFSET              (20)

static volatile bool g_fRunning = true;
static volatile bool g_fConnected = false;
static volatile bool g_fConfig = false;
static volatile bool g_fIPValid = false;

static volatile bool g_fChat = false;
static uint32_t g_uChatIp = 0;
static uint32_t g_uChatPort = 0;

static char g_rgszIPAddress[IP_ADDRESS_MAX_LEN];
static char g_rgcReceiveBuffer[RECEIVE_BUFFER_SIZE];

static volatile long g_lSocketHandle = EFAIL;

uint32_t string_to_uint(_In_z_ const char *psz)
{
    uint32_t uVal = 0;

    while ((*psz >= '0') && (*psz <= '9'))
    {
        uVal = (uVal * 10) + (*psz - '0');
        psz++;
    }

    return uVal;
}

char *byte_to_string(_In_ uint8_t uByte, _Out_ char *psz)
{
    int iLen;
    char rgcDigits[3];

    if (uByte == 0)
    {
        *psz = '0';
        return psz + 1;
    }

    // Cache the digits
    for (iLen = 0; uByte; iLen++)
    {
        rgcDigits[iLen] = uByte % 10;
        uByte /= 10;
    }

    // Digits are located backwards
    for (; iLen > 0; iLen--)
    {
        *psz = rgcDigits[iLen - 1] + '0';
        psz++;
    }

    *psz = '\0';
    return psz;
}

static char *ip_to_string(
    _In_ uint8_t uByte0,
    _In_ uint8_t uByte1,
    _In_ uint8_t uByte2,
    _In_ uint8_t uByte3,
    _Out_ char *psz)
{
    psz = byte_to_string(uByte0, psz);
    *psz = '.';
    psz = byte_to_string(uByte1, psz + 1);
    *psz = '.';
    psz = byte_to_string(uByte2, psz + 1);
    *psz = '.';
    psz = byte_to_string(uByte3, psz + 1);
    *psz = '\0';

    return psz;
}

static uint32_t string_to_ip(_In_z_ const char *psz)
{
    uint32_t uIpAddr = 0;

    for (int i = 0; *psz && (i < 4); i++)
    {
        uIpAddr = (uIpAddr << 8) | string_to_uint(psz);

        // Move to the next entry
        while ((*psz >= '0') && (*psz <= '9'))
        {
            psz++;
        }
        if (*psz)
        {
            psz++;
        }
    }

    return uIpAddr;
}

static void WlanCallback(_In_ long lEventType, char *pszData, unsigned char ucLength)
{
    switch (lEventType)
    {
    case HCI_EVNT_WLAN_ASYNC_SIMPLE_CONFIG_DONE:
        g_fConfig = false;
        break;

    case HCI_EVNT_WLAN_UNSOL_CONNECT:
        g_fConnected = true;
        g_fConfig = false;
        break;

    case HCI_EVNT_WLAN_UNSOL_DISCONNECT:
        g_fConnected = false;
        g_fConfig = false;
        Debug::SetLed(LED_RED);
        break;

    case HCI_EVNT_WLAN_UNSOL_DHCP:
        // Notes: 
        // 1) IP config parameters are received swapped
        // 2) IP config parameters are valid only if status is OK
        if (*(pszData + NETAPP_IPCONFIG_MAC_OFFSET) == 0)
        {
            ip_to_string(pszData[3], pszData[2], pszData[1], pszData[0], g_rgszIPAddress);
            g_fIPValid = true;
            Debug::SetLed(LED_GREEN);
        }
        else
        {
            g_fIPValid = false;
        }
        break;

    case HCI_EVENT_CC3000_CAN_SHUT_DOWN:
        // OkToDoShutDown = 1;
        // TODO
        break;

    default:
        break;
    }
}

static char *SendNullPatch(_Out_ unsigned long *pulLength)
{
    *pulLength = 0;
    return NULL;
}

static void RunSmartConfig(_In_z_ char *pszArgs)
{
    // Setup the AES key
    char rgszSmartConfigKey[SMART_CONFIG_KEY_MAX_LEN];
    char *pszConfigKey = strtok_r(pszArgs, " ", &pszArgs);
    if (pszConfigKey)
    {
        strncpy(rgszSmartConfigKey, pszConfigKey, SMART_CONFIG_KEY_MAX_LEN);
        rgszSmartConfigKey[SMART_CONFIG_KEY_MAX_LEN - 1] = '\0';
    }
    else
    {
        // Use the default key
        strcpy(rgszSmartConfigKey, "jerryberry123456");
    }

    // Reset all of the previous configurations
    g_fConfig = true;
    wlan_ioctl_set_connection_policy(0, 0, 0);
    wlan_ioctl_del_profile(255);

    // Wait until the CC3000 is disconnected
    while (g_fConnected)
    {
        SysCtlDelay(100);
        hci_unsolicited_event_handler();
    }

    // Start blinking the red LED during the Smart Configuration process
    Debug::SetLed(LED_RED);

    wlan_smart_config_set_prefix("TTT");
    Debug::TurnOffLed(LED_RED);

    // Start the SmartConfig start process
    wlan_smart_config_start(1);
    Debug::TurnOnLed(LED_RED);

    // Wait for Smart config to finish
    while (g_fConfig)
    {
        Debug::ToggleLed(LED_RED);
        SysCtlDelay(7500000);
    }

    // Use a blue LED to indicate a pending connection
    Debug::SetLed(LED_BLUE);

    // create new entry for AES encryption key
    nvmem_create_entry(NVMEM_AES128_KEY_FILEID, strlen(rgszSmartConfigKey));
    aes_write_key((unsigned char *)rgszSmartConfigKey);

    // Decrypt configuration information and add profile
    wlan_smart_config_process();

    // Configure to connect automatically to the AP retrieved in the Smart config process
    wlan_ioctl_set_connection_policy(0, 0, 0);

    // reset the CC3000
    wlan_stop();

    SysCtlDelay(100000);
    wlan_start(0);

    // Mask out all non-required events
    wlan_set_event_mask(HCI_EVNT_WLAN_KEEPALIVE | HCI_EVNT_WLAN_UNSOL_INIT |
        HCI_EVNT_WLAN_ASYNC_PING_REPORT);
}

/*
 * Arguments are: connect [SSID] [encryption] [password]
 */
static void Connect(_In_z_ char *pszArgs)
{
    // Parse arguments (and use defaults when needed)
    char rgszSSID[SSID_MAX_LEN] = "vik";
    char rgszPassword[PASSWORD_MAX_LEN] = "jerryberry";
    uint32_t uEncryption = WLAN_SEC_WPA2;

    char *pszNextArg;
    if ((pszNextArg = strtok_r(pszArgs, " ", &pszArgs)))
    {
        // SSID
        strncpy(rgszSSID, pszNextArg, SSID_MAX_LEN);
        rgszSSID[SSID_MAX_LEN - 1] = '\0';
    }
    if ((pszNextArg = strtok_r(pszArgs, " ", &pszArgs)))
    {
        // Encryption
        uEncryption = string_to_uint(pszNextArg);
    }
    if ((pszNextArg = strtok_r(pszArgs, " ", &pszArgs)))
    {
        // Password
        strncpy(rgszPassword, pszNextArg, PASSWORD_MAX_LEN);
        rgszPassword[PASSWORD_MAX_LEN - 1] = '\0';
    }

    if (g_fConnected)
    {
        Console::WriteLine("ERROR: Already connected");
        return;
    }

    // Use a blue LED to indicate a pending connection
    Debug::SetLed(LED_BLUE);
    g_fConfig = true;

#ifndef CC3000_TINY_DRIVER
    wlan_connect(uEncryption, rgszSSID, strlen(rgszSSID), NULL, (unsigned char *)rgszPassword,
        strlen(rgszPassword));
#else
    wlan_connect(rgszSSID, strlen(rgszSSID));
#endif

    // Block until connection is made and we have an ip, or the connection fails
    while (g_fConfig || (!g_fIPValid && g_fConnected))
    {
        // TODO: possibly wait for an interrupt
    }

    if (g_fConnected && g_fIPValid)
    {
        Console::Write("Connection successful!\r\nIP: ");
        Console::WriteLine(g_rgszIPAddress);
    }
    else
    {
        Console::WriteLine("ERROR: Connection unsuccessful");
    }
}

static void Disconnect(_In_z_ char *pszArgs)
{
    if (!g_fConnected)
    {
        Console::WriteLine("ERROR: Not connected");
        return;
    }

    wlan_disconnect();

    while (g_fConnected)
    {
        // TODO: possibly wait for interrupt
    }
}

static void Open(_In_z_ char *pszArgs)
{
    if (!g_fConnected)
    {
        Console::WriteLine("ERROR: Not connected");
        return;
    }
    if (g_lSocketHandle != EFAIL)
    {
        Console::WriteLine("ERROR: Socket already open");
        return;
    }

    g_lSocketHandle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (g_lSocketHandle == EFAIL)
    {
        Console::WriteLine("ERROR: Failed to open socket");
    }
    else
    {
        Console::WriteLine("Socket opened successfully");
        Console::Write("Handle: ");
        Console::WriteLine(g_lSocketHandle);
    }
}

static void Close(_In_z_ char *pszArgs)
{
    if (!g_fConnected)
    {
        Console::WriteLine("ERROR: Not connected");
        return;
    }
    if (g_lSocketHandle == EFAIL)
    {
        Console::WriteLine("ERROR: Socket not open");
        return;
    }

    if (closesocket(g_lSocketHandle) == 0)
    {
        Console::WriteLine("Socket closed successfully");
    }
    else
    {
        Console::WriteLine("ERROR: Failed to close socket");
    }

    g_lSocketHandle = -1;
}

static void Bind(_In_z_ char *pszArgs)
{
    if (!g_fConnected)
    {
        Console::WriteLine("ERROR: Not connected");
        return;
    }
    if (g_lSocketHandle == EFAIL)
    {
        Console::WriteLine("ERROR: Socket not open");
        return;
    }

    // Handle the arguments (requires a port number)
    char *pszPort = strtok_r(pszArgs, " ", &pszArgs);
    if (!pszPort)
    {
        Console::WriteLine("ERROR: Bind requires a port number");
        return;
    }

    sockaddr socketAddr;
    uint32_t uPort = string_to_uint(pszPort);

    socketAddr.sa_family = AF_INET;

    // the source port
    socketAddr.sa_data[0] = HI_BYTE(uPort);
    socketAddr.sa_data[1] = LO_BYTE(uPort);

    // all 0 IP address
    memset(&socketAddr.sa_data[2], 0, 4);

    if (bind(g_lSocketHandle, &socketAddr, sizeof(sockaddr)))
    {
        Console::WriteLine("ERROR: Could not bind to port");
    }
    else
    {
        Console::WriteLine("Bound to port successfully");
    }
}

static void Receive(_In_z_ char *pszArgs)
{
    if (!g_fConnected)
    {
        Console::WriteLine("ERROR: Not connected");
        return;
    }
    if (g_lSocketHandle == EFAIL)
    {
        Console::WriteLine("ERROR: Socket not open");
        return;
    }

    sockaddr socketAddr;
    socklen_t packetLen;

    volatile long lLength = recvfrom(g_lSocketHandle, g_rgcReceiveBuffer, RECEIVE_BUFFER_SIZE, 0,
        &socketAddr, &packetLen);

    if (lLength <= 0)
    {
        // No data
        Console::WriteLine("ERROR: No data received");
    }
    else
    {
        // Data received successfully
        Console::WriteBuffer(g_rgcReceiveBuffer, lLength);
        Console::WriteLine("");
    }
}

/*
 * Arguments are: send port ip message
 */
static void Send(_In_z_ char *pszArgs)
{
    if (!g_fConnected)
    {
        Console::WriteLine("ERROR: Not connected");
        return;
    }
    if (g_lSocketHandle == EFAIL)
    {
        Console::WriteLine("ERROR: Socket not open");
        return;
    }

    char *pszPort = strtok_r(pszArgs, " ", &pszArgs);
    char *pszIP = strtok_r(pszArgs, " ", &pszArgs);
    char *pszMessage = pszArgs;

    if (!pszPort)
    {
        Console::WriteLine("ERROR: Port number required");
        return;
    }
    if (!pszIP)
    {
        Console::WriteLine("ERROR: Desitnation IP required");
    }

    uint32_t uPort = string_to_uint(pszPort);
    uint32_t uIP = string_to_ip(pszIP);

    sockaddr socketAddr;
    socketAddr.sa_family = AF_INET;         // Always AF_INET

    socketAddr.sa_data[0] = HI_BYTE(uPort);
    socketAddr.sa_data[1] = LO_BYTE(uPort);

    socketAddr.sa_data[2] = (uIP & 0xFF000000) >> 24;
    socketAddr.sa_data[3] = (uIP & 0x00FF0000) >> 16;
    socketAddr.sa_data[4] = (uIP & 0x0000FF00) >> 8;
    socketAddr.sa_data[5] = (uIP & 0x000000FF);

    int nMessageLen = strlen(pszMessage);
    int nBytesSent = sendto(g_lSocketHandle, pszMessage, nMessageLen, 0, &socketAddr,
        sizeof(sockaddr));

    if (nBytesSent == -1)
    {
        Console::WriteLine("ERROR: Failed to send message");
    }
    else if (nBytesSent < nMessageLen)
    {
        Console::WriteLine("ERROR: Unable to send whole packet");
    }
    else
    {
        Console::WriteLine("Packet sent successfully");
    }
}

/*
 * Arguments are: chat in_port out_port ip
 */
static void Chat(_In_z_ char *pszLine)
{
    if (!g_fConnected)
    {
        Console::WriteLine("ERROR: Not connected");
        return;
    }
    if (g_lSocketHandle != EFAIL)
    {
        Console::WriteLine("ERROR: Socket already open");
        return;
    }

    char *pszInPort = strtok_r(pszLine, " ", &pszLine);
    char *pszOutPort = strtok_r(pszLine, " ", &pszLine);
    char *pszIpAddr = strtok_r(pszLine, " ", &pszLine);
    if (!pszInPort)
    {
        Console::WriteLine("ERROR: Input port number required");
    }
    if (!pszOutPort)
    {
        Console::WriteLine("ERROR: Output port number required");
    }
    if (!pszIpAddr)
    {
        Console::WriteLine("ERROR: Destination IP Address required");
    }

    uint32_t uInPort = string_to_uint(pszInPort);
    g_uChatPort = string_to_uint(pszOutPort);
    g_uChatIp = string_to_ip(pszIpAddr);

    // Open the socket
    g_lSocketHandle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (g_lSocketHandle == EFAIL)
    {
        Console::WriteLine("ERROR: Failed to open socket");
        return;
    }

    // Bind socket to port
    sockaddr socketAddr;

    socketAddr.sa_family = AF_INET;
    socketAddr.sa_data[0] = HI_BYTE(uInPort);
    socketAddr.sa_data[1] = LO_BYTE(uInPort);

    memset(&socketAddr.sa_data[2], 0, 4);

    if (bind(g_lSocketHandle, &socketAddr, sizeof(sockaddr)))
    {
        Console::WriteLine("ERROR: Could not bind to port");
        closesocket(g_lSocketHandle);
        g_lSocketHandle = EFAIL;
        return;
    }

    if (listen(g_lSocketHandle, 0))
    {
        Console::WriteLine("ERROR: Failed to start listening");
    }

    // Chat initialized successfully
    g_fChat = true;
}

static void SendChatMessage(_In_z_ char *pszMessage)
{
    sockaddr socketAddr;
    socketAddr.sa_family = AF_INET;         // Always AF_INET

    socketAddr.sa_data[0] = HI_BYTE(g_uChatPort);
    socketAddr.sa_data[1] = LO_BYTE(g_uChatPort);

    socketAddr.sa_data[2] = (g_uChatIp & 0xFF000000) >> 24;
    socketAddr.sa_data[3] = (g_uChatIp & 0x00FF0000) >> 16;
    socketAddr.sa_data[4] = (g_uChatIp & 0x0000FF00) >> 8;
    socketAddr.sa_data[5] = (g_uChatIp & 0x000000FF);

    int nMessageLen = strlen(pszMessage);
    int nBytesSent = sendto(g_lSocketHandle, pszMessage, nMessageLen, 0, &socketAddr,
        sizeof(sockaddr));

    if (nBytesSent == -1)
    {
        Console::WriteLine("ERROR: Failed to send message");
    }
    else if (nBytesSent < nMessageLen)
    {
        Console::WriteLine("ERROR: Unable to send whole packet");
    }
}

static void OnConsoleInput(_In_z_ char *pszLine)
{
    // Disable input until the command finishes
    Console::Disable();

    // If we're in chat, redirect all commands to that
    if (g_fChat)
    {
        if (strcmp(pszLine, "exit") == 0)
        {
            // Exit chat
            g_fChat = false;
        }
        else
        {
            // Otherwise, this is a message to send
            SendChatMessage(pszLine);
        }
    }
    else
    {
        // Otherwise, treat as normal commands
        char *pszCommand = strtok_r(pszLine, " ", &pszLine);
        if (!pszCommand)
        {
            // No command; just ignore
        }
        else if (strcmp(pszCommand, "exit") == 0)
        {
            g_fRunning = false;
        }
        else if (strcmp(pszCommand, "smart-config") == 0)
        {
            RunSmartConfig(pszLine);
        }
        else if (strcmp(pszCommand, "connect") == 0)
        {
            Connect(pszLine);
        }
        else if (strcmp(pszCommand, "disconnect") == 0)
        {
            Disconnect(pszLine);
        }
        else if (strcmp(pszCommand, "open") == 0)
        {
            Open(pszLine);
        }
        else if (strcmp(pszCommand, "close") == 0)
        {
            Close(pszLine);
        }
        else if (strcmp(pszCommand, "bind") == 0)
        {
            Bind(pszLine);
        }
        else if (strcmp(pszCommand, "receive") == 0)
        {
            Receive(pszLine);
        }
        else if (strcmp(pszCommand, "send") == 0)
        {
            Send(pszLine);
        }
        else if (strcmp(pszCommand, "chat") == 0)
        {
            Chat(pszLine);
        }
        else if (strcmp(pszCommand, "") == 0)
        {
            // Ignore empty lines
        }
        else
        {
            Console::WriteLine("Invalid command");
        }
    }

    Console::Enable();
}

int main(void)
{
    // Run all the initializers
    Tiva::InitClock();
    WlanSPI::Init();
    Debug::Init();
    SysTick::Init();
    Console::Init();

    Console::WriteLine("\r\n\r\nInitializing...");

    // Enable processor interrupts
    MAP_IntMasterEnable();

    // WLAN Initialization
    wlan_init(WlanCallback, SendNullPatch, SendNullPatch, SendNullPatch,
        WlanSPI::ReadWlanInterrupt, WlanSPI::WlanInterruptEnable, WlanSPI::WlanInterruptDisable,
        WlanSPI::WriteWlan);

    // Trigger a WLAN device
    wlan_start(0);

    // Mask out unnecessary events
    wlan_set_event_mask(HCI_EVNT_WLAN_KEEPALIVE | HCI_EVNT_WLAN_UNSOL_INIT |
        HCI_EVNT_WLAN_ASYNC_PING_REPORT);

    Debug::SetLed(LED_RED);
    Console::WriteLine("Initialization complete!");
    Console::Write("> ");

    char szBuff[CONSOLE_BUFFER_SIZE + 1];
    while (g_fRunning)
    {
        if (Console::HasNextLine())
        {
            Console::ReadLine(szBuff);
            OnConsoleInput(szBuff);
            if (g_fRunning && !g_fChat)
            {
                Console::Write("> ");
            }
        }

        // Check for chat messages
        if (g_fChat)
        {
            fd_set readsds, exceptsds;
            timeval timeout;
            timeout.tv_usec = 5 * 1000;     // 5 ms is smallest timeout

            FD_ZERO(&readsds);
            FD_ZERO(&exceptsds);
            FD_SET(g_lSocketHandle, &readsds);

            if (select(g_lSocketHandle + 1, &readsds, NULL, &exceptsds, &timeout))
            {
                // There is a message to read
                sockaddr socketAddr;
                socklen_t packetLen;

                volatile long lLength = recvfrom(g_lSocketHandle, g_rgcReceiveBuffer,
                    RECEIVE_BUFFER_SIZE, 0, &socketAddr, &packetLen);

                Console::WriteBuffer(g_rgcReceiveBuffer, lLength);
                Console::WriteLine("");
            }
        }
    }

    Console::WriteLine("Goodbye!");
    Debug::SetLed(LED_BLACK);
}
