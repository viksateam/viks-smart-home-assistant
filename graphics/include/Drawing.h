/*
 * Drawing.h
 *
 * Author(s):
 *      Duncan Horn
 *
 * Functions used to draw shapes in OpenGL. In general, it is not recommended that you call these
 * functions directly unless you are creating a custom UIElement.
 *
 * Additionally, unless there is good reason not to, you should use the drawing functions that
 * accept an appropriate Graphics struct (e.g. Rect, Oval, etc.) as input. This is especially true
 * for drawing ovals as the points would otherwise need to be calculated each time.
 *
 * Finally, each function has a version that accepts a color and a version that does not accept a
 * color. For the functions that do not accept a color, the color set by glColor* is used. This can
 * allow for some (slight) optimizations to be done when drawing many elements with the same color.
 */
#pragma once

#include <cstdint>

#include "Colors.h"
#include "Shapes.h"

namespace vik
{
    namespace graphics
    {
        /* Rectangles */
        GLvoid DrawRect(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
        GLvoid DrawRect(
            _In_ GLfloat x,
            _In_ GLfloat y,
            _In_ GLfloat width,
            _In_ GLfloat height,
            _In_ const Color &color);
        GLvoid DrawRect(_In_ const Rect &rc);
        GLvoid DrawRect(_In_ const Rect &rc, _In_ const Color &color);

        GLvoid FillRect(_In_ GLfloat x, _In_ GLfloat y, _In_ GLfloat width, _In_ GLfloat height);
        GLvoid FillRect(
            _In_ GLfloat x,
            _In_ GLfloat y,
            _In_ GLfloat width,
            _In_ GLfloat height,
            _In_ const Color &color);
        GLvoid FillRect(_In_ const Rect &rc);
        GLvoid FillRect(_In_ const Rect &rc, _In_ const Color &color);

        /* Ovals (and Circles) */
        GLvoid DrawOval(
            _In_ GLfloat x,
            _In_ GLfloat y,
            _In_ GLfloat width,
            _In_ GLfloat height,
            _In_ uint32_t res);
        GLvoid DrawOval(
            _In_ GLfloat x,
            _In_ GLfloat y,
            _In_ GLfloat width,
            _In_ GLfloat height,
            _In_ uint32_t res,
            _In_ const Color &color);
        GLvoid DrawOval(_In_ const Oval &oval);
        GLvoid DrawOval(_In_ const Oval &oval, _In_ const Color &color);

        GLvoid FillOval(
            _In_ GLfloat x,
            _In_ GLfloat y,
            _In_ GLfloat width,
            _In_ GLfloat height,
            _In_ uint32_t res);
        GLvoid FillOval(
            _In_ GLfloat x,
            _In_ GLfloat y,
            _In_ GLfloat width,
            _In_ GLfloat height,
            _In_ uint32_t res,
            _In_ const Color &color);
        GLvoid FillOval(_In_ const Oval &oval);
        GLvoid FillOval(_In_ const Oval &oval, _In_ const Color &color);

        /* Triangles; can't do width and height */
        //GLvoid DrawTriangle();
    }
}

